<?php

/*
 * Plugin Name: AWPCP Fee Per Category Module
 * Plugin URI: http://awpcp.com/premium-modules/fee-per-category-module
 * Version: 3.6
 * Description: Allows you to set different prices for each ad category - if you want to.
 * Author: D. Rodenbaugh
 * Author URI: http://www.skylineconsult.com
 */

/******************************************************************************/
// This module is not included in the core of Another Wordpress Classifieds
// Plugin.
//
// It is a separate add-on premium module and is not subject to the terms of
// the GPL license used in the core package.
//
// This module cannot be redistributed or resold in any modified versions of
// the core Another Wordpress Classifieds Plugin product. If you have this
// module in your possession but did not purchase it via awpcp.com or otherwise
// obtain it through awpcp.com please be aware that you have obtained it
// through unauthorized means and cannot be given technical support through
// awpcp.com.
/******************************************************************************/

define( 'AWPCP_FPC_MODULE', 'Another WordPress Classifieds Plugin - Featured Per Category' );
define( 'AWPCP_FPC_MODULE_DB_VERSION', '3.6' );
define( 'AWPCP_FPC_MODULE_REQUIRED_AWPCP_VERSION', '3.6' );
define( 'AWPCP_FPC_MODULE_BASENAME', str_replace( basename( __FILE__ ), '', plugin_basename( __FILE__ ) ) );
define( 'AWPCP_FPC_MODULE_URL', WP_CONTENT_URL . '/plugins/' . AWPCP_FPC_MODULE_BASENAME );

function awpcp_fee_per_category_required_awpcp_version_notice() {
    if ( current_user_can( 'activate_plugins' ) ) {
        $module_name = __( 'Fee Per Category Module', 'awpcp-fee-per-category' );
        $required_awpcp_version = AWPCP_FPC_MODULE_REQUIRED_AWPCP_VERSION;

        $message = __( 'The AWPCP <module-name> requires AWPCP version <awpcp-version> or newer!', 'awpcp-fee-per-category' );
        $message = str_replace( '<module-name>', '<strong>' . $module_name . '</strong>', $message );
        $message = str_replace( '<awpcp-version>', $required_awpcp_version, $message );
        $message = sprintf( '<strong>%s:</strong> %s', __( 'Error', 'awpcp-fee-per-category' ), $message );
        echo '<div class="error"><p>' . $message . '</p></div>';
    }
}

if ( ! class_exists( 'AWPCP_ModulesManager' )  ) {

    add_action( 'admin_notices', 'awpcp_fee_per_category_required_awpcp_version_notice' );

} else {

class AWPCP_FeePerCategoryModule extends AWPCP_Module {

    public function __construct() {
        parent::__construct(
            __FILE__,
            'Fee Per Category Module',
            'fee-per-category',
            AWPCP_FPC_MODULE_DB_VERSION,
            AWPCP_FPC_MODULE_REQUIRED_AWPCP_VERSION
        );
    }

    public function required_awpcp_version_notice() {
        return awpcp_fee_per_category_required_awpcp_version_notice();
    }

    protected function module_setup() {
        parent::module_setup();

        add_filter( 'awpcp-get-payment-term-fee-params-from-db', 'awpcp_fpc_get_payment_term_fee_params_from_db_object', 10, 2 );
    }

    protected function admin_setup() {
        if ( awpcp_current_user_is_admin() ) {
            fpc_check_fee_plans();
        }
    }
}

function awpcp_fee_per_category_module() {
    return new AWPCP_FeePerCategoryModule( /*awpcp_fee_per_category_module_installer()*/ );
}

function awpcp_activate_fee_per_category_module() {
    awpcp_fee_per_category_module()->install_or_upgrade();
}
awpcp_register_activation_hook( __FILE__, 'awpcp_activate_fee_per_category_module' );

function awpcp_load_fee_per_category_module( $manager ) {
    $manager->load( awpcp_fee_per_category_module() );
}
add_action( 'awpcp-load-modules', 'awpcp_load_fee_per_category_module' );

function fpc_check_fee_plans() {
    global $wpdb, $fee_cat_msg;

    // TODO: move this to an install method
    if ( ! awpcp_column_exists( AWPCP_TABLE_ADFEES, 'categories' ) ) {
        $wpdb->query("ALTER TABLE " . AWPCP_TABLE_ADFEES . "  ADD `categories` TEXT COLLATE utf8_general_ci");
    }

    // there's at least one plan with no cats assigned, good.
    if ( awpcp_are_there_any_fees_with_no_associated_categories() ) {
        return;
    }

    // check to see if there are any cats not assigned to fee plans

    // get all categories IDs
    $sql = 'select category_id from ' . AWPCP_TABLE_CATEGORIES . ' where category_name != ""';
    $catids = $wpdb->get_results($sql, ARRAY_A);

    // no categories defined?
    if (!$catids) return;

    // get all fee plan categories in one query
    $sql = 'select categories from ' . AWPCP_TABLE_ADFEES;
    $feeplans = $wpdb->get_results($sql,ARRAY_A);

    if (!$feeplans) return;

    // flatten this out
    foreach($catids as $id) {
        $cats[] = $id['category_id'];
    }
    // get unique cats with fees
    foreach($feeplans as $fp) {
        $feecats = explode(',', $fp['categories']);
        foreach($feecats as $cat)
        $fc[] = $cat;
    }

    $diff = array_diff($cats, $fc);

    if ( count($diff) > 0 ) {
        foreach($diff as $d) {
            $sql = 'select category_name from '.$wpdb->prefix.'awpcp_categories where category_id = '.$d;
            $name = $wpdb->get_var($sql);
            $fee_cat[] = $name;
        }

        $fee_cat_msg = implode( ', ' , $fee_cat );

        add_action('admin_notices', 'fpc_fee_plan_warning');
    }
}

// find any plans with no cats defined
function awpcp_are_there_any_fees_with_no_associated_categories() {
    // find any plans with no cats defined
    $fees = awpcp_fees_collection()->all();
    $fee_plans_with_no_categories = array();

    foreach ( $fees as $fee ) {
        if ( empty( $fee->categories ) ) {
            return true;
        }
    }

    return false;
}

function fpc_fee_plan_warning() {
    global $fee_cat_msg;
    $message = __('<strong>Warning</strong>: You have fee categories not assigned to fee plans','awpcp-fee-per-category' );
    if ('' != $fee_cat_msg) {
        $message .= ': (<em>'. $fee_cat_msg . '</em>).<br/><br/>';
    } else {
        $message .= '. ';
    }
    $message .= __('Either assign the categories to fee plans, or define at least one fee plan with no categories associated.','awpcp-fee-per-category' );
    $message .= '&nbsp;<a href="' . add_query_arg('page', 'awpcp-admin-fees', admin_url('admin.php')) . '">' . __('Click here to configure fees','awpcp-fee-per-category' ) . '</a>.';
    echo "<div class='error'><p>$message</p></div>";
}

/**
 * XXX: You can't delete this function because it is used
 *      to check for the existence of this module.
 * @deprecated 3.2.2
 */
function awpcp_price_cats( $cats = array(), $adterm_id = '') {
    _deprecated_function( __FUNCTION__, '3.2.2', '' );
}

function awpcp_price_cats_fees() {
    $fee_cats = awpcp_request_param('fee_cats');
    if (!empty($fee_cats)) {
        $fee_cats = clean_field($fee_cats);
        $fee_cats = implode(',', $fee_cats);
    }
    return $fee_cats;
}

function awpcp_fpc_get_payment_term_fee_params_from_db_object( $params, $object ) {
    if ( isset( $object->categories ) ) {
        $params['categories'] = array_filter( explode( ',', $object->categories ), 'intval' );
    }

    return $params;
}

}
