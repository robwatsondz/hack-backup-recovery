Install Instructions for AWPCP Premium Module:  Fee Per Category
------------------------------------------------------------
This premium module is a full Wordpress Plugin.  You can upload it via the
normal Upload mechanism in the Wordpress Admin Panel.

** If you are upgrading from a prior version of the plugin:
Upgrade Instructions:
1)  Make sure you deactivate and delete your old Fee Per Category plugin FIRST
    
Installation instructions:
1)  Download the plugin to your local machine
2)  Login to your Wordpress site as an administrator
3)  Click on Plugins->Add New
4)  Under "Install Plugins" at the top, click on the "Upload" link.
5)  Click on Browse to locate the ZIP you just downloaded.  After you find it, click OK.
6)  Now click "Install Now"
7)  When the upload is complete, click on "Activate Plugin".  

Fee Per Category is now installed.

HELPFUL HINT:  Here's the documentation on Fee Per Category:  http://www.awpcp.com/documentation#premium-fee

If you have specific questions about using the module please feel free to post them on the plugin support site at http://forum.awpcp.com
