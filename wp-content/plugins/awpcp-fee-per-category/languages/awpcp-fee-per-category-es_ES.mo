��          L      |       �   K   �      �   i        |  E   �  �  �  d   �  2     t   E     �  D   �                                         <strong>Warning</strong>: You have fee categories not assigned to fee plans Click here to configure fees Either assign the categories to fee plans, or define at least one fee plan with no categories associated. Error The AWPCP Fee Per Category module requires AWPCP version %s or newer! Project-Id-Version: AWPCP
Report-Msgid-Bugs-To: http://wordpress.org/tag/awpcp-fee-per-category
POT-Creation-Date: 2015-11-11 19:07:05+00:00
PO-Revision-Date: 
Last-Translator: Willington Vega <wvega@wvega.com>
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _e;__;_ex;_x
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.6
X-Poedit-SearchPath-0: .
 <strong>Advertencia</strong>: Usted tiene categorías que no están asignados a ningún plan de pago Haga clic aquí para configurar los planes de pago Asigne las categorías para los planes de pago, o defina por lo menos un plan de pago sin categorías seleccionadas. Error El modulo AWPCP RSS Sitemap requiere AWPCP versión %s o más nueva! 