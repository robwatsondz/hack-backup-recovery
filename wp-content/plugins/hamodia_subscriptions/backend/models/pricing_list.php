<?php
if (! class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Pricing_List_Table extends WP_List_Table
{
    public $wpdb;
    public $prefix;

    public function __construct()
    {
        global $wpdb;

        parent::__construct([
            'singular' => 'Pricing Option',
            'plural'   => 'Pricing Options',
            'ajax'     => false,
        ]);

        $this->wpdb = $wpdb;

        $this->prefix = $this->wpdb->prefix . 'subscriptions_';

        $this->prepare_items();
    }

    public function no_items()
    {
        echo 'No Pricing Options set.';
    }

    public function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'amount_saved':
            case 'charge':
            case 'price_per_issue':
                return $item[ $column_name ] ?
                       $this->to_currency($item[ $column_name ]) :
                       '';

            case 'subscription_type':
                return ucfirst($item[ $column_name ]) . ' edition';

            case 'delivery_method':
                return ucfirst($item[ $column_name ]) . ' delivery';

            case 'payment_type':
                return $item[ $column_name ] == 'payg' ? 'Pay Per Month' : 'Lump Sum';

            case 'duration':
                return $item[ $column_name ];

            default:
                // Show the whole array for troubleshooting purposes
                return print_r($item, true);
        }
    }

    public function column_group_name($item)
    {
        $admin_page = admin_url('admin.php?page=hamodia_subscriptions_pricing_options_page');
        $javascript = "onclick=\"return confirm('Ae you sure you want to delete this Pricing Option?')\"";

        $actions = [
            'edit'   => sprintf('<a href="%s&action=%s&ID=%s">Edit</a>',                    $admin_page, 'edit',   $item['ID']),
            'delete' => sprintf('<a href="%s&noheader=true&action=%s&ID=%s" %s>Delete</a>', $admin_page, 'delete', $item['ID'], $javascript),
        ];

        return sprintf('%s <span style="color:silver">(%s - %s - %s)</span> %s',
            $item['group_name'],
            ucfirst($item['subscription_type']),
            $item['payment_type'] == 'payg' ? 'Pay Per Month' : 'Lump Sum',
            $item['duration'],
            $this->row_actions($actions)
        );
    }

    public function get_columns()
    {
        return [
            'group_name'        => 'Group',
            'price_per_issue'   => 'Price Per Issue',
            'subscription_type' => 'Subscription Type',
            'delivery_method'   => 'Delivery Method',
            'duration'          => 'Duration',
            'payment_type'      => 'Payment Type',
            'charge'            => 'Charge',
            'amount_saved'      => 'Amount Saved',
        ];
    }

    public function get_sortable_columns()
    {
        $sorted_by = ! empty($_REQUEST['orderby']) ? $_REQUEST['orderby'] : 'group_name';

        $sortable_columns = [];

        foreach ($this->get_columns() as $column => $title) {
            $sortable_columns[$column] = [$column, $sorted_by == $column];
        }

        return $sortable_columns;
    }

    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = [];
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = [$columns, $hidden, $sortable];

        /* ------------ */

        $per_page = 25;
        $total_items = $this->count_pricing_options();

        $this->items = $this->get_pricing_options(
            ! empty($_REQUEST['orderby']) ? $_REQUEST['orderby'] : 'group_name',
            ! empty($_REQUEST['order']) ? $_REQUEST['order']   : 'asc',
            $this->get_pagenum(),
            $per_page
        );

        $this->set_pagination_args([
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items / $per_page),
        ]);
    }

    public function get_pricing_options($order_by, $order, $page_num, $limit)
    {
        $pricing = $this->prefix . 'pricing';
        $groups  = $this->prefix . 'groups';

        $order_by = esc_sql($order_by);
        $order    = esc_sql($order);
        $offset   = ($page_num - 1) * $limit;
        $limit    = (int) $limit;

        return $this->wpdb->get_results(
            "SELECT	p.ID,
					g.group_name,
					p.price_per_issue,
					p.charge,
					p.amount_saved,
					p.delivery_method,
					p.payment_type,
					p.subscription_type,
					p.duration

			FROM	`$pricing` as p,
					`$groups` as g

			WHERE p.group_id = g.id
			ORDER BY $order_by $order
			LIMIT $offset, $limit",
        ARRAY_A);
    }

    public function count_pricing_options()
    {
        $pricing = $this->prefix . 'pricing';

        return $this->wpdb->get_var("SELECT count(ID) FROM $pricing");
    }

    public function display_message()
    {
        if (! isset($_GET['msg'])) {
            return;
        }

        ?>
		<div id="message" class="updated fade">
			<p>
				<strong><?= $_GET['msg'] ?></strong>
			</p>
		</div>
		<?php
    }

    public function to_currency($num)
    {
        $num = (string) $num;

        if (! $num) {
            return '';
        }

        $dollars = substr($num, 0, -2);
        $cents   = substr($num, -2);

        // If no dollars, use 0
        if (! $dollars) {
            $dollars = '0';
        }

        // Add commas
        $dollars = preg_replace('~(\d)(?=(?:\d{3})+$)~', '$1,', $dollars);

        return "\$$dollars.$cents";
    }
}
