<?php

class Location_groups
{
    public static $wpdb;
    public static $page;
    public static $action;
    public static $cache = [];

    public static function init()
    {
        global $wpdb;

        self::$wpdb = $wpdb;

        self::$page = admin_url('admin.php?page=' . $_GET['page']);

        self::$action = empty($_GET['action']) ? '' : $_GET['action'];

        $validate = array(
            'edit'       => 'ID',
            'delete'     => 'ID',
            'update'     => 'ID',
            'add_zips'   => 'zip_codes',
            'delete_zip' => 'zip_code',
        );

        foreach ($validate as $action => $var) {
            if (self::$action == $action && empty($_REQUEST[ $var ])) {
                self::redirect();
            }
        }

        switch (self::$action) {
            case 'create':
                self::create_group();
                break;

            case 'update':
                self::update_name();
                break;

            case 'update_text':
                self::update_text();
                break;

            case 'delete':
                self::delete_group();
                break;

            case 'add_zips':
                self::add_zips();

            case 'delete_zip':
                self::delete_zip();
                break;
        }
    }

    public static function stylesheet_url()
    {
        return plugins_url('views/location_groups.css', dirname(__FILE__));
    }

    public static function get_title()
    {
        switch (self::$action) {
            case 'edit':
                return 'Edit <strong>' . self::get_group_info('group_name') . '</strong> Group';
                break;

            case 'new':
                return 'Add New Group';
                break;

            default:
                $add_new_link = self::$page . '&action=new';
                return 'Location Groups <a href="' . $add_new_link . '" class="add-new-h2">Add New</a>';
        }
    }

    public static function display_body()
    {
        switch (self::$action) {
            case 'edit':
                require_once(dirname(__FILE__) . '/zip_codes_list.php');
                $model = new Zip_Codes_List_Table;
                self::display_forms();
                $model->display();
                break;

            case 'new':
                self::display_new_group_form();
                break;

            default:
                require_once(dirname(__FILE__) . '/groups_list.php');
                $model = new Groups_List_Table;
                $model->display();
        }
    }

    // Get either group name, or group text
    public static function get_group_info($bit)
    {
        if (! isset(self::$cache['group_info'])) {
            $group_name = self::$wpdb->prefix . 'subscriptions_groups';

            $id = (int) $_GET['ID'];

            self::$cache['group_info'] = self::$wpdb->get_row("SELECT * FROM $group_name WHERE ID = $id");
        }

        return self::$cache['group_info']->$bit;
    }

    public static function create_group()
    {
        if (empty($_POST['group_name'])) {
            self::redirect([
                'error'  => 'The Group Name cannot be empty.',
                'suffix' => '&action=new',
            ]);
        }

        $groups     = self::$wpdb->prefix . 'subscriptions_groups';
        $group_name = esc_sql($_POST['group_name']);

        if (self::$wpdb->get_var("SELECT group_name FROM $groups WHERE group_name = '$group_name'")) {
            self::redirect([
                'error'  => 'Group names should be unique. You already have a group by the name "' . $_POST['group_name'] . '".',
                'suffix' => '&action=new',
            ]);
        }

        self::$wpdb->insert($groups, [
            'group_name' => stripslashes_deep($_POST['group_name']),
            'text'       => stripslashes_deep($_POST['group_text']),
        ]);

        self::redirect([
            'msg' => 'Successfully created new "' . $group_name . '" group.',
        ]);
    }

    public static function update_name()
    {
        if (empty($_POST['group_name'])) {
            self::redirect([
                'error'  => 'The Group Name cannot be empty.',
                'suffix' => '&action=edit&ID=' . $_POST['ID'],
            ]);
        }

        self::$wpdb->update(
            self::$wpdb->prefix . 'subscriptions_groups',
            ['group_name' => stripslashes_deep($_POST['group_name'])],
            ['ID' => $_POST['ID']],
            ['%s'],
            ['%d']
        );

        self::redirect([
            'msg'    => 'Group Name successfully updated!',
            'suffix' => '&action=edit&ID=' . $_POST['ID'],
        ]);
    }

    public static function update_text()
    {
        self::$wpdb->update(
            self::$wpdb->prefix . 'subscriptions_groups',
            ['text' => stripslashes_deep($_POST['group_text'])],
            ['ID' => $_POST['ID']],
            ['%s'],
            ['%d']
        );

        self::redirect([
            'msg'    => 'Sale Text successfully updated!',
            'suffix' => '&action=edit&ID=' . $_POST['ID'],
        ]);
    }

    public static function delete_group()
    {
        $id        = (int) $_GET['ID'];
        $groups    = self::$wpdb->prefix . 'subscriptions_groups';
        $zip_codes = self::$wpdb->prefix . 'subscriptions_zip_codes';

        $group_name = self::$wpdb->get_var("SELECT group_name FROM `$groups` WHERE ID = $id");

        self::$wpdb->query("DELETE FROM `$groups` WHERE ID = $id");
        self::$wpdb->query("DELETE FROM `$zip_codes` WHERE group_id = $id");

        self::redirect([
            'msg' => '"' . $group_name . '" group successfully deleted.',
        ]);
    }

    public static function add_zips()
    {
        $id              = (int) $_POST['ID'];
        $zip_codes       = $_POST['zip_codes'];
        $zip_codes_table = self::$wpdb->prefix . 'subscriptions_zip_codes';
        $msg             = false;
        $error           = false;

        // Split the $zip_codes string into an array of actual ZIP Codes
        preg_match_all('~[a-z\d-]+~i', $zip_codes, $zip_codes);

        $zip_codes = $zip_codes[0];

        $zips_query = '"' . implode('","', $zip_codes) . '"';

        $used_zip_codes = self::$wpdb->get_col("SELECT zip_code FROM $zip_codes_table WHERE zip_code IN ($zips_query)");

        if ($used_zip_codes) {
            $error     = 'The following ZIP Codes were not added, because they are already in the system: ' . implode(', ', $used_zip_codes);
            $zip_codes = array_diff($zip_codes, $used_zip_codes);
        }

        if ($zip_codes) {
            // Construct $values SQL string.
            // Example: ('11219', 6),('11211', 6)
            $values = implode("', $id), ('", $zip_codes);
            $values = "('$values', $id)";

            self::$wpdb->query("INSERT INTO $zip_codes_table (zip_code, group_id) VALUES $values");

            $msg = count($zip_codes) . ' ZIP Code(s) successfully added.';
        }

        $redirect_data = array('suffix' => '&action=edit&ID=' . $id);

        if ($msg) {
            $redirect_data['msg'] = $msg;
        }

        if ($error) {
            $redirect_data['error'] = $error;
        }

        self::redirect($redirect_data);
    }

    public static function delete_zip()
    {
        $zip_code  = $_GET['zip_code'];
        $zip_codes = self::$wpdb->prefix . 'subscriptions_zip_codes';
        $group_id  = self::$wpdb->get_var("SELECT group_id FROM `$zip_codes` WHERE zip_code = $zip_code");

        self::$wpdb->query("DELETE FROM `$zip_codes` WHERE zip_code = $zip_code");

        self::redirect([
            'msg'    => $zip_code . ' successfully deleted.',
            'suffix' => '&action=edit&ID=' . $group_id,
        ]);
    }

    public static function display_forms()
    {
        $base_url = self::$page . '&noheader=true';

        $name_form_action      = $base_url . '&action=update';
        $zip_codes_form_action = $base_url . '&action=add_zips';
        $text_form_action      = $base_url . '&action=update_text';

        $delete_link       = $base_url . '&action=delete&ID=' . $_GET['ID'];
        $delete_javascript = "return confirm('Are you sure you want to delete this Group?')";

        ?>
        <form action="<?= $name_form_action ?>" method="post" class="standalone">
            <input type="hidden" name="ID" value="<?= $_GET['ID'] ?>">

            <label class="textinput" for="group_name">Group name:</label>

            <input class="textinput" id="group_name" name="group_name" value="<?= esc_attr(self::get_group_info('group_name')) ?>">
            <input type="submit" name="submit" class="button-primary" value="Save Name">
        </form>

        <form action="<?= $zip_codes_form_action ?>" method="post" class="standalone">
            <input type="hidden" name="ID" value="<?= $_GET['ID'] ?>">

            <label class="textinput" for="zip_codes">Add ZIP Codes:</label>
            <input class="textinput" id="zip_codes" name="zip_codes" placeholder="e.g. 11218, 11219, 10977, 10952">

            <input type="submit" name="submit" class="button-primary" value="Add ZIP Codes">
        </form>

        <form action="<?= $text_form_action ?>" method="post" class="standalone">
            <input type="hidden" name="ID" value="<?= $_GET['ID'] ?>">

            <label class="textinput" for="group_text">Sale text:</label>
            <textarea class="textinput" id="group_text" name="group_text"><?= esc_textarea(self::get_group_info('text')) ?></textarea>

            <input type="submit" name="submit" class="button-primary" value="Change Text">
        </form>

        <p>
            <a class="delete-group" href="<?= $delete_link ?>" onclick="<?= $delete_javascript ?>">
                Delete this Group
            </a>
        </p>
        <?php
    }

    public static function display_new_group_form()
    {
        $form_action = self::$page . '&noheader=true&action=create';

        ?>
        <form action="<?= $form_action ?>" method="post" class="standalone">
            <p>
                <label class="textinput" for="group_name">Group name:</label>
                <input class="textinput" id="group_name" name="group_name">
            </p>

            <p>
                <label class="textinput" for="group_text">Sale text:</label>
                <textarea class="textinput" id="group_text" name="group_text"></textarea>
            </p>

            <input type="submit" name="submit" class="button-primary" value="Create new group">
        </form>
        <?php
    }

    public static function display_messages()
    {
        if (isset($_GET['msg'])) {
            self::display_message(stripslashes(rawurldecode($_GET['msg'])));
        }

        if (isset($_GET['error'])) {
            self::display_message(stripslashes(rawurldecode($_GET['error'])), true);
        }
    }

    public static function display_message($msg, $is_error = false)
    {
        ?>
        <div id="message" class="<?= $is_error ? 'error' : 'updated' ?> fade">
            <p>
                <strong><?= $msg ?></strong>
            </p>
        </div>
        <?php
    }

    public static function redirect($params = [])
    {
        $url_suffix = '';

        if (isset($params['msg'])) {
            $url_suffix .= '&msg=' . rawurlencode($params['msg']);
        }
        if (isset($params['error'])) {
            $url_suffix .= '&error=' . rawurlencode($params['error']);
        }
        if (isset($params['suffix'])) {
            $url_suffix .= $params['suffix'];
        }

        wp_redirect(self::$page . $url_suffix);
        exit;
    }
}

Location_groups::init();
