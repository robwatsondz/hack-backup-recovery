<?php
if (! class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Groups_List_Table extends WP_List_Table
{
    public $wpdb;
    public $prefix;
    public $zip_codes = [];

    public function __construct()
    {
        global $wpdb;

        parent::__construct([
            'singular' => 'Group',
            'plural'   => 'Groups',
            'ajax'     => false,
        ]);

        $this->wpdb = $wpdb;

        $this->prefix = $this->wpdb->prefix . 'subscriptions_';

        $this->prepare_items();
    }

    public function no_items()
    {
        echo 'No Groups created yet.';
    }

    public function column_default($item, $column_name)
    {
        return print_r($item, true);
    }

    public function column_group_name($item)
    {
        $admin_page = admin_url('admin.php?page=hamodia_subscriptions_groups');
        $javascript = "onclick=\"return confirm('Ae you sure you want to delete this Group?')\"";

        $actions = $this->row_actions([
            'edit'   => sprintf('<a href="%s&action=%s&ID=%s">Edit</a>',                    $admin_page, 'edit',    $item['ID']),
            'delete' => sprintf('<a href="%s&noheader=true&action=%s&ID=%s" %s>Delete</a>', $admin_page, 'delete',  $item['ID'], $javascript),
        ]);

        // We don't want the Mail Delivery Group to be edited or deleted
        if ($item['group_name'] == 'Mail Delivery Group') {
            $actions = '';
        }

        return sprintf('%s <span style="color: silver">(%s)</span>%s',
            $item['group_name'],
            $item['count'],
            $actions
        );
    }

    public function column_zip_codes($item)
    {
        if (! array_key_exists($item['group_name'], $this->zip_codes)) {
            return $item['group_name'] == 'Mail Delivery Group' ? 'All ZIP Codes.' : '';
        }

        $zip_codes = $this->zip_codes[ $item['group_name'] ];

        if (count($zip_codes) > 40) {
            $zip_codes = array_slice($zip_codes, 0, 40);
            $zip_codes = implode(', ', $zip_codes);
            $zip_codes = $zip_codes . ' .............';
        } else {
            $zip_codes = implode(', ', $zip_codes);
        }

        return $zip_codes;
    }

    public function get_columns()
    {
        return [
            'group_name' => 'Group Name',
            'zip_codes'  => 'ZIP Codes',
        ];
    }

    public function get_sortable_columns()
    {
        $sorted_by = ! empty($_REQUEST['orderby']) ? $_REQUEST['orderby'] : 'ID';

        return ['group_name' => ['group_name', $sorted_by == 'group_name']];
    }

    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = [];
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = [$columns, $hidden, $sortable];

        /* ------------ */

        $this->items = $this->get_groups(
            ! empty($_REQUEST['orderby']) ? $_REQUEST['orderby'] : 'ID',
            ! empty($_REQUEST['order'])   ? $_REQUEST['order']   : 'asc'
        );

        $this->prepare_zip_codes();
    }

    public function prepare_zip_codes()
    {
        $groups    = $this->prefix . 'groups';
        $zip_codes = $this->prefix . 'zip_codes';

        $data = $this->wpdb->get_results(
            "SELECT	g.group_name, z.zip_code
			FROM `$groups` as g, `$zip_codes` as z
			WHERE g.ID = z.group_id
			ORDER BY zip_code",
        ARRAY_A);

        foreach ($data as $row) {
            $this->zip_codes[$row['group_name']][] = $row['zip_code'];
        }
    }

    public function get_groups($order_by, $order)
    {
        $groups    = $this->prefix . 'groups';
        $zip_codes = $this->prefix . 'zip_codes';

        $order_by = esc_sql($order_by);
        $order    = esc_sql($order);

        return $this->wpdb->get_results(
            "SELECT g.ID, g.group_name, count(z.zip_code) as count
			FROM $groups as g
			LEFT JOIN $zip_codes as z
			ON g.ID = z.group_id
			GROUP BY g.ID
			ORDER BY $order_by $order",
        ARRAY_A);
    }

    public function display_message()
    {
        if (! isset($_GET['msg'])) {
            return;
        }

        ?>
		<div id="message" class="updated fade">
			<p>
				<strong><?= $_GET['msg'] ?></strong>
			</p>
		</div>
		<?php
    }
}
