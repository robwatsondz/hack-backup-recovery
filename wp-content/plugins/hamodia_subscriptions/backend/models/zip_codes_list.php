<?php if (! class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Zip_Codes_List_Table extends WP_List_Table
{
    public $wpdb;
    public $prefix;

    public function __construct()
    {
        global $wpdb;

        parent::__construct([
            'singular' => 'ZIP Code',
            'plural'   => 'ZIP Codes',
            'ajax'     => false,
        ]);

        $this->wpdb = $wpdb;

        $this->prefix = $this->wpdb->prefix . 'subscriptions_';

        $this->prepare_items();
    }

    public function no_items()
    {
        echo 'No ZIP Codes added.';
    }

    public function column_default($item, $column_name)
    {
        return print_r($item, true);
    }

    public function column_zip_code($item)
    {
        $admin_page = admin_url('admin.php?page=' . $_GET['page']);
        $javascript = "onclick=\"return confirm('Ae you sure you want to delete this ZIP Code?')\"";

        $actions = [
            'delete' => sprintf('<a href="%s&noheader=true&action=%s&zip_code=%s" %s>Delete</a>', $admin_page, 'delete_zip', $item['zip_code'], $javascript),
        ];

        return $item['zip_code'] . $this->row_actions($actions);
    }

    public function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="%s[]" value="%s" />',
            'zip_codes',
            $item['zip_code']
        );
    }

    public function get_columns()
    {
        return array(
            // 'cb'    => '<input type="checkbox" />',
            'zip_code' => 'ZIP Code'
        );
    }

    public function get_sortable_columns()
    {
        return [
            'zip_code' => ['zip_code', true],
        ];
    }

    public function prepare_items()
    {
        $columns  = $this->get_columns();
        $hidden   = [];
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = [$columns, $hidden, $sortable];

        /* ------------ */

        $per_page = 100;
        $total_items = $this->count_zip_codes();

        $this->items = $this->get_zip_codes(
            ! empty($_REQUEST['orderby']) ? $_REQUEST['orderby'] : 'zip_code',
            ! empty($_REQUEST['order']) ? $_REQUEST['order']   : 'asc',
            $this->get_pagenum(),
            $per_page
        );

        $this->set_pagination_args([
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($total_items / $per_page),
        ]);
    }

    public function get_zip_codes($order_by, $order, $page_num, $limit)
    {
        $zip_codes = $this->prefix . 'zip_codes';
        $group_id  = (int) $_GET['ID'];

        $order_by = esc_sql($order_by);
        $order    = esc_sql($order);
        $offset   = ($page_num - 1) * $limit;
        $limit    = (int) $limit;

        return $this->wpdb->get_results(
            "SELECT	zip_code
			FROM `$zip_codes`
			WHERE group_id = $group_id
			ORDER BY $order_by $order
			LIMIT $offset, $limit",
        ARRAY_A);
    }

    public function count_zip_codes()
    {
        $zip_codes = $this->prefix . 'zip_codes';
        $group_id  = $_GET['ID'];

        return $this->wpdb->get_var("SELECT count(*) FROM $zip_codes WHERE group_id = $group_id");
    }
}
