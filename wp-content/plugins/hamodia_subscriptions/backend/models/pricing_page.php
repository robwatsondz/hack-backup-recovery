<?php

class Pricing_page
{
    public $wpdb;
    public $prefix;
    public $is_new_entry = true;
    public $row_data;

    public function __construct()
    {
        global $wpdb;

        $this->wpdb = $wpdb;

        $this->prefix = $this->wpdb->prefix . 'subscriptions_';

        if (! isset($_GET['action'])) {
            return;
        }

        switch ($_GET['action']) {
            case 'edit':
                $this->is_new_entry = false;
                $this->setup_row_data();
                break;

            case 'delete':
                $this->delete();
                break;

            case 'update':
                $this->update();
                break;

            case 'create':
                $this->insert();
                break;
        }
    }

    public function setup_row_data()
    {
        $pricing = $this->prefix . 'pricing';

        $id = (int) $_GET['ID'];

        $this->row_data = $this->wpdb->get_row("SELECT * FROM `$pricing` WHERE ID = $id", ARRAY_A);
    }

    public function page_title()
    {
        echo $this->is_new_entry ? 'New Pricing Option' : 'Edit Pricing Option';
    }

    public function stylesheet_url()
    {
        return plugins_url('views/pricing_page.css', dirname(__FILE__));
    }

    public function form_link()
    {
        $action = $this->is_new_entry ? 'create' : 'update';

        return admin_url('admin.php?page=hamodia_subscriptions_pricing_options_page&noheader=true&action=' . $action);
    }

    public function display_form_fields()
    {
        if (! $this->is_new_entry) {
            $this->render_hidden_input();
        }

        $textarea_fields = [
            'group_id',
            'subscription_type',
            'payment_type',
            'delivery_method',
        ];

        foreach ($textarea_fields as $field) {
            $field = 'display_' . $field;
            $this->$field();
        }

        $input_fields = [
            'duration'        => 'Duration',
            'charge'          => 'Charge',
            'price_per_issue' => 'Price Per Issue/Week',
            'amount_saved'    => 'Amount Saved',
        ];

        foreach ($input_fields as $key => $name) {
            $this->display_input($key, $name);
        }
    }

    public function display_group_id()
    {
        $groups_table = $this->prefix . 'groups';

        $groups = $this->flatten_rows(
            $this->wpdb->get_results("SELECT * FROM $groups_table ORDER BY ID", ARRAY_N)
        );

        $this->render_textarea(
            'group_id',
            'Location Group',
            $groups,
            $this->is_new_entry ? null : $this->row_data['group_id']
        );
    }

    public function display_subscription_type()
    {
        $options = [
            'daily'   => 'Daily Edition',
            'weekend' => 'Weekend Edition',
        ];

        $this->render_textarea(
            'subscription_type',
            'Subscription Type',
            $options,
            $this->is_new_entry ? null : $this->row_data['subscription_type']
        );
    }

    public function display_payment_type()
    {
        $options = [
            'lump' => 'Lump Sum',
            'payg' => 'Pay Per Month',
        ];

        $this->render_textarea(
            'payment_type',
            'Payment Type',
            $options,
            $this->is_new_entry ? null : $this->row_data['payment_type']
        );
    }

    public function display_delivery_method()
    {
        $options = [
            'home'  => 'Home Delivery',
            'mail'  => 'Mail Delivery',
            'group' => 'Group Delivery',
        ];

        $this->render_textarea(
            'delivery_method',
            'Delivery Method',
            $options,
            $this->is_new_entry ? null : $this->row_data['delivery_method']
        );
    }

    public function display_input($key, $name)
    {
        $value = null;

        $placeholder = [
            'duration'        => 'e.g. 6 Months',
            'charge'          => 'e.g. $120',
            'price_per_issue' => 'e.g. $4.40',
            'amount_saved'    => 'e.g. $20',
        ];

        if (! $this->is_new_entry) {
            $value = $key == 'duration' ?
                $this->row_data['duration'] :
                $this->to_currency($this->row_data[ $key ]);
        }

        $this->render_input(
            $key,
            $name,
            $placeholder[$key],
            $value
        );
    }

    public function render_input($field, $name, $placeholder = '', $value = '')
    {
        ?>
		<p>
			<label class="textinput" for="<?= $field ?>"><?= $name ?>:</label>
			<input class="textinput"
                   type="text"
                   id="<?= $field ?>"
                   name="<?= $field ?>"
                   value="<?= esc_attr($value) ?>"
                   placeholder="<?= $placeholder ?>"
            >
		</p>
		<?php
    }

    public function render_textarea($field, $name, $options, $value = null)
    {
        ?>
		<p>
			<label class="textinput" for="<?= $field ?>"><?= $name ?>:</label>
			<select name="<?= $field ?>" id="<?= $field ?>">
    			<?php foreach ($options as $key => $val): ?>
    				<option <?= $key == $value ? 'selected' : '' ?> value="<?= esc_attr($key) ?>">
                        <?= esc_attr($val) ?>
                    </option>
    			<?php endforeach; ?>
			</select>
		</p>
		<?php
    }

    public function render_hidden_input()
    {
        ?>
		<input type="hidden" name="ID" value="<?= $this->row_data['ID'] ?>">
		<?php
    }

    public function display_form_buttons()
    {
        $button_text = $this->is_new_entry ? 'Save New Pricing Option' : 'Save Changes';
        $javascript  = "return confirm('Are you sure you want to delete this Pricing Option?');";

        ?>
		<input type="submit"
               name="submit"
               id="submit"
               class="button-primary"
               value="<?= $button_text ?>"
        >

		<?php if (! $this->is_new_entry): ?>
            <?php $link = sprintf('%s&noheader=true&action=%s&ID=%s', $_GET['page'], 'delete', $this->row_data['ID']); ?>
			<a class="delete" onclick="<?= $javascript ?>" href="<?= $link ?>">Delete this Pricing Option</a>
		<?php endif;
    }

    public function insert()
    {
        $this->wpdb->insert(
            $this->prefix . 'pricing',
            $this->get_data(),
            $this->get_format()
        );

        $this->redirect('New option created!');
    }

    public function update()
    {
        $id = $this->get_id();

        $this->wpdb->update(
            $this->prefix . 'pricing',
            $this->get_data(),
            ['ID' => $id],
            $this->get_format(),
            ['%d']
        );

        $this->redirect('Option successfully updated!');
    }

    public function delete()
    {
        $id      = $this->get_id();
        $pricing = $this->prefix . 'pricing';

        $this->wpdb->query("DELETE FROM `$pricing` WHERE ID = $id");

        $this->redirect('Pricing Option successfully deleted.');
    }

    public function get_id()
    {
        if (! isset($_REQUEST['ID'])) {
            $this->redirect();
        }

        return (int) $_REQUEST['ID'];
    }

    public function get_data()
    {
        return [
            'group_id' => (int) $_POST['group_id'],

            'price_per_issue' => $this->to_db_num($_POST['price_per_issue']),
            'charge'          => $this->to_db_num($_POST['charge']),
            'amount_saved'    => $this->to_db_num($_POST['amount_saved']),

            'delivery_method'   => $_POST['delivery_method'],
            'payment_type'      => $_POST['payment_type'],
            'subscription_type' => $_POST['subscription_type'],

            'duration' => esc_sql($_POST['duration']),
        ];
    }

    public function get_format()
    {
        return [
            '%d',
            '%d', '%d', '%d',
            '%s', '%s', '%s', '%s'
        ];
    }

    public function redirect($msg = '')
    {
        if ($msg) {
            $msg = '&msg=' . urlencode($msg);
        }

        wp_redirect(admin_url('admin.php?page=hamodia_subscriptions_pricing_options' . $msg));

        exit;
    }


    //// Helper function //////////////////////////////////

    public function flatten_rows($rows)
    {
        $out = [];

        foreach ($rows as $row) {
            $out[$row[0]] = $row[1];
        }

        return $out;
    }

    public function to_currency($num)
    {
        $num = (string) $num;

        if (! $num) {
            return '';
        }

        $dollars = substr($num, 0, -2);
        $cents   = substr($num, -2);

        // If no dollars, use 0
        if (! $dollars) {
            $dollars = '0';
        }

        // Add commas
        $dollars = preg_replace('~(\d)(?=(?:\d{3})+$)~', '$1,', $dollars);

        return "\$$dollars.$cents";
    }

    public function to_db_num($num)
    {
        $num = ((float) preg_replace('~[^\d.]~', '', $num)) * 100;

        if (! $num) {
            return '';
        }

        // Blame this monstrosity on floating point arithmetic. All I want
        // to do is "floor" the value. But that sometimes results in it
        // losing a number. Example: $79.99 magically turns to 7998.
        return (int) (string) $num;
    }
}
