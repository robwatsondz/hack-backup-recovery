<?php require_once(dirname(__FILE__) . '/../models/location_groups.php'); ?>

<link rel="stylesheet" href="<?= Location_groups::stylesheet_url() ?>">

<div class="wrap">

	<h2><?= Location_groups::get_title() ?></h2>

	<?php Location_groups::display_messages(); ?>

	<?php Location_groups::display_body(); ?>

</div>
