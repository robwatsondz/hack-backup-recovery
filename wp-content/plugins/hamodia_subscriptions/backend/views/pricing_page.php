<?php require_once(dirname(__FILE__) . '/../models/pricing_page.php');

$model = new Pricing_page; ?>

<link rel="stylesheet" href="<?= $model->stylesheet_url(); ?>">

<div class="wrap">

	<h2><?php $model->page_title(); ?></h2>

	<form method="post" action="<?php echo $model->form_link(); ?>" id="pricing_page_form">

		<?php $model->display_form_fields(); ?>

		<?php $model->display_form_buttons(); ?>

	</form>

</div>
