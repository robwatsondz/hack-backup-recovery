<?php require_once(dirname(__FILE__) . '/../models/pricing_list.php');

$model = new Pricing_List_Table;

$add_new_link = admin_url('admin.php?page=hamodia_subscriptions_pricing_options_page'); ?>

<div class="wrap">

	<h2>Pricing Options <a href="<?= $add_new_link ?>" class="add-new-h2">Add New</a></h2>

	<?php $model->display_message(); ?>

	<?php $model->display(); ?>

</div>
