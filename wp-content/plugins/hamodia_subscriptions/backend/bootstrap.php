<?php

add_action('admin_menu', function () {
    // My ID is 1, T Gottesman's ID is 2
    if (get_current_user_id() > 2) {
        return;
    }

    add_menu_page(
        'Subscriptions Pricing Options',
        'Subscriptions',
        'read',
        'hamodia_subscriptions_pricing_options',
        function () {
            include_once('views/pricing_list.php');
        }
    );

    // This points to the same page as the main menu page
    add_submenu_page(
        'hamodia_subscriptions_pricing_options',
        'Subscriptions Pricing Options',
        'Pricing Options',
        'read',
        'hamodia_subscriptions_pricing_options'
    );

    add_submenu_page(
        'hamodia_subscriptions_pricing_options',
        'Pricing Option',
        'Add New Pricing',
        'read',
        'hamodia_subscriptions_pricing_options_page',
        function () {
            include_once('views/pricing_page.php');
        }
    );

    add_submenu_page(
        'hamodia_subscriptions_pricing_options',
        'Location Groups',
        'Location Groups',
        'read',
        'hamodia_subscriptions_groups',
        function () {
            include_once('views/location_groups.php');
        }
    );
});
