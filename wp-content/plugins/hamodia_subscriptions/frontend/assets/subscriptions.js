// Add handlebars object helpers: https://gist.github.com/3829688
Handlebars.registerHelper("key_value",function(b,d){var c="",a;for(a in b)b.hasOwnProperty(a)&&(c+=d.fn({key:a,value:b[a]}));return c});

// jQuery plugin to convert a form toa JSON compilable object: http://stackoverflow.com/questions/1184624/convert-form-data-to-js-object-with-jquery#1186309
(function(b){b.fn.serializeObject=function(){var a={};b.each(this.serializeArray(),function(){void 0!==a[this.name]?(a[this.name].push||(a[this.name]=[a[this.name]]),a[this.name].push(this.value||"")):a[this.name]=this.value||""});return a}})(jQuery);

// jQuery plugin to scroll to a specific element on the page: https://gist.github.com/3848793
(function(a){var b=a(window),c=a(document),d=a("body, html");a.fn.scrollTo=function(a){void 0===a&&(a=300);d.animate({scrollTop:Math.min(this.offset().top-10,c.height()-b.height())},a);return this}})(jQuery);

// jQuery placeholder plugin: https://github.com/danielstocks/jQuery-Placeholder
(function(b){function d(a){this.input=a;a.attr("type")=="password"&&this.handlePassword();b(a[0].form).submit(function(){if(a.hasClass("placeholder")&&a[0].value==a.attr("placeholder"))a[0].value=""})}d.prototype={show:function(a){if(this.input[0].value===""||a&&this.valueIsPlaceholder()){if(this.isPassword)try{this.input[0].setAttribute("type","text")}catch(b){this.input.before(this.fakePassword.show()).hide()}this.input.addClass("placeholder");this.input[0].value=this.input.attr("placeholder")}},hide:function(){if(this.valueIsPlaceholder()&&this.input.hasClass("placeholder")&&(this.input.removeClass("placeholder"),this.input[0].value="",this.isPassword)){try{this.input[0].setAttribute("type","password")}catch(a){}this.input.show();this.input[0].focus()}},valueIsPlaceholder:function(){return this.input[0].value==this.input.attr("placeholder")},handlePassword:function(){var a=this.input;a.attr("realType","password");this.isPassword=!0;if(b.browser.msie&&a[0].outerHTML){var c=b(a[0].outerHTML.replace(/type=(['"])?password\1/gi,"type=$1text$1"));this.fakePassword=c.val(a.attr("placeholder")).addClass("placeholder").focus(function(){a.trigger("focus");b(this).hide()});b(a[0].form).submit(function(){c.remove();a.show()})}}};var e=!!("placeholder"in document.createElement("input"));b.fn.placeholder=function(){return e?this:this.each(function(){var a=b(this),c=new d(a);c.show(!0);a.focus(function(){c.hide()});a.blur(function(){c.show(!1)});b.browser.msie&&(b(window).load(function(){a.val()&&a.removeClass("placeholder");c.show(!0)}),a.focus(function(){if(this.value==""){var a=this.createTextRange();a.collapse(!0);a.moveStart("character",0);a.select()}}))})}})(jQuery);

// Crockford's JSON implementation
window.JSON||(JSON={},function(){function k(a){return 10>a?"0"+a:a}function p(a){q.lastIndex=0;return q.test(a)?'"'+a.replace(q,function(a){var c=s[a];return"string"===typeof c?c:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+a+'"'}function m(a,j){var c,d,h,n,g=e,f,b=j[a];b&&("object"===typeof b&&"function"===typeof b.toJSON)&&(b=b.toJSON(a));"function"===typeof i&&(b=i.call(j,a,b));switch(typeof b){case "string":return p(b);case "number":return isFinite(b)?String(b):"null";case "boolean":case "null":return String(b);case "object":if(!b)return"null";e+=l;f=[];if("[object Array]"===Object.prototype.toString.apply(b)){n=b.length;for(c=0;c<n;c+=1)f[c]=m(c,b)||"null";h=0===f.length?"[]":e?"[\n"+e+f.join(",\n"+e)+"\n"+g+"]":"["+f.join(",")+"]";e=g;return h}if(i&&"object"===typeof i){n=i.length;for(c=0;c<n;c+=1)"string"===typeof i[c]&&(d=i[c],(h=m(d,b))&&f.push(p(d)+(e?": ":":")+h))}else for(d in b)Object.prototype.hasOwnProperty.call(b,d)&&(h=m(d,b))&&f.push(p(d)+(e?": ":":")+h);h=0===f.length?"{}":e?"{\n"+e+f.join(",\n"+e)+"\n"+g+"}":"{"+f.join(",")+"}";e=g;return h}}"function"!==typeof Date.prototype.toJSON&&(Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+k(this.getUTCMonth()+1)+"-"+k(this.getUTCDate())+"T"+k(this.getUTCHours())+":"+k(this.getUTCMinutes())+":"+k(this.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(){return this.valueOf()});var r=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,q=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,e,l,s={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},i;"function"!==typeof JSON.stringify&&(JSON.stringify=function(a,j,c){var d;l=e="";if("number"===typeof c)for(d=0;d<c;d+=1)l+=" ";else"string"===typeof c&&(l=c);if((i=j)&&"function"!==typeof j&&("object"!==typeof j||"number"!==typeof j.length))throw Error("JSON.stringify");return m("",{"":a})});"function"!==typeof JSON.parse&&(JSON.parse=function(a,e){function c(a,d){var g,f,b=a[d];if(b&&"object"===typeof b)for(g in b)Object.prototype.hasOwnProperty.call(b,g)&&(f=c(b,g),void 0!==f?b[g]=f:delete b[g]);return e.call(a,d,b)}var d,a=String(a);r.lastIndex=0;r.test(a)&&(a=a.replace(r,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)}));if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return d=eval("("+a+")"),"function"===typeof e?c({"":d},""):d;throw new SyntaxError("JSON.parse");})}());

// Mailbox installation
(function ($) {

    var $trigger = $('.subscriptions-install-mailbox');
    var $form = $('.subscriptions-install-mailbox-form');
    var ajax_url = $('#logo').prop('href') + '/wp-admin/admin-ajax.php';

    $trigger.on('click', function () {
        $trigger.addClass('hidden');

        $form.removeClass('hidden')
             .find('input').first().focus();
    });

    $form.on('submit', function (e) {
        e.preventDefault();

        $.post(ajax_url + '?action=submit_mailbox_install_form', $form.serialize(), function () {
            alert('Your request was successfully submitted.');
        });
    });

}(jQuery));

(function($)
{
	var pricing_data,
		ajax_url		= $('#logo').prop('href') + '/wp-admin/admin-ajax.php',
		$window			= $(window),
		$document		= $(document),
		$first_list		= $('#first-list'),
		$second_list	= $('#second-list'),
		$billing		= $('#subscribe-billing'),

		$zip_form		= $('form.zip-code-search'),

		// We don't want the form to be submitted twice consecutively,
		// so we keep track of whether it is being submitted
		is_form_submitting	= false,

		delivery_descriptions = {
			daily: {
				home: 'Get the daily Hamodia delivered to your door Monday-Friday (includes the weekend edition delivered on Wednesday).',
				mail: 'Get the Monday-Friday editions mailed to your home (includes the weekend edition). Delivery time is not guaranteed.'
			},
			weekend: {
				home: {
					pcf: 'Get the Hamodia weekend edition home delivered to your door every Wednesday.',
					other: 'Get the Hamodia weekend edition home delivered to your door before Shabbos.'
				},
				mail: 'Get the Hamodia weekend edition mailed to your home every week. Delivery time is not guaranteed.',
				group: 'Others in your area have grouped together to receive a weekly shipment delivered to one location for pickup before Shabbos.'
			}
		};

		templates = {
			first	: 'first-list-template',
			second	: 'second-list-template',
			billing	: 'billing-template',
			thanks	: 'thank-you-template'
		},

		App = {
			init: function ()
			{
				// Compile the templates
				$.each(templates, function (name, id)
				{
					templates[ name ] = Handlebars.compile( $('#' + id).html() );
				});

				$zip_form.on( 'submit',					App.on_zip_form_submit );

				$first_list.on( 'click', 'li',			App.on_first_list_select );
				$second_list.on( 'click', 'li',			App.on_second_list_select );

				$billing.on( 'change', '[name=is_same]',	App.on_same_toggle );
				$billing.on( 'change', '[name=is_gift]',	App.on_gift_toggle );

				$billing.on( 'submit', 'form',			App.on_billing_submit );
			},
			on_zip_form_submit: function (e)
			{
				e.preventDefault();

				$zip_form.addClass('searching');

				$([ $first_list, $second_list, $billing ]).empty();

				$.getJSON(ajax_url, {
					action: 'zip_code_search',
					zip: $('[name=zip]', this).val()
				},
				function( data )
				{
					if ( data.error ) return alert( data.error );

					pricing_data = data;

					// Build the data structure for the template
					template_data = {};
					$.each(pricing_data.pricing, function (subscription_type, delivery_types)
					{
						template_data[ subscription_type ] = {};

						$.each(delivery_types, function (delivery_type, options)
						{
							// Create a copy of the option
							var option = $.extend({}, options[0]);

							// Split price_per_issue into dollars and cents
							option.price_per_issue = option.price_per_issue.split('.');
							option.price_per_issue = {
								dollars	: option.price_per_issue[0],
								cents	:option.price_per_issue[1]
							};

							// Set delivery description
							if ( option.subscription_type == 'daily' )
							{
								option.delivery_description = delivery_descriptions.daily[ option.delivery_method ];
							}
							else
							{
								if ( option.delivery_method == 'home' )
								{
									option.delivery_description = delivery_descriptions.weekend.home[ pricing_data.group_id == 6 ? 'pcf' : 'other' ];
								}
								else
								{
									option.delivery_description = delivery_descriptions.weekend[ option.delivery_method ];
								}
							}

							template_data[ subscription_type ][ delivery_type ] = option;
						});
					});

					template_data.sale_text = pricing_data.sale_text;

					// Render the template
					$first_list
						.html( templates.first( template_data ) )
						.scrollTo();
				})
				.always(function ()
				{
					$zip_form.removeClass('searching');
				});
			},
			on_first_list_select: function ()
			{
				var $this				= $(this),
					subscription_type	= $this.data('subscription-type'),
					delivery_method		= $this.data('delivery-method'),
					template_data		= {};

				// Set as selected, by setting the `passive` class on its siblings
				$this.removeClass('passive').siblings().addClass('passive');

				// Build template data structure
				$.each( pricing_data.pricing[ subscription_type ][ delivery_method ], function (i, option)
				{
					template_data[ option.payment_type ] || ( template_data[ option.payment_type ] = [] );

					template_data[ option.payment_type ].push( option );
				});

				// The daily paper might be pitched as $4/week, whereas the weekend is pitched on a per issue basis
				template_data.issue_wording = subscription_type == 'daily' ? 'week' : 'issue';

				// Render template
				$second_list
					.html( templates.second( template_data ) )
					.scrollTo();
			},
			on_second_list_select: function ()
			{
				$(this)
					.addClass('selected')
				.closest('.options-list').find('li').not(this)
					.removeClass('selected');

				if ( ! $billing.children().length ) App.render_billing_form();

			},
			render_billing_form: function ()
			{
				var year = new Date().getFullYear(),
					template_data = $.extend({
						usa		: pricing_data.zip.usa,
						zip		: pricing_data.zip.zip
					}, pricing_data.zip || {});

				template_data.years = Array.apply(null, { length: 10 }).map(function (value, index) {
					return index + year;
				});

				template_data.is_pcf_group = pricing_data.group_id == 6;

				$billing
					.html( templates.billing(template_data) )
					.scrollTo()
				.find('[placeholder]')
					.placeholder();
			},
			on_same_toggle: function ()
			{
				// Hide billing inputs if `is_same` checkbox is checked
				$(this).closest('fieldset').toggleClass('hide-optionals', this.checked);
			},
			on_gift_toggle: function ()
			{
				this.checked && $billing.find('[name=is_same]').prop('checked', false).trigger('change');

				// Show "Gift Card Information" fieldset if `gift` is checked & ask where to send renewal notice
				$('.gift-wrapper').toggleClass('hide-optionals', ! this.checked);
				$('.gift-renewal').toggleClass('hidden', ! this.checked);
			},
			on_billing_submit: function (e)
			{
				e.preventDefault();

				var $required_fields		= $('.shipping-information, .payment-information', this).find(':input'),
					$selected_pricing		= $('#second-list').find('li.selected'),
					$phone_fields			= $('[name=ship_phone]', this),
					$email_fields			= $('[name=ship_email]', this),
					$card_number_field		= $('[name=card_number]', this),
					$submit_button			= $('input[type=submit]', this),
					billing_info_required 	= ! $('[name=is_same]', this).prop('checked'),
					form_data;

				if ( ! $selected_pricing.length )
				{
					alert('Please select your subscription type.');
					$second_list.scrollTo();
					return false;
				}

				if ( billing_info_required )
				{
					$required_fields = $required_fields.add('.billing-information :input', this);
					$phone_fields = $phone_fields.add('[name=bill_phone]', this);
					$email_fields = $email_fields.add('[name=bill_email]', this);
				}

				if (
					App.verify.required( $required_fields ) &&
					App.verify.phone( $phone_fields ) &&
					App.verify.email( $email_fields ) &&
					App.verify.card_number( $card_number_field )
				)
				{
					App.submit_subscription_form( $(this), $selected_pricing.data('id') );
				}
				else
				{
					$(this).find('[placeholder]').placeholder();
				}
			},
			submit_subscription_form: function ( $form, pricing_id )
			{
				var form_data;

				if ( is_form_submitting )
				{
					return false;
				}
				else
				{
					App.set_submitting_status( true );
				}

				form_data = $form.serializeObject();
				form_data.pricing_id = pricing_id;

				$.post(ajax_url + '?action=submit_subscription_form', {
					data: JSON.stringify( form_data )
				})
				.done(function ( data )
				{
					// This shouldn't be needed, since we're sending correct JSON headers
					// and jQuery usually takes care of those, but it's here for extra precaution
					if ( typeof data === 'string' ) data = JSON.parse( data );

					data.success ?
						App.on_submit_success( data, form_data ) :
						App.on_submit_error();
				})
				.fail(App.on_ajax_error);
			},
			on_submit_success: function ( server_data, form_data )
			{
				var card_number = form_data.card_number,
					template_data = {
						description: server_data.description,
						info: form_data
					};

				// Mask the card number
				template_data.info.card_number = card_number.substr(0, card_number.length - 4).replace(/./g, '*') + card_number.substr(-4);

				$('.post-box')
					.html( templates.thanks(template_data) )
					.scrollTo();
			},
			on_submit_error: function ()
			{
				alert("There was an error processing your order.\r\nPlease re-check all the information provided.");
				App.set_submitting_status( false );
			},
			on_ajax_error: function ()
			{
				alert("Sorry, but we lost the connection to hamodia.com's servers.\r\nPlease try again.");
				App.set_submitting_status( false );
			},
			set_submitting_status: function ( submittimg )
			{
				$billing.toggleClass('ajaxing', is_form_submitting = submittimg);
			},
			verify: {
				required: function ( $required_fields )
				{
					var form_error = false;

					$required_fields.each(function ()
					{
						var $this, field_name;

						if ( this.value === '' )
						{
							$this = $(this);
							field_name = $this.is('select') ? $this.find('option:first-child').text() : $this.attr('placeholder');
							alert(field_name + ' cannot be empty.');
							form_error = true;
							this.focus();
							return false;
						}
					});

					return ! form_error;
				},
				phone: function ( $phone_fields )
				{
					var form_error = false;

					$phone_fields.each(function ()
					{
						var numbers_length = this.value.replace(/[^\d]/g, '').length;

						if ( numbers_length < 7 )
						{
							alert('That doesn\'t seem to be a phone number.');
							form_error = true;
							this.focus();
							return false;
						}
						else if ( numbers_length < 10 )
						{
							alert('Please enter a 10 digit phone number.');
							form_error = true;
							this.focus();
							return false;
						}
					});

					return ! form_error;
				},
				email: function ( $email_fields )
				{
					var form_error = false;

					$email_fields.each(function ()
					{
						if ( ! /[^@]+@[^@]+\.[^@]{2,}/.test( this.value ) )
						{
							alert('That doesn\'t seem to be an email address.');
							form_error = true;
							this.focus();
							return false;
						}
					});

					return ! form_error;
				},
				card_number: function ( $card_fields )
				{
					var form_error = false;

					$card_fields.each(function ()
					{
						var numbers_length = this.value.replace(/[^\d]/g, '').length;

						if ( numbers_length < 13 || numbers_length > 16 )
						{
							alert('That doesn\'t seem to be a valid credit card number.');
							this.focus();
							form_error = true;
							return false;
						}
					});

					return ! form_error;
				}
			}
		};

	App.init();
}
(jQuery));
