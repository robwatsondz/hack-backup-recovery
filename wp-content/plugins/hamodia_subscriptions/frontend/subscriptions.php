<?php

class Subscriptions_frontend
{
    public static $wpdb;
    public static $tables = [];
    public static $admin_email = 'subscriptions@hamodia.com';
    public static $usaepay_use_sandbox = false;
    public static $usaepay_merchant_key = 'ba8Pqv3k741KVhBesqX1k1M7gXUSgsEH';
    public static $usaepay_sandbox_merchant_key = 'MISSING';

    public static function init()
    {
        global $wpdb;

        self::$wpdb = $wpdb;

        $prefix = self::$wpdb->prefix . 'subscriptions_';

        self::$tables = array(
            'groups'    => $prefix . 'groups',
            'zip_codes'    => $prefix . 'zip_codes',
            'pricing'    => $prefix . 'pricing'
        );

        nocache_headers();

        header('Content-type: application/json');
    }

    public static function zip_code_search()
    {
        if (empty($_REQUEST['zip'])) {
            echo json_encode([
                'error' => 'Please enter a ZIP code.',
            ]);

            exit;
        }

        $is_mail_available = false;
        $mail_group_id     = 5;
        $fields            = 'id, group_id, price_per_issue, charge, amount_saved, delivery_method, payment_type, subscription_type, duration';
        $zip_code          = esc_sql(trim($_REQUEST['zip']));
        $groups_table      = self::$tables['groups'];
        $zip_codes_table   = self::$tables['zip_codes'];
        $pricing_table     = self::$tables['pricing'];

        $data['zip'] = self::is_valid_us_zip_code($zip_code);

        if ($data['zip']->found) {
            $is_mail_available = true;
            $data['zip']->usa = true;
        }

        $data['zip']->zip = $zip_code;

        $options = self::$wpdb->get_results(
            "SELECT $fields
			FROM `$pricing_table`
			WHERE group_id = (
				SELECT group_id
				FROM `$zip_codes_table`
				WHERE `zip_code` = '$zip_code'
			)" .
            ($is_mail_available ? " OR group_id = " . $mail_group_id : '') .
            " ORDER BY
				price_per_issue ASC,
				payment_type ASC"
        );


        if (count($options)) {
            $data['group_id'] = self::get_group_id($options);

            // Get the sale text for this group
            $data['sale_text'] = self::$wpdb->get_var(
                "SELECT `text`
				FROM `$groups_table`
				WHERE id = " . $data['group_id']
            );

            foreach ($options as $option) {
                // Convert cents to currency
                $option->charge          = self::to_currency($option->charge);
                $option->amount_saved    = self::to_currency($option->amount_saved);
                $option->price_per_issue = self::to_currency($option->price_per_issue);

                // Build data structure
                $data['pricing'][$option->subscription_type][$option->delivery_method][] = $option;
            }
        } else {
            $data['error'] = 'Unfortunaely, we do not do subscriptions in your area.';
        }

        echo json_encode($data);

        exit;
    }

    public static function submit_form()
    {
        $form_data = json_decode(stripslashes_deep($_POST['data']), true);

        // Remove any non-digits from the card number
        $form_data['card_number'] = preg_replace('~[^\d]~', '', $form_data['card_number']);

        $pricing_data = self::get_price_and_description($form_data['pricing_id'], $form_data['ship_zip']);

        if (! $pricing_data['success']) {
            echo json_encode([
                'success' => false,
                'error_msg' => $pricing_data['error_msg']
            ]);

            exit;
        }

        $charge_response = self::charge_card(
            self::get_bill_info($form_data),
            $pricing_data['amount'],
            $pricing_data['description']
        );

        // Send email to admin regardless of whether the charge was successful
        self::send_email('admin', $pricing_data['description'], $form_data, $charge_response);

        // If charge was successful, send email to user
        if ($charge_response['success']) {
            self::send_email('user', $pricing_data['description'], $form_data, $charge_response);
        }

        // Return response to browser
        echo json_encode([
            'success'     => $charge_response['success'],
            'info'        => $charge_response['info'],
            'amount'      => $pricing_data['amount'],
            'description' => $pricing_data['description'],
        ]);

        exit;
    }

    private static function is_valid_us_zip_code($zip_code)
    {
        $zips = require __DIR__.'/zips.php';

        return (object) [
            'found' => in_array($zip_code, $zips),
        ];
    }

    private static function get_price_and_description($id, $zip)
    {
        $id = (int) $id;
        $zip = trim($zip);
        $pricing_table = self::$tables['pricing'];
        $zip_codes_table = self::$tables['zip_codes'];
        $pricing_data = self::$wpdb->get_row("SELECT * FROM $pricing_table WHERE ID = $id");

        // If this pricing option ID does not exist
        if (! $pricing_data) {
            return [
                'success' => false,
                'error_msg' => 'Invalid ID',
            ];
        }

        // Is this ZIP code in that group?
        $zip_mismatch = ! self::$wpdb->get_row(
            self::$wpdb->prepare(
                "SELECT * FROM $zip_codes_table
				WHERE group_id = $pricing_data->group_id
				AND zip_code = %s",
                $zip
            )
        );

        // If not, maybe it's a valid US ZIP code?
        // The mail group does not really have any ZIP codes attached to it.
        // If a ZIP code is a valid US ZIP code, they are automatically included in the mail group.
        if ($zip_mismatch && $pricing_data->delivery_method == 'mail') {
            $zip_mismatch = ! self::is_valid_us_zip_code($zip)->found;
        }

        $out = [
            'success'     => ! $zip_mismatch,
            'amount'      => $pricing_data->charge / 100,
            'description' => $pricing_data->duration . ' subscription to the ' . $pricing_data->subscription_type . ' edition of the Hamodia for $' . ($pricing_data->charge / 100),
            'error_msg'   => $zip_mismatch ? 'ZIP code mismatch' : '',
        ];

        if ($pricing_data->payment_type == 'payg') {
            $out['description'] .= ' - with recurring billing';
        }

        return $out;
    }

    private static function get_bill_info($form_data)
    {
        $prefix = isset($form_data['is_same']) ? 'ship' : 'bill';

        return [
            'cardholder' => $form_data[ $prefix . '_first_name' ] . '  ' . $form_data[ $prefix . '_last_name' ],
            'street'     => $form_data[ $prefix . '_address' ],
            'zip'        => $form_data[ $prefix . '_zip' ],
            'card'       => $form_data['card_number'],
            'exp'        => $form_data['card_month'] . substr($form_data['card_year'], -2),
            'cvv2'       => $form_data['card_cvv'],
        ];
    }

    private static function charge_card($data, $amount, $description = '')
    {
        require_once(dirname(__FILE__) . '/usaepay/usaepay.php');

        $tran = new umTransaction;

        // If we're working local, we have to load our own CA bundle
        if (defined('TEST_ENVIRONMENT') && TEST_ENVIRONMENT) {
            $tran->cabundle = dirname(__FILE__) . '/usaepay/cacert.pem';
        }

        $tran->usesandbox = self::$usaepay_use_sandbox;
        $tran->key = self::$usaepay_use_sandbox ? self::$usaepay_test_merchant_key : self::$usaepay_merchant_key;

        $tran->description = 'Online - ' . $description;
        $tran->invoice     = time();

        $tran->cardholder = $data['cardholder']; // e.g. "Test T Jones";
        $tran->street     = $data['street'];     // e.g. "1234 Main Street";
        $tran->zip        = $data['zip'];        // e.g. "90036";

        $tran->amount = $amount;                 // e.g. "1.00";

        $tran->card  = $data['card'];            // e.g. "4005562233445564";
        $tran->exp   = $data['exp'];             // e.g. "0312";
        $tran->cvv2  = $data['cvv2'];            // e.g. "435";

        if ($tran->Process()) {
            $info = "Card approved\r\n";
            $info .= "Authcode: " . $tran->authcode . "\r\n";
            $info .= "AVS Result: " . $tran->avs_result . "\r\n";
            $info .= "Cvv2 Result: " . $tran->cvv2_result . "\r\n";

            return [
                'success' => true,
                'info' => $info,
            ];
        } else {
            $info = "Card Declined (" . $tran->result . ")\r\n";
            $info .= "Reason: " . $tran->error . "\r\n";

            if (! empty($tran->curlerror)) {
                $info .= "Curl Error: " . $tran->curlerror . "\r\n";
            }

            return [
                'success' => false,
                'info' => $info,
            ];
        }
    }

    private static function send_email($recipient, $description, $form_data, $charge_response)
    {
        $message = "Recipient Information\r\n";
        $message .= "-------------------\r\n";
        $message .= "{$form_data['title']} {$form_data['ship_first_name']} {$form_data['ship_last_name']}\r\n";
        $message .= "{$form_data['ship_address']}\r\n";
        $message .= "{$form_data['ship_city']}, {$form_data['ship_state']} {$form_data['ship_zip']}\r\n";
        $message .= "{$form_data['ship_country']}\r\n\r\n";

        $message .= "{$form_data['ship_phone']}\r\n";
        $message .= "{$form_data['ship_email']}\r\n\r\n\r\n\r\n";

        if (! isset($form_data['is_same'])) {
            $message .= "Billing Information\r\n";
            $message .= "-------------------\r\n";
            $message .= "{$form_data['bill_first_name']} {$form_data['bill_last_name']}\r\n";
            $message .= "{$form_data['bill_address']}\r\n";
            $message .= "{$form_data['bill_city']}, {$form_data['bill_state']} {$form_data['bill_zip']}\r\n";
            $message .= "{$form_data['bill_country']}\r\n\r\n";

            $message .= "{$form_data['bill_phone']}\r\n";
            $message .= "{$form_data['bill_email']}\r\n\r\n\r\n\r\n";
        }

        $message .= "Credit Card\r\n";
        $message .= "-------------------\r\n";
        $message .= str_pad(substr($form_data['card_number'], -4), strlen($form_data['card_number']), '*', STR_PAD_LEFT) . "\r\n";
        $message .= "{$form_data['card_month']} / {$form_data['card_year']}\r\n\r\n\r\n\r\n";

        if (isset($form_data['is_gift'])) {
            $message .= "Gift Information\r\n";
            $message .= "-------------------\r\n";
            $message .= "From: {$form_data['gift_from']}\r\n";
            $message .= "To: {$form_data['gift_to']}\r\n";
            $message .= "Message: {$form_data['gift_message']}\r\n\r\n\r\n\r\n\r\n";
        }


        if ($recipient == 'admin') {
            $email = self::$admin_email;

            $subject = 'Online ';
            $subject .= $charge_response['success'] ? 'charge' : 'decline';
            $subject .= ' - ';
            $subject .= $description;

            $message .= 'Send renewal notice by: ' . $form_data['renewal_method'] . "\r\n";

            if (isset($form_data['delivery_box'])) {
                $message .= "Please install a delivery box\r\n";
            }

            if (isset($form_data['is_gift'])) {
                $message .= 'Send renewal notice to: ' . $form_data['renewal_notice'];
            }

            $message .= "\r\n\r\n\r\n\r\n" . $charge_response['info'];
        } else {
            // Send email to shipping address, unless there's a billing address and it's a gift
            $prefix = (isset($form_data['is_same']) || ! isset($form_data['is_gift'])) ? 'ship' : 'bill';
            $email  = $form_data[ $prefix . '_email' ];

            $subject = 'Hamodia ' . $description;

            $message = "You have successfully signed up for a $description.\r\n" .
                        "Thank you for your order.\r\n\r\n\r\n\r\n\r\n" .
                        $message;
        }

        wp_mail(
            $email,
            $subject,
            $message,
            'From: "Hamodia" <' . self::$admin_email . '>'
        );
    }

    private static function to_currency($num)
    {
        $num = (string) $num;

        if (! $num) {
            return '';
        }

        $dollars = substr($num, 0, -2);
        $cents   = substr($num, -2);

        // If no dollars, use 0
        if (! $dollars) {
            $dollars = '0';
        }

        // Add commas
        $dollars = preg_replace('~(\d)(?=(?:\d{3})+$)~', '$1,', $dollars);

        // No $ symbol. If needed, will be added client-side
        return "$dollars.$cents";
    }

    private static function get_group_id($pricing_options)
    {
        $isMail = false;

        for ($i = 0; $i < count($pricing_options); $i++) {
            if ($pricing_options[$i]->group_id == 5) {
                $isMail = true;
            } else {
                return $pricing_options[ $i ]->group_id;
            }
        }

        return $isMail ? 5 : null;
    }
}

Subscriptions_frontend::init();
