<?php

! is_admin() && add_action('wp', function () {
    global $post;

    if ($post && $post->ID == 94) {
        wp_enqueue_script(
            'handlebars',
            plugins_url('frontend/assets/handlebars.js', dirname(__FILE__)),
            [],
            '1.0.rc.1',
            true
        );

        wp_enqueue_script(
            'subscriptions',
            plugins_url('frontend/assets/subscriptions.js', dirname(__FILE__)),
            ['handlebars'],
            '0.1',
            true
        );

        wp_enqueue_style(
            'subscriptions',
            plugins_url('frontend/assets/subscriptions.css', dirname(__FILE__)),
            [],
            '1.10'
        );
    }
});

add_action('wp_ajax_zip_code_search',            'subscriptions_zip_code_search');
add_action('wp_ajax_nopriv_zip_code_search',    'subscriptions_zip_code_search');

function subscriptions_zip_code_search()
{
    require_once(dirname(__FILE__) . '/subscriptions.php');

    Subscriptions_frontend::zip_code_search();
}

add_action('wp_ajax_submit_subscription_form',            'subscriptions_submit_form');
add_action('wp_ajax_nopriv_submit_subscription_form',    'subscriptions_submit_form');

function subscriptions_submit_form()
{
    require_once(dirname(__FILE__) . '/subscriptions.php');

    Subscriptions_frontend::submit_form();
}
