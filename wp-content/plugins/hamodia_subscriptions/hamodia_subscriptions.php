<?php
/*
Plugin Name: Hamodia Subscriptions
Plugin URI: http://josephsilber.com
Description: Hamodia Subscriptions plugin built specifically for Hamodia.com's needs.
Author: Joseph Silber
Version: 0.1
Author URI: http://josephsilber.com
*/ 

include_once('backend/bootstrap.php');
include_once('frontend/bootstrap.php');
