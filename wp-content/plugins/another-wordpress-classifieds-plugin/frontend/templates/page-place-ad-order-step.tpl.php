<?php
$breadcrumbs = array();
	$breadcrumbs[] = array('Name' => 'Classifieds', 'Link' => '/hamodia-classifieds');
	$breadcrumbs[] = array('Name' => 'Place Ad', 'Link' => '/place-ad/','CurrentPage' => true);
?>

<?php if ( get_awpcp_option( 'freepay' ) == 1 ): ?>
<h2><?php echo esc_html( _x( 'Create and Submit Your Ad', 'place ad order step', 'another-wordpress-classifieds-plugin' ) ); ?></h2>
<?php else: ?>
<h2><?php echo esc_html( _x( 'Select Category', 'place ad order step', 'another-wordpress-classifieds-plugin' ) ); ?></h2>
<?php endif; ?>

<?php
    if ( get_awpcp_option( 'show-create-listing-form-steps' ) ) {
        echo awpcp_render_listing_form_steps( 'select-category' );
    }
?>

<?php foreach ($messages as $message): ?>
    <?php echo awpcp_print_message($message); ?>
<?php endforeach ?>

<?php foreach ($transaction_errors as $error): ?>
    <?php echo awpcp_print_message($error, array('error')); ?>
<?php endforeach ?>

<?php awpcp_print_form_errors( $form_errors ); ?>

<?php if ( ! $skip_payment_term_selection && ! awpcp_current_user_is_admin() ): ?>
<?php echo $payments->render_account_balance(); ?>
<?php endif ?>

<form class="awpcp-order-form" method="post">
    <?php if (awpcp_current_user_is_moderator()): ?>
    <h3><?php echo esc_html( _x( 'Please select the owner for this Ad', 'place ad order step', 'another-wordpress-classifieds-plugin' ) ); ?></h3>
    <p class="awpcp-form-spacer">
        <?php
            echo awpcp_users_field()->render( array(
                'required' => true,
                'selected' => awpcp_array_data( 'user', '', $form ),
                'label' => __( 'User', 'another-wordpress-classifieds-plugin' ),
                'default' => __( 'Select an User owner for this Ad', 'another-wordpress-classifieds-plugin' ),
                'id' => 'ad-user-id',
                'name' => 'user',
                'class' => array( 'awpcp-users-dropdown', 'awpcp-dropdown' ),
            ) );
        ?>
        <?php echo awpcp_form_error( 'user', $form_errors ); ?>
    </p>

    <?php endif ?>
	
	<h2 style="text-align:center;margin-top:15px;margin-bottom:15px">For a Limited Time Take Advantage Of Our Special Introductory Rates!</h2>

    <?php if ( ! $skip_payment_term_selection ): ?>
    <?php echo $payments->render_payment_terms_form_field( $transaction, $table, $form_errors ); ?>
    <?php endif; ?>
	
	<div style="float:left;width:100%;margin-top:20px;clear:both;"></div>

<h4 style="margin-bottom:25px">Feature your ad and reach more viewers with a highlighted placement at the top of its category.</h4>
	
	<h3><?php echo esc_html( _x( 'Please select a Category for your Ad', 'place ad order step', 'another-wordpress-classifieds-plugin' ) ); ?></h3>

    <p class="awpcp-form-spacer">
        <?php $dropdown = new AWPCP_CategoriesDropdown(); ?>
        <?php echo $dropdown->render( array( 'selected' => awpcp_array_data( 'category', '', $form ), 'name' => 'category', 'hide_empty' => false ) ); ?>
        <?php echo awpcp_form_error( 'category', $form_errors ); ?>
    </p>

    <p class="awpcp-form-submit">
        <input class="button" type="submit" value="<?php echo esc_attr( _x( 'Continue', 'listing order form', 'another-wordpress-classifieds-plugin' ) ); ?>" id="submit" name="submit">
        <?php if (!is_null($transaction)): ?>
        <input type="hidden" value="<?php echo esc_attr( $transaction->id ); ?>" name="transaction_id">
        <?php endif; ?>
        <input type="hidden" value="order" name="step">
    </p>
</form>
