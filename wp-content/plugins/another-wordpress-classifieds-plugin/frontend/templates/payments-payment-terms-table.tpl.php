<?php $columns = $this->get_columns(); $group = ''; ?>


<style inline>



/* awcp updates  start */

.awcp_x2_packages
{
	width:100%;
	clear:both;
	float:left;
}

.awcp_x2_packages .awcp_x2_package
{
	float:left;	
	width:100%;
	max-width:calc(20% - 2px);
	background:#ddd;
	margin:5px 0;	
	border-bottom:1px solid #999;	
	border-left:1px solid #999;
	position:relative;
}

.awcp_x2_packages > div:nth-of-type(5n)
{	
	border-right:1px solid #999;
}

.awcp_x2_packages .awcp_x2_package .awcp_x2_header
{
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#2d2d2d+0,2a2929+100 */
	background: #2d2d2d; /* Old browsers */
	background: -moz-linear-gradient(top, #2d2d2d 0%, #2a2929 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top, #2d2d2d 0%,#2a2929 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom, #2d2d2d 0%,#2a2929 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2d2d2d', endColorstr='#2a2929',GradientType=0 ); /* IE6-9 */
	text-align:center;	
}


.awcp_x2_packages .awcp_x2_package .awcp_x2_header span
{
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#0075a1+0,0056a9+100 */
	background: #0075a1; /* Old browsers */
	background: -moz-linear-gradient(top, #0075a1 0%, #0056a9 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top, #0075a1 0%,#0056a9 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom, #0075a1 0%,#0056a9 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0075a1', endColorstr='#0056a9',GradientType=0 ); /* IE6-9 */

	width:100%;
	display:block;
}

.awcp_x2_packages .awcp_x2_package .awcp_x2_header
{
	color:#fff !important;
	font-size:20px !important;
	line-height:50px !important;
}

.awcp_x2_packages .awcp_x2_package .awcp_x2_package_details
{
	padding:10px;	
}

.awcp_x2_packages .awcp_x2_package .awcp_x2_package_details p
{
	margin:0 !important;	
	padding:0 !important;
	line-height:40px !important;
	border-bottom:1px solid #c0c0c0;
}

.awcp_x2_packages .awcp_x2_package .awcp_x2_package_details p:nth-last-of-type(1),
.awcp_x2_packages .awcp_x2_package .awcp_x2_package_details p:nth-last-of-type(2)
{
	border-bottom:none;
}

.awcp_x2_packages .awcp_x2_package .awcp_x2_package_details .awpcp-payment-terms-table-payment-term-price
{
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#5ec655+0,29b036+100 */
	background: #5ec655; /* Old browsers */
	background: -moz-linear-gradient(top, #5ec655 0%, #29b036 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top, #5ec655 0%,#29b036 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom, #5ec655 0%,#29b036 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5ec655', endColorstr='#29b036',GradientType=0 ); /* IE6-9 */
	color:#fff;
	margin-top:20px;
	line-height:40px;
	font-weight:bold;
	border:1px solid #28a52e;
	border-radius:5px;
}

.awcp_x2_packages .awcp_x2_package .awcp_x2_package_details .awpcp-payment-terms-table-payment-term-price:before
{
	content:'Select Plan';
}

.awcp_x2_packages .awcp_x2_package.selected .awcp_x2_package_details .awpcp-payment-terms-table-payment-term-price:before
{
	content:'Selected';
}

.awcp_x2_packages .awcp_x2_package .awcp_x2_package_details .awpcp-payment-terms-table-payment-term-price input,
.awcp_x2_packages .awcp_x2_package .awcp_x2_package_details .awpcp-payment-terms-table-payment-term-price span
{
	display:none;
}

.awcp_x2_packages .awcp_x2_package.selected:after
{
	border:4px solid #ffd400;
	content:'';
	position:absolute;
	z-index:50;
	top:-4px;
	left:-4px;
	width:100%;
	height:100%;
	background:rgba(255,255,255,0.1);
}

.awcp_x2_packages > div:nth-of-type(last)
{	
	border-right:1px solid #999;
}

.awcp_x2_packages .feature_toggle
{
	color:#000;
}

@media screen and (max-width:1080px)
{
	
	.awcp_x2_packages .awcp_x2_package
	{
		max-width:calc(25% - 5px);
	}
	
	.awcp_x2_packages > div:nth-of-type(4n)
	{	
		border-right:1px solid #999;
	}
	
	.awcp_x2_packages > div:nth-of-type(5n)
	{	
		border-right:none;
	}
	
}

@media screen and (max-width:930px)
{
	
	.awcp_x2_packages .awcp_x2_package
	{
		max-width:calc(33.333% - 5px);
	}
	
	.awcp_x2_packages > div:nth-of-type(3n)
	{	
		border-right:1px solid #999;
	}

	.awcp_x2_packages > div:nth-of-type(4n)
	{	
		border-right:none;
	}

}


@media screen and (max-width:768px)
{
	
	.awcp_x2_packages .awcp_x2_package
	{
		max-width:calc(50% - 5px);
	}
	
	.awcp_x2_packages > div:nth-of-type(2n)
	{	
		border-right:1px solid #999;
	}

	.awcp_x2_packages > div:nth-of-type(3n)
	{	
		border-right:none;
	}

}

@media screen and (max-width:500px)
{
	
	.awcp_x2_packages .awcp_x2_package
	{
		max-width:100%;
	}

	.awcp_x2_packages > div,
	.awcp_x2_packages > div:nth-of-type(3n),
	.awcp_x2_packages > div:nth-of-type(2n),
	.awcp_x2_packages > div:nth-of-type(4n),
	.awcp_x2_packages > div:nth-of-type(5n)
	{	
		border-right:1px solid #999;
	}

}

.awpcp-form-submit
{
	margin-top:20px;
	float:right;
}

.awpcp-dropdown
{	
	padding:10px;
}

.feature_toggle
{
	z-index:1000;
	position:relative;
	line-height:40px !important;
	border-top:1px solid #c0c0c0;
}

.awcp_x2_package.featured
{
	display:none;
}

/* awcp updates  end */

</style>

<div class="awcp_x2_packages">
	<?php foreach ($this->get_items() as $item): ?>
	
			<?php 
				$package_detail = '';
				$package_name = '';
				$price = 0;
			?>
            <?php foreach ($columns as $column => $name): ?>
            <?php 
				
				if ($column == 'ads')
					continue;
			
				if ($column == 'price')
				{
					$price = $this->item_column($item, 'fee');
					$package_detail .= '<p data-title="'. esc_attr( $name ) . '">'. $this->item_column( $item, $column ) .'</p>';
					continue;
				}
				
				if ($column == 'name')
				{
					$package_name = $this->item_column( $item, 'name' );
					continue;
				}
				
				if ($column == 'images')
				{					
					$package_detail .= '<p data-title="Number of Images">Number of Images : ' . $this->item_column( $item, $column ) .'</p>';
					continue;
				}
				
				$package_detail .= '<p data-title="'. esc_attr( $name ) . '">'. esc_html( $name ) . ' : ' . $this->item_column( $item, $column ) .'</p>';
				
			?>
            <?php endforeach ?>
			
			<div class="awcp_x2_package <?php if (stripos(esc_html( strtolower($package_name) ),'featured')) echo "featured";?>">
			
			<p class="awcp_x2_header <?php if (stripos(esc_html( strtolower($package_name) ),'featured')) echo "featured";?>" data-package_name="<?php echo esc_html( $package_name ); ?>"><?php echo esc_html( $package_name ); ?><br /><span class="header_fee"><?php echo $price; ?></span></p>
			<div class="awcp_x2_package_details"><?php echo $package_detail;?></div>
		</div>
	<?php endforeach ?>
</div>