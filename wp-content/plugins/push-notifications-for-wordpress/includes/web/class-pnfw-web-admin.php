<?php

class PNFW_Web_Admin {

 public function __construct() {
  if (is_admin()) {
   add_action('wp_ajax_nopriv_pnfw_register_device', array($this, 'register_device'));
   add_action('wp_ajax_pnfw_register_device', array($this, 'register_device'));
  }
  else {
   add_filter('redirect_canonical', array($this, 'redirect_canonical'), 10, 2);
   add_action('wp_enqueue_scripts', array($this, 'enqueue_web_scripts'));
   add_action('wp_head', array($this, 'manifest_file'));
   add_action('template_redirect', array($this, 'front_controller'));
  }
 }

 function redirect_canonical($redirect_url, $requested_url) {
  $control_action = get_query_var('control_action') ? get_query_var('control_action') : '';
  if ($control_action == 'sw')
   return $requested_url;

  return $redirect_url;
 }

 function front_controller() {
  global $wp_query;

  $control_action = isset($wp_query->query_vars['control_action']) ? $wp_query->query_vars['control_action'] : '';

  switch ($control_action) {
   case 'manifest':
    $manifest = array(
     'gcm_sender_id' => get_option('pnfw_web_push_sender_id')
    );
    header("Content-type: application/json");
    echo json_encode((object)$manifest);
    exit;
   case 'icon':
    $file = get_attached_file(get_option('pnfw_web_push_icon_media_id'));
    header("Content-type: image/png");
    echo file_get_contents($file);
    exit;
   case 'sw':
    header("Content-type: application/javascript");
    echo file_get_contents(dirname(__FILE__) . '/../../assets/js/push_sw.js');
    exit;
  }
 }

 public function manifest_file() {
  echo '<link rel="manifest" href="' . esc_url(home_url('pnfw/manifest/')) . '">';
 }

 function enqueue_web_scripts() {
  wp_register_script('push_web', $this->canonicalize(plugin_dir_url(__FILE__) . '/../../../assets/js/push_web.js'), array('jquery'));
  wp_enqueue_script('push_web');

  $data = array(
   'sw_path' => get_option('siteurl') . '/push_sw.js',
   'ajaxurl' => admin_url('admin-ajax.php'),
  );

  wp_localize_script('push_web', 'pnfw_vars', $data);
 }

 function register_device() {
  if (isset($_POST["endpoint"]) && !empty($_POST['endpoint'])) {
   global $wpdb;
   $push_tokens = $wpdb->get_blog_prefix().'push_tokens';
   $push_encryption_keys = $wpdb->get_blog_prefix().'push_encryption_keys';
   $push_logs = $wpdb->get_blog_prefix().'push_logs';

   $endpoint = explode('/', rawurldecode($_POST["endpoint"]));
   $token = end($endpoint);
   $os = $this->get_os($endpoint[2]);
   $type = $this->get_type($os);
   $lang = isset($_POST['lang']) ? $this->get_lang(rawurldecode($_POST['lang'])) : null;

   if (isset($_POST["prevEndpoint"])) {
    $prevEndpoint = explode('/', rawurldecode($_POST["prevEndpoint"]));
    $prevToken = end($prevEndpoint);

    // If token is unchanged
    if ($prevToken == $token) {
     exit; // nothing to do
    }

    // Check if registered
    $tokenObj = $this->get_token($prevToken, $os);
    if (is_null($tokenObj)) {
     $this->header_error(404, __('prevToken not found', 'pnfw'));
    }

    pnfw_log($type, sprintf(__('[%s] Updating device.', 'pnfw'), self::get_remote_addr()));

    // Check if destination token is already present
    if (!$this->is_token_missing($token, $os)) {
     pnfw_log(PNFW_SYSTEM_LOG, sprintf(__('Attempted an update of an %s token equal to a token already present: %s.', 'pnfw'), $os, $token));

     // Delete destination token to allow overwrite
     $wpdb->delete($push_tokens, array('token' => $token, 'os' => $os));
    }

    $data = array(
     'token' => $token,
     'os' => $os,
     'lang' => $lang
    );

    // Update prevToken with new token	
    $wpdb->update($push_tokens, $data, array('token' => $prevToken, 'os' => $os));

    // Check if encryption keys must be saved
    if (isset($_POST['key']) && isset($_POST['token'])) {
     $data = array(
      'token_id' => $tokenObj->id,
      'user_public_key' => $_POST['key'],
      'user_auth_token' => $_POST['token'],
     );

     // Update or add encryption keys
     $this->update_encryption_keys($tokenObj->id, $data);
    }
   }
   else {
    // If the device does not exist it is created
    if ($this->is_token_missing($token, $os)) {
     pnfw_log($type, sprintf(__('[%s] Registering device.', 'pnfw'), self::get_remote_addr()));

     $current_user = wp_get_current_user();
     if (0 == $current_user->ID) {
      // Not logged in.
      $user_id = $this->create_user();
     }
     else {
      // Logged in.
      $current_user->add_role(PNFW_Push_Notifications_for_WordPress::USER_ROLE);
      $user_id = $current_user->ID;
     }

     // Following code should not be accessed simultaneously by different threads
     $wp_options = $wpdb->options;
     $wpdb->query("LOCK TABLES $push_tokens WRITE, $push_logs WRITE, $push_encryption_keys WRITE, $wp_options READ;");

     $data = array(
      'token' => $token,
      'os' => $os,
      'lang' => $lang,
      'timestamp' => current_time('mysql'),
      'user_id' => $user_id,
      'active' => true
     );
     $wpdb->insert($push_tokens, $data);

     if (isset($_POST['key']) && isset($_POST['token'])) {
      $data = array(
       'token_id' => $wpdb->insert_id,
       'user_public_key' => $_POST['key'],
       'user_auth_token' => $_POST['token'],
      );
      $wpdb->insert($push_encryption_keys, $data);
     }

     $wpdb->query("UNLOCK TABLES;");
    }
   }
  }
  exit;
 }

 function get_os($domain) {
  switch($domain) {
   case 'updates.push.services.mozilla.com': return 'Firefox';
   case 'android.googleapis.com': return 'Chrome';
   default: {
    $this->header_error(500, __('Web Push endpoint not supported.', 'pnfw'));
   }
  }
 }

 function get_type($os) {
  switch($os) {
   case 'Firefox': return PNFW_FIREFOX_LOG;
   case 'Chrome': return PNFW_CHROME_LOG;
  }
 }

 function get_lang($lang) {
  $ret = substr($lang, 0, 2);

  return strlen($ret) == 2 ? $ret : null;
 }

 private function create_user() {
  $user_login = $this->create_unique_user_login();

  $user_id = wp_insert_user(array(
   'user_login' => $user_login,
   'user_email' => NULL,
   'user_pass' => NULL,
   'role' => PNFW_Push_Notifications_for_WordPress::USER_ROLE
  ));

  if (is_wp_error($user_id)) {
   $this->header_error(500, $user_id->get_error_message());
  }

  return $user_id;
 }

 protected function is_token_missing($token, $os) {
  return is_null($this->get_token($token, $os));
 }

 private function get_token($token, $os) {
  global $wpdb;

  $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';

  return $wpdb->get_row($wpdb->prepare("SELECT * FROM $push_tokens WHERE token=%s AND os=%s LIMIT 1", $token, $os));
 }

 private function update_encryption_keys($token_id, $data) {
  global $wpdb;

  $push_encryption_keys = $wpdb->get_blog_prefix() . 'push_encryption_keys';

  $encryption_id = $wpdb->get_var($wpdb->prepare("SELECT id FROM $push_encryption_keys WHERE token_id=%d LIMIT 1", $token_id));

  if (is_null($encryption_id)) {
   $wpdb->insert($push_encryption_keys, $data);
  }
  else {
   $wpdb->update($push_encryption_keys, $data, array('id' => $encryption_id));
  }
 }

 private function create_unique_user_login() {
  $rand = wp_rand();

  $tmp_user_login = 'anonymous' . $rand;

  if (username_exists($tmp_user_login)) {
   return $this->create_unique_user_login();
  }
  else {
   return $tmp_user_login;
  }
 }

 protected static function get_remote_addr() {
  $res = '';

  if (isset($_SERVER['REMOTE_ADDR'])) {
   $res = $_SERVER['REMOTE_ADDR'];
  }

  if ($res == '::1') {
   $res = '127.0.0.1';
  }

  return $res;
 }

 private function canonicalize($address) {
  $pieces = explode('/', $address);
  $keys = array_keys($pieces, '..');

  foreach($keys as $keypos => $key) {
   array_splice($pieces, $key - ($keypos * 2 + 1), 2);
  }

  return str_replace('./', '', implode('/', $pieces));
 }

 function header_error($error, $message = NULL) {
  switch ($error) {
   case 404:
    header('HTTP/1.1 404 Not Found');
    $reason = __('Not Found', 'pnfw');
    break;
   default:
    header('HTTP/1.1 500 Internal Server Error');
    $reason = __('Internal Server Error', 'pnfw');
  }

  if (isset($message)) {
   pnfw_log(PNFW_ALERT_LOG, sprintf(__('[%s] %s %s: %s.', 'pnfw'), self::get_remote_addr(), $error, $reason, $message));
  }

  exit;
 }

}
