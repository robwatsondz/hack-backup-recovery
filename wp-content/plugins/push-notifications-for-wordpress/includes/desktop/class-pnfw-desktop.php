<?php

abstract class PNFW_Desktop {

 function header_error($error, $message = NULL) {
  switch ($error) {
   case 401:
    header('HTTP/1.1 401 Unauthorized');
    $reason = __('Unauthorized', 'pnfw');
    break;
   case 404:
    header('HTTP/1.1 404 Not Found');
    $reason = __('Not Found', 'pnfw');
    break;
   case 405:
    header('HTTP/1.1 405 Invalid HTTP method');
    $reason = __('Invalid HTTP method', 'pnfw');
    break;
   default:
    header('HTTP/1.1 500 Internal Server Error');
    $reason = __('Internal Server Error', 'pnfw');
  }

  if (isset($message)) {
   pnfw_log(PNFW_ALERT_LOG, sprintf(__('[%s] %s %s: %s.', 'pnfw'), self::get_remote_addr(), $error, $reason, $message));
  }

  exit;
 }

 function get_option_file($option, $description) {
  $file = get_attached_file(get_option($option));

  if (empty($file)) {
   $this->header_error(500, sprintf(__('Safari %s is not correctly set', 'pnfw'), $description));
  }

  if (!file_exists($file)) {
   $this->header_error(500, sprintf(__('Safari %s does not exists', 'pnfw'), $description));
  }

  return $file;
 }

 function get_option_string($option, $description) {
  $string = get_option($option);

  if (empty($string)) {
   $this->header_error(500, sprintf(__('Safari %s is not correctly set', 'pnfw'), $description));
  }

  return $string;
 }

 protected static function get_remote_addr() {
  $res = '';

  if (isset($_SERVER['REMOTE_ADDR'])) {
   $res = $_SERVER['REMOTE_ADDR'];
  }

  if ($res == '::1') {
   $res = '127.0.0.1';
  }

  return $res;
 }

 private function canonicalize($address) {
  $pieces = explode('/', $address);
  $keys = array_keys($pieces, '..');

  foreach($keys as $keypos => $key) {
   array_splice($pieces, $key - ($keypos * 2 + 1), 2);
  }

  return str_replace('./', '', implode('/', $pieces));
 }

 function safari_certificate_admin_notices() {
  if (!current_user_can('activate_plugins'))
   return;

  $media_id = get_option('pnfw_safari_pem_certificate_media_id');

  $expires = pnfw_certificate_expiration($media_id);

  if (is_null($expires))
   return;

  $gmt_offset = get_option('gmt_offset');
  $date_format = get_option('date_format');
  $time_format = get_option('time_format');
  $tz_format = sprintf('%s %s', $date_format, $time_format);

  $human_readable_date = date_i18n($tz_format, $expires);

  if ($expires < time()) {
   $class = "error";
   $message = sprintf(__('The Safari push notifications certificate is expired on %s. Renew it now and upload it <a href="%s">here</a>.', 'pnfw'), $human_readable_date, admin_url('admin.php?page=pnfw-settings-identifier&tab=safari_tab'));
   echo"<div class=\"$class\"> <p>$message</p></div>";
  }
  else if ($expires - WEEK_IN_SECONDS < time()) {
   $class = "update-nag";
   $message = sprintf(__('The Safari push notifications certificate is about to expire on %s. Renew it soon and upload it <a href="%s">here</a>.', 'pnfw'), $human_readable_date, admin_url('admin.php?page=pnfw-settings-identifier&tab=safari_tab'));
   echo"<div class=\"$class\"> <p>$message</p></div>";
  }
 }

}
