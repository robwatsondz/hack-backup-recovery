<?php

require_once dirname(__FILE__ ) . '/class-pnfw-desktop.php';

class PNFW_Desktop_Log extends PNFW_Desktop {

 public function __construct() {
  $input = file_get_contents('php://input');
  if ($input === FALSE) {
   $this->header_error(500, __('Empty log content', 'pnfw'));
  }

  $contents = json_decode($input);
  if (is_null($contents)) {
   $this->header_error(500, __('Log content is not a JSON', 'pnfw'));
  }

  foreach ($contents->logs as $log) {
   pnfw_log(PNFW_SAFARI_LOG, sprintf(__('[%s] Log: %s', 'pnfw'), self::get_remote_addr(), $log));
  }

  exit;
 }

}
