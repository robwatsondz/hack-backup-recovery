<?php

require_once dirname(__FILE__ ) . '/class-pnfw-desktop.php';

class PNFW_Desktop_Packages extends PNFW_Desktop {

 protected $authentication_token;

 public function __construct() {
  $body = @file_get_contents("php://input");
  $body = json_decode($body, true);
  $this->authentication_token = $this->get_authentication_token($body['userId']);

  $certificate = $this->get_option_file('pnfw_safari_p12_certificate_media_id', __('P12 certificate', 'pnfw'));
  $passphrase = $this->get_option_string('pnfw_safari_p12_certificate_password', __('P12 certificate password', 'pnfw'));

  // Create a temporary directory in which to assemble the push package
  $package_dir = sys_get_temp_dir() . '/pushPackage' . time();
  if (!mkdir($package_dir)) {
   unlink($package_dir);
   $this->header_error(500, __('Temporary directory is not accessible', 'pnfw'));
  }

  $this->copy_raw_push_package_files($package_dir);
  $this->create_manifest($package_dir);
  $this->create_signature($package_dir, $certificate, $passphrase);
  $package_path = $this->package_raw_data($package_dir);

  // clean up temp folder http://stackoverflow.com/questions/3338123/how-do-i-recursively-delete-a-directory-and-its-entire-contents-filessub-dirs
  $files = new RecursiveIteratorIterator(
   new RecursiveDirectoryIterator($package_dir, RecursiveDirectoryIterator::SKIP_DOTS),
   RecursiveIteratorIterator::CHILD_FIRST
  );
  foreach ($files as $fileinfo) {
   $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
   $todo($fileinfo->getRealPath());
  }
  rmdir($package_dir);

  if (empty($package_path)) {
   $this->header_error(500, __('Empty package', 'pnfw'));
  }

  nocache_headers();
  header("Content-type: application/zip");

  echo file_get_contents($package_path);
  unlink($package_path); //http://stackoverflow.com/questions/1217636/remove-file-after-time-in-php

  exit;
 }

 function get_authentication_token($user_id) {
  return sprintf("%016x", intval($user_id));
 }

 function raw_files() {
  return array(
   'icon.iconset/icon_16x16.png',
   'icon.iconset/icon_16x16@2x.png',
   'icon.iconset/icon_32x32.png',
   'icon.iconset/icon_32x32@2x.png',
   'icon.iconset/icon_128x128.png',
   'icon.iconset/icon_128x128@2x.png',
   'website.json'
  );
 }

 // Copies the raw push package files to $package_dir.
 function copy_raw_push_package_files($package_dir) {
  mkdir($package_dir . '/icon.iconset');

  $icon_16 = $this->get_option_file('pnfw_safari_icon_16_media_id', __('16x16 icon', 'pnfw'));
  copy($icon_16, "$package_dir/icon.iconset/icon_16x16.png");

  $icon_32 = $this->get_option_file('pnfw_safari_icon_32_media_id', __('32x32 icon', 'pnfw'));
  copy($icon_32, "$package_dir/icon.iconset/icon_16x16@2x.png");
  copy($icon_32, "$package_dir/icon.iconset/icon_32x32.png");

  $icon_64 = $this->get_option_file('pnfw_safari_icon_64_media_id', __('64x64 icon', 'pnfw'));
  copy($icon_64, "$package_dir/icon.iconset/icon_32x32@2x.png");

  $icon_128 = $this->get_option_file('pnfw_safari_icon_128_media_id', __('128x128 icon', 'pnfw'));
  copy($icon_128, "$package_dir/icon.iconset/icon_128x128.png");

  $icon_256 = $this->get_option_file('pnfw_safari_icon_256_media_id', __('256x256 icon', 'pnfw'));
  copy($icon_256, "$package_dir/icon.iconset/icon_128x128@2x.png");

  $push_id = $this->get_option_string('pnfw_safari_website_push_id', __('website push ID', 'pnfw'));

  $url_parts = parse_url(get_option('home'));
  $allowed_domain = $url_parts['scheme'] . "://" . $url_parts['host'];

  $website = array(
   'websiteName' => wp_specialchars_decode(pnfw_get_blogname(), ENT_QUOTES),
   'websitePushID' => $push_id,
   'allowedDomains' => array($allowed_domain),
   'urlFormatString' => home_url('pnfw/goto/?post=%@&token=%@'),
   'authenticationToken' => $this->authentication_token,
   'webServiceURL' => home_url('pnfw')
  );

  file_put_contents("$package_dir/website.json", json_encode($website, JSON_UNESCAPED_SLASHES));
 }

 // Creates the manifest by calculating the SHA1 hashes for all of the raw files in the package.
 function create_manifest($package_dir) {
  // Obtain SHA1 hashes of all the files in the push package
  $manifest_data = array();
  foreach ($this->raw_files() as $raw_file) {
   $manifest_data[$raw_file] = sha1(file_get_contents("$package_dir/$raw_file"));
  }
  file_put_contents("$package_dir/manifest.json", json_encode((object)$manifest_data));
 }

 // Creates a signature of the manifest using the push notification certificate.
 function create_signature($package_dir, $cert_path, $cert_password) {
  // Load the push notification certificate
  $pkcs12 = file_get_contents($cert_path);
  $certs = array();
  if(!openssl_pkcs12_read($pkcs12, $certs, $cert_password)) {
   return;
  }

  $signature_path = "$package_dir/signature";

  // Sign the manifest.json file with the private key from the certificate
  $cert_data = openssl_x509_read($certs['cert']);
  $private_key = openssl_pkey_get_private($certs['pkey'], $cert_password);
  openssl_pkcs7_sign(
   "$package_dir/manifest.json",
   $signature_path,
   $cert_data,
   $private_key,
   array(),
   PKCS7_BINARY | PKCS7_DETACHED,
   dirname(__FILE__ ) . '/../../assets/certs/AppleWWDRCA.pem'
  );

  // Convert the signature from PEM to DER
  $signature_pem = file_get_contents($signature_path);
  $matches = array();
  if (!preg_match('~Content-Disposition:[^\n]+\s*?([A-Za-z0-9+=/\r\n]+)\s*?-----~', $signature_pem, $matches)) {
   return;
  }
  $signature_der = base64_decode($matches[1]);
  file_put_contents($signature_path, $signature_der);
 }

 // Zips the directory structure into a push package, and returns the path to the archive.
 function package_raw_data($package_dir) {
  $zip_path = "$package_dir.zip";

  // Package files as a zip file
  $zip = new ZipArchive();
  if (!$zip->open("$package_dir.zip", ZIPARCHIVE::CREATE)) {
   error_log('Could not create ' . $zip_path);
   return;
  }

  $raw_files = $this->raw_files();
  $raw_files[] = 'manifest.json';
  $raw_files[] = 'signature';
  foreach ($raw_files as $raw_file) {
   $zip->addFile("$package_dir/$raw_file", $raw_file);
  }

  $zip->close();
  return $zip_path;
 }

}
