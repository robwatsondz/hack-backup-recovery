<?php

require_once dirname(__FILE__ ) . '/class-pnfw-desktop.php';

class PNFW_Desktop_Devices extends PNFW_Desktop {

 protected $os;
 protected $token;
 protected $lang;
 protected $websitePushID;

 public function __construct() {
  global $wp_query;
  $this->os = 'Safari';
  $this->token = isset($wp_query->query_vars['deviceToken']) ? $wp_query->query_vars['deviceToken'] : '';
  $this->websitePushID = isset($wp_query->query_vars['websitePushID']) ? $wp_query->query_vars['websitePushID'] : '';
  $this->lang = pnfw_get_current_language();

  global $wpdb;
  $push_tokens = $wpdb->get_blog_prefix().'push_tokens';

  if ($_SERVER['REQUEST_METHOD'] == "POST") {
   // If the device does not exist it is created
   if ($this->is_token_missing()) {
    pnfw_log(PNFW_SAFARI_LOG, sprintf(__('[%s] Registering device.', 'pnfw'), self::get_remote_addr()));

    $user_id = $this->get_authorized_user_id();
    if (0 == $user_id) {
     // Not logged in.
     $user_id = $this->create_user();
    }
    else {
     // Logged in.
     $user = new WP_User($user_id);
     $user->add_role(PNFW_Push_Notifications_for_WordPress::USER_ROLE);
    }

    // Following code should not be accessed simultaneously by different threads
    $push_logs = $wpdb->get_blog_prefix().'push_logs';
    $wp_options = $wpdb->options;
    $wpdb->query("LOCK TABLES $push_tokens WRITE, $push_logs WRITE, $wp_options READ;");

    $data = array(
     'token' => $this->token,
     'os' => $this->os,
     'lang' => $this->lang,
     'timestamp' => current_time('mysql'),
     'user_id' => $user_id,
     'active' => true,
    );
    $wpdb->insert($push_tokens, $data);

    $wpdb->query("UNLOCK TABLES;");
   }
  }
  else if ($_SERVER['REQUEST_METHOD'] == "DELETE") {
   pnfw_log(PNFW_SAFARI_LOG, sprintf(__('[%s] Forgetting device.', 'pnfw'), self::get_remote_addr()));

   $user_id = $wpdb->get_var($wpdb->prepare("SELECT user_id FROM $push_tokens WHERE token = %s AND os = %s", $this->token, $this->os));

   if (is_null($user_id)) {
    $this->header_error(404, __('Unable to delete token', 'pnfw'));
   }

   $res = $wpdb->delete($push_tokens, array("token" => $this->token, "os" => $this->os));

   if ($res === false) {
    $this->header_error(500, __('Unable to delete token', 'pnfw'));
   }

   $user = new WP_User($user_id);

   if (in_array(PNFW_Push_Notifications_for_WordPress::USER_ROLE, $user->roles) && empty($user->user_email)) {
    pnfw_log(PNFW_SAFARI_LOG, sprintf(__("Automatically deleted the anonymous user %s (%s) since left without tokens.", 'pnfw'), $user->user_login, $user_id));
    if (is_multisite()) {
     if (is_user_member_of_blog($user_id)) {
      require_once(ABSPATH . 'wp-admin/includes/ms.php');
      wpmu_delete_user($user_id);
     }
    }
    else {
     require_once(ABSPATH . 'wp-admin/includes/user.php');
     wp_delete_user($user_id);
    }
   }
  }
  else {
   $this->header_error(405, __('Only POST and DELETE request allowed', 'pnfw'));
  }

  exit;
 }

 private function get_authorized_user_id() {
  if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
   $res = explode(" ", $_SERVER['HTTP_AUTHORIZATION']);
   $user_id = hexdec($res[1]);

   $user = get_userdata($user_id);
   if ($user) {
    return $user_id;
   }
  }
  return 0;
 }

 private function create_user() {
  $user_login = $this->create_unique_user_login();

  $user_id = wp_insert_user(array(
   'user_login' => $user_login,
      'user_email' => NULL,
      'user_pass' => NULL,
      'role' => PNFW_Push_Notifications_for_WordPress::USER_ROLE
  ));

  if (is_wp_error($user_id)) {
   $this->header_error(500, $user_id->get_error_message());
  }

  return $user_id;
 }

 protected function is_token_missing($token = null) {
  global $wpdb;
  if (is_null($token))
   $token = $this->token;

  $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';
  $res = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $push_tokens WHERE token=%s AND os=%s", $token, $this->os));

  return empty($res);
 }

 private function create_unique_user_login() {
  $rand = wp_rand();

  $tmp_user_login = 'anonymous' . $rand;

  if (username_exists($tmp_user_login)) {
   return $this->create_unique_user_login();
  }
  else {
   return $tmp_user_login;
  }
 }

}
