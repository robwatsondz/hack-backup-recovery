<?php

class PNFW_Desktop_Admin {

 public function __construct() {
  if (is_admin()) {
   add_action('admin_notices', array($this, 'safari_certificate_admin_notices'));
  }
  else {
   add_action('wp_enqueue_scripts', array($this, 'enqueue_web_scripts'));
   add_action('template_redirect', array($this, 'front_controller'));
  }
 }

 function front_controller() {
  global $wp_query;

  $control_action = isset($wp_query->query_vars['control_action']) ? $wp_query->query_vars['control_action'] : '';

  switch ($control_action) {
   case 'log':
    require_once(dirname(__FILE__) . '/class-pnfw-desktop-log.php');
    $log = new PNFW_Desktop_Log();
    break;
   case 'pushPackages':
    require_once(dirname(__FILE__) . '/class-pnfw-desktop-packages.php');
    $packages = new PNFW_Desktop_Packages();
    break;
   case 'devices':
    require_once(dirname(__FILE__) . '/class-pnfw-desktop-devices.php');
    $devices = new PNFW_Desktop_Devices();
    break;
  }
 }

 function enqueue_web_scripts() {
  wp_register_script('admin_safari', $this->canonicalize(plugin_dir_url(__FILE__) . '/../../../assets/js/admin_safari.js'), array('jquery'));
  wp_enqueue_script('admin_safari');

  $data = array(
   'webServiceURL' => home_url('pnfw'),
   'websitePushID' => get_option('pnfw_safari_website_push_id'),
   'userInfo' => array('userId' => strval(wp_get_current_user()->ID))
  );

  wp_localize_script('admin_safari', 'params', $data);
 }

 private function canonicalize($address) {
  $pieces = explode('/', $address);
  $keys = array_keys($pieces, '..');

  foreach($keys as $keypos => $key) {
   array_splice($pieces, $key - ($keypos * 2 + 1), 2);
  }

  return str_replace('./', '', implode('/', $pieces));
 }

 function safari_certificate_admin_notices() {
  if (!current_user_can('activate_plugins'))
   return;

  $media_id = get_option('pnfw_safari_pem_certificate_media_id');

  $expires = pnfw_certificate_expiration($media_id);

  if (is_null($expires))
   return;

  $gmt_offset = get_option('gmt_offset');
  $date_format = get_option('date_format');
  $time_format = get_option('time_format');
  $tz_format = sprintf('%s %s', $date_format, $time_format);

  $human_readable_date = date_i18n($tz_format, $expires);

  if ($expires < time()) {
   $class = "error";
   $message = sprintf(__('The Safari push notifications certificate is expired on %s. Renew it now and upload it <a href="%s">here</a>.', 'pnfw'), $human_readable_date, admin_url('admin.php?page=pnfw-settings-identifier&tab=safari_tab'));
   echo"<div class=\"$class\"> <p>$message</p></div>";
  }
  else if ($expires - WEEK_IN_SECONDS < time()) {
   $class = "update-nag";
   $message = sprintf(__('The Safari push notifications certificate is about to expire on %s. Renew it soon and upload it <a href="%s">here</a>.', 'pnfw'), $human_readable_date, admin_url('admin.php?page=pnfw-settings-identifier&tab=safari_tab'));
   echo"<div class=\"$class\"> <p>$message</p></div>";
  }
 }

}
