<?php

class PNFW_Goto {

 private $post_id;
 private $token;
 private $os;
 private $user_id;

 public function __construct() {
  $this->os = $this->opt_string_parameter('os', 'Safari');
  $this->post_id = $this->get_int_parameter('post');
  $this->token = $this->get_string_parameter('token');

  $this->user_id = $this->current_user_id();

  if (is_null($this->user_id)) {
   wp_redirect(home_url(), 301);
   exit;
  }

  if (!$this->is_viewed())
   $this->set_viewed();

  wp_redirect(home_url() . '/?p=' . $this->post_id, 301);
  exit;
 }

 // Get parameters from get or post
 function get_parameters() {
  return strtoupper($_SERVER['REQUEST_METHOD']) == 'POST' ? $_POST : $_GET;
 }

 // Get mandatory parameter from get or post
 function get_parameter($parameter, $filter) {
  $pars = $this->get_parameters();

  if (!array_key_exists($parameter, $pars)) {
   wp_redirect(home_url(), 301);
   exit;
  }

  return $this->filter($pars[$parameter], $filter);
 }

 // Get optional parameter from get or post
 function opt_parameter($parameter, $filter, $default = NULL) {
  $pars = $this->get_parameters();

  if (!array_key_exists($parameter, $pars))
   return $default;

  return $this->filter($pars[$parameter], $filter);
 }

 function opt_string_parameter($parameter, $default = NULL) {
  return $this->opt_parameter($parameter, FILTER_SANITIZE_STRING, $default);
 }

 function get_string_parameter($parameter) {
  return $this->get_parameter($parameter, FILTER_SANITIZE_STRING);
 }

 function get_int_parameter($parameter) {
  return $this->get_parameter($parameter, FILTER_SANITIZE_NUMBER_INT);
 }

 private function filter($value, $filter) {
  $res = filter_var($value, $filter);

  if (!$res) {
   wp_redirect(home_url(), 301);
   exit;
  }

  return $res;
 }

 public function is_viewed($post_id = null) {
  if (is_null($post_id))
   $post_id = $this->post_id;

  global $wpdb;
  $push_viewed = $wpdb->get_blog_prefix() . 'push_viewed';
  return (boolean)$wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $push_viewed WHERE post_id=%d AND user_id=%d", $post_id, $this->user_id));
 }

 public function set_viewed($viewed = true) {
  global $wpdb;
  $push_viewed = $wpdb->get_blog_prefix() . 'push_viewed';

  if ($viewed) {
   $wpdb->insert($push_viewed, array('post_id' => $this->post_id, 'user_id' => $this->user_id, 'timestamp' => current_time('mysql')));
  }
  else {
   $wpdb->delete($push_viewed, array('post_id' => $this->post_id, 'user_id' => $this->user_id));
  }
 }

 protected function current_user_id() {
  return self::get_user_id($this->token, $this->os);
 }

 public static function get_user_id($token, $os) {
  global $wpdb;
  $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';
  return $wpdb->get_var($wpdb->prepare("SELECT user_id FROM $push_tokens WHERE token=%s AND os=%s", $token, $os));
 }

}
