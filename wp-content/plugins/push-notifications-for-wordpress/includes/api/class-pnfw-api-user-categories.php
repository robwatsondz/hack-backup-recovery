<?php

require_once dirname(__FILE__ ) . '/class-pnfw-api-registered.php';

class PNFW_API_User_Categories extends PNFW_API_Registered {

 public function __construct() {
  parent::__construct(home_url('pnfw/user-categories/'));

  // Optional
  $timestamp = $this->opt_parameter('timestamp', FILTER_SANITIZE_NUMBER_INT);
  if ($timestamp == $this->get_last_modification_timestamp())
   $this->header_error('304');

  switch($this->get_method()) {
   case 'GET': {
    $raw_terms = get_terms('user_cat', array('hide_empty' => false));
    $raw_terms = $this->lang_terms($raw_terms, 'user_cat');
    $raw_terms = apply_filters('pnfw_before_user_categories_api', $raw_terms);

    $categories = array();

    foreach ($raw_terms as $raw_term) {
     // Mandatory fields
     $category = array(
      'id' => (int)$raw_term->term_id,
      'name' => $raw_term->name
     );

     // Optional fields
     $description = $raw_term->description;
     if (!empty($description))
      $category['description'] = $description;

     $category = apply_filters('pnfw_output_user_categories_api', $category);

     $categories[] = $category;
    }

    header('Content-Type: application/json');

    echo json_encode(array(
     'categories' => $categories,
     'timestamp' => $this->get_last_modification_timestamp()
    ));

    break;
   }
   case 'POST': {
    $category_id = $this->get_parameter('id', FILTER_VALIDATE_INT);

    $category_id = (int)pnfw_get_normalized_term_id($category_id);

    $valid_categories = get_terms('user_cat', array('hide_empty' => false, 'fields' => 'ids'));
    if (!in_array($category_id, $valid_categories)) {
     $this->json_error('404', __('User category not found', 'pnfw'));
    }

    $this->set_category($this->current_user_id(), $category_id, true);
    break;
   }
   default:
    $this->json_error('401', __('Invalid HTTP method', 'pnfw'));
  }
  exit;
 }

 private function set_category($user_id, $category_id, $replace) {
  global $sitepress;

  if (isset($sitepress)) { // Removes a WPML warning
   remove_action('deleted_term_relationships', array($sitepress, 'deleted_term_relationships'));
  }

  wp_set_object_terms($user_id, $category_id, 'user_cat', !$replace);
  clean_object_term_cache($user_id, 'user_cat');

  if (isset($sitepress)) {
   add_action('deleted_term_relationships', array($sitepress, 'deleted_term_relationships'), 10, 2);
  }
 }

 private function lang_terms($terms, $type) {
  if (function_exists('icl_object_id')) {
   $res = array();

   foreach ($terms as $term) {
    $xlat = icl_object_id($term->term_id, $type, false);

    if (!is_null($xlat))
     $res[] = $term;
   }

   return $res;
  }
  else {
   return $terms;
  }
 }
}
