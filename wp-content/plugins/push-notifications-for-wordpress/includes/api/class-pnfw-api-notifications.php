<?php

require_once dirname(__FILE__ ) . '/class-pnfw-api-activated.php';

class PNFW_API_Notifications extends PNFW_API_Activated {

 public function __construct() {
  parent::__construct(home_url('pnfw/notifications/', 'GET'));

  $post_id = $this->get_post_id();

  if ($post_id == null) {
   $this->fallback_response();
  }

  $post = get_post($post_id);

  if ($post == null || $post->post_status != 'publish' || !$this->current_user_can_view_post($post_id)) {
   $this->fallback_response();
  }

  $this->response($post_id, wp_specialchars_decode(pnfw_get_blogname(), ENT_QUOTES), $post->post_title);
 }

 private function fallback_response() {
  $this->response(0, wp_specialchars_decode(pnfw_get_blogname(), ENT_QUOTES), __('A new post has been published.', 'pnfw'));
 }

 private function response($id, $title, $message) {
  $response = array(
   'id' => $id,
   'title' => $title,
   'icon' => $this->get_icon(get_option('pnfw_web_push_icon_media_id')),
   'message' => apply_filters('pnfw_notification_title', $message, $id)
  );

  $response = apply_filters('pnfw_output_notifications_api', $response);

  nocache_headers();
  header('Content-Type: application/json');
  echo json_encode($response);
  exit;
 }

 private function get_icon($media_id) {
  if (is_null($media_id))
   return null;

  $array = wp_get_attachment_image_src($media_id, 'full');

  return $array[0];
 }

 private function get_post_id() {
  global $wpdb;
  $push_sent = $wpdb->get_blog_prefix() . 'push_sent';
  return $wpdb->get_var($wpdb->prepare("SELECT post_id FROM $push_sent WHERE user_id=%d ORDER BY timestamp DESC LIMIT 1", $this->current_user_id()));
 }

 function check_oauth_signature() {
  // Avoid check
 }
}
