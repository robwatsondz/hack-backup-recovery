<?php

if (!defined('ABSPATH')) {
 exit; // Exit if accessed directly
}

require_once dirname(__FILE__ ) . '/class-pnfw-notifications.php';

class PNFW_Notifications_Chrome extends PNFW_Notifications {

 protected $push_encryption_keys;
 protected $sender;

 public function __construct() {
  parent::__construct('Chrome');
 }

 protected function raw_send($tokens, $title, $user_info) {
  // No devices, do nothing
  if (empty($tokens)) {
   return 0;
  }

  $this->push_encryption_keys = $this->wpdb->get_blog_prefix() . 'push_encryption_keys';

  require_once(dirname(__FILE__) . '/../../libs/PHP_GCM/Message.php');
  require_once(dirname(__FILE__) . '/../../libs/PHP_GCM/Sender.php');
  require_once(dirname(__FILE__) . '/../../libs/PHP_GCM/Result.php');
  require_once(dirname(__FILE__) . '/../../libs/PHP_GCM/MulticastResult.php');
  require_once(dirname(__FILE__) . '/../../libs/PHP_GCM/Constants.php');
  require_once(dirname(__FILE__) . '/../../libs/PHP_GCM/InvalidRequestException.php');

  $api_key = get_option('pnfw_web_push_google_api_key');
  if (empty($api_key)) {
   pnfw_log(PNFW_CHROME_LOG, __('Google API Key is not correctly set.', 'pnfw'));
   return 0;
  }

  $this->sender = new PHP_GCM\Sender($api_key);
  $sent = 0;

  if (get_option('pnfw_web_push_payload')) {
   require_once(dirname(__FILE__) . '/../../libs/web-push/vendor/autoload.php');

   $payload_data = array_merge(
    array(
     'title' => wp_specialchars_decode(pnfw_get_blogname(), ENT_QUOTES),
     'message' => $title,
     'icon' => $this->get_icon(get_option('pnfw_web_push_icon_media_id'))
    ),
    $user_info
   );

   $payload = Minishlink\WebPush\Encryption::padPayload(json_encode($payload_data), false);

   foreach ($tokens as &$token) {
    $encryption_keys = $this->get_encryption_keys($token);
    if (isset($encryption_keys)) {
     $encrypted = Minishlink\WebPush\Encryption::encrypt(
      $payload,
      $encryption_keys['user_public_key'],
      $encryption_keys['user_auth_token'],
      version_compare(phpversion(), '7.1', '>=')
     );
     $message = new PHP_GCM\Message('push');
     $message->publicKey($encrypted['localPublicKey']);
     $message->salt($encrypted['salt']);
     $message->rawData(base64_encode($encrypted['cipherText']));

     $sent += $this->send_message($message, array($token));
    }
   }
  }
  else {
   $message = new PHP_GCM\Message('push');

   $max_bulk_size = 999;
   $chunks = array_chunk($tokens, $max_bulk_size);

   foreach ($chunks as $chunk) {
    $sent += $this->send_message($message, $chunk);
   }
  }

  return $sent;
 }

 private function send_message($message, $chunk) {
  try {
   $multicastResult = $this->sender->sendNoRetryMulti($message, $chunk);
   $results = $multicastResult->getResults();

   for ($i = 0; $i < count($results); $i++) {
    $result = $results[$i];
    // This means error
    if (is_null($result->getMessageId())) {
     switch ($result->getErrorCode()) {
      // If device is not registered or invalid remove it from table
      case 'NotRegistered':
      case 'InvalidRegistration':
       $this->delete_token($chunk[$i]);
       break;

      // else not recoverable errors, ignore
      case 'Unavailable':
      case 'InternalServerError':
       $this->log_fcm_error($result->getErrorCode(), __('You could retry to send it late in another request.', 'pnfw'));
       break;

      case 'MissingRegistration':
       $this->log_fcm_error($result->getErrorCode(), __('Check that the request contains a registration ID.', 'pnfw'));
       break;

      case 'InvalidPackageName':
       $this->log_fcm_error($result->getErrorCode(), __('Make sure the message was addressed to a registration ID whose package name matches the value passed in the request.', 'pnfw'));
       break;

      case 'MismatchSenderId':
       $this->log_fcm_error($result->getErrorCode(), __('Make sure the Sender Id (Project Number) in your App is correct.', 'pnfw'));
       break;

      case 'MessageTooBig':
       $this->log_fcm_error($result->getErrorCode(), __('Check that the total size of the payload data included in a message does not exceed 4096 bytes.', 'pnfw'));
       break;

      case 'InvalidDataKey':
       $this->log_fcm_error($result->getErrorCode(), __('Check that the payload data does not contain a key that is used internally by FCM.', 'pnfw'));
       break;

      case 'InvalidTtl':
       $this->log_fcm_error($result->getErrorCode(), __('Check that the value used in time_to_live is an integer representing a duration in seconds between 0 and 2,419,200.', 'pnfw'));
       break;

      case 'DeviceMessageRateExceed':
       $this->log_fcm_error($result->getErrorCode(), __('Reduce the number of messages sent to this device.', 'pnfw'));
       break;
     }
    }
    else {
     $this->notification_sent($chunk[$i]);

     // If there is a canonical registration id we must update
     $token = $result->getCanonicalRegistrationId();
     if (!is_null($token)) {
      $this->update_token($chunk[$i], $token);
     }
    }
   }

   unset($result);

   return $multicastResult->getSuccess();
  }
  catch (\InvalidArgumentException $e) {
   pnfw_log(PNFW_CHROME_LOG, sprintf(__('Invalid argument (%s): %s', 'pnfw'), (string)$e->getCode(), strip_tags($e->getMessage())));
  } catch (PHP_GCM\InvalidRequestException $e) {
   pnfw_log(PNFW_CHROME_LOG, sprintf(__('FCM didn\'t return a 200 or 503 status (%s): %s', 'pnfw'), (string)$e->getCode(), strip_tags($e->getMessage())));
  } catch (\Exception $e) {
   pnfw_log(PNFW_CHROME_LOG, sprintf(__('Could not send message (%s): %s', 'pnfw'), (string)$e->getCode(), strip_tags($e->getMessage())));
  }

  return 0;
 }

 private function log_fcm_error($error_code, $error_message) {
  pnfw_log(PNFW_CHROME_LOG, sprintf(__('Could not send message (%s): %s', 'pnfw'), $error_code, $error_message));
 }

 private function get_icon($media_id) {
  if (is_null($media_id))
   return null;

  $array = wp_get_attachment_image_src($media_id, 'full');

  return $array[0];
 }

 protected function get_encryption_keys($token) {
  $token_id = $this->get_token_id($token);

  if (isset($token_id)) {
   return $this->wpdb->get_row($this->wpdb->prepare("SELECT user_public_key, user_auth_token FROM {$this->push_encryption_keys} WHERE token_id=%d", $token_id), ARRAY_A);
  }

  return null;
 }

 protected function delete_token($token) {
  $token_id = $this->get_token_id($token);

  $this->wpdb->delete($this->push_encryption_keys, array('token_id' => $token_id));

  parent::delete_token($token);
 }

 private function get_token_id($token) {
  return $this->wpdb->get_var($this->wpdb->prepare("SELECT id FROM {$this->push_tokens} WHERE token=%s AND os=%s", $token, $this->os));
 }

}
