<?php



if (!defined('ABSPATH')) {
 exit; // Exit if accessed directly
}

class PNFW_Notifications {

 protected $wpdb;
 protected $push_viewed;
 protected $push_sent;
 protected $os;
 protected $push_tokens;
 protected $push_excluded_categories;
 protected $type;
 protected $enabled_post_types;
 protected $visible_posts;
 protected $offset;

 public function __construct($os) {
  global $wpdb;
  $this->wpdb = &$wpdb;
  $this->os = $os;
  $this->push_tokens = $this->wpdb->get_blog_prefix() . 'push_tokens';
  $this->push_viewed = $this->wpdb->get_blog_prefix() . 'push_viewed';
  $this->push_sent = $this->wpdb->get_blog_prefix() . 'push_sent';
  $this->push_excluded_categories = $this->wpdb->get_blog_prefix() . 'push_excluded_categories';
  $this->enabled_post_types = get_option('pnfw_enabled_post_types', array());
  $this->visible_posts = $this->get_visible_posts();

  switch ($this->os) {
   case 'iOS': $this->type = PNFW_IOS_LOG; break;
   case 'Android': $this->type = PNFW_ANDROID_LOG; break;
   case 'Safari': $this->type = PNFW_SAFARI_LOG; break;
   case 'Chrome': $this->type = PNFW_CHROME_LOG; break;
   case 'Firefox': $this->type = PNFW_FIREFOX_LOG; break;
  }
 }

 public function send_chunk($post, $offset) {

  $PAGE_LENGTH = 500;




  $this->offset = $offset;

  $user_cat = get_post_meta($post->ID, 'pnfw_user_cat', true);
  $category_id = pnfw_get_normalized_term_id((int)$this->get_category_id($post));
  $lang = pnfw_get_post_lang($post->ID);


  $title = apply_filters('pnfw_notification_title', $post->post_title, $post->ID);




  $sent = 0;
  $total = 0;
  $subpage_total = 0;


  do {

   $tokens = $this->get_tokens($category_id, $lang);
   $subpage_total = count($tokens);

   pnfw_log($this->type, sprintf(__('Processing new subpage with %d tokens', 'pnfw'), $subpage_total));

   if (empty($user_cat)) {
    // Send tokens to all categories (no filter)

    $out_tokens = array();
    foreach ($tokens as $token) {
     $user_id = $this->get_user_id($token);
     $send = apply_filters('pnfw_before_sending_notification_to_user', true, $user_id, $post);

     if ($send) {
      $out_tokens[] = $token;
     }
    }
    $sent += $this->raw_send($out_tokens, $title, $post->ID);



   }
   else {
    // Send tokens only to selected category
    $out_tokens = array();
    foreach ($tokens as $token) {
     $user_id = $this->get_user_id($token);
     if ($this->user_should_be_notified($user_id, $user_cat)) {

      $send = apply_filters('pnfw_before_sending_notification_to_user', true, $user_id, $post);

      if ($send) {

       $out_tokens[] = $token;

      }

     }
    }
    $sent += $this->raw_send($out_tokens, $title, $post->ID);
   }

   $total += $subpage_total;






  } while ($subpage_total > 0 && $total < $PAGE_LENGTH);


  return array('total' => $total, 'sent' => $sent);
 }


 public function send_generic_notification($user_id, $message, $user_info) {
  $out_tokens = $this->wpdb->get_col($this->wpdb->prepare("SELECT token FROM {$this->push_tokens} WHERE os=%s AND active=1 AND user_id=%d", $this->os, $user_id));
  return $this->raw_send($out_tokens, $message, $user_info);
 }


 private function user_should_be_notified($user_id, $post_user_category) {
  if (empty($post_user_category)) {
   // All
   return true;
  }

  $user_categories = wp_get_object_terms($user_id, 'user_cat', array('fields' => 'slugs'));



  if ($this->is_user_anonymous($user_id)) {
   array_push($user_categories, 'anonymous-users');
  }
  else {
   array_push($user_categories, 'registered-users');
  }

  return in_array($post_user_category, $user_categories);
 }

 private function is_user_anonymous($user_id) {
  $user = get_userdata($user_id);

  return in_array(PNFW_Push_Notifications_for_WordPress::USER_ROLE, $user->roles) && empty($user->user_email);
 }

 public function send_title_to_tokens($title, $tokens) {

  $payload = apply_filters('pnfw_notification_payload', array('id' => 0), 0);

		
		// X2WEB for testing purposes
		if (isset($_GET['action']) && $_GET['action'] == 'send_test_notification')
		{
			//for testing with 
			$url = 'https://hamodia.com?t=' . time();
			$payload['message_alert'] = 'Would you like to view this article?' . rand(0,100000);
			$payload['mail_queue_id'] = null;
			$payload['controller'] = 'cms';
			$payload['FROM_GCM'] = 1;			
			$payload['data_url'] = $url;
			$payload['data_title'] = 'Test Title' . rand(0,100000);
			
			$payload['icon'] = 'ic_stat_name';
			$payload['body'] = 'Tap to read more';
			
			// need to remove tokens that are not set to receive for the category
			$post_id = 141069;// column article
			//$post_id = 140920;// inyan article
			//$post_id = 140942;// feature article
			//$post_id = 141076;// israel article
			//$post_id = 141032;// community article
			$user_id = 485;
			
			$post_type = get_post_type($post_id);
			
			$post = get_post($post_id);			
			
			$custom_taxonomy = '';
			// Dont forget we switched features and columns.
			//echo $post_type;
			switch ($post_type) {
				case 'hamodia_frominyan':
									$custom_taxonomy = 'frominyan_category';
									break;
				case 'hamodia_feature':
									$custom_taxonomy = 'feature_category';
									break;
				case 'hamodia_column':
									$custom_taxonomy = 'hamodia_column';
									break;
			}
			
			if ($custom_taxonomy != '')
			{
				// for each token we need to check and remove.
				$final_tokens = array();
				foreach ($tokens as $token)
				{
					
					// get the user by token.
					$user_id = -1;

					// using prepare safeguards against sql injection
					$results = $this->wpdb->get_results($this->wpdb->prepare( 
						"
							SELECT user_id 
							FROM hamod_push_tokens 
							WHERE token = %s 						
						", 
							array(
							$token						
						) 
					));
					
					if (isset($results[0]->user_id))
						$user_id = $results[0]->user_id;
					
					if ($user_id > 0)
					{
						
						$exclude = $this->does_user_have_post_type_excluded($user_id, $custom_taxonomy);
						if (!$exclude)
							$final_tokens[] = $token;
						
					}
					
				}
				$tokens = $final_tokens;
			}
			//echo "cccc";
			
			//print_r($tokens);
			
			
			
			
			//mail('mikeypengelly@gmail.com','Hamodia Debug Tokens',print_r($tokens,true));
		}


  return $this->raw_send($tokens, $title, $payload);
 }
 
 // X2CMS added function
 protected function does_user_have_post_type_excluded($user_id, $post_type)
{
	$args = array(
		'hide_empty' => false
	);
	
	$terms = get_terms($post_type,$args); // Get all terms of a taxonomy

	// If we have a record.
	if (isset($terms[0]))
	{
		$term = $terms[0];
		$results = $this->wpdb->get_results('SELECT * FROM hamod_push_excluded_categories WHERE user_id = '.$user_id.' and category_id= '.$term->term_id);

		// If the record exists
		if (isset($results[0]))
			return true;

		return false;
	}

	return -1;

}

 protected function raw_send($tokens, $title, $post_id) {
  return 0; // Do nothing; should be reimplementend in the subclasses
 }

 protected function notification_sent($token) {
  // Do nothing; can be reimplementend in the subclasses
 }

 protected function update_token($prevToken, $token) {
  if ($prevToken == $token)
   return;

  $count = $this->wpdb->get_var($this->wpdb->prepare("SELECT COUNT(*) FROM {$this->push_tokens} WHERE token = %s AND os = %s", $token, $this->os));

  if ($count != 0) {
   // This situation can happen on an Android device, for example when you uninstall the app and then reinstall it. In that case, FCM assigns to the device two different tokens, one before the uninstall and one after the reinstall. But when we send the notification to the old token ($prevToken), FCM informs us that it does no longer exists and gives us its new token ($token). Since the new token is already present in our db we have to delete the old one.
   pnfw_log($this->type, sprintf(__('Attempted an update of an %s token equal to a token already present: %s.', 'pnfw'), $this->os, $token));

   $this->delete_token($prevToken);

   return;
  }

  pnfw_log($this->type, sprintf(__("Updated %s token from %s to %s.", 'pnfw'), $this->os, $prevToken, $token));

  $this->wpdb->update($this->push_tokens,
   array('token' => $token),
   array('token' => $prevToken, 'os' => $this->os)
  );
 }

 protected function delete_token($token) {
  pnfw_log($this->type, sprintf(__("Removed invalid %s token: %s.", 'pnfw'), $this->os, $token));

  $this->offset--;
  $user_id = $this->get_user_id($token);

  $this->wpdb->delete($this->push_tokens,
   array('token' => $token, 'os' => $this->os)
  );

  $user = new WP_User($user_id);

  if (in_array(PNFW_Push_Notifications_for_WordPress::USER_ROLE, $user->roles) && empty($user->user_email)) {
   pnfw_log($this->type, sprintf(__("Automatically deleted the anonymous user %s (%s) since left without tokens.", 'pnfw'), $user->user_login, $user_id));

   if (is_multisite()) {
    if (is_user_member_of_blog($user_id)) {
     require_once(ABSPATH . 'wp-admin/includes/ms.php');
     wpmu_delete_user($user_id);
    }
   }
   else {
    require_once(ABSPATH . 'wp-admin/includes/user.php');
    wp_delete_user($user_id);
   }
  }
 }

 protected function get_visible_posts() {
  $raw_posts = array();
  if (get_option('pnfw_enabled_post_types')) {
   $raw_posts = get_posts(
    array(
     'posts_per_page' => get_option('pnfw_posts_per_page'),
     'post_type' => get_option('pnfw_enabled_post_types'),
     'fields' => 'ids'
    )
   );
  }
  return $raw_posts;
 }

 private function get_forged_query($raw_sql, $args, $limit, $offset, $lang) {
  if (is_null($lang)) {
   $values = array_merge($args, array($limit, $offset));

   return $this->wpdb->prepare(
    $raw_sql . " ORDER BY id LIMIT %d OFFSET %d",
    $values
   );
  }
  else {
   $langs = array($lang);
   global $sitepress;
   if ($lang == $sitepress->get_default_language()) {
    // All user langs
    $all_langs = $this->wpdb->get_col("SELECT DISTINCT lang FROM {$this->push_tokens}");

    // All user langs not supported
    $unsupported_langs = array_diff($all_langs, pnfw_get_wpml_langs());

    // All user langs not supported plus default and empty string
    $langs = array_merge($unsupported_langs, $langs, array(''));
   }
   $in = join(',', array_fill(0, count($langs), '%s'));
   $values = array_merge($args, $langs, array($limit, $offset));

   return $this->wpdb->prepare(
    $raw_sql . " AND lang IN ($in) ORDER BY id LIMIT %d OFFSET %d",
    $values
   );
  }
 }

 protected function get_tokens($category_id, $lang) {

  $SUBPAGE_LENGTH = 100;



  if (is_null($category_id)) {
   $sql = $this->get_forged_query(
    "SELECT token
    FROM {$this->push_tokens}
    WHERE os = %s AND active = %d AND token NOT LIKE 'tokenless_%%'",
    array($this->os, true), $SUBPAGE_LENGTH, $this->offset, $lang
   );
  }
  else {
   $sql = $this->get_forged_query(
    "SELECT token
    FROM {$this->push_tokens}
    WHERE os = %s AND active = %d AND token NOT LIKE 'tokenless_%%' AND (user_id NOT IN (SELECT user_id FROM {$this->push_excluded_categories} WHERE category_id = %d))",
    array($this->os, true, $category_id), $SUBPAGE_LENGTH, $this->offset, $lang
   );
  }

  $res = $this->wpdb->get_col($sql);

  $this->offset += count($res);

  return $res;
 }

 protected function set_sent($post_id, $token) {
  if ($post_id == 0)
   return;

  $this->wpdb->query(
   $this->wpdb->prepare("INSERT INTO {$this->push_sent} (`post_id`, `user_id`, `timestamp`) VALUES (%d, %d, %s) ON DUPLICATE KEY UPDATE timestamp = timestamp", $post_id, $this->get_user_id($token), current_time('mysql'))
  );
 }

 protected function get_user_id($token) {
  return $this->wpdb->get_var($this->wpdb->prepare("SELECT user_id FROM {$this->push_tokens} WHERE token=%s AND os=%s", $token, $this->os));
 }

 protected function get_category_id($post) {
  // Select all taxonomies of this post
  $object_taxonomies = get_object_taxonomies($post->post_type);

  // Retrieve all enabled taxonomies
  $enabled_object_taxonomies = get_option('pnfw_enabled_object_taxonomies', array());

  // Intersect this post categories with enabled taxonomies to get the one with "category" meaning
  $object_categories = array_intersect($object_taxonomies, $enabled_object_taxonomies);

  if (empty($object_categories))
   return null;

  // Retrieve categories for this post
  $categories = get_the_terms($post->ID, reset($object_categories));

  if (empty($categories))
   return null;

  return $categories[0]->term_id;
 }

 public function get_offset() {
  return $this->offset;
 }
}
