<?php

if (!defined('ABSPATH')) {
 exit; // Exit if accessed directly
}

require_once dirname(__FILE__ ) . '/class-pnfw-notifications-web.php';

use Minishlink\WebPush\WebPush;

class PNFW_Notifications_Firefox extends PNFW_Notifications_Web {

 public function __construct() {
  parent::__construct(
   'Firefox',
   'https://updates.push.services.mozilla.com/wpush/v1/',
   new WebPush()
  );
 }

}
