<?php

if (!defined('ABSPATH')) {
 exit; // Exit if accessed directly
}

require_once(dirname(__FILE__) . '/../../libs/web-push/vendor/autoload.php');
require_once dirname(__FILE__ ) . '/class-pnfw-notifications.php';

class PNFW_Notifications_Web extends PNFW_Notifications {

 protected $push_encryption_keys;
 protected $endpoint;
 protected $webPush;

 public function __construct($os, $endpoint, $webpush) {
  parent::__construct($os);

  $this->push_encryption_keys = $this->wpdb->get_blog_prefix() . 'push_encryption_keys';
  $this->endpoint = $endpoint;
  $this->webPush = $webpush;
  $this->webPush->setAutomaticPadding(false);
 }

 protected function raw_send($tokens, $title, $user_info) {
  $pnfw_web_push_payload = (bool)get_option('pnfw_web_push_payload');

  $user_info = array_merge(
   array(
    'title' => wp_specialchars_decode(pnfw_get_blogname(), ENT_QUOTES),
    'message' => $title,
    'icon' => $this->get_icon(get_option('pnfw_web_push_icon_media_id'))
   ),
   $user_info
  );

  $payload = json_encode($user_info);

  $sent = 0;

  try {
   foreach ($tokens as &$token) {
    if ($pnfw_web_push_payload) {
     $encryption_keys = $this->get_encryption_keys($token);

     $this->webPush->sendNotification(
      $this->endpoint . $token,
      $payload,
      $encryption_keys['user_public_key'],
      $encryption_keys['user_auth_token']
     );
    }
    else {
     $this->webPush->sendNotification(
      $this->endpoint . $token
     );
    }
    $ret = $this->webPush->flush();

    if ($ret === true) {
     $this->notification_sent($token);
     $sent++;
    }
    else {
     $status_code = $ret[0]['statusCode'];
     pnfw_log($this->type, sprintf(__('Could not send message (%s)', 'pnfw'), $status_code));
     if ($status_code === 400 || $status_code === 410) {
      $this->delete_token($token);
     }
    }
   }
  }
  catch (\ErrorException $exception) {
   pnfw_log($this->type, $exception->getMessage());
  }
  catch (\RuntimeException $exception) {
   pnfw_log($this->type, $exception->getMessage());
  }

  return $sent;
 }

 private function get_icon($media_id) {
  if (is_null($media_id))
   return null;

  $array = wp_get_attachment_image_src($media_id, 'full');

  return $array[0];
 }

 protected function get_encryption_keys($token) {
  $token_id = $this->get_token_id($token);

  if (isset($token_id)) {
   return $this->wpdb->get_row($this->wpdb->prepare("SELECT user_public_key, user_auth_token FROM {$this->push_encryption_keys} WHERE token_id=%d", $token_id), ARRAY_A);
  }

  return null;
 }

 protected function delete_token($token) {
  $token_id = $this->get_token_id($token);

  $this->wpdb->delete($this->push_encryption_keys, array('token_id' => $token_id));

  parent::delete_token($token);
 }

 private function get_token_id($token) {
  return $this->wpdb->get_var($this->wpdb->prepare("SELECT id FROM {$this->push_tokens} WHERE token=%s AND os=%s", $token, $this->os));
 }

}
