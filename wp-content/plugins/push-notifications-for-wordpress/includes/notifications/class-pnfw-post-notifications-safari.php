<?php

if (!defined('ABSPATH')) {
 exit; // Exit if accessed directly
}

require_once dirname(__FILE__ ) . '/class-pnfw-notifications-safari.php';

class PNFW_Post_Notifications_Safari extends PNFW_Notifications_Safari {

 protected $post_id;

 protected function raw_send($tokens, $title, $post_id) {
  $this->post_id = $post_id;

  $payload = apply_filters('pnfw_notification_payload', array('id' => $post_id), $post_id);

  return parent::raw_send($tokens, $title, $payload);
 }

 protected function notification_sent($token) {
  $this->set_sent($this->post_id, $token);
 }

}
