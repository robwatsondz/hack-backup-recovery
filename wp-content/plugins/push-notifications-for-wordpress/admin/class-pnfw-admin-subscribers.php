<?php

if (!defined('ABSPATH')) {
 exit; // Exit if accessed directly
}

class PNFW_Admin_Subscribers {
 public static function output() { ?>
  <div class="wrap">
   <div id="icon-options-general" class="icon32"></div>
   <h2><?php _e('App Subscribers', 'pnfw'); ?>

    <a href="<?php echo add_query_arg('pnfw_download_subscribers', 'true', admin_url('admin.php?page=pnfw-app-subscribers-identifier')); ?>" class="add-new-h2"><?php _e('Export CSV', 'pnfw'); ?></a>

   </h2>
   <?php
   if (isset($_REQUEST['action']) && 'delete' === $_REQUEST['action']) {
    if (!isset($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'delete' . $_REQUEST['id'])) {
     _e('Are you sure you want to do this?', 'pnfw');
     die;
    }

    pnfw_log(PNFW_ALERT_LOG, sprintf(__("Removed from the App Subscribers page the user with ID %s.", 'pnfw'), $_REQUEST['id']));

    if (is_multisite()) {
     $blog_id = get_current_blog_id();

     if (pnfw_is_exclusive_user_member_of_blog($_REQUEST['id'], $blog_id)) {
      require_once(ABSPATH . 'wp-admin/includes/ms.php');

      // If the user is linked only to this site it will be completely removed
      wpmu_delete_user($_REQUEST['id']);

      pnfw_log(PNFW_SYSTEM_LOG, sprintf(__("Completely deleted the user %s.", 'pnfw'), $_REQUEST['id']));
     }
     else {
      // If the user is also linked to other sites it will be removed only from this
      remove_user_from_blog($_REQUEST['id'], $blog_id);
      pnfw_log(PNFW_SYSTEM_LOG, sprintf(__("Disassociated the user %s from blog %s", 'pnfw'), $_REQUEST['id'], $blog_id));
     }
    }
    else {
     wp_delete_user($_REQUEST['id']);
    }?>

    <div class="updated below-h2" id="message"><p><?php _e('User deleted', 'pnfw'); ?></p></div>

    <?php $_SERVER['REQUEST_URI'] = remove_query_arg(array('action', 'id', '_wpnonce')); // "consumes" the used parameters
   }
   else if (isset($_REQUEST['action']) && 'send_test_notification' === $_REQUEST['action']) {
    if (!isset($_REQUEST['_wpnonce']) || !wp_verify_nonce($_REQUEST['_wpnonce'], 'send_test_notification' . $_REQUEST['id'])) {
     _e('Are you sure you want to do this?', 'pnfw');
     die;
    }

    global $wpdb;
    $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';

    $rows = $wpdb->get_results($wpdb->prepare("SELECT os, token FROM $push_tokens WHERE user_id = %d", $_REQUEST['id']));

    foreach ($rows as $row) {
     $title = __('This is a test notification', 'pnfw');
     $count = 0;

     if ('iOS' == $row->os) {
      require_once dirname(__FILE__ ) . '/../includes/notifications/class-pnfw-notifications-ios.php';

      $sender = new PNFW_Notifications_iOS();
      $count = $sender->send_title_to_tokens($title, array($row->token));
     }
     else if ('Android' == $row->os) {
      require_once dirname(__FILE__ ) . '/../includes/notifications/class-pnfw-notifications-android.php';

      $sender = new PNFW_Notifications_Android();
      $count = $sender->send_title_to_tokens($title, array($row->token));
     }

     else if ('Safari' == $row->os) {
      require_once dirname(__FILE__ ) . '/../includes/notifications/class-pnfw-notifications-safari.php';

      $sender = new PNFW_Notifications_Safari();
      $count = $sender->send_title_to_tokens($title, array($row->token));
     }
     else if ('Chrome' == $row->os) {
      require_once dirname(__FILE__ ) . '/../includes/notifications/class-pnfw-notifications-chrome.php';

      $sender = new PNFW_Notifications_Chrome();
      $count = $sender->send_title_to_tokens($title, array($row->token));
     }
     else if ('Firefox' == $row->os) {
      require_once dirname(__FILE__ ) . '/../includes/notifications/class-pnfw-notifications-firefox.php';

      $sender = new PNFW_Notifications_Firefox();
      $count = $sender->send_title_to_tokens($title, array($row->token));
     }

     if ($count > 0) {
      ?> <div class="updated below-h2" id="message"><p><?php echo sprintf(__('Notification sent to %s device', 'pnfw'), $row->os); ?></p></div> <?php
     }
     else {
      $url = admin_url('admin.php?page=pnfw-debug-identifier');

      ?> <div class="error below-h2" id="message"><p><?php echo sprintf(__("There was an error sending the notification. For more information, see the <a href='%s'>Debug</a> page", 'pnfw'), $url); ?></p></div> <?php
     }
    }

    $_SERVER['REQUEST_URI'] = remove_query_arg(array('action', 'id', '_wpnonce')); // "consumes" the used parameters
   }?>


   <form id="pnfw-app-subscribers-table-form" method="post">
   <?php wp_nonce_field('bulk', '_wpbulknonce'); ?>

   <?php $app_subscribers = new App_Subscribers_Table();
   $app_subscribers->prepare_items();
   $app_subscribers->display(); ?>

   </form>

  </div>
 <?php }
}

if (!class_exists( 'WP_List_Table')) {
  require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class App_Subscribers_Table extends WP_List_Table {
 public function __construct() {
  parent::__construct(array(
   'singular' => __('App Subscriber', 'pnfw'),
   'plural' => __('App Subscribers', 'pnfw'),
   'ajax' => false
  ));
 }

 function get_columns() {
  $columns = array(

   'cb' => '<input type="checkbox" />',

   'username' => __('Username', 'pnfw'),
   'email' => __('E-mail', 'pnfw'),

   'user_categories' => __('Categories', 'pnfw'),
   'notifications_sent' => __('Notifications Sent', 'pnfw'),
   'posts_viewed' => __('Posts Viewed', 'pnfw'),

   'devices' => __('Devices', 'pnfw'),
   'excluded_categories' => __('Excluded Categories', 'pnfw'),
  );


  $custom_parameters = apply_filters('pnfw_register_custom_parameters', array());

  foreach ($custom_parameters as $custom_parameter) {
   $parname = $custom_parameter['name'];
   $columns[$parname] = $custom_parameter['description'];
  }


  return $columns;
 }

 function prepare_items() {
  $columns = $this->get_columns();
  $hidden = array();
  $sortable = $this->get_sortable_columns();

  $this->_column_headers = array($columns, $hidden, $sortable);


  $this->process_bulk_action();


  $per_page = 40;

  $paged = $this->get_pagenum();

  $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'id';
  $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'desc';

  $args = array(
   'number' => $per_page,
   'offset' => ($paged - 1) * $per_page,
   'role' => PNFW_Push_Notifications_for_WordPress::USER_ROLE,
   'fields' => 'all_with_meta',
   'order' => $order,
   'orderby' => $orderby
  );

  $user_query = new WP_User_Query($args);

  $this->items = $user_query->get_results();

  $this->set_pagination_args(array(
   'total_items' => $user_query->get_total(),
   'per_page' => $per_page,
   'total_pages' => ceil($user_query->get_total() / $per_page)
  ));
 }

 function column_default($item, $column_name) {
  switch ($column_name) {
   case 'username':
    return $item->display_name;

   case 'email':
    return $item->user_email;


   case 'user_categories':
    $user_groups = wp_get_object_terms($item->ID, 'user_cat', array('fields' => 'names'));

    return implode(', ', $user_groups);

   case 'notifications_sent':
    global $wpdb;
    $push_sent = $wpdb->get_blog_prefix() . 'push_sent';

    $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $push_sent WHERE user_id=%s", $item->ID));

    return $count;

   case 'posts_viewed':
    global $wpdb;
    $push_viewed = $wpdb->get_blog_prefix() . 'push_viewed';

    $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $push_viewed WHERE user_id=%s", $item->ID));

    return $count;


   case 'devices':
    global $wpdb;
    $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';

    $devices = $wpdb->get_col($wpdb->prepare("SELECT os FROM $push_tokens WHERE user_id=%s", $item->ID));

    return implode(', ', $devices);

   case 'excluded_categories':
    $object_taxonomies = get_option('pnfw_enabled_object_taxonomies', array());

    if (empty($object_taxonomies)) {
     return '';
       }

    $terms = get_terms($object_taxonomies, array('hide_empty' => false));
    $excluded_categories = array();

    foreach ($terms as $term) {
     $is_category_excluded = $this->is_category_excluded($item->ID, pnfw_get_normalized_term_id((int)$term->term_id));

     if ($is_category_excluded) {
      $excluded_categories[] = $term->name;
     }
    }

    return implode(", ", $excluded_categories);

   default: // custom parameters

    return get_user_meta($item->ID, $column_name, true);



  }
 }


 function column_cb($item) {
  return sprintf('<input type="checkbox" name="subscribers[]" value="%d" />', $item->ID);
 }



 public function process_bulk_action() {
        if (isset($_POST['_wpbulknonce']) && !empty($_POST['_wpbulknonce'])) {
            if (!wp_verify_nonce($_POST['_wpbulknonce'], 'bulk')) {
    _e('Are you sure you want to do this?', 'pnfw');
    die;
   }
        }

        $action = $this->current_action();

  if ($action === 'bulk-remove-categories') {
   $user_ids = $_REQUEST['subscribers'];

   foreach ($user_ids as $user_id) {
    $this->set_category($user_id, NULL);
   }
  }
  else if (pnfw_starts_with($action, 'bulk-assign-to-')) {
   $user_category_id = (int)str_replace('bulk-assign-to-', '', $action);

   $user_ids = $_REQUEST['subscribers'];

   foreach ($user_ids as $user_id) {
    $this->set_category($user_id, $user_category_id);
   }
        }

        return;
    }


 public function get_sortable_columns() {
  $sortable_columns = array(
   'username' => array('login', false),
   'email' => array('email', false),
  );

  return $sortable_columns;
 }

 function column_username($item) {
  $paged = isset($_REQUEST['paged']) ? (int)$_REQUEST['paged'] : 1;

  $actions = array(
   'delete' => sprintf('<a href="?page=%s&paged=%d&action=%s&id=%s&_wpnonce=%s">%s</a>', $_REQUEST['page'], $paged, 'delete', $item->ID, wp_create_nonce('delete' . $item->ID), __('Delete', 'pnfw')),
  );

  global $wpdb;
  $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';
  $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $push_tokens WHERE user_id=%d AND token NOT LIKE 'tokenless_%%'", $item->ID));

  if ($count > 0) {
   $actions['send_test_notification'] = sprintf('<a href="?page=%s&paged=%d&action=%s&id=%s&_wpnonce=%s">%s</a>', $_REQUEST['page'], $paged, 'send_test_notification', $item->ID, wp_create_nonce('send_test_notification' . $item->ID), __('Send test notification', 'pnfw'));
  }

  return sprintf('%1$s %2$s', $item->display_name, $this->row_actions($actions));
 }


 public function get_bulk_actions() {
  $raw_terms = get_terms('user_cat', array('hide_empty' => false));

  $res = array();

  $res['bulk-remove-categories'] = __('Remove categories', 'pnfw');

  foreach ($raw_terms as $raw_term) {
   $res['bulk-assign-to-' . $raw_term->term_id] = sprintf(__('Assign to %s', 'pnfw'), $raw_term->name);
  }

  return $res;
 }


 public function no_items() {
  _e('No app subscribers were found.', 'pnfw');
 }

 private function is_category_excluded($user_id, $category_id) {
  global $wpdb;
  $push_excluded_categories = $wpdb->get_blog_prefix().'push_excluded_categories';
  return (boolean)$wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $push_excluded_categories WHERE category_id=%d AND user_id=%d", $category_id, $user_id));
 }


 private function set_category($user_id, $category_id) {
  global $sitepress;

  if (isset($sitepress)) { // Removes a WPML warning
   remove_action('deleted_term_relationships', array($sitepress, 'deleted_term_relationships'));
  }

  wp_set_object_terms($user_id, $category_id, 'user_cat', false);
  clean_object_term_cache($user_id, 'user_cat');

  if (isset($sitepress)) {
   add_action('deleted_term_relationships', array($sitepress, 'deleted_term_relationships'), 10, 2);
  }
 }

}
