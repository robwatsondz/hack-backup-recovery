<?php

if (!defined('ABSPATH')) {
 exit; // Exit if accessed directly
}

class PNFW_Admin_Stats {
 public static function output() { ?>
  <div class="wrap">
   <div id="icon-options-general" class="icon32"></div>
   <h2><?php _e('Stats', 'pnfw'); ?></h2>

   <h3><?php _e('Overview', 'pnfw'); ?></h3>

   <div id="overview-chart-div">
    <canvas id="overview-chart"></canvas>
   </div>

   <h3><?php _e('Read by OS', 'pnfw'); ?></h3>

   <div id="read-by-os-chart-div">
    <canvas id="read-by-os-chart"></canvas>
   </div>

   <h3><?php _e('New subscribers per day', 'pnfw'); ?></h3>

   <div id="subscribers-per-day-chart-div">
    <canvas id="subscribers-per-day-chart"></canvas>
   </div>

   <h3><?php _e('Subscribers per category', 'pnfw'); ?></h3>

   <div id="categories-chart-div">
    <canvas id="categories-chart" width="500" height="500"></canvas>
   </div>

   <h3><?php _e('Devices', 'pnfw'); ?></h3>

   <div id="devices-chart-div">
    <canvas id="devices-chart" width="400" height="400"></canvas>
   </div>

   <h3><?php _e('App subscribers', 'pnfw'); ?></h3>

   <div id="users-chart-div">
    <canvas id="users-chart" width="400" height="400"></canvas>
   </div>

   <h3><?php _e('Tokens', 'pnfw'); ?></h3>

   <div id="tokens-chart-div">
    <canvas id="tokens-chart" width="400" height="400"></canvas>
   </div>
  </div>
 <?php }
}
