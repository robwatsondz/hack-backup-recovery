<?php

if (!defined('ABSPATH')) {
 exit; // Exit if accessed directly
}

$admin_dashboard = new PNFW_Admin();

final class PNFW_Admin {
  public function __construct() {
   add_action('admin_init', array($this, 'admin_init'));
    add_action('admin_menu', array($this, 'menus'));

    add_action('admin_enqueue_scripts', array($this, 'load_admin_meta_box_script'));

  $plugin_filename = plugin_basename(__FILE__);
  add_filter("plugin_action_links_$plugin_filename", array($this, 'settings_link'));

  add_action('admin_head', array($this, 'admin_header'));
  add_filter('upload_mimes', array($this, 'allow_pem_and_p12'));
  add_action('add_meta_boxes', array($this, 'adding_meta_box'), 10, 2);
  add_action('save_post', array($this, 'save_postdata'), 1);
  add_action('admin_notices', array($this, 'ios_certificate_admin_notices'));

  add_action('admin_init', array($this, 'export_subscribers'));
  add_action('admin_init', array($this, 'export_logs'));
  add_action('admin_init', array($this, 'export_tokens'));

  // Send in progress
  add_action('wp_ajax_send_in_progress', array($this, 'send_in_progress'));
  add_action('admin_notices', array($this, 'progress_bar_admin_notices'));

  // User Category stuff
  add_action('admin_menu', array($this, 'add_user_category_admin_page'));
  add_action('manage_user_cat_custom_column', array(&$this,'manage_user_cat_column'), 10, 3);
  add_filter('manage_edit-user_cat_columns', array(&$this,'manage_user_cat_user_column'));
  add_action('show_user_profile', array(&$this, 'edit_user_cat_section'));
  add_action('edit_user_profile', array(&$this, 'edit_user_cat_section'));
  add_action('personal_options_update', array(&$this, 'save_user_cat_terms'));
  add_action('edit_user_profile_update', array(&$this, 'save_user_cat_terms'));
  add_filter('parent_file', array(&$this, 'fix_user_tax_page'));
  add_filter('sanitize_user', array(&$this, 'disable_username'));

 }

 function admin_header() {
  echo '<style type="text/css">';

  // Common
  echo '.textfield { width: 100%; }';

  // App Subscribers page
  echo '.wp-list-table .column-username { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';
  echo '.wp-list-table .column-email { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';
  echo '.wp-list-table .column-user_categories { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';
  echo '.wp-list-table .column-devices { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';
  echo '.wp-list-table .column-excluded_categories { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';

  // Tokens page
  echo '.wp-list-table .column-id { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';
  echo '.wp-list-table .column-token  { max-width: 200px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';
  echo '.wp-list-table .column-user_id { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';
  echo '.wp-list-table .column-timestamp { overflow: hidden; text-overflow: ellipsis; }';
  echo '.wp-list-table .column-os { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';
  echo '.wp-list-table .column-lang { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';
  echo '.wp-list-table .column-status { overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';

  // Debug page
  echo '.wp-list-table .column-type { width: 9%; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; }';
  echo '.wp-list-table .column-timestamp { width: 16%; overflow: hidden; text-overflow: ellipsis; }';
  echo '.wp-list-table .column-text { overflow: hidden; text-overflow: ellipsis; }';

  echo '.log-type-' . PNFW_SYSTEM_LOG . ' { width: 20px; height: 20px; border-radius: 50%; background-color: #cccccc; }';
  echo '.log-type-' . PNFW_IOS_LOG . ' { width: 20px; height: 20px; border-radius: 50%; background-color: #3980d5; }';
  echo '.log-type-' . PNFW_ANDROID_LOG . ' { width: 20px; height: 20px; border-radius: 50%; background-color: #99cc00; }';
  echo '.log-type-' . PNFW_FEEDBACK_PROVIDER_LOG . ' { width: 20px; height: 20px; border-radius: 50%; background-color: #3980d5; }';
  echo '.log-type-' . PNFW_ALERT_LOG . ' { width: 20px; height: 20px; border-radius: 50%; background-color: #f27d7d; }';
  echo '.log-type-' . PNFW_SAFARI_LOG . ' { width: 20px; height: 20px; border-radius: 50%; background-color: #1fbbd7; }';
  echo '.log-type-' . PNFW_CHROME_LOG . ' { width: 20px; height: 20px; border-radius: 50%; background-color: #ffcd41; }';
  echo '.log-type-' . PNFW_FIREFOX_LOG . ' { width: 20px; height: 20px; border-radius: 50%; background-color: #ca4128; }';
  echo '.log-type-' . PNFW_OTHER_LOG . ' { width: 20px; height: 20px; border-radius: 50%; background-color: #be2ad5; }';
  echo '#user-categories { display:none; }'; //X2CMS

  echo '</style>';
 }

 function admin_init() {
  $custom_post_types = get_post_types(array('public' => 1));
  $custom_post_types = array_diff($custom_post_types, array('page', 'attachment'));
  $taxonomies = get_object_taxonomies($custom_post_types);
  foreach ($taxonomies as $taxonomy) {
   add_action("delete_{$taxonomy}", array($this, 'delete_term'));
  }
 }

 function menus() {
  $admin_capability = 'activate_plugins';
  $editor_capability = 'publish_pages';

  $menu_slug = 'push-notifications-for-wordpress';

  $page_hook_suffix = add_menu_page(
   __('Push Notifications', 'pnfw'),
   __('Push Notifications', 'pnfw'),
   $editor_capability,
   $menu_slug,
   array($this, 'stats_page'),
   plugin_dir_url(__FILE__) . '../assets/imgs/icon-menu.png',
   200);


  // Use the hook suffix to compose the hook and register an action executed when plugin's options page is loaded
  add_action('load-' . $page_hook_suffix , array($this, 'chart_add_scripts'));
  add_action('load-' . $page_hook_suffix , array($this, 'progressbar_add_scripts'));


  $page_hook_suffix = add_submenu_page(
   $menu_slug,
   __('Settings', 'pnfw'),
   __('Settings', 'pnfw'),
   $admin_capability,
   'pnfw-settings-identifier',
   array($this, 'settings_page'));

  add_action('admin_print_scripts-' . $page_hook_suffix, array($this, 'plugin_admin_scripts'));


  add_action('load-' . $page_hook_suffix , array($this, 'progressbar_add_scripts'));


  $page_hook_suffix = add_submenu_page(
   $menu_slug,
   __('OAuth', 'pnfw'),
   __('OAuth', 'pnfw'),
   $admin_capability,
   'pnfw-oauth-identifier',
   array($this, 'oauth_page'));


  add_action('load-' . $page_hook_suffix , array($this, 'progressbar_add_scripts'));


  $page_hook_suffix = add_submenu_page(
   $menu_slug,
   __('App Subscribers', 'pnfw'),
   __('App Subscribers', 'pnfw'),
   $editor_capability,
   'pnfw-app-subscribers-identifier',
   array($this, 'app_subscribers_page'));


  add_action('load-' . $page_hook_suffix , array($this, 'progressbar_add_scripts'));


  $page_hook_suffix = add_submenu_page(
   $menu_slug,
   __('Tokens', 'pnfw'),
   __('Tokens', 'pnfw'),
   $editor_capability,
   'pnfw-tokens-identifier',
   array($this, 'tokens_page'));


  add_action('load-' . $page_hook_suffix , array($this, 'progressbar_add_scripts'));


  $page_hook_suffix = add_submenu_page(
   $menu_slug,
   __('Debug', 'pnfw'),
   __('Debug', 'pnfw'),
   $admin_capability,
   'pnfw-debug-identifier',
   array($this, 'debug_page'));


  add_action('load-' . $page_hook_suffix , array($this, 'progressbar_add_scripts'));

 }

 function stats_page() {
  require_once dirname(__FILE__ ) . '/class-pnfw-admin-stats.php';

  PNFW_Admin_Stats::output();
 }

 function settings_page() {
  require_once dirname(__FILE__ ) . '/class-pnfw-admin-settings.php';

  PNFW_Admin_Settings::output();
 }

 function oauth_page() {
  require_once dirname(__FILE__ ) . '/class-pnfw-admin-oauth.php';

  PNFW_Admin_OAuth::output();
 }

 function app_subscribers_page() {
  require_once dirname(__FILE__ ) . '/class-pnfw-admin-subscribers.php';

  PNFW_Admin_Subscribers::output();
 }

 function tokens_page() {
  require_once dirname(__FILE__ ) . '/class-pnfw-admin-tokens.php';

  PNFW_Admin_Tokens::output();
 }

 function debug_page() {
  require_once dirname(__FILE__ ) . '/class-pnfw-admin-debug.php';

  PNFW_Admin_Debug::output();
 }


 function chart_add_scripts() {
  wp_register_script(
   'Chart',
   plugin_dir_url(__FILE__) . '../libs/Chart/Chart.min.js',
   false,
   null,
   true
  );

  wp_register_script(
   'adminCharts',
   plugin_dir_url(__FILE__) . '../assets/js/admin_charts.js',
   array('Chart', 'jquery'),
   '1.0',
   true
  );
     wp_enqueue_script('adminCharts');

  wp_localize_script('adminCharts', 'data_overview', $this->data_for_overview_graph());
  wp_localize_script('adminCharts', 'data_read_by_os', $this->data_for_read_by_os_graph());
  wp_localize_script('adminCharts', 'data_subscribers_per_day', $this->data_for_subscribers_per_day_graph());
  wp_localize_script('adminCharts', 'data_categories', $this->data_for_categories_graph());
  wp_localize_script('adminCharts', 'data_devices', $this->data_for_devices_graph());
  wp_localize_script('adminCharts', 'data_users', $this->data_for_users_graph());
  wp_localize_script('adminCharts', 'data_tokens', $this->data_for_tokenless_graph());
  wp_localize_script('adminCharts', 'strings', array(
   'sent' => __('Users to which the notifications were sent', 'pnfw'),
   'read' => __('Users who read the notifications', 'pnfw'),
   'new_subscribers' => __('New subscribers', 'pnfw'),
   'anonymous' => __('Anonymous app subscribers', 'pnfw'),
   'registered' => __('Registered app subscribers', 'pnfw'),
   'unsubscribed' => __('Unsubscribed', 'pnfw'),
   'err_no_data_available' => __('No data available in this chart', 'pnfw'),
   'with_active_notifications' => __('With active notifications', 'pnfw'),
   'with_notifications_disabled' => __('With notifications disabled', 'pnfw')
  ));
 }

 function progressbar_add_scripts() {
  wp_register_style('pnfw-progressbar-css', plugins_url('../assets/css/progressbar.css', __FILE__));

  wp_enqueue_style('pnfw-progressbar-css');

  wp_register_script(
   'adminProgress',
   plugin_dir_url(__FILE__) . '../assets/js/admin_progress.js',
   array('jquery', 'jquery-ui-progressbar'),
   '1.0',
   true
  );

     wp_enqueue_script('adminProgress');

  wp_localize_script('adminProgress', 'param', array('admin_url' => admin_url('admin-ajax.php')));
 }


 function plugin_admin_scripts() {
  wp_enqueue_media();
  wp_enqueue_script('admin_settings', plugin_dir_url(__FILE__) . '../assets/js/admin_settings.js', array('jquery'));
  wp_localize_script('admin_settings', 'data', array(
   'uploader_title' => __('Upload', 'pnfw'),
   'uploader_button_text' => __('Select', 'pnfw')
  ));
 }

 function load_admin_meta_box_script() {
  global $pagenow;

  if (is_admin() && ($pagenow == 'post-new.php' || $pagenow == 'post.php')) {
   wp_register_script(
    'admin_meta_box',
    plugin_dir_url(__FILE__) . '../assets/js/admin_meta_box.js',
    array('jquery'),
    null,
    false);

   wp_enqueue_script('admin_meta_box');

   wp_localize_script('admin_meta_box',
    'strings',
    array(
     'str1' => __('Send and make visible only to', 'pnfw') . ':',
     'str2' => __('Make visible only to', 'pnfw') . ':'
    )
   );
  }
 }

 /**
	  * Add file extensions 'PEM' and 'P12' to the list of acceptable file extensions WordPress
	  * checks during media uploads
	  */
 function allow_pem_and_p12($mimes) {
  $mimes['pem'] = 'application/x-pem-file';
  $mimes['p12'] = 'application/x-pkcs12';
  return $mimes;
 }

 /**
	  * Add a meta box to the new post/new custom post type edit screens
	  */
 function adding_meta_box($post_type, $post) {
  $enabled_post_types = get_option('pnfw_enabled_post_types', array());

  if (empty($enabled_post_types) || !in_array($post_type, $enabled_post_types)) {
   return false;
  }

  add_meta_box(
   'pnfw-meta-box',
   __('Push Notifications', 'pnfw'),
   array($this, 'render_meta_box'),
   $post_type,
   'side',
   'high'
  );
 }

 /**
	  * Print the meta box content
	  */
 function render_meta_box($post) {
  wp_nonce_field('pnfw_meta_box', 'pnfw_meta_box_nonce');

  $value = get_post_meta($post->ID, 'pnfw_do_not_send_push_notifications_for_this_post', true);
  // X2CMS force default value when displaying
  $value = 1;

  ?>
  <label><input type="checkbox"<?php echo (!empty($value) ? ' checked="checked"' : null) ?> value="1" name="pnfw_do_not_send_push_notifications_for_this_post" id="pnfw-do-not-send-push-notifications-for-this-post" /> <?php echo sprintf(__('Do not send for this %s', 'pnfw'), strtolower(get_post_type_object($post->post_type)->labels->singular_name)); ?></label>

  <?php
  $user_cat = get_post_meta($post->ID, 'pnfw_user_cat', true); ?>

  <div id='user-categories'>
   <ul>
    <li><strong id='send-and-make-visible-only-to-box'><?php _e('Send and make visible only to', 'pnfw'); ?>:</strong></li>

    <li>
     <input type="radio" name="user_cat" id="user_cat-all" value="all" <?php checked($user_cat, ''); ?> />
     <label for="user_cat-all"><?php _e('All', 'pnfw'); ?></label>
    </li>

    <li>
     <input type="radio" name="user_cat" id="user_cat-anonymous-users" value="anonymous-users" <?php checked($user_cat, 'anonymous-users'); ?> />
     <label for="user_cat-anonymous-users"><?php _e('Anonymous users', 'pnfw'); ?></label>
    </li>

    <li>
     <input type="radio" name="user_cat" id="user_cat-registered-users" value="registered-users" <?php checked($user_cat, 'registered-users'); ?> />
     <label for="user_cat-registered-users"><?php _e('Registered users', 'pnfw'); ?></label>
    </li>

    <?php
    $terms = get_terms('user_cat', array('hide_empty' => false));

    foreach ($terms as $term) { ?>
     <li>
      <input type="radio" name="user_cat" id="user_cat-<?php echo esc_attr($term->slug); ?>" value="<?php echo esc_attr($term->slug); ?>" <?php checked($user_cat, $term->slug); ?> />
      <label for="user_cat-<?php echo esc_attr($term->slug); ?>"><?php echo $term->name; ?>
       <?php
       global $wpdb;

       $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $wpdb->term_relationships WHERE term_taxonomy_id = %d", $term->term_taxonomy_id)); ?>

       (<?php echo $count; ?>)
      </label>
     </li>
    <?php } ?>

   </ul>
  </div> <!-- user-categories -->
 <?php }

 /**
	  * When the post/custom post type is saved, saves our custom data
	  */
 function save_postdata($postid) {
  // Check if our nonce is set.
  if (!isset( $_POST['pnfw_meta_box_nonce']))
   return $postid;

  $nonce = $_POST['pnfw_meta_box_nonce'];

  if (!wp_verify_nonce($nonce, 'pnfw_meta_box'))
   return $postid;

  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) // stop it from being called during auto drafts
   return false;

  if (!current_user_can('edit_post', $postid))
   return false;

  $enabled_post_types = get_option('pnfw_enabled_post_types', array());

  if (empty($postid) || isset($_POST['post_type']) && !in_array($_POST['post_type'], $enabled_post_types))
   return false;

  update_option('pnfw_last_save_timestamp', time());

  if (isset($_POST['pnfw_do_not_send_push_notifications_for_this_post'])) {
   add_post_meta($postid, 'pnfw_do_not_send_push_notifications_for_this_post', true, true);
  }
  else {
   delete_post_meta($postid, 'pnfw_do_not_send_push_notifications_for_this_post');
  }

  if (isset($_POST['user_cat'])) {
   $cat = $_POST['user_cat'];

   delete_post_meta($postid, 'pnfw_user_cat');

   if ($cat != 'all') {
    add_post_meta($postid, 'pnfw_user_cat', $cat, true);
   }
  }
 }

 function ios_certificate_admin_notices() {
  if (!current_user_can('activate_plugins'))
   return;

  $pnfw_ios_push_notifications = (bool)get_option('pnfw_ios_push_notifications');

  if (!$pnfw_ios_push_notifications)
   return;

  $pnfw_ios_use_sandbox = (bool)get_option('pnfw_ios_use_sandbox');

  $media_id = get_option($pnfw_ios_use_sandbox ? 'pnfw_sandbox_ssl_certificate_media_id' : 'pnfw_production_ssl_certificate_media_id');

  $expires = pnfw_certificate_expiration($media_id);

  if (is_null($expires))
   return;

  $gmt_offset = get_option('gmt_offset');
  $date_format = get_option('date_format');
  $time_format = get_option('time_format');
  $tz_format = sprintf('%s %s', $date_format, $time_format);

  $human_readable_date = date_i18n($tz_format, $expires);

  if ($expires < time()) {
   $class = "error";
   $message = sprintf(__('The iOS push notifications certificate is expired on %s. Renew it now and upload it <a href="%s">here</a>.', 'pnfw'), $human_readable_date, admin_url('admin.php?page=pnfw-settings-identifier&tab=ios_tab'));
   echo"<div class=\"$class\"> <p>$message</p></div>";
  }
  else if ($expires - WEEK_IN_SECONDS < time()) {
   $class = "update-nag";
   $message = sprintf(__('The iOS push notifications certificate is about to expire on %s. Renew it soon and upload it <a href="%s">here</a>.', 'pnfw'), $human_readable_date, admin_url('admin.php?page=pnfw-settings-identifier&tab=ios_tab'));
   echo"<div class=\"$class\"> <p>$message</p></div>";
  }
 }

 /**
	  * Place a link to the Settings page right from the WordPress Installed Plugins page
	  */
 function settings_link($links) {
  $url = admin_url('admin.php?page=pnfw-settings-identifier');
  $settings_link = "<a href='$url'>" . __('Settings', 'pnfw') . '</a>';
  array_unshift($links, $settings_link);

  return $links;
 }

 function delete_term($term_id) {
  pnfw_log(PNFW_SYSTEM_LOG, sprintf(__('Automatically deleted excluded category %d.', 'pnfw'), $term_id));

  global $wpdb;
  $wpdb->delete($wpdb->get_blog_prefix().'push_excluded_categories', array('category_id' => $term_id));
 }


 private function data_for_overview_graph() {
  global $wpdb;

  $wp_posts = array();
  if (get_option('pnfw_enabled_post_types')) {
   $wp_posts = get_posts(
    array(
     'posts_per_page' => 50,
     'order' => 'DESC',
     'post_type' => get_option('pnfw_enabled_post_types')
    )
   );
  }

  $posts = array();
  $sent = array();
  $read = array();

  $push_sent = $wpdb->get_blog_prefix() . 'push_sent';
  $push_viewed = $wpdb->get_blog_prefix() . 'push_viewed';

  foreach ($wp_posts as $post) {
   $short_title = strlen($post->post_title) > 30 ? substr($post->post_title, 0, 30). '...' : $post->post_title;

   array_push($posts, $short_title);
   array_push($sent, (int)$wpdb->get_var("SELECT COUNT(*) FROM $push_sent WHERE post_ID = {$post->ID}"));
   array_push($read, (int)$wpdb->get_var("SELECT COUNT(*) FROM $push_viewed WHERE post_ID = {$post->ID}"));
  }

  $res = array(
   'data_type' => 'overview',
   'post_data' => array(
    'posts' => array_reverse($posts),
    'sent' => array_reverse($sent),
    'read' => array_reverse($read)
   )
  );

  return $res;
 }

 private function data_for_read_by_os_graph() {
  global $wpdb;

  $wp_posts = array();
  if (get_option('pnfw_enabled_post_types')) {
   $wp_posts = get_posts(
    array(
     'posts_per_page' => 3,
     'order' => 'DESC',
     'post_type' => get_option('pnfw_enabled_post_types')
    )
   );
  }

  $posts = array();
  $ios = array();
  $android = array();
  $safari = array();
  $chrome = array();
  $firefox = array();
  $unsubscribed = array();

  $push_sent = $wpdb->get_blog_prefix() . 'push_sent';
  $push_viewed = $wpdb->get_blog_prefix() . 'push_viewed';
  $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';

  foreach ($wp_posts as $post) {
   $short_title = strlen($post->post_title) > 30 ? substr($post->post_title, 0, 30). '...' : $post->post_title;

   array_push($posts, $short_title);

   $results = $wpdb->get_results("SELECT COALESCE(os, 'Unsubscribed') as os, COUNT(COALESCE(os, 'Unsubscribed')) AS count
    FROM $push_viewed AS r
    LEFT JOIN $push_tokens AS d ON r.user_id = d.user_id
    WHERE r.post_ID = {$post->ID}
    GROUP BY os
    ORDER BY count DESC",
    ARRAY_A
   );

   $ios_count = 0;
   $android_count = 0;
   $safari_count = 0;
   $chrome_count = 0;
   $firefox_count = 0;
   $unsubscribed_count = 0;

   foreach ($results as $row) {
    switch ($row['os']) {
     case 'iOS':
      $ios_count = (int)$row['count'];
      break;
     case 'Android':
      $android_count = (int)$row['count'];
      break;
     case 'Safari':
      $safari_count = (int)$row['count'];
      break;
     case 'Chrome':
      $chrome_count = (int)$row['count'];
      break;
     case 'Firefox':
      $firefox_count = (int)$row['count'];
      break;
     default:
      $unsubscribed_count = (int)$row['count'];
    }
   }

   array_push($ios, $ios_count);
   array_push($android, $android_count);
   array_push($safari, $safari_count);
   array_push($chrome, $chrome_count);
   array_push($firefox, $firefox_count);
   array_push($unsubscribed, $unsubscribed_count);
  }

  $res = array(
   'data_type' => 'read_by_os',
   'post_data' => array(
    'posts' => array_reverse($posts),
    'ios' => array_reverse($ios),
    'android' => array_reverse($android),
    'safari' => array_reverse($safari),
    'chrome' => array_reverse($chrome),
    'firefox' => array_reverse($firefox),
    'unsubscribed' => array_reverse($unsubscribed)
   )
  );

  return $res;
 }

 private function data_for_subscribers_per_day_graph() {
  global $wpdb;

  $NUM_OF_DAYS_BACK = 30;

  // temporary table needed to pad empty dates
  $tmp_table = $wpdb->get_blog_prefix() . 'push_tmp_dates';
  $wpdb->query("CREATE TEMPORARY TABLE $tmp_table (days DATE, PRIMARY KEY(days))");

  $all_days = array();
  for ($i = 0; $i < $NUM_OF_DAYS_BACK; ++$i) {
   $timestamp = time();
   $tm = DAY_IN_SECONDS * $i;
   $tm = $timestamp - $tm;

   $the_date = date('Y-m-d', $tm);

   $all_days[] = $the_date;
     }

  foreach ($all_days as $day) {
   $wpdb->insert($tmp_table, array('days' => $day));
  }

  $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';
  $rows = $wpdb->get_results($wpdb->prepare("SELECT r.days as days, COUNT(DATE(d.timestamp)) AS count
   FROM $tmp_table AS r
   LEFT JOIN $push_tokens AS d ON r.days = DATE(d.timestamp)
   GROUP BY DATE(r.days)
   ORDER BY r.days ASC
   LIMIT %d", $NUM_OF_DAYS_BACK), OBJECT_K);

  $wpdb->query("DROP TABLE $tmp_table");

  $days = array_keys($rows);
  $count = array();

  foreach ($days as $day) {
   $count[] = $rows[$day]->count;
  }

  $res = array(
   'data_type' => 'subscribers',
   'post_data' => array(
    'days' => $days,
    'count' => $count
   )
  );

  return $res;
 }

 private function data_for_categories_graph() {
  global $wpdb;

  $object_taxonomies = get_option('pnfw_enabled_object_taxonomies', array());

  if (empty($object_taxonomies)) {
   return array(
    'data_type' => 'categories',
    'post_data' => array()
   );
  }

  $terms = get_terms($object_taxonomies, array('hide_empty' => false));
  $categories = array();

  $user_query = new WP_User_Query(array('role' => PNFW_Push_Notifications_for_WordPress::USER_ROLE));
  $subscribers = count($user_query->get_results());

  $values = array();
  $push_excluded_categories = $wpdb->get_blog_prefix() . 'push_excluded_categories';

  foreach ($terms as $term) {
   $excluded_count = (int)$wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $push_excluded_categories WHERE category_id = %s", $term->term_id));

   $values[$term->name] = $subscribers - $excluded_count;
  }

  $res = array(
   'data_type' => 'categories',
   'post_data' => $values
  );

  return $res;
 }

 private function data_for_devices_graph() {
  global $wpdb;
  $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';

  $ios_count = (int)$wpdb->get_var("SELECT COUNT(*) FROM $push_tokens WHERE os = 'iOS'");
  $android_count = (int)$wpdb->get_var("SELECT COUNT(*) FROM $push_tokens WHERE os = 'Android'");
  $safari_count = (int)$wpdb->get_var("SELECT COUNT(*) FROM $push_tokens WHERE os = 'Safari'");
  $chrome_count = (int)$wpdb->get_var("SELECT COUNT(*) FROM $push_tokens WHERE os = 'Chrome'");
  $firefox_count = (int)$wpdb->get_var("SELECT COUNT(*) FROM $push_tokens WHERE os = 'Firefox'");

  $users = array(
   'ios' => $ios_count,
   'android' => $android_count,
   'safari' => $safari_count,
   'chrome' => $chrome_count,
   'firefox' => $firefox_count
  );

  $res = array(
   'data_type' => 'devices_distribution',
   'post_data' => $users
  );

  return $res;
 }

 private function data_for_users_graph() {
  global $wpdb;

  $registered_query = new WP_User_Query(array('role' => PNFW_Push_Notifications_for_WordPress::USER_ROLE, 'count_total' => true));
  $registered_query->query_where = $registered_query->query_where . ' AND user_email != ""';
  $registered_query->query();
  $registered_count = (int)$registered_query->get_total();

  $anonymous_query = new WP_User_Query(array('role' => PNFW_Push_Notifications_for_WordPress::USER_ROLE, 'count_total' => true));
  $anonymous_query->query_where = $anonymous_query->query_where . ' AND user_email = ""';
  $anonymous_query->query();
  $anonymous_count = (int)$anonymous_query->get_total();

  $users = array('registered' => $registered_count, 'anonymous' => $anonymous_count);

  $res = array(
   'data_type' => 'users_distribution',
   'post_data' => $users
  );

  return $res;
 }

 private function data_for_tokenless_graph() {
  global $wpdb;
  $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';

  $count = (int)$wpdb->get_var("SELECT COUNT(*) FROM $push_tokens");
  $tokenless_count = (int)$wpdb->get_var("SELECT COUNT(*) FROM $push_tokens WHERE token LIKE 'tokenless_%%'");

  $tokens = array('withtoken' => ($count - $tokenless_count), 'tokenless' => $tokenless_count);

  $res = array(
   'data_type' => 'tokens_distribution',
   'post_data' => $tokens
  );

  return $res;
 }

 public function export_subscribers() {
  if (empty($_GET['pnfw_download_subscribers'])) {
   return;
  }

  global $wpdb;

  $args = array(
   'role' => PNFW_Push_Notifications_for_WordPress::USER_ROLE,
   'fields' => 'all_with_meta',
   'order' => 'desc',
   'orderby' => 'id'
  );

  $user_query = new WP_User_Query($args);

  $items = $user_query->get_results();

  $separator = apply_filters('pnfw_csv_separator', ',');

  $row = array();
  $row[] = __('Username', 'pnfw');
  $row[] = __('Email', 'pnfw');
  $row[] = __('Categories', 'pnfw');
  $row[] = __('Notifications Sent', 'pnfw');
  $row[] = __('Posts Viewed', 'pnfw');
  $row[] = __('Devices', 'pnfw');
  $row[] = __('Excluded Categories', 'pnfw');

  $custom_parameters = apply_filters('pnfw_register_custom_parameters', array());

  if (!empty($custom_parameters)) {
   foreach ($custom_parameters as $custom_parameter) {
    $row[] = $custom_parameter['name'];
   }
  }

  $rows = array();
  $rows[] = '"' . implode('"' . $separator . '"', $row) . '"';

  if (!empty($items)) {
   $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';
   $push_sent = $wpdb->get_blog_prefix() . 'push_sent';
   $push_viewed = $wpdb->get_blog_prefix() . 'push_viewed';
   $push_excluded_categories = $wpdb->get_blog_prefix() . 'push_excluded_categories';

   foreach ($items as $item) {
    $user_groups = wp_get_object_terms($item->ID, 'user_cat', array('fields' => 'names'));
    $count_sent = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $push_sent WHERE user_id=%s", $item->ID));
    $count_viewed = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM $push_viewed WHERE user_id=%s", $item->ID));
    $devices = $wpdb->get_col($wpdb->prepare("SELECT os FROM $push_tokens WHERE user_id=%s", $item->ID));
    $excluded_categories = $wpdb->get_col($wpdb->prepare("SELECT category_id FROM $push_excluded_categories WHERE user_id=%s", $item->ID));

    $row = array();

    $row[] = $item->display_name;
    $row[] = $item->user_email;
    $row[] = implode(', ', $user_groups);
    $row[] = $count_sent;
    $row[] = $count_viewed;
    $row[] = implode(', ', $devices);
    $row[] = implode(', ', $excluded_categories);

    if (!empty($custom_parameters)) {
     foreach ($custom_parameters as $custom_parameter) {
      $row[] = get_user_meta($item->ID, $custom_parameter['name'], true);
     }
    }

    $rows[] = '"' . implode('"' . $separator . '"', $row) . '"';
   }
  }

  $this->generate_csv($rows, 'subscribers_log.csv');

  exit;
 }

 public function export_tokens() {
  if (empty($_GET['pnfw_download_tokens'])) {
   return;
  }

  global $wpdb;

  $push_tokens = $wpdb->get_blog_prefix() . 'push_tokens';
  $tokens = $wpdb->get_results("SELECT * FROM $push_tokens ORDER BY id DESC;");

  $separator = apply_filters('pnfw_csv_separator', ',');

  $row = array();
  $row[] = __('Token', 'pnfw');
  $row[] = __('Operating System', 'pnfw');
  $row[] = __('Registration timestamp', 'pnfw');
  $row[] = __('Language', 'pnfw');
  $row[] = __('Active', 'pnfw');

  $rows = array();
  $rows[] = '"' . implode('"' . $separator . '"', $row) . '"';

  if (!empty($tokens)) {
   foreach ($tokens as $token) {
    $row = array();

    $row[] = $token->token;
    $row[] = $token->os;
    $row[] = $token->timestamp;
    $row[] = $token->lang;
    $row[] = $token->active;

    $rows[] = '"' . implode('"' . $separator . '"', $row) . '"';
   }
  }

  $this->generate_csv($rows, 'tokens.csv');

  exit;
 }

 public function export_logs() {
  if (empty($_GET['pnfw_download_logs'])) {
   return;
  }

  global $wpdb;

  $push_logs = $wpdb->get_blog_prefix() . 'push_logs';
  $items = $wpdb->get_results("SELECT * FROM $push_logs ORDER BY id DESC;");

  $separator = apply_filters('pnfw_csv_separator', ',');

  $row = array();
  $row[] = __('Timestamp', 'pnfw');
  $row[] = __('Type', 'pnfw');
  $row[] = __('Text', 'pnfw');

  $rows = array();
  $rows[] = '"' . implode('"' . $separator . '"', $row) . '"';

  if (!empty($items)) {
   foreach ($items as $item) {
    $row = array();

    $row[] = $item->timestamp;
    $row[] = $item->type;
    $row[] = $item->text;

    $rows[] = '"' . implode('"' . $separator . '"', $row) . '"';
   }
  }

  $this->generate_csv($rows, 'debug_log.csv');

  exit;
 }

 private function generate_csv($rows, $filename) {
  $blog_title = $this->clean_string(strtolower(get_bloginfo('name')));

  $filename = $blog_title . '_' . $filename;

  header('Content-type: text/csv');
  header('Content-Disposition: attachment; filename=' . $filename);
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

  $log = implode("\n", $rows);
  header('Content-Length: ' . strlen($log));
  echo $log;
 }

 function clean_string($string) {
    $string = str_replace(' ', '_', $string); // replaces all spaces with underscore

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // removes special chars
 }

 function send_in_progress() {
  $send_in_progress = get_option('pnfw_send_in_progress', 0);

  echo $send_in_progress;

  wp_die();
 }

 function progress_bar_admin_notices() {
  if (isset($_GET["page"]) && ($_GET["page"] == 'push-notifications-for-wordpress' || pnfw_starts_with($_GET["page"], 'pnfw-'))) {
   echo '<div id="progressbar" style="display:none" class="update-nag below-h2">' . __('Sending notifications...', 'pnfw') . '</div>';
  }
 }

 function add_user_category_admin_page() {
  $tax = get_taxonomy('user_cat');

  $page = add_users_page(
   esc_attr($tax->labels->menu_name),
   esc_attr($tax->labels->menu_name),
   $tax->cap->manage_terms,
   'edit-tags.php?taxonomy=' . $tax->name
  );
 }

 function manage_user_cat_column($display, $column, $term_id) {
  if ('users' === $column) {
   $term = get_term($term_id, 'user_cat');
   echo $term->count;
  }
 }

 function manage_user_cat_user_column($columns) {
  unset($columns['posts']);
  $columns['users'] = __('Users', 'pnfw');
  return $columns;
 }

 function edit_user_cat_section($user) {
  $tax = get_taxonomy('user_cat');

  if (!current_user_can($tax->cap->assign_terms) || !current_user_can('edit_users'))
   return;

  $terms = get_terms('user_cat', array('hide_empty' => false)); ?>

  <h3><?php _e('User Category', 'pnfw'); ?></h3>
  <table class="form-table">
   <tr>
    <th><label for="user_cat"><?php _e('Select User Category', 'pnfw'); ?></label></th>
    <td><?php
    if (!empty($terms)) {
     foreach ($terms as $term) { ?>
      <input type="radio" name="user_cat" id="user_cat-<?php echo esc_attr($term->slug); ?>" value="<?php echo esc_attr($term->slug); ?>" <?php checked(true, is_object_in_term($user->ID, 'user_cat', $term)); ?> /> <label for="user_cat-<?php echo esc_attr($term->slug); ?>"><?php echo $term->name; ?></label> <br /><?php
     }
    }
    else {
     _e('There are no user categories available.', 'pnfw');
    }?>
    </td>
   </tr>
  </table><?php
 }

 function save_user_cat_terms($user_id) {
  $tax = get_taxonomy('user_cat');

  if (!current_user_can('edit_user', $user_id) && current_user_can($tax->cap->assign_terms))
   return false;

  if (isset($_POST['user_cat'])) {
   $term = esc_attr($_POST['user_cat']);

   wp_set_object_terms($user_id, array($term), 'user_cat', false);
   clean_object_term_cache($user_id, 'user_cat');
  }
 }

 function fix_user_tax_page($parent_file = '') {
  global $pagenow;

  if (!empty($_GET['taxonomy']) && $_GET['taxonomy'] == 'user_cat' && $pagenow == 'edit-tags.php') {
   $parent_file = 'users.php';
  }

  return $parent_file;
 }

 function disable_username($username) {
  if ('user_cat' === $username)
   $username = '';

  return $username;
 }

}
