<input id="<?php echo esc_attr( $html['id'] ); ?>" class="awpcp-textfield inputbox <?php echo esc_attr( $html['class'] ); ?>" type="text" datepicker-placeholder value="<?php echo esc_attr( $value_formatted ); ?>" autocomplete="off">
<input type="hidden" name="<?php echo esc_attr( $html['name'] ); ?>" value="<?php echo esc_attr( $value ); ?>" />
