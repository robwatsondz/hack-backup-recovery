��          �   %   �      p     q     �     �     �     �     �     	  :     "   Z  2   }  H   �  #   �        	   >  %   H  !   n     �     �     �  5   �  3   �  8     3   V  !   �  \   �     	       �  &     �  ,   �  (   �     #     =  %   O     u  T   �  0   �  G   	  ]   `	  4   �	  *   �	     
  ;   +
  +   g
     �
     �
     �
     �
  4   �
  ;   	  A   E  1   �  �   �     :     K                                      	                                                          
                           &mdash; Select &mdash; City / Country custom fields? City based on a custom field City field is empty! Cookies cleared. Country based on a custom field Detecting location... Enable geolocation based on custom fields for this weather Enable geolocation on this weather Force geolocation on this weather when page loads? HTML5 Geolocation supported, but location data is currently unavailable. Latitude / Longitude custom fields? Latitude based on a custom field Locate me Location data retrieved from cookies. Longitude based on a custom field Request failed Set location Submit The browser timed out before retrieving the location. The browser was unable to determine your location:  The user prevented this page from retrieving a location. There was an error while retrieving your location:  Use this weather for geolocation? Your site doesn't have an SSL certificate. This is mandatory by browsers to use geolocation. clear cookies eg: london, gb Project-Id-Version: WP Cloudy
POT-Creation-Date: 2017-08-31 15:50+0200
PO-Revision-Date: 2017-08-31 15:50+0200
Last-Translator: 
Language-Team: Benjamin DENIS <contact@wpcloudy.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.1
X-Poedit-KeywordsList: __;_e;_x
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
 &mdash; Selectionner &mdash; Champs personnalisés de type Ville / Pays ? Ville basée sur un champ personnalisée Le champ Ville est vide ! Cookies effacés. Pays basé sur un champ personnalisé Détection de la position... Activer la géolocalisation à partir des champs personnalisés pour cette météo ? Activer la géolocalisation pour cette météo ? Forcer la géolocalisation sur cette météo au chargement de la page ? Géolocalisation HTML5 supportée, mais données de localisation indisponible pour le moment. Champs personnalisés de type Latitude / Longitude ? Latitude basée sur un champ personnalisé Me localiser Données de localisation retrouvées à partir des cookies. Longitude basée sur un champ personnalisé Echec de la requête Déterminer la position OK Délai dépassé Nous n'avons pas pu déterminer votre localisation : L'utilisateur a empêché la récupération de la position. Une erreur est survenue lors de la détection de votre position : Utiliser cette météo pour la géolocalisation ? Votre site ne dispose pas de certificate SSL. C'est une obligation de la part des navigateurs pour utiliser la géolocalisation. nettoyer cookies ex: Biarritz, fr 