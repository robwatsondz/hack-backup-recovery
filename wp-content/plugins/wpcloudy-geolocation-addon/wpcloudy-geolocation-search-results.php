<?php 
	
						
	$wpcloudy_city_search_value 		= $_POST["wpc_cloudy_city"]; 
	$wpc_geo_api_key 					= $_POST["wpc_geo_api_key"]; 
	
	$wpcloudy_city_search_xml_url 		= "http://api.openweathermap.org/data/2.5/find?q=$wpcloudy_city_search_value&type=like&mode=xml&APPID=$wpc_geo_api_key&lang=$wpcloudy_lang_owm";
	
	
	if (@simplexml_load_file($wpcloudy_city_search_xml_url)) {
	    $wpcloudy_city_search_xml = simplexml_load_file($wpcloudy_city_search_xml_url);
	} else {
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $wpcloudy_city_search_xml_url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_HEADER, false);
	    $xml = curl_exec($ch);
	    $wpcloudy_city_search_xml = simplexml_load_string($xml);
	}
	
?>

<div class="wpc-listing-cities">

	<?php	
		foreach ($wpcloudy_city_search_xml->list[0] as $list) { 
	
			$wpcloudy_city_id = $list->city['id'];
			$wpcloudy_city_name = $list->city['name'];
			$wpcloudy_city_lat = $list->city->coord['lat'];
			$wpcloudy_city_lon = $list->city->coord['lon'];
	?>
	
	<a href="#" onclick="wpc_load_city(<?php echo $wpcloudy_city_id; ?>, '<?php echo $wpcloudy_city_name; ?>', '<?php echo $wpcloudy_city_lat; ?>', '<?php echo $wpcloudy_city_lon; ?>')" >
		
	
	<?php echo $list->city['name']; ?>,
	
	<?php echo $list->city->country; ?>
	
   	</a><br/>	

	<?php
	}
	?>
	
</div>