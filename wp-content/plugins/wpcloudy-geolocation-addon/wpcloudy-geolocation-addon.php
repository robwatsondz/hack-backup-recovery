<?php
/*
Plugin Name: WP Cloudy Geolocation Add-on
Plugin URI: https://wpcloudy.com/
Description: WP Cloudy Geolocation Add-on allows visitors to get weather based on their geolocation (WP Cloudy plugin required).
Version: 2.6.1
Author: Benjamin DENIS
Author URI: https://wpcloudy.com/
License: GPLv2
Text Domain: wpcloudy
Domain Path: /lang
*/

/*  Copyright 2013 - 2016  Benjamin DENIS  (email : contact@wpcloudy.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// To prevent calling the plugin directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Please don&rsquo;t call the plugin directly. Thanks :)';
	exit;
}

function weather_geolocation_activation() {
}
register_activation_hook(__FILE__, 'weather_geolocation_activation');
function weather_geolocation_deactivation() {
}
register_deactivation_hook(__FILE__, 'weather_geolocation_deactivation');

load_plugin_textdomain('wpcloudy', false, basename( dirname( __FILE__ ) ) . '/lang' );

///////////////////////////////////////////////////////////////////////////////////////////////////
//Translation
///////////////////////////////////////////////////////////////////////////////////////////////////

function wpcloudy_geolocation_init() {
  load_plugin_textdomain( 'wpcloudy', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' ); 
}
add_action('plugins_loaded', 'wpcloudy_geolocation_init');

///////////////////////////////////////////////////////////////////////////////////////////////////
//Loads Geolocation
///////////////////////////////////////////////////////////////////////////////////////////////////

//Weather in dashboard
function wpc_geolocation_add_dashboard_scripts() {	
	global $post;
	 
	 if (wp_script_is( 'jquery.cookie.min.js', 'enqueued' )) {
       return;
     } else {
       wp_register_script('wpcloudy-geolocation-cookies-js', plugins_url('js/jquery.cookie.js', __FILE__), '', '', false);
	   wp_enqueue_script('wpcloudy-geolocation-cookies-js');
     }

	wp_register_script('wpc-modal-js', plugins_url('js/modal.min.js', __FILE__), '', '', false);
	wp_enqueue_script('wpc-modal-js');
	
	wp_register_style('wpcloudy-geolocation-addon', plugins_url('css/wpcloudy-geolocation-addon.css', __FILE__));
	wp_enqueue_style('wpcloudy-geolocation-addon');

}
add_action('admin_head-index.php', 'wpc_geolocation_add_dashboard_scripts');

///////////////////////////////////////////////////////////////////////////////////////////////////
//Loads Cookies JS
///////////////////////////////////////////////////////////////////////////////////////////////////

function wpcloudy_geolocation_scripts_fe() {
	global $post;
	wp_enqueue_script('jquery');
	
    if (wp_script_is( 'jquery.cookie.min.js', 'enqueued' )) {
       return;
     } else {
       wp_register_script('wpcloudy-geolocation-cookies-js', plugins_url('js/jquery.cookie.js', __FILE__), '', '', false);
	   wp_enqueue_script('wpcloudy-geolocation-cookies-js');
     }

    //Boostrap Modal JS
	function get_admin_modal_js() {
		$wpc_admin_modal_js_option = get_option("wpc_option_name");
		if ( ! empty ( $wpc_admin_modal_js_option ) ) {
			foreach ($wpc_admin_modal_js_option as $key => $wpc_admin_modal_js_value)
				$options[$key] = $wpc_admin_modal_js_value;
			if (isset($wpc_admin_modal_js_option['wpc_advanced_modal_js'])) {
				return $wpc_admin_modal_js_option['wpc_advanced_modal_js'];
			}
		}
	};
				
	function wpc_advanced_modal_js() {
		if (get_admin_modal_js() != '') {
			return get_admin_modal_js();
		}
	}
	$wpc_advanced_modal_js = wpc_advanced_modal_js();
	if ($wpc_advanced_modal_js != 'yes') {
		wp_register_script('wpc-modal-js', plugins_url('js/modal.min.js', __FILE__), '', '', false);
		wp_enqueue_script('wpc-modal-js');
	}
	
	wp_register_style('wpcloudy-geolocation-addon', plugins_url('css/wpcloudy-geolocation-addon.css', __FILE__));
	wp_enqueue_style('wpcloudy-geolocation-addon');
}

add_action('wp_enqueue_scripts', 'wpcloudy_geolocation_scripts_fe');

///////////////////////////////////////////////////////////////////////////////////////////////////
//Display metabox in Weather Custom Post Type
///////////////////////////////////////////////////////////////////////////////////////////////////

add_action('add_meta_boxes','init_metabox_wpc_geolocation');
function init_metabox_wpc_geolocation(){ 
    add_meta_box('wpcloudy_geolocation', 'WP Cloudy Geolocation', 'wpcloudy_geolocation', 'wpc-weather', 'normal');  
}

function wpcloudy_geolocation($post){
    $wpcloudy_enable_geolocation				= get_post_meta($post->ID,'_wpcloudy_enable_geolocation',true);
    $wpcloudy_custom_field_city_name 			= get_post_meta($post->ID,'_wpcloudy_custom_field_city_name',true);
    $wpcloudy_custom_field_country_name 		= get_post_meta($post->ID,'_wpcloudy_custom_field_country_name',true);
    $wpcloudy_custom_field_lat_name 			= get_post_meta($post->ID,'_wpcloudy_custom_field_lat_name',true);
    $wpcloudy_custom_field_lon_name 			= get_post_meta($post->ID,'_wpcloudy_custom_field_lon_name',true);
    $wpcloudy_enable_geolocation_custom_field	= get_post_meta($post->ID,'_wpcloudy_enable_geolocation_custom_field',true);
   	$wpcloudy_lat_lon_cf						= get_post_meta($post->ID,'_wpcloudy_lat_lon_cf',true);
   	$wpcloudy_force_geolocation					= get_post_meta($post->ID,'_wpcloudy_force_geolocation',true);
        
    global $wpdb;
	$post = get_post( $post );
	$limit = (int) apply_filters( 'postmeta_form_limit', 100 );
	$keys = $wpdb->get_col( "
		SELECT meta_key
		FROM $wpdb->postmeta
		GROUP BY meta_key
		HAVING meta_key NOT LIKE '\_%'
		ORDER BY meta_key
		LIMIT $limit" );
	if ( $keys ) {
		natcasesort( $keys );
		$meta_key_input_id = 'metakeyselect';
	} else {
		$meta_key_input_id = 'metakeyinput';
	}

	_e( 'Use this weather for geolocation?', 'wpcloudy' );
	echo 
        '<p>				
			<label for="wpcloudy_enable_geolocation_meta">
				<input type="checkbox" name="wpcloudy_enable_geolocation" id="wpcloudy_enable_geolocation_meta" value="yes" '. checked( $wpcloudy_enable_geolocation, 'yes', false ) .' />
					'. __( 'Enable geolocation on this weather', 'wpcloudy' ) .'
			</label>
		</p>';
		
		if (!is_ssl()) {
			echo '<p><em><span class="dashicons dashicons-editor-help"></span>'.__('Your site doesn\'t have an SSL certificate. This is mandatory by browsers to use geolocation.','wpcloudy').'</em></p>';

			echo '<p><a class="button-secondary" href="https://www.namecheap.com/?aff=105841" target="_blank">'.__('Buy an SSL now!','wpcloudy').'</a></p>';
		}

		echo '<p>				
			<label for="wpcloudy_enable_geolocation_custom_field_meta">
				<input type="checkbox" name="wpcloudy_enable_geolocation_custom_field" id="wpcloudy_enable_geolocation_custom_field_meta" value="yes" '. checked( $wpcloudy_enable_geolocation_custom_field, 'yes', false ) .' />
					'. __( 'Enable geolocation based on custom fields for this weather', 'wpcloudy' ) .'
			</label>			
		</p>
	
		<p>
			<label for="wpcloudy_lat_lon_cf_yes_meta">
				<input type="radio" name="wpcloudy_lat_lon_cf" id="wpcloudy_lat_lon_cf_yes_meta" value="yes" '. checked( $wpcloudy_lat_lon_cf, 'yes', false ) .' />
					'. __( 'Latitude / Longitude custom fields?', 'wpcloudy' ) .'
			</label>
		</p>
		<p>
			<label for="wpcloudy_lat_lon_cf_no_meta">
				<input type="radio" name="wpcloudy_lat_lon_cf" id="wpcloudy_lat_lon_cf_no_meta" value="no" '. checked( $wpcloudy_lat_lon_cf, 'no', false ) .' />
					'. __( 'City / Country custom fields?', 'wpcloudy' ) .'
			</label>
		</p>

		<p>
			<label for="wpcloudy_custom_field_city_name_meta">'. __( 'City based on a custom field', 'wpcloudy' ) .'</label><br />';
			
			if ( $keys ) {
								
				echo '<select id="metakeyselect" name="wpcloudy_custom_field_city_name">';
				echo '<option value="#NONE#">'. __( '&mdash; Select &mdash;' ).'</option>';
				
				foreach ( $keys as $wpc_custom_field_city ) {
					if ( is_protected_meta( $wpc_custom_field_city, 'post' ) || ! current_user_can( 'add_post_meta', $post->ID, $wpc_custom_field_city ) )
						continue;
					echo "\n<option " . selected( $wpc_custom_field_city, $wpcloudy_custom_field_city_name, false ) . " value='" . esc_attr($wpc_custom_field_city) . "'>" . esc_html($wpc_custom_field_city) . "</option>";
				}
				
				echo '</select>';
			}
					
		echo 
		'</p>
		
		<p>
			<label for="wpcloudy_custom_field_country_name_meta">'. __( 'Country based on a custom field', 'wpcloudy' ) .'</label><br/>';
			
			if ( $keys ) {
								
				echo '<select id="metakeyselect" name="wpcloudy_custom_field_country_name">';
				echo '<option value="#NONE#">'. __( '&mdash; Select &mdash;' ).'</option>';
				
				foreach ( $keys as $wpc_custom_field_country ) {
					if ( is_protected_meta( $wpc_custom_field_country, 'post' ) || ! current_user_can( 'add_post_meta', $post->ID, $wpc_custom_field_country ) )
						continue;
					echo "\n<option " . selected( $wpc_custom_field_country, $wpcloudy_custom_field_country_name, false ) . " value='" . esc_attr($wpc_custom_field_country) . "'>" . esc_html($wpc_custom_field_country) . "</option>";
				}
				
				echo '</select>';
			}
					
		echo 
		'</p>
		
		<p>
			<label for="wpcloudy_custom_field_lat_meta">'. __( 'Latitude based on a custom field', 'wpcloudy' ) .'</label><br />';
			
			if ( $keys ) {
								
				echo '<select id="metakeyselect" name="wpcloudy_custom_field_lat_name">';
				echo '<option value="#NONE#">'. __( '&mdash; Select &mdash;' ).'</option>';
				
				foreach ( $keys as $wpc_custom_field_lat ) {
					if ( is_protected_meta( $wpc_custom_field_lat, 'post' ) || ! current_user_can( 'add_post_meta', $post->ID, $wpc_custom_field_lat ) )
						continue;
					echo "\n<option " . selected( $wpc_custom_field_lat, $wpcloudy_custom_field_lat_name, false ) . " value='" . esc_attr($wpc_custom_field_lat) . "'>" . esc_html($wpc_custom_field_lat) . "</option>";
				}
				
				echo '</select>';
			}
					
		echo 
		'</p>
		
		<p>
			<label for="wpcloudy_custom_field_lon_meta">'. __( 'Longitude based on a custom field', 'wpcloudy' ) .'</label><br/>';
			
			if ( $keys ) {
								
				echo '<select id="metakeyselect" name="wpcloudy_custom_field_lon_name">';
				echo '<option value="#NONE#">'. __( '&mdash; Select &mdash;' ).'</option>';
				
				foreach ( $keys as $wpc_custom_field_lon ) {
					if ( is_protected_meta( $wpc_custom_field_lon, 'post' ) || ! current_user_can( 'add_post_meta', $post->ID, $wpc_custom_field_lon ) )
						continue;
					echo "\n<option " . selected( $wpc_custom_field_lon, $wpcloudy_custom_field_lon_name, false ) . " value='" . esc_attr($wpc_custom_field_lon) . "'>" . esc_html($wpc_custom_field_lon) . "</option>";
				}
				
				echo '</select>';
			}
					
		echo 
		'</p>
		<p>				
			<label for="wpcloudy_force_geolocation_meta">
				<input type="checkbox" name="wpcloudy_force_geolocation" id="wpcloudy_force_geolocation_meta" value="yes" '. checked( $wpcloudy_force_geolocation, 'yes', false ) .' />
					'. __( 'Force geolocation on this weather when page loads?', 'wpcloudy' ) .'
			</label>			
		</p>';

}

add_action('save_post','save_metabox_wpc_geolocation');
function save_metabox_wpc_geolocation($post_id){
	if( isset( $_POST[ 'wpcloudy_enable_geolocation' ] ) ) {
		update_post_meta( $post_id, '_wpcloudy_enable_geolocation', 'yes' );
		delete_transient( "myweather_current_".$post_id ); 
		delete_transient( "myweather_".$post_id ); 
		delete_transient( "myweather_sevendays_".$post_id ); 
	} else {
		update_post_meta( $post_id, '_wpcloudy_enable_geolocation', '' );
	}
	if( isset( $_POST[ 'wpcloudy_enable_geolocation_custom_field' ] ) ) {
		update_post_meta( $post_id, '_wpcloudy_enable_geolocation_custom_field', 'yes' );
		delete_transient( "myweather_current_".$post_id ); 
		delete_transient( "myweather_".$post_id ); 
		delete_transient( "myweather_sevendays_".$post_id ); 
	} else {
		update_post_meta( $post_id, '_wpcloudy_enable_geolocation_custom_field', '' );
	}
	if( isset( $_POST[ 'wpcloudy_lat_lon_cf' ] ) ) {
		update_post_meta( $post_id, '_wpcloudy_lat_lon_cf', $_POST[ 'wpcloudy_lat_lon_cf' ] );
	}
	if(isset($_POST['wpcloudy_custom_field_city_name'])) {
	  update_post_meta($post_id, '_wpcloudy_custom_field_city_name', $_POST['wpcloudy_custom_field_city_name']);
	}
	if(isset($_POST['wpcloudy_custom_field_country_name'])) {
	  update_post_meta($post_id, '_wpcloudy_custom_field_country_name', $_POST['wpcloudy_custom_field_country_name']);
	}
	if(isset($_POST['wpcloudy_custom_field_lat_name'])) {
	  update_post_meta($post_id, '_wpcloudy_custom_field_lat_name', $_POST['wpcloudy_custom_field_lat_name']);
	}
	if(isset($_POST['wpcloudy_custom_field_lon_name'])) {
	  update_post_meta($post_id, '_wpcloudy_custom_field_lon_name', $_POST['wpcloudy_custom_field_lon_name']);
	}
	if( isset( $_POST[ 'wpcloudy_force_geolocation' ] ) ) {
		update_post_meta( $post_id, '_wpcloudy_force_geolocation', 'yes' );
	} else {
		update_post_meta( $post_id, '_wpcloudy_force_geolocation', '' );
	}
}

function wpc_geolocation_form($attr) { 
	extract(shortcode_atts(array( 'id' => ''), $attr));
	
	$wpc_current_url = $_SERVER['REQUEST_URI'];	
	
	$wpc_id =	$id;
	
	$wpc_geo_api_key						= wpc_get_api_key();
	$html = null;
	$html .='
	<script>
				    var latitude;
				    var longitude;
				    var accuracy;
				    
				    function wpc_load_location() {
				    	
				        if(navigator.geolocation) {
				        	jQuery.cookie("wpc-detectGeolocation", 1);
				            document.getElementById("status").innerHTML = "'. __('Detecting location...','wpcloudy').'";
				            document.getElementById("status").style.color = "#3498DB";
				            
				            if(jQuery.cookie("wpc-posLat")) {
				                latitude = jQuery.cookie("wpc-posLat");
				                longitude = jQuery.cookie("wpc-posLon");
				                accuracy = jQuery.cookie("wpc-posAccuracy");
				                document.getElementById("status").innerHTML = "'. __('Location data retrieved from cookies.','wpcloudy').'<a id=\'clear_cookies\' href=\' javascript:wpc_clear_cookies()\' style=\'cursor:pointer;\'>'. __('clear cookies','wpcloudy').'</a>";
				    
				                
				            } else {
				                navigator.geolocation.getCurrentPosition(			
				                    wpc_success_handler, 
				                    wpc_error_handler, 
				                    {timeout:10000});
				            }
				        }
				    }
				
				    function wpc_success_handler(position) {
				        latitude = position.coords.latitude;
				        longitude = position.coords.longitude;
				        accuracy = position.coords.accuracy;
				        
				        if (!latitude || !longitude) {
				            document.getElementById("status").innerHTML = "'. __('HTML5 Geolocation supported, but location data is currently unavailable.', 'wpcloudy').'";
				            return;
				        }
				        
				        if(jQuery.cookie("wpc-posLat")) {
					        jQuery.cookie("wpc-posLat", latitude, { expires: 30 });
					        jQuery.cookie("wpc-posLon", longitude, { expires: 30 });
					        jQuery.cookie("wpc-posAccuracy", accuracy, { expires: 30 });
						}
						else {
							jQuery.cookie("wpc-posLat", latitude, { expires: 30 });
					        jQuery.cookie("wpc-posLon", longitude, { expires: 30 });
					        jQuery.cookie("wpc-posAccuracy", accuracy, { expires: 30 });
					        jQuery.cookie("wpc-posCityId", null);
					        jQuery.cookie("wpc-posCityName", null);
							
							location.reload();
						}
				    }

				    function wpc_error_handler(error) {
				        var locationError = "";
				        
				        switch(error.code){
				        case 0:
				            locationError = "'. __("There was an error while retrieving your location: ", "wpcloudy").'" + error.message;
				            break;
				        case 1:
				            locationError = "'. __("The user prevented this page from retrieving a location.", "wpcloudy").'";
				            break;
				        case 2:
				            locationError = "'. __("The browser was unable to determine your location: ", "wpcloudy").'" + error.message;
				            break;
				        case 3:
				            locationError = "'. __("The browser timed out before retrieving the location.", "wpcloudy").'";
				            break;
				        }
				
				        document.getElementById("status").innerHTML = locationError;
				        document.getElementById("status").style.color = "#D03C02";
				    }
				    
				    function wpc_clear_cookies() {
				        jQuery.removeCookie("wpc-posLat");
				        jQuery.removeCookie("wpc-posLon");
				        jQuery.removeCookie("wpc-posCityId");
				        jQuery.removeCookie("wpc-posCityName");
				        jQuery.removeCookie("wpc-posAccuracy");
				        jQuery.removeCookie("wpc-detectGeolocation");
				        jQuery.removeCookie("wpc-manualGeolocation");
				        document.getElementById("status").innerHTML = "'. __("Cookies cleared.","wpcloudy").'";
				    } 
				    
					function wpc_load_city(wpcloudy_city_id, wpcloudy_city_name, wpcloudy_city_lat, wpcloudy_city_lon) {
						jQuery.removeCookie("wpc-detectGeolocation");
						jQuery.cookie("wpc-manualGeolocation", 1);
						jQuery.cookie("wpc-posCityId", wpcloudy_city_id, { expires: 30 });
						jQuery.cookie("wpc-posCityName", wpcloudy_city_name, { expires: 30 });
						jQuery.cookie("wpc-posLat", wpcloudy_city_lat, { expires: 30 });
						jQuery.cookie("wpc-posLon", wpcloudy_city_lon, { expires: 30 });
						jQuery.cookie("wpc-posAccuracy", null);
						location.reload();
					};
				</script>
		<div id="wpc-geolocation-addon">		
		<button id="#wpc_modal_'.$wpc_id.'" class="wpc-set-location" title="'.__('Set location','wpcloudy').'" data-toggle="modal" data-target=".bs-example-modal-sm-'.$wpc_id.'"></button>
		
		
		<div class="modal fade bs-example-modal-sm-'.$wpc_id.'" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-sm">
		    <div class="modal-content">
		    
	    
				
				<a onclick="wpc_load_location()" title="'.__('Locate me','wpcloudy').'" id="wpcloudy-detect-location" href="#wpc_modal_'.$wpc_id.'">'.__('Locate me','wpcloudy').'</a>
	
				<form action="'.$wpc_current_url.'" id="wpcloudy-city-search-form" method="post">
					<input id="wpc_cloudy_city" type="text" name="wpc_cloudy_city" placeholder="'. __("eg: london, gb","wpcloudy").'">
					<input type="hidden" name="wpc_geo_api_key" value="'.$wpc_geo_api_key.'">
					<input type="submit" value="'. __("Submit","wpcloudy").'">
				</form>
				<div class="wpc-loading-spinner" style="display:none">
					<img src="'.plugins_url('img/ajax-loader.gif', __FILE__).'" alt="loader"/>
				</div>
				<div id="result_'.$wpc_id.'" class="result"></div>
				<div id="status"></div>
				
				<script>
		
					jQuery(function () {
						jQuery("#wpcloudy-city-search-form").on("submit", function (e) {
							e.preventDefault();
							
							var wpc_cloudy_city     = jQuery("#wpc_cloudy_city").attr("value");

							if ( jQuery.trim( jQuery("#wpc_cloudy_city").val() ) == "" ){
						    	alert("'.__('City field is empty!','wpcloudy').'");
							} else {
								jQuery.ajax({
									type: "post",
									url: "'.plugins_url('wpcloudy-geolocation-search-results.php', __FILE__).'",
									timeout: 10000,
									data: jQuery("#wpcloudy-city-search-form").serialize(),
									success: function (data) {
										document.getElementById("result_'.$wpc_id.'").innerHTML = data;
									},
									beforeSend: function(){
								       jQuery(".wpc-loading-spinner").show();
								    },
								    complete: function(){
								       jQuery(".wpc-loading-spinner").hide();
								    },
								    error: function(){
								    	alert("'.__('Request failed','wpcloudy').'");
								    }	
								});
							};
							
						});
	
					});
			    </script>			    
	  
		    </div>
		  </div>
		</div>
	  </div>
		';
		return $html;
	}

?>