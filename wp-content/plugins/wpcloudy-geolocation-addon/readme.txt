=== WP Cloudy Geolocation Add-on ===
Contributors: rainbowgeek
Donate link: http://wpcloudy.com/
Tags: weather, forecast
Requires at least: 4.5
Tested up to: 4.8
Stable tag: 2.6.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

WP Cloudy Geolocation Add-on allows visitors to get weather based on their geolocation (WP Cloudy plugin required).

== Installation ==

1. Upload 'wpcloudy-geolocation-addon' to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Edit a weather from custom post type Weather
4. Enable geolocation for this weather
5. Save weather

== Frequently Asked Questions ==
Go to http://wpcloudy.com/support

= Any questions? =

All the answers are on our site (at least we try): http://wpcloudy.com

== Screenshots ==

1. Geolocation in action
2. Geolocation in BE

== Changelog ==
= 2.6.1 =
* INFO Add SSL notice
= 2.6 =
* Use CURL if simplexml_load_string isn't supported by webhost
= 2.5.1 =
* FIX CSS overlay
* FIX Undefined html variable in wpcloudy-geolocation-addon.php
= 2.5 = 
* NEW Force Geolocated weather when page loads
= 2.4.1 =
* FIX Warning: Invalid argument supplied for foreach() in wpcloudy-geolocation-search-results.php on line 17
* FIX OWM API Key is now used in Geolocation Add-on properly
* FIX Text domain for translation
= 2.4 =
* INFO Upgrade Bootstrap Modal JS
* INFO Tested with TwentySixteen
= 2.3 =
* FIX Add a new option to disable Bootstrap Modal JS to avoid conflict (Settings > WP Cloudy > Advanced tab) 
* INFO Updated for WP Cloud 3.3 
= 2.2 =
* FIX Ajax search results to prevent reloading page / conflicts
= 2.1 =
* FIX PHP warning when user submit form with empty input
= 2.0 =
* NEW Build weather based on custom fields
* FIX Undefined variable
= 1.0.2 =
* FIX Geolocation issues
= 1.0.1 =
* FIX Cache system issues
= 1.0 =
* Stable release.

