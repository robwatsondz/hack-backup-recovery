<?php
/*
1.  Enqueue scripts
2.  Define the custom boxes for regular posts
3.  When the post is saved, save our custom data
4.  Define the custom boxes for digital edition
5.  When the `digital edition` post is saved, save our custom data
6.  Remove article from homepage placement upon deletion
7.  Add favicon to admin head
8.  Add HR button to TinyMCE
9.  Add TinyMCE paste filter
10. Turn the tags metabox into a checklist
11. Register the quality control class
12. Register the story updates class
*/

include_once('admin_restrictions.php');
include_once('quality-control.php');
include_once('quick_publish.php');
//include_once('tags.php');

// 1. Enqueue scripts
add_action('admin_enqueue_scripts', function () {
    // All this crap for the jQuery UI Datepicker:
    wp_enqueue_style('jquery-ui-smoothness', plugin_dir_url(__FILE__).'/smoothness/jquery-ui-1.8.17.custom.css');
    wp_enqueue_script('hamodia_post_meta_datepicker', get_home_url().'/wp-includes/js/jquery/ui/datepicker.min.js', 'jquery-ui-core', '1.11.2', true);
    wp_enqueue_script('hamodia_post_meta', plugin_dir_url(__FILE__) . '/main.js', 'hamodia_post_meta_datepicker', '0.0.2', true);
});

// 2. Define the custom boxes for regular posts
add_action('add_meta_boxes', function () {
    // Meta box for State, Source, Author & homepage
    foreach (['post', 'hamodia_feature','hamodia_frominyan','hamodia_column'] as $post_type) {
        add_meta_box('hamodia_post_meta', 'Meta information', function ($post) {
            include __DIR__.'/views/meta-boxes/meta-information.php';
        }, $post_type);
    }

    // Meta box for Print Edition
    foreach (['post', 'hamodia_feature', 'digital_edition', 'letter','hamodia_frominyan','hamodia_column'] as $post_type) {
        add_meta_box('hamodia_post_meta_print_edition', 'Print Edition', function ($post) {
            wp_nonce_field(plugin_basename(__FILE__), 'hamodia_post_meta_nonce');

            include __DIR__.'/views/meta-boxes/print-edition.php';
        }, $post_type);
    }

    HomepagePlacement::make()->register();
});

// 3. When the post is saved, save our custom data
add_action('save_post', function ($id) {
    // We shouldn't do this on autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    // Verify nonce
    if (! isset($_POST['hamodia_post_meta_nonce']) || ! wp_verify_nonce($_POST['hamodia_post_meta_nonce'], plugin_basename(__FILE__))) {
        return;
    }
    // Check permissions
    if (! current_user_can('edit_post', $id)) {
        return;
    }

	if (!wp_is_post_revision($id) === false)
    	return;
										
	// OK, we're authenticated. Let's go:
    foreach (['state', 'source', 'author', 'date', 'page'] as $key) {
        $key = 'hamodia_post_meta_' . $key;

        update_post_meta(
            $id,
            '_' . $key,
            esc_attr($_POST[$key])
        );
    }

	// X2CMS 2018/02/14 - update to save page position

	// Update the meta key for this..

	if (isset($_POST['hamodia_home_page_article']) && $_POST['hamodia_home_page_article'] != '')
	{
		$previous_meta_value = (int)get_post_meta($post_id, 'homepage_placement', true);

		update_post_meta($id, 'homepage_placement', (int)$_POST['hamodia_home_page_article']);

		// If its set to publish
		if ($_POST['post_status'] == 'publish')
			update_homepage_positions($id,(int)$_POST['hamodia_home_page_article'], $previous_meta_value);
	}
	else
	{
		// remove entry
		delete_post_meta($id, 'homepage_placement');
	}
	
   // HomepagePlacement::make()->update($id, $_POST['hamodia_home_page_article']);
});

// 4. Define the custom boxes for digital edition
add_action('add_meta_boxes', function () {
    add_meta_box('hamodia_epaperflip', 'ePaperFlip', function ($post) {
        wp_nonce_field(plugin_basename(__FILE__), 'hamodia_epaperflipid_nonce');

        $value = get_post_meta($post->ID, '_hamodia_epaperflipid', true);

        include __DIR__.'/views/meta-boxes/epaperflip.php';
    }, 'digital_edition');
});

// 5. When the `digital edition` post is saved, save our custom data
add_action('save_post', function ($post_id) {
    // We shouldn't do this on autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    // Verify nonce
    if (! isset($_POST['hamodia_epaperflipid_nonce']) || ! wp_verify_nonce($_POST['hamodia_epaperflipid_nonce'], plugin_basename(__FILE__))) {
        return;
    }
    // Check permissions
    if (! current_user_can('edit_post', $post_id)) {
        return;
    }

    // OK, we're authenticated. Let's go:
    update_post_meta($post_id, '_hamodia_epaperflipid', esc_attr($_POST['hamodia_epaperflipid']));
});

// 6. Remove article from homepage placement upon deletion
HomepagePlacement::make()->registerOnDelete();

// 7. Add favicon to admin head
add_action('admin_head', function () {
    $favicon = get_bloginfo('template_url') . '/favicon.png';
    echo '<link rel="shortcut icon" href="' . $favicon . '" />';
});

// 8. Add HR button to TinyMCE
add_filter("mce_buttons_2", function ($buttons) {
    $buttons[] = 'hr';

    return $buttons;
});

// 9. Add TinyMCE paste filter
add_filter('tiny_mce_before_init', function ($in) {
    $in['paste_preprocess'] = file_get_contents(__DIR__.'/TinyMCE-paste-preprocess.js');

    return $in;
});

// 10. Turn the tags metabox into a checklist
//HamodiaTags::register();

// 11. Register the quality control class
HamodiaQualityControl::register();

// 12. Register the story updates class
HamodiaStoryUpdates::register();


add_action( 'init', 'blockusers_init' );

function blockusers_init() {
	if ( is_admin() && current_user_can( 'oio_admin_user' ) &&
	! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) )
	{

		$paths_allowed = array(
			'/wp-admin/admin.php?page=oiopub-',			
			'/wp-login.php?action=logout'
		);

		$result = array_filter(
			$paths_allowed,
			function ($item) {
				return strtolower(substr($_SERVER['REQUEST_URI'],0, strlen($item))) === strtolower($item);
			}
		);


		if (empty($result) && strtolower($_SERVER['REQUEST_URI']) != '/wp-admin/')
		{

			echo "No access to this page";
			exit;
		}

	}
}

/* tag removal */

function my_remove_meta_boxes() {	
	remove_meta_box( 'tagsdiv-post_tag', 'post', 'normal' );	
}

add_action( 'admin_init', 'my_remove_meta_boxes' );

function my_manage_columns( $columns ) {
    unset($columns['tags']);
    return $columns;
}

function my_column_init() {	
    add_filter( 'manage_posts_columns' , 'my_manage_columns' );
}

add_action( 'admin_init' , 'my_column_init' );

function my_remove_sub_menus() {   
    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
}

add_action('admin_menu', 'my_remove_sub_menus');