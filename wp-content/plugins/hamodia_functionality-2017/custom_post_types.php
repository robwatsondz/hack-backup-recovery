<?php
/*
1. Features
2. Letters
3. Digtal Edition
*/

// Features is now renamed as Column
// Column is now named features but keeping categories the same for data

// 1. Features
add_action('after_setup_theme', function () {
    register_post_type('hamodia_feature', [
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'menu_position'      => 4,
        'capability_type'    => 'post',
        'hierarchical'       => false,
        'has_archive'        => true,
        'rewrite'            => ['slug' => 'columns', 'with_front' => false],
        'supports'           => ['title', 'editor'],
        'labels'             => [
            'name'               => 'Columns',
            'singular_name'      => 'Column',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Column',
            'edit_item'          => 'Edit Column',
            'new_item'           => 'New Column',
            'all_items'          => 'All Columns',
            'view_item'          => 'View Column',
            'search_items'       => 'Search Columns',
            'not_found'          => 'No Columns found',
            'not_found_in_trash' => 'No Columns found in Trash',
            'menu_name'          => 'Columns',
        ],
    ]);

    register_taxonomy('feature_category', 'hamodia_feature', [
        'hierarchical' => true,
        'show_ui'      => true,
        'query_var'    => true,
        'rewrite'      => ['slug' => 'feature-category'],
        'labels'       => [
            'name'              => 'Categories',
            'singular_name'     => 'Category',
            'search_items'      => 'Search Categories',
            'all_items'         => 'All Categories',
            'parent_item'       => 'Parent Category',
            'parent_item_colon' => 'Parent Category:',
            'edit_item'         => 'Edit Category',
            'update_item'       => 'Update Category',
            'add_new_item'      => 'Add New Category',
            'new_item_name'     => 'New Category Name',
            'menu_name'         => 'Categories',
        ],
    ]);
});

// 2. Letters
add_action('after_setup_theme', function () {
    register_post_type('letter', [
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'menu_position'      => 4,
        'capability_type'    => 'post',
        'hierarchical'       => false,
        'has_archive'        => true,
        'rewrite'            => ['slug' => 'letters', 'with_front' => false],
        'supports'           => ['title', 'editor','custom-fields'],
        'labels'             => [
            'name'               => 'Letters',
            'singular_name'      => 'Feature',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Letter',
            'edit_item'          => 'Edit Letter',
            'new_item'           => 'New Letter',
            'all_items'          => 'All Letters',
            'view_item'          => 'View Letter',
            'search_items'       => 'Search Letters',
            'not_found'          => 'No Letters found',
            'not_found_in_trash' => 'No Letters found in Trash',
            'menu_name'          => 'Letters',
        ],
    ]);
});

// 3. Digtal Edition
add_action('after_setup_theme', function () {
    register_post_type('digital_edition', [
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'menu_position'      => 4,
        'capability_type'    => 'post',
        'hierarchical'       => false,
        'has_archive'        => true,
        'rewrite'            => ['slug' => 'digital-edition', 'with_front' => false],
        'supports'           => ['title', 'thumbnail'],
        'labels'             => [
            'name'               => 'Digital Editions',
            'singular_name'      => 'Digital Edition',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Digital Edition',
            'edit_item'          => 'Edit Digital Edition',
            'new_item'           => 'New Digital Edition',
            'all_items'          => 'All Digital Editions',
            'view_item'          => 'View Digital Edition',
            'search_items'       => 'Search Digital Editions',
            'not_found'          => 'No Digital Editions found',
            'not_found_in_trash' => 'No Digital Editions found in Trash',
            'menu_name'          => 'Digital Edition',
        ],
    ]);

    add_theme_support('post-thumbnails', ['digital_edition']);
    add_image_size('paper-front-page', 130, 180, true);
});





// 4. From Inyan Section
add_action('after_setup_theme', function () {
    register_post_type('hamodia_frominyan', [
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'menu_position'      => 4,
        'capability_type'    => 'post',
        'hierarchical'       => false,
        'has_archive'        => true,
        'rewrite'            => ['slug' => 'frominyan', 'with_front' => false],
        'supports'           => ['title', 'editor'],
        'labels'             => [
            'name'               => 'FromINYAN Article',
            'singular_name'      => 'FromINYAN Article',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New FromINYAN Article',
            'edit_item'          => 'Edit FromINYAN Article',
            'new_item'           => 'New FromINYAN Article',
            'all_items'          => 'All FromINYAN Articles',
            'view_item'          => 'View FromINYAN Article',
            'search_items'       => 'Search FromINYAN Articles',
            'not_found'          => 'No FromINYAN Articles found',
            'not_found_in_trash' => 'No FromINYAN Articles found in Trash',
            'menu_name'          => 'FromINYAN Articles',
        ],
    ]);

    register_taxonomy('frominyan_category', 'hamodia_frominyan', [
        'hierarchical' => true,
        'show_ui'      => true,
        'query_var'    => true,
        'rewrite'      => ['slug' => 'frominyan-category'],
        'labels'       => [
            'name'              => 'Categories',
            'singular_name'     => 'Category',
            'search_items'      => 'Search Categories',
            'all_items'         => 'All Categories',
            'parent_item'       => 'Parent Category',
            'parent_item_colon' => 'Parent Category:',
            'edit_item'         => 'Edit Category',
            'update_item'       => 'Update Category',
            'add_new_item'      => 'Add New Category',
            'new_item_name'     => 'New Category Name',
            'menu_name'         => 'Categories',
        ],
    ]);
});




// 5. Columns Section
add_action('after_setup_theme', function () {
    register_post_type('hamodia_column', [
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'menu_position'      => 4,
        'capability_type'    => 'post',
        'hierarchical'       => false,
        'has_archive'        => true,
        'rewrite'            => ['slug' => 'prime', 'with_front' => false],
        'supports'           => ['title', 'editor'],
        'labels'             => [
            'name'               => 'Prime',
            'singular_name'      => 'Prime',
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New Prime',
            'edit_item'          => 'Edit Prime',
            'new_item'           => 'New Prime',
            'all_items'          => 'All Prime',
            'view_item'          => 'View Prime',
            'search_items'       => 'Search Prime',
            'not_found'          => 'No Primes found',
            'not_found_in_trash' => 'No Primes found in Trash',
            'menu_name'          => 'Primes',
        ],
    ]);

    register_taxonomy('column_category', 'hamodia_column', [
        'hierarchical' => true,
        'show_ui'      => true,
        'query_var'    => true,
        'rewrite'      => ['slug' => 'column-category'],
        'labels'       => [
            'name'              => 'Categories',
            'singular_name'     => 'Category',
            'search_items'      => 'Search Categories',
            'all_items'         => 'All Categories',
            'parent_item'       => 'Parent Category',
            'parent_item_colon' => 'Parent Category:',
            'edit_item'         => 'Edit Category',
            'update_item'       => 'Update Category',
            'add_new_item'      => 'Add New Category',
            'new_item_name'     => 'New Category Name',
            'menu_name'         => 'Categories',
        ],
    ]);
});