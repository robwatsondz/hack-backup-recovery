<?php
/*
Plugin Name: Hamodia Functionality 2017
Plugin URI: http://josephsilber.com
Description: Special functionality plugin built specifically for Hamodia.com's needs.
Author: Joseph Silber
Version: 0.1
Author URI: http://josephsilber.com
*/

include_once('login.php');
include_once('custom_post_types.php');
include_once('homepage_placement.php');
include_once('story-updates.php');

add_image_size('hero-image', 760, 425, true);
add_image_size('category-listing-image', 760, 760, true);

if (is_admin()) {
    include_once('backend.php');
} else {
    include_once('frontend.php');
}

register_activation_hook(__FILE__, function () {
    // Add edit pages capability to "editor"
    $role_object = get_role('editor');
    $role_object->add_cap('edit_pages');
});

register_deactivation_hook(__FILE__, function () {
    // Remove pages capability from "editor"
    $role_object = get_role('editor');
    $role_object->remove_cap('edit_pages');
});



// For detecting the change of post status
function post_published_notification($new_status, $old_status, $post)
{
	if (!wp_is_post_revision($post->ID) === false)
    	return;

	global $wpdb;

	$post_id = $post->ID;

	// is it a future to publish transition?
	// for testing if ($new_status == 'publish' && ($old_status == 'future' || $old_status == 'publish'))
	if ($new_status == 'publish' && $old_status == 'future')
	{

		// load the data as it currently is.
		$articles = HomepagePlacement::make()->get();

		$current_position = 0;
		// Check to see if we already have it in the list.
		foreach ($articles as $key=>$value)
		{
			if ((int)$value == (int)$post_id)
			{
				$current_position = $key;
			}
		}

		update_homepage_positions($post_id, (int)get_post_meta($post_id, 'homepage_placement', true), $current_position, true);
	}

}

add_action( 'transition_post_status', 'post_published_notification', 10, 4 );


function updated_post_data( $meta_id, $post_id, $meta_key, $meta_value){

	if ($meta_key == 'homepage_placement')
	{
		if (!wp_is_post_revision($post_id) === false)
    		return;

		$post = get_post($post_id);

		$current_homepage_listing = HomepagePlacement::make()->get();

		// has this come form a form post direct..
		if (isset($_POST['hamodia_home_page_article']) && $_POST['hamodia_home_page_article'] != '')
			if ($_POST['post_status'] != 'publish')
				return;

		// This has to be loaded before the data is update in the db..
		$articles = $current_homepage_listing;

		$current_position = 0;

		foreach ($articles as $key=>$value)
		{
			if ((int)$value != (int)$post_id)
				continue;

			$current_position = $key;
		}

		update_homepage_positions($post_id, $meta_value, $current_position);

		return false;

	}

}

add_action( 'update_post_meta', 'updated_post_data', 10, 4 ); //don't forget the last argument to allow all three arguments of the function


function update_homepage_positions($postID, $new_position, $current_position, $from_auto = false)
{
	global $wpdb;

	if (!wp_is_post_revision($postID) === false)
    	return;

	// dont run if were not setting one or if its the same positions.
	if ($new_position == 0 || ($current_position == $new_position && !$from_auto))
		return;

	// is the post published?
	if (get_post_status($postID) != 'publish')
		return;

	// check to see if the position is already taken by a post not our own..
	$args = array(
		'meta_key' => 'homepage_placement',
		'meta_value' => $new_position,
		'meta_compare' => '=',
		'post_status' => 'publish',
		'post__not_in' => array($postID), // If not its own
		'orderby' => 'post_date',
		'order' => 'ASC' // Load oldest first so when we loop through we will have the latest content id incase of duplicates.
	);

	// Custom query.
	$query = new WP_Query( $args );

	// We have results so there is already a post in this position
	if ( $query->have_posts() ) {

		// is the current position 0... if not we dont have this article listed.
		if ($current_position == 0 || $from_auto)
		{
			// The article is not already listed but there are others with the slot..
			// We allow the save as usual and then update everything after it by one.
			// dont want to update any post thats a future.. (allows for multiple schedules)
			$update_query = 'UPDATE hamod_postmeta SET meta_value=meta_value+1 WHERE meta_value >= ' .
			$new_position . ' AND (SELECT post_status from hamod_posts where ID = hamod_postmeta.post_id) = \'publish\' AND  post_id != ' . $postID . ' AND  meta_key=\'homepage_placement\';';
		}
		else
		{

			// The article is already listed. Where are we moving it to and from?
			if ($current_position > $new_position )
			{
				// If we move 3 > 1 for example

				$update_query = 'UPDATE hamod_postmeta ' .
					'SET meta_value=meta_value+1 ' .
					'WHERE meta_value >= ' . $new_position .
					' AND  meta_value <= ' . $current_position .
					' AND  post_id != ' . $postID .
					' AND  meta_key=\'homepage_placement\';';
			}
			else
			{

				// If we move 1 > 3 for example

				$update_query = 'UPDATE hamod_postmeta ' .
					'SET meta_value=meta_value-1 ' .
					'WHERE meta_value <= ' . $new_position .
					' AND  meta_value >= ' . $current_position .
					' AND  post_id != ' . $postID .
					' AND  meta_key=\'homepage_placement\';';
			}

		}

		// Nudge everything accordingly step.
		$wpdb->query($wpdb->prepare($update_query));

		// any that are out of the 5 are removed.
		$wpdb->query($wpdb->prepare('DELETE from hamod_postmeta WHERE meta_key=\'homepage_placement\' AND meta_value > 5;'));

	}

	// Restore original post data.
	wp_reset_postdata();

}