<?php

class Hamodia_homepage
{
    public static function promotional()
    {
        if ($page = Hamodia_homepage_model::get_promotional()) {
            static::render_promotional($page);
        }
    }

    public static function main_articles($limit = 4, $skip = 0)
    {


		// get the main articles from the pre existing function
		$xarticles = Hamodia_homepage_model::get_main_articles($limit);

		// extract the relevent articles
		$xarticles = array_slice($xarticles,$skip,$limit);

		// output the article
		static::render_main_articles_updated($xarticles,'category');

/*		if ($_SERVER['REMOTE_ADDR'] == '79.78.189.162')
		{

		// get the main articles from the pre existing function
		$xarticles = Hamodia_homepage_model::get_main_articles($limit);

		// extract the relevent articles
		$xarticles = array_slice($xarticles,$skip,$limit);

		// output the article
        static::render_main_articles_updated($xarticles,'category');


		}
		else
		{

		foreach (HomepagePlacement::make()->get() as $id)
		{
			// If we are at a position less than or = to skip number
			if ($skip<= $pos)
			{
				if (count($ids) < $limit) // If we are within our limit
				{
					$ids[] = $id;
					// Return details for post
					$query  = new WP_Query([
												'post__in'      => array($id),

										]);

					while ($query->have_posts()) {
						// Loop through posts
						$query->the_post();

						$post = get_post($id);

						// Setup the mark used flag for after
						$articles[] = [
							'post' => $post,
							'img'  => get_the_thumbnail('hero-image', 'hero-image', true, $post),
						];

						static::render_article('category');

					}

				}
			}

			$pos++;
		}

		Hamodia_homepage_model::set_articles_used($articles);
		}
*/
        //$articles = Hamodia_homepage_model::get_main_articles();

        //static::render_main_articles($articles);
    }

    public static function category($category, $articles = 4, $hero = true, $no_more_link = false)
       {
           static::display_heading($category);

           if ($hero) {
               static::display_hero($category);
           }

           static::display_posts($category, $articles);

   		if (!$no_more_link)
           	static::display_more_in_category_link($category);
    }

    public static function custom_category($post_type = '',$category_taxonomy = '',$articles = 4)
	{
	        static::display_custom_posts($post_type, $category_taxonomy, $articles);
    }

    public static function get_custom_category_post_count($post_type = '',$category_taxonomy = '')
    {
    	$query = new WP_Query( array( 'post_status' => 'publish','post_type' => $post_type, ) );
    	$count = $query->found_posts;
    	return $count;
    }

    public static function newsletter()
    {
    	if (!X2_IS_APP)
		{
        ?>
		<article id="homepage-newsletter">
			<h3>Choose your news!</h3>
			<p>
				Sign up for <b>HAMODIA'S</b><br>
				email newsletter today*
			</p>
			<form action="<?= get_site_url() ?>/standalone/newsletter.php" target="_blank" method="get">
				<input type="email" placeholder="Your Email Here" name="email" required>
				<button type="submit">Go</button>
			</form>
			<footer>*You can select which emails you'd like to receive.</footer>
		</article>
		<?php
		}

    }

    protected static function display_heading($category)
    {
        $category = Hamodia_homepage_model::get_category($category);

        static::render_heading($category);
    }

    protected static function display_hero($category)
    {
        $article = Hamodia_homepage_model::get_category_hero($category);

        static::render_hero($article, 'hero-medium');
    }

    protected static function display_posts($category, $articles)
	    {
	        $query = Hamodia_homepage_model::get_category_articles($category, $articles);

	        while ($query->have_posts()) {
	            $query->the_post();

	            static::render_article();
	        }
    } 

    protected static function display_custom_posts($post_type = '',$category_taxonomy = '',$articles = 2)
	{
	        $query  = new WP_Query([
			            'post_type'            => $post_type,
			            'post_status'        => 'publish',
			            'order'                => 'DESC',
			            'posts_per_page'    => $articles
        		]);

	        while ($query->have_posts()) {
	            $query->the_post();

	            static::render_article($category_taxonomy);
	        }
    }



    protected static function display_more_in_category_link($category)
    {
        $category = Hamodia_homepage_model::get_category($category);

        ?>
        <a href="<?= get_category_link($category) ?>" class="home-category-more-link">
            More in <?= $category->name ?> »
        </a>
        <?php
    }

    protected static function render_promotional(WP_Post $page)
    {
        ?>
        <div class="homepage-promotional">
            <?= $page->post_content ?>
        </div>
        <?php
    }

    protected static function render_main_articles($articles)
       {
           ?>
   		<div class="row home-row-main-primary">
   			<?php static::render_hero(array_shift($articles), 'hero-big'); ?>
   		</div>
   		<div class="row home-row-main-secondary">
   			<?php foreach ($articles as $article): ?>
   				<div class="four columns">
   					<?php static::render_hero($article, 'hero-small'); ?>
   				</div>
   			<?php endforeach; ?>
   		</div>
   		<?php
    }

     protected static function render_main_articles_updated($articles, $term_taxonomy = '')
	 {


	    foreach ($articles as $article)
	    {

			    	$post = $article['post'];
					$img  = $article['img'];

					if (is_array($img))
					{
						if (isset($img['img']) && $img['img'] != '')
						{
							$img = $img['img'];
						}
					}

					$image = str_replace('class="hero-image hero-image-size"','',$img);

					?>

					<a <?php if (trim($post->post_title) == '') {echo 'style="padding-bottom:0px !important;margin-bottom:0px !important;border-width:0px !important;"'; } ?> class="home-article-link <?php if ($image == '' ){echo "no_image";}?>" href="<?php echo get_the_permalink($post->ID); ?>">

					<?php

					if ($image != '' )
					{
					?>

					<div class="image_container" <?php if (trim($post->post_title) == '') {echo 'style="margin-bottom:0px !important;"'; } ?>>
						<?php echo $image; ?>
					</div>

					<?php
					}
				
					if ($term_taxonomy != '')
					{
						$tags_text = '';
						if ($term_taxonomy == 'category')
						{

							$terms = get_the_category($post->ID);

							if ( $terms ) {
								
								foreach( $terms as $tag ) {
									
									$tagname = $tag->name;
									
									if (trim(strtolower($tagname)) != 'general')
									{
										if (trim(strtolower($tagname)) == 'technology')
											$tagname = 'Health, Science & Technology';
									
										$tags_text .= '<span class="post_tag" data-url="/tag/'.$tag->slug.'">'. $tagname . '</span>';
									}
								}
								
							}

						}
						else
						{

							$terms = get_the_terms($post->ID, $term_taxonomy, '', ', ', '');

							if ( $terms ) {
								
								foreach( $terms as $tag ) {
									
									$tagname = $tag->name;
									
									if (trim(strtolower($tagname)) != 'general')
									{
										if (trim(strtolower($tagname)) == 'technology')
											$tagname = 'Health, Science & Technology';
									
										$tags_text .= '<span class="post_tag" data-url="/tag/'.$tag->slug.'">'. $tagname . '</span>';
									}
								}
								
							}

						}
						
						if ($tags_text != '')
							echo '<span class="post_tags">' . $tags_text . '</span>';
						
					}
					
		if (trim($post->post_title) != '')
		{
		?>
			<span class="home-article-title"><?php echo $post->post_title ?></span>
		
		<?php
		}
		?>
						
						<span class="author"><?php echo get_the_author($post->ID); ?></span>
						<?php
						$format = get_post_format($post->ID) ? : 'standard';
						if ($format != 'image')
						{
						?>
							<span><p><?php echo static::return_excerpt($post->post_content,get_the_permalink($post->ID)); ?></p></span>
						<?php
						}
						?>
					</a>
					<?php

	    }

    }

    protected static function return_excerpt($content = '',$link = '')
    {
    		$content = strip_shortcodes(wp_strip_all_tags($content));

			// We have to count the words
			$excerpt = explode(' ', $content);

			// if they're 30 or less, we don't need an ellipsis
			if (count($excerpt) <= 30) {
				return implode(' ', $excerpt);
			} else {
				$out = implode(' ', array_slice($excerpt, 0, 30)).'&hellip;</p>';
				if (! is_front_page()) {
					$out .= '<p class="read-more"><a href="'.$link.'">Read more &raquo;</a></p>';
				}
				return $out;
		}
    }

    protected static function render_heading($category)
    {
        ?>
		<h2 class="home-category-heading">
			<a href="<?= get_category_link($category) ?>" class="home-category-link">
				<?= static::get_category_title($category) ?>
			</a>
		</h2>
		<?php
    }

    protected static function get_category_title($category)
    {
        return $category->name == 'Technology' ? 'Health, Science & Technology' : $category->name;
    }

    protected static function render_hero($article, $class = null)
    {
        $post = $article['post'];
        $img  = $article['img'];

        $attributes = static::get_hero_attributes($post, $img, $class);

        ?>
		<a <?= $attributes ?>>
			<h2 class="hero-title"><?= get_the_title($post) ?></h2>
			<?= $img['img'] ?>
		</a>
		<?php
    }

    protected static function get_hero_attributes($post, $img, $class)
    {
        if (has_post_format('gallery', $post)) {
            $class .= ' gallery-format';
        }

        return static::attributes([
            'href'  => get_permalink($post),
            'style' => 'padding-bottom: 55.921%',
            'class' => 'hero '.$class,
        ]);
    }

    protected static function render_article($term_taxonomy = '')
    {
    	$post = $article['post'];
		$img  = $article['img'];
		$image = get_the_thumbnail('category-listing-image');
		$mobile_image = get_the_thumbnail('hero-image');

		// do we have the image.. if not lets display the large one to save outputing full res...
		if (stripos($image,'760x760') === false)
			$image = get_the_thumbnail('large');

		if (is_array($image))
		{
			if (isset($image['img']) && $image['img'] != '')
			{
				$image = $image['img'];
			}
		}


		if (is_array($mobile_image))
		{
			if (isset($mobile_image['img']) && $mobile_image['img'] != '')
			{
				$mobile_image = $mobile_image['img'];
			}
		}


		?>

		<a class="home-article-link <?php if ($image == '' ){echo "no_image";}?>" href="<?php the_permalink(); ?>">

		<?php

		if ($image != '' )
		{
		?>

		<div class="image_container">
			<?php echo $image; ?>
			<div style="display:none" class="mobile_image">
				<?php echo $mobile_image; ?>
			</div>
		</div>

		<?php
		}

		if ($term_taxonomy != '')
		{
			if ($term_taxonomy == 'category')
			{

				$terms = get_the_category(get_the_ID());

				if ( $terms ) {
					echo '<span class="post_tags">';
					foreach( $terms as $tag ) {
						if (trim(strtolower($tag->name)) != 'general')
							echo '<span class="post_tag" data-url="/tag/'.$tag->slug.'">'. $tag->name . '</span>';
					}
					echo '</span>';
				}

			}
			else
			{

				$terms = get_the_terms(get_the_ID(), $term_taxonomy, '', ', ', '');

				if ( $terms ) {
					echo '<span class="post_tags">';
					foreach( $terms as $tag ) {
						if (trim(strtolower($tag->name)) != 'general')
							echo '<span class="post_tag" data-url="/tag/'.$tag->slug.'">'. $tag->name . '</span>';
					}
					echo '</span>';
				}

			}
		}

		?>
			<span class="home-article-title"><?php the_title(); ?></span><span class="author"><?php the_author(); ?></span><span><?php 
			$format = get_post_format(get_the_ID()) ? : 'standard';
			if ($format != 'image')
				the_excerpt();
			?></span>
		</a>
		<?php
    }

    protected static function attributes(array $attributes)
    {
        $output = [];

        foreach ($attributes as $key => $value) {
            $output[] = $key.'="'.esc_attr($value).'"';
        }

        return implode(' ', $output);
    }
}




class Hamodia_homepage_model
{
    protected static $used = [];

    protected static $promotional_title = 'Homepage Promotional';

    protected static $ids = [
        'business'   => 15,
        'community'  => 49,
        'editorial'  => 18,
        'israel'     => 14,
        'markets'    => 4524,
        'national'   => 48,
        'opinion'    => 17,
        'politics'   => 7,
        'regional'   => 29,
        'technology' => 629,
        'world'      => 6,
		'coronavirus'      => 13027,
    ];

    public static function get_promotional()
    {
        $page = get_page_by_path(static::$promotional_title);

        return $page && $page->post_status == 'publish' ? $page : null;
    }

    public static function set_articles_used($articles = array())
    {
    	foreach ($articles as $article)
    	{
    		static::mark_used($article['post']);
    	}
    }

    public static function get_main_articles($count = 4)
    {
        $articles = [];

        foreach (HomepagePlacement::make()->get() as $id) {
            $post = get_post($id);

			// Check we dont have a scheduled post.
			if (time() >= strtotime($post->post_date))
			{
	            $articles[] = [
                	'post' => $post,
	                'img'  => get_the_thumbnail('hero-image', 'hero-image', true, $post),
            	];
            }
        }


       if (count($articles) != $count) {
            foreach ($articles as $article) {
                static::mark_used($article['post']);
            }

           $articles = array_merge($articles, static::get_posts_with_image(
                null, 'hero-image', 'hero-image', $count - count($articles), $count
            ));

        }

		// restrict the array based on our size requirement
        $articles = array_slice($articles,0,$count);

        foreach ($articles as $article) {
            static::mark_used($article['post']);
        }

        return $articles;
    }

    public static function get_category($category)
    {
        return get_category(static::$ids[$category]);
    }

    public static function get_category_hero($category)
    {
        if ($sticky = static::get_category_sticky($category)) {
            $article = [
                'post' => $sticky,
                'img'  => get_the_thumbnail('hero-image', 'hero-image', true, $sticky),
            ];
        } else {
            $article = static::get_first_post_with_image(
                $category, 'hero-image', 'hero-image'
            );
        }

        static::mark_used($article['post']);

        return $article;
    }

    public static function get_category_articles($category, $limit)
    {
        $query = static::query($category, $limit);

        static::mark_used($query);

        return $query;
    }

    protected static function get_category_sticky($category)
    {
        $stickies = (array) get_option('sticky_posts');

        if (! $stickies) {
            return null;
        }

        $query = static::query($category, 1, ['post__in' => $stickies]);

        return $query->post_count ? $query->posts[0] : null;
    }

    protected static function get_first_post_with_image($category, $size, $class)
    {
        $posts = static::get_posts_with_image($category, $size, $class, 1);

        if (count($posts)) {
            return $posts[0];
        }
    }

    protected static function get_posts_with_image($category, $size, $class, $count = 1, $article_count = 1)
    {
        $found = [];

        list($offset, $limit) = [0, $article_count];
        $safety = 5;
        $counter = 0;

        do {
        	$counter++;
            $query = static::query($category, $limit, compact('offset'));

            while ($query->have_posts()) {
                $query->the_post();

                if ($img = get_the_thumbnail($size, $class, true)) {
                    $post = $query->posts[$query->current_post];

                    $found[] = compact('img', 'post');
                }

                if (count($found) == $count) {
                    return $found;
                }
            }

            $offset += $limit;
        } while (count($query->posts) && $safety > $counter);

        return $found;
    }

    protected static function query($category, $limit = 3, $options = [])
    {
        return new WP_Query($options + [
            'posts_per_page' => $limit,
            'post_status'    => 'publish',
            'cat'            => $category ? static::$ids[$category] : null,
            'post__not_in'   => static::$used,
        ]);
    }

    protected static function mark_used($query)
    {
        if (! $query instanceof WP_Query) {
            static::$used[] = $query->ID;

            return;
        }

        foreach ($query->posts as $post) {
            static::$used[] = $post->ID;
        }
    }
}
