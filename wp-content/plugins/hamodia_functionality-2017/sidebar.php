<?php
 
class Hamodia_sidebar
{
    public static function callathon()
    {
        ?>
            <a href="<?php echo get_page_link(94); ?>"
               class="sidebar-module"
               style="
                    background: url(/wp-content/themes/hamodia-sept17/images/callathon-chanukah-2016/sidebar.jpg) no-repeat center center;
                    display: block;
                    text-indent: -999px;
                    overflow: hidden;
                    font-size: 0;
                    padding-top: 158%;
                    background-size: contain;
               "
            >Subscribe to the Hamodia print edition</a>
        <?php
    }

    public static function more_in_current_category()
    {
        if (get_post_type() == 'letter') {
            return;
        }

        self::more_in_category_template(
            Hamodia_sidebar_model::more_in_current_category(),
            'More in'
        );
    }

    public static function more_in_categories()
    {
        if (! $more_in_categories = Hamodia_sidebar_model::more_in_categories()) {
            return;
        }

        foreach ($more_in_categories as $data) {
            self::more_in_category_template($data);
        }
    }

    public static function daily_paper()
    {
        $data = Hamodia_sidebar_model::daily_paper();

        ?>
        <h3 style="margin-top:30px">Digital Archive</h3>
        <div class="sidebar-module" id="daily-paper-module2">

                <a class="todays-edition2" href="<?= $data['todays_link'] ?>" target="_blank"><img src="<?= bloginfo('template_url') . '/images/digital-edition-banner.png' ?>" alt="Digital Edition Banner" /></a>
                <p><a class="todays-edition2" href="<?= $data['todays_link'] ?>" target="_blank">Read Today's Print Edition</a>
                	<a class="more-bar" href="<?= $data['link'] ?>">Print Archive ></a>
                </p>

        </div>
        <?php
    }

    public static function newsletter()
    {
        ?>
        <article class="sidebar-module" id="newsletter-sidebar">
            <h3>Sign Up For Breaking News & Special Offers Email</h3>
            <form action="<?= get_site_url() ?>/standalone/newsletter.php" target="_blank" method="get">
                <input type="email" placeholder="Your Email Here" name="email" required>
                <button type="submit">Go</button>
            </form>
        </article>
        <?php

    }

    public static function weather()
    {
        
		if ((defined('X2_IS_APP') && X2_IS_APP == false) || !defined('X2_IS_APP'))
		{
        ?>
		
		<div class="sidebar-module info-module" id="weather-module">
            <h3>Weather</h3>
			
			<?php
			//echo do_shortcode('[awesome-weather units="auto" hide_attribution="1" use_user_location="1" allow_user_to_change="1" size="tall" forecast_days="3" background_by_weather="1" inline_style="width:100%"]');
			//echo do_shortcode('[wpc-weather id="147963"/]');
			//<iframe src="/weather-test?app=true" style="width:100%;height:100%;">
			?>
			
			<div class="weather_container" style="width:100%;height:290px;">
				<iframe src="about:blank" style="width:100%;height:100%;">
				</iframe> 
			</div>
        </div>
        <?php
		}
    }

    public static function currency()
    {
        if (! $data = Hamodia_sidebar_model::currency()) {
            return;
        }

        ?>
        <a class="sidebar-module info-module" id="currency-module" href="<?= $data['page_link'] ?>">
            <h3>Currency</h3>

            <ul>
            <?php foreach ($data['currencies'] as $key): ?>
                <li>
                    <abbr title="<?= $data['currency']['currencies'][$key] ?>">
                        <span class="flag flag-<?= $key ?>"></span>
                        <span class="country"><?= $key ?></span>
                        <span class="rate"><?= $data['currency']['rates'][$key] ?></span>
                    </abbr>
                </li>
            <?php endforeach; ?>
            </ul>

            <p class="more-text">More Currency <span class="fa fa-angle-right"></span></p>
        </a>
        <?php
    }

    public static function sidebar_ad_single($number = 0)
    {
    	echo '<div class="xright_col_block" style="text-align:center;">';
		
		// 3 : 1, 4 : 2, 5 : 3, 6 : 4
		/*switch ($number)
		{
			case 3 : echo "<!-- /21739840349/rightcolumn1-200x200 -->
				<div class='gpt-ad' id='div-gpt-ad-1535391872406-0'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1535391872406-0'); });
				</script>
				</div>";
				break;
			case 4 : echo "<!-- /21739840349/rightcol2-200x200 -->
				<div class='gpt-ad'  id='div-gpt-ad-1535392012584-0'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1535392012584-0'); });
				</script>
				</div>";
				break;
			case 5 : echo "<!-- /21739840349/rightcol3-200x200 -->
				<div class='gpt-ad'  id='div-gpt-ad-1535392059431-0'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1535392059431-0'); });
				</script>
				</div>";
				break;
			case 6 : echo "<!-- /21739840349/rightcol4-200x200 -->
				<div class='gpt-ad' id='div-gpt-ad-1535392189194-0'>
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1535392189194-0'); });
				</script>
				</div>";
				break;			
			
		}*/
		
		switch ($number)
		{
			case 3 : echo "<!-- /21739840349/new_right_col_1_200_200 -->
<div class='gpt-ad' id='div-gpt-ad-1571615207403-0' style='width: 200px; height: 200px;'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1571615207403-0'); });
  </script>
</div>
";
				break;
			case 4 : echo "<!-- /21739840349/new_right_column_2_200_200 -->
<div class='gpt-ad' id='div-gpt-ad-1571615359407-0' style='width: 200px; height: 200px;'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1571615359407-0'); });
  </script>
</div>
";
				break;
			case 5 : echo "<!-- /21739840349/new_right_column_3_200_200 -->
<div class='gpt-ad' id='div-gpt-ad-1571615575865-0' style='width: 200px; height: 200px;'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1571615575865-0'); });
  </script>
  </div>";
				break;
			case 6 : echo "<!-- /21739840349/new_right_column_4_200_200 -->
<div class='gpt-ad' id='div-gpt-ad-1571615782942-0' style='width: 200px; height: 200px;'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1571615782942-0'); });
  </script>
</div>";
				break;			
			
		}
		
    	
      	echo '</div>';
    }

     public static function sidebar_ads_123()
	    {
	    ?><div class="xright_col_block">
	    	<?php if(function_exists('oiopub_banner_zone')) oiopub_banner_zone(3, 'center'); ?>
	    </div>
	    <div class="xright_col_block">
	    	<?php if(function_exists('oiopub_banner_zone')) oiopub_banner_zone(4, 'center'); ?>
	    <div class="xright_col_block" style="margin-bottom:35px !important">
	    	<?php if(function_exists('oiopub_banner_zone')) oiopub_banner_zone(5, 'center'); ?>
	    </div>
	    <?php
    }

    public static function sidebar_ads_456()
	    {
	    ?><div class="xright_col_block" style="margin-top:0px !important;">
	    	<?php if(function_exists('oiopub_banner_zone')) oiopub_banner_zone(6, 'center'); ?>
	    </div>
	    <div class="xright_col_block">
	    	<?php if(function_exists('oiopub_banner_zone')) oiopub_banner_zone(7, 'center'); ?>
	    <div class="xright_col_block">
	    	<?php if(function_exists('oiopub_banner_zone')) oiopub_banner_zone(8, 'center'); ?>
	    </div>
	    <?php
    }

    public static function stocks($search_only = false)
    {
        $data = Hamodia_sidebar_model::stocks(! $search_only);

        if ($search_only): ?>
            <div class="sidebar-module info-module" id="stocks-module" >

                <h3>Market Watch</h3>
                <form class="compact" action="<?= $data['page_link'] ?>" method="get">
                    <input type="text" name="symbol" placeholder="Enter symbol" />
                    <input type="submit" value="Go" />
                </form>

                <?php if (! is_page('stocks')): ?>
                    <a class="more-bar" href="<?= $data['page_link'] ?>">See all stocks »</a>
                <?php endif; ?>

            </div>
            <?php return; ?>
        <?php endif; ?>


        <div id="stocks-module" class="sidebar-module info-module" style="margin-bottom:0;">
		
			<h3><!--<a  href="<?= $data['page_link'] ?>">-->Market Watch<!--</a>--></h3>
			
			<iframe id="sidebar_stocks" frameborder="0" class="stockdio_market_overview" scrolling="no" width="100%" src="https://api.stockdio.com/visualization/financial/charts/v1/marketoverview?app-key=090E5F415E9A45369C55F1DE5301FDDD&wp=1&addVolume=false&showUserMenu=false&culture=English-US&motif=Financial&logoMaxHeight=20&logoMaxWidth=90&includeEquities=true&includeIndices=true&includeCommodities=false&includeCurrencies=false&includeLogo=false&includeEquitiesSymbol=true&includeEquitiesName=false&includeIndiceSymbol=false&includeIncidesName=false&includeCommoditiesSymbol=false&includeCommoditiesName=true&includeCurrenciesSymbol=true&includeCurrenciesName=false&allowSort=true&includePrice=true&includeChange=true&includePercentChange=true&includeTrend=true&includeVolume=false&showHeader=false&showBorderAndTitle=false&width=100%25&intraday=true&onload=marketoverview1&linkUrl=http://hamodia.com/stocks/?symbol={symbol}&linkTarget=self" height="183"></iframe>
			
			<script>
			jQuery('#sidebar_stocks').on('load',function () {
				
			   if (typeof(stockdio_events) == "undefined") {
				  stockdio_events = true;
				  var stockdio_eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
				  var stockdio_eventer = window[stockdio_eventMethod];
				  var stockdio_messageEvent = stockdio_eventMethod == "attachEvent" ? "onmessage" : "message";
				  stockdio_eventer(stockdio_messageEvent, function (e) {					 
					 if (typeof(e.data) != "undefined" && typeof(e.data.method) != "undefined") {
						eval(e.data.method);
					 }
				  },false);
			   }
			});
			</script>
			
		</div>
        <?php
    }

    public static function page_links()
    {
        ?>
        <h3 class="sidebar_title">Community Notices</h3>
        <div class="community_notices">
	        <a target="_blank" class="sidebar-module sidebar-zmanim" href="<?= get_page_link(232) ?>">Hadlakas Neiros</a>
        	<a target="_blank" class="sidebar-module sidebar-shiurim" href="<?= get_page_link(234) ?>">Shiurim</a>
	        <a target="_blank" class="sidebar-module sidebar-simchos" href="<?= get_page_link(1554) ?>">Simchas</a>
        	<a target="_blank" class="sidebar-module sidebar-classifieds" href="<?= get_page_link(9624) ?>">Classifieds</a>
	      <!--  <a class="more-bar more-bar-classifieds" href="<?= get_page_link(6161) ?>">Submit your classified ad »</a>-->
	     </div>
        <?php
    }

    public static function switching_boxes()
       {
           extract(Hamodia_sidebar_model::switching_boxes());

           ?>
           <div class="switching-box sidebar-module">

               <h3>OP-ED</h3>
               <?php self::switching_boxes_template($oped, 'sidebar-op-ed'); ?>

               <h3>Features</h3>
               <?php self::switching_boxes_template($features, 'sidebar-latest-features'); ?>

           </div>
           <?php
       }

     public static function sidebar_articles($show_oped = true,$show_features = false,$show_columns = false)
	{
		extract(Hamodia_sidebar_model::switching_boxes());

		?>
		<?php if ($show_oped): ?>
				<h3><a href="/category/op-ed/">OP-ED</a></h3>
				<div class="sidebar_articles sidebar-module">
					<?php self::sidebar_article_template($oped, 'sidebar-op-ed'); ?>
        <?php endif; ?>

        <?php if ($show_features): ?>
			<h3><a href="<?= get_post_type_archive_link('hamodia_feature') ?>">Columns</a></h3>
				<div class="sidebar_articles sidebar-module">
					<?php self::sidebar_article_template($features, 'sidebar-latest-features'); ?>
						
        <?php endif; ?>

        <?php if ($show_columns): ?>
				<h3><a href="/category/features/">Features</a></h3>
			<div class="sidebar_articles sidebar-module">
			<?php self::sidebar_article_template($columns, 'sidebar-latest-columns'); ?>	
			
        <?php endif; ?>

		</div>
		<?php
	}


	private static function switching_boxes_template($query, $id)
	    {
	        ?>
	        <ul id="<?= $id ?>">
	            <?php while ($query->have_posts()) : $query->the_post(); ?>
	                <li>
	                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	                </li>
	            <?php endwhile; ?>
	        </ul>
	        <?php
    }

    private static function sidebar_article_template($query, $id)
    {
    	while ($query->have_posts()) : $query->the_post();

    			$image = get_the_thumbnail('large');
    	?>
    					<a class="home-article-link" href="<?php the_permalink(); ?>">
    					<?php
						if ($image != '' )
						{
						?>

						<div class="image_container">
							<?php echo $image; ?>
						</div>

						<?php
						}
						?>
	                    <span class="home-article-title"><?php the_title(); ?></span><span class="author"><?php the_author(); ?></span><span><p><?php the_excerpt(); ?></p></span>
	                    </a>

        <?php endwhile;
    }

    private static function more_in_category_template($data, $title_text = '')
    {
		if (isset($data['term']) && $data['term'] != '')
		{
        ?>
        <div class="more-articles">

            <h3><?= $title_text ?>
                <a href="<?= get_term_link($data['term']->slug, $data['taxonomy']) ?>">
                    <?= $data['term']->name ?>
                </a>
            </h3>

            <ul>
                <?php while ($data['query']->have_posts()) : $data['query']->the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                        </a>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
        <?php
		}
    }

    public static function news_tip()
    {
        ?>
        <article>
            <a href="<?= get_site_url() ?>/standalone/news-tip.php" target="_blank">
                <h3 style="line-height:135%">Got a news tip, picture or story to share? Click here</h3>
                
            </a>
        </article>
        <?php
    }
}













class Hamodia_sidebar_model
{
    private static $category_ids = [
        'world'      => 6,
        'politics'   => 7,
        'business'   => 15,
        'regional'   => 29,
        'israel'     => 14,
        'opinion'    => 17,
        'editorial'  => 18,
        'national'   => 48,
        'community'  => 49,
        'markets'    => 4524,
        'technology' => 629,
    ];

    public static function more_in_current_category()
    {
        global $post;

        $data = self::get_term_and_taxonomy();

        if ($data['taxonomy'] == 'category') {
            $post_type  = 'post';
            $taxonomy   = 'category';
            $query_key  = 'category_name';
            $query_val  = $data['term']->slug;
        } else {
            $post_type = 'hamodia_feature';
            $taxonomy  = 'feature_category';
            $query_key = $taxonomy;
            $query_val = $data['term']->slug;
        }

        return [
            'term'     => $data['term'],
            'taxonomy' => $taxonomy,
            'query'    => new WP_Query([
                $query_key       => $query_val,
                'post_type'      => $post_type,
                'post__not_in'   => (array) $post->ID,
                'post_status'    => 'publish',
                'order'          => 'DESC',
                'posts_per_page' => 5.
            ]),
        ];
    }

    public static function daily_paper()
    {
        global $post;

        $out = [
            'link' => get_post_type_archive_link('digital_edition'),
            'todays_link' => '#',
        ];

        // TODO: There's a bug in this query that somehow
        // has the SQL query run with LIMIT set to 50 (you can
        // see the raw SQL by calling `$query->result`).
        // This bug is not critical, since we just use the
        // first post returned, but it's a waste of resources.
        $query = new WP_Query(array(
            'post_type'      => 'digital_edition',
            'posts_per_page' => 1,
            'orderby'        => 'date',
            'order'          => 'DESC'
        ));

        if ($query->have_posts()) {
            $query->the_post();
            $out['todays_link'] = get_permalink();
        }

        return $out;
    }

    public static function weather()
    {
        $weather = Hamodia_APIs::get_weather_info();

        $out = [];

        $out['page_link'] = get_page_link(get_page_by_title('Weather')->ID);
        $out['temp']      = $weather['current']['temp_f'];
        $out['condition'] = $weather['current']['condition'];
        $out['icon_url']  = $weather['current']['icon'];

        return $out;
    }

    public static function currency()
    {
        $currency_page = get_page_by_title('Currency Exchange Rates');

        if (! $currency_page || is_page($currency_page->ID)) {
            return false;
        }

        return [
            'currency'   => Hamodia_APIs::get_currency_info(),
            'currencies' => array('EUR', 'GBP', 'ILS', 'CHF'),
            'page_link'  => get_page_link($currency_page->ID)
        ];
    }

    public static function stocks($get_stocks = true)
    {
        $out = [
            'page_link' => get_page_link('207')
        ];

        if ($get_stocks) {
            $out['stocks'] = Hamodia_APIs::get_stocks_info([
                '^DJI',
                '^IXIC',
                '^GSPC'
            ]);
        }

        return $out;
    }

    public static function switching_boxes()
    {
        $oped = new WP_Query([
            'post_status'        => 'publish',
            'order'                => 'DESC',
            'posts_per_page'    => 2,
            'cat'                => 16,
        ]);

        $features = new WP_Query([
	               'post_type'            => 'hamodia_feature',
	               'post_status'        => 'publish',
	               'order'                => 'DESC',
	               'posts_per_page'    => 2
        ]);

         $columns = new WP_Query([
		            'post_type'            => 'hamodia_column',
		            'post_status'        => 'publish',
		            'order'                => 'DESC',
		            'posts_per_page'    => 2
        ]);

        return compact('oped', 'features', 'columns');
    }

    public static function more_in_categories()
    {
        $data = self::get_term_and_taxonomy();
        $out = [];

        if (! isset($data['taxonomy']) || $data['taxonomy'] == 'category') {
            $top_categories = ['business', 'israel', 'world'];

            if (isset($data['term'])) {
                // Remove the current category from the array
                $top_categories = array_diff($top_categories, [$data['term']->slug]);
            }

            // but we only want the first 2 top categories
            $top_categories = array_slice($top_categories, 0, 2);

            foreach ($top_categories as $category) {
                $out[] = [
                    'taxonomy' => 'category',
                    'term'     => get_term_by('slug', $category, 'category'),
                    'query'    => new WP_Query([
                        'category_name'  => $category,
                        'post_status'    => 'publish',
                        'order'          => 'DESC',
                        'posts_per_page' => 5
                    ]),
                ];
            }

            return $out;
        }
    }

    public static function get_term_and_taxonomy()
    {
        global $post;

        $out = [];
        $post_type = get_post_type();

        switch ($post_type) {
            case 'post':
                $categories = get_the_category();

                $out['taxonomy'] = 'category';
                $out['term']     = $categories[0];
                break;

            case 'hamodia_feature':
                $terms = get_the_terms($post->ID, 'feature_category');

                $out['taxonomy'] = 'feature_category';
                $out['term']     = array_shift($terms);
                break;
        }

        return $out;
    }
}
