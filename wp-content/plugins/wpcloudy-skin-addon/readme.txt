=== WP Cloudy Skin Add-on ===
Contributors: rainbowgeek
Donate link: http://wpcloudy.com/
Tags: weather, forecast
Requires at least: 3.8
Tested up to: 4.5
Stable tag: 2.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

WP Cloudy Skin Add-on provides 12 weather skins + 2 themes for WP Cloudy plugin (required).

== Installation ==

1. Upload 'wpcloudy-skin-addon' to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Click on the custom post type called Weather, edit a weather and choose a skin.

== Frequently Asked Questions ==

= Any questions? =

All the answers are on our site: http://wpcloudy.com/

== Changelog ==
= 2.3 =
* NEW Forecast precipitation
= 2.2 =
* NEW Weather now automatically adapts to its parent container (if Fluid design option enabled)
* INFO Improve responsive
* FIX Z-index bug with Theme2
= 2.1 =
* FIX CSS iconvault font
* FIX Duplicate unit if Show unit options is enabled
= 2.0.2 =
* FIX Forecast and Today now translatable 
= 2.0.1 = 
* FIX Missing weather condition code for automatic background
= 2.0 =
* NEW Automatic background image based on weather
* FIX WP Skins Admin CSS in BE
* FIX Dashboard weather widgets CSS/JS loading
= 1.9 =
* FIX Warning/Fatal error in [weather_db]
= 1.8.1 =
* INFO Loading CSS
= 1.8 = 
* INFO Updated for WP Cloud 3.3 
= 1.7 = 
* NEW Responsive Design for Skins and Themes
* FIX Undefined offset
= 1.6 = 
* FIX Font Iconvault in WordPress Dashboard
= 1.5 = 
* NEW Forecast font available
= 1.4 = 
* FIX CSS
= 1.3 = 
* NEW theme
= 1.2 =
* Add precipitation
* FIX Theme 1
= 1.1 =
* New theme
= 1.0 =
* Stable release.

