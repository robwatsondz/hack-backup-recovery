<?php
/*
Plugin Name: WP Cloudy Skin Addon
Plugin URI: http://wpcloudy.com/
Description: WP Cloudy Skin Add-on provides 12 skins + 2 themes for WP Cloudy plugin (required).
Version: 2.3
Author: Benjamin DENIS
Author URI: http://wpcloudy.com/
License: GPLv2
*/

/*  Copyright 2014 - 2016  Benjamin DENIS  (email : contact@wpcloudy.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// To prevent calling the plugin directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Please don&rsquo;t call the plugin directly. Thanks :)';
	exit;
}

function weather_skin_activation() {
}
register_activation_hook(__FILE__, 'weather_skin_activation');
function weather_skin_deactivation() {
}
register_deactivation_hook(__FILE__, 'weather_skin_deactivation');

load_plugin_textdomain('wpcloudy', false, basename( dirname( __FILE__ ) ) . '/lang' );

///////////////////////////////////////////////////////////////////////////////////////////////////
//Translation
///////////////////////////////////////////////////////////////////////////////////////////////////

function wpcloudy_skin_init() {
  load_plugin_textdomain( 'wpcloudy', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' ); 
}
add_action('plugins_loaded', 'wpcloudy_skin_init');

///////////////////////////////////////////////////////////////////////////////////////////////////
//Load Weather Background Image
///////////////////////////////////////////////////////////////////////////////////////////////////

add_action( 'wp_enqueue_scripts', 'wpcloudy_weather_bg_img', 10 );
add_action( 'admin_enqueue_scripts', 'wpcloudy_weather_bg_img', 10, 1 );
function wpcloudy_weather_bg_img() {
	 wp_register_style('wpcloudy-skin-addon-weather-bg-img', plugins_url('css/wpcloudy-weather-bg-img.css', __FILE__), array(), 'v1.0', false);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//Load Icons Pack Fonts
///////////////////////////////////////////////////////////////////////////////////////////////////

add_action( 'wp_enqueue_scripts', 'wpcloudy_icons_pack', 10 );
add_action( 'admin_enqueue_scripts', 'wpcloudy_icons_pack', 10, 1 );
function wpcloudy_icons_pack() {
		wp_register_style('wpcloudy-skin-addon', plugins_url('css/wpcloudy-skin-addon.css', __FILE__), array(), 'v1.0', false);
		//wp_register_style('wpcloudy-skin-addon-forecast', plugins_url('css/wpcloudy-skin-addon-forecast.css', __FILE__), array(), 'v1.0', false);
		wp_register_style('wpcloudy-forecast-icons', plugins_url('css/wpcloudy-forecast-icons.css', __FILE__), array(), 'v1.0', false);
		
		$wpc_today = '"'.__('today','wpcloudy').'"';
		$wpc_forecast = '"'.__('forecast','wpcloudy').'"';
		$wpcloudy_skin_translate_css = "
	        #wpc-weather.skin1 .hours:before,
	        #wpc-weather.skin2 .hours:before,
	        #wpc-weather.skin3 .hours:before,
	        #wpc-weather.skin4 .hours:before,
	        #wpc-weather.skin5 .hours:before,
	        #wpc-weather.skin6 .hours:before,
	        #wpc-weather.skin7 .hours:before,
	        #wpc-weather.skin8 .hours:before,
	        #wpc-weather.skin9 .hours:before,
	        #wpc-weather.skin10 .hours:before {
			    content: {$wpc_today};
			}
			#wpc-weather.skin1 .forecast:before,
			#wpc-weather.skin2 .forecast:before,
			#wpc-weather.skin3 .forecast:before,
			#wpc-weather.skin4 .forecast:before,
			#wpc-weather.skin5 .forecast:before,
			#wpc-weather.skin6 .forecast:before,
			#wpc-weather.skin7 .forecast:before,
			#wpc-weather.skin8 .forecast:before, 
			#wpc-weather.skin9 .forecast:before, 
			#wpc-weather.skin10 .forecast:before {
			   content: {$wpc_forecast};
			}
			";
        wp_add_inline_style( 'wpcloudy-skin-addon', $wpcloudy_skin_translate_css );
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//Enqueue JS/CSS
///////////////////////////////////////////////////////////////////////////////////////////////////

$my_weather 			= get_option( 'wpc_dashboard_widget_option' );
if ($my_weather != null) {
	$wpcloudy_skin_db 		= get_post_meta($my_weather['weather_db'],'_wpcloudy_skin',true);

	function wpcloudy_skin_styles_db() {
		wp_register_style('wpcloudy-skin-addon', plugins_url('css/wpcloudy-skin-addon.css', __FILE__));
		wp_enqueue_style('wpcloudy-skin-addon');	
		wp_register_style('wpcloudy-forecast-addon', plugins_url('css/wpcloudy-forecast-icons.css', __FILE__));
		wp_enqueue_style('wpcloudy-forecast-addon');	

		$wpc_image_bg_value = get_post_meta($id,'_wpcloudy_image_bg',true);
			
		if ($wpc_image_bg_value == 'yes') {
			wp_enqueue_style('wpcloudy-skin-addon-weather-bg-img');
		}

		$wpc_icons_pack_value = get_post_meta($id,'_wpcloudy_icons',true);
		$wpc_skin_value = get_post_meta($id,'_wpcloudy_skin',true);

		if ($wpc_icons_pack_value == 'default' && $wpc_skin_value !='default') {
			wp_enqueue_style('wpcloudy-skin-addon');
		}

		if ($wpc_icons_pack_value == 'forecast_font') {
			wp_enqueue_style('wpcloudy-skin-addon');
			wp_enqueue_style('wpcloudy-forecast-icons');
		}
	}

	//Dashboard
	if ($wpcloudy_skin_db !== 'default') {
		add_action('admin_head-index.php', 'wpcloudy_skin_styles_db');
	}
	//CSS Admin
	function wpc_add_admin_addon_scripts( $hook ) {
	
	global $post;
		
		if ( $hook == 'post-new.php' || $hook == 'post.php') {
			
			if ( 'wpc-weather' === $post->post_type) { 
				wp_register_style( 'wpcloudy-skins-admin', plugins_url('css/wpcloudy-skin-admin.css', __FILE__));
				wp_enqueue_style( 'wpcloudy-skins-admin' );
			}
		}
	}
	add_action( 'admin_enqueue_scripts', 'wpc_add_admin_addon_scripts', 10, 1 );
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//Load Google Webfonts
///////////////////////////////////////////////////////////////////////////////////////////////////

add_action( 'wp_enqueue_scripts', 'wpcloudy_google_fonts', 10 );

function wpcloudy_google_fonts() {
		 wp_register_style('Open Sans', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,700', array(), 'v1.0', false);
		 wp_register_style('Ubuntu', 'http://fonts.googleapis.com/css?family=Ubuntu', array(), 'v1.0', false);
		 wp_register_style('Lato', 'http://fonts.googleapis.com/css?family=Lato:400,300', array(), 'v1.0', false);
		 wp_register_style('Asap', 'http://fonts.googleapis.com/css?family=Asap:400,700', array(), 'v1.0', false);
		 wp_register_style('Exo 2', 'http://fonts.googleapis.com/css?family=Exo+2:400,700', array(), 'v1.0', false);
		 wp_register_style('Roboto', 'http://fonts.googleapis.com/css?family=Roboto:400,300', array(), 'v1.0', false);
	     wp_register_style('Source Sans Pro', 'http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300', array(), 'v1.0', false);
	     wp_register_style('Droid Serif', 'http://fonts.googleapis.com/css?family=Droid+Serif', array(), 'v1.0', false);
	     wp_register_style('Arvo', 'http://fonts.googleapis.com/css?family=Arvo', array(), 'v1.0', false);
	     wp_register_style('Bitter', 'http://fonts.googleapis.com/css?family=Bitter', array(), 'v1.0', false);
		 wp_register_style('Francois One', 'http://fonts.googleapis.com/css?family=Francois+One', array(), 'v1.0', false);
		 wp_register_style('Cabin', 'http://fonts.googleapis.com/css?family=Cabin', array(), 'v1.0', false);
	     wp_register_style('Nunito', 'http://fonts.googleapis.com/css?family=Nunito:400,300', array(), 'v1.0', false);
	     wp_register_style('Josefin Sans', 'http://fonts.googleapis.com/css?family=Josefin+Sans:400,300', array(), 'v1.0', false);
	     wp_register_style('Signika', 'http://fonts.googleapis.com/css?family=Signika:400,300', array(), 'v1.0', false);
	     wp_register_style('Merriweather Sans', 'http://fonts.googleapis.com/css?family=Merriweather+Sans:400,300', array(), 'v1.0', false);
		 wp_register_style('Tangerine', 'http://fonts.googleapis.com/css?family=Tangerine', array(), 'v1.0', false);
		 wp_register_style('Pacifico', 'http://fonts.googleapis.com/css?family=Pacifico', array(), 'v1.0', false);
	     wp_register_style('Inconsolata', 'http://fonts.googleapis.com/css?family=Inconsolata', array(), 'v1.0', false);
	     wp_register_style('Ribeye', 'http://fonts.googleapis.com/css?family=Ribeye', array(), 'v1.0', false);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//Display metabox in Weather Custom Post Type
///////////////////////////////////////////////////////////////////////////////////////////////////

add_action('add_meta_boxes','init_metabox_wpc_skin');
function init_metabox_wpc_skin(){ 
    add_meta_box('wpcloudy_skins', 'WP Cloudy Skins', 'wpcloudy_skins', 'wpc-weather', 'advanced');  
}

function wpcloudy_skins($post){
    $wpcloudy_skin 								= get_post_meta($post->ID,'_wpcloudy_skin',true);
	$wpcloudy_fonts								= get_post_meta($post->ID,'_wpcloudy_fonts',true);
	$wpcloudy_icons								= get_post_meta($post->ID,'_wpcloudy_icons',true);
	$wpcloudy_image_bg							= get_post_meta($post->ID,'_wpcloudy_image_bg',true);
	$wpcloudy_image_bg_cover					= get_post_meta($post->ID,'_wpcloudy_image_bg_cover',true);
	$wpcloudy_image_bg_position_horizontal		= get_post_meta($post->ID,'_wpcloudy_image_bg_position_horizontal',true);
	$wpcloudy_image_bg_position_vertical		= get_post_meta($post->ID,'_wpcloudy_image_bg_position_vertical',true);

	_e( 'Select a skin for your weather (all settings related to design will be bypassed): ', 'wpcloudy' );

	echo 
        '<p>
            <select name="wpcloudy_skin">
				<option ' . selected( 'default', $wpcloudy_skin, false ) . ' value="default">'. __( 'No Skin', 'wpcloudy' ) .'</option>
                <option ' . selected( 'skin1', $wpcloudy_skin, false ) . ' value="skin1">'. __( 'Skin 1', 'wpcloudy' ) .'</option>
                <option ' . selected( 'skin2', $wpcloudy_skin, false ) . ' value="skin2">'. __( 'Skin 2', 'wpcloudy' ) .'</option>
				<option ' . selected( 'skin3', $wpcloudy_skin, false ) . ' value="skin3">'. __( 'Skin 3', 'wpcloudy' ) .'</option>
				<option ' . selected( 'skin4', $wpcloudy_skin, false ) . ' value="skin4">'. __( 'Skin 4', 'wpcloudy' ) .'</option>
				<option ' . selected( 'skin5', $wpcloudy_skin, false ) . ' value="skin5">'. __( 'Skin 5', 'wpcloudy' ) .'</option>
				<option ' . selected( 'skin6', $wpcloudy_skin, false ) . ' value="skin6">'. __( 'Skin 6', 'wpcloudy' ) .'</option>
				<option ' . selected( 'skin7', $wpcloudy_skin, false ) . ' value="skin7">'. __( 'Skin 7', 'wpcloudy' ) .'</option>
				<option ' . selected( 'skin8', $wpcloudy_skin, false ) . ' value="skin8">'. __( 'Skin 8', 'wpcloudy' ) .'</option>
				<option ' . selected( 'skin9', $wpcloudy_skin, false ) . ' value="skin9">'. __( 'Skin 9', 'wpcloudy' ) .'</option>
				<option ' . selected( 'skin10', $wpcloudy_skin, false ) . ' value="skin10">'. __( 'Skin 10', 'wpcloudy' ) .'</option>
				<option ' . selected( 'theme1', $wpcloudy_skin, false ) . ' value="theme1">'. __( 'Theme 1', 'wpcloudy' ) .'</option>
				<option ' . selected( 'theme2', $wpcloudy_skin, false ) . ' value="theme2">'. __( 'Theme 2', 'wpcloudy' ) .'</option>
			</select>
        </p>';
	
	_e( 'Select a Google Webfont for your weather: ', 'wpcloudy' );

	echo 
        '<p>
            <select name="wpcloudy_fonts">
				<option ' . selected( 'default', $wpcloudy_fonts, false ) . ' value="default">'. __( 'No Webfonts', 'wpcloudy' ) .'</option>
                <option ' . selected( 'Open Sans', $wpcloudy_fonts, false ) . ' value="Open Sans">'. __( 'Open Sans', 'wpcloudy' ) .'</option>
                <option ' . selected( 'Ubuntu', $wpcloudy_fonts, false ) . ' value="Ubuntu">'. __( 'Ubuntu', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Asap', $wpcloudy_fonts, false ) . ' value="Asap">'. __( 'Asap', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Lato', $wpcloudy_fonts, false ) . ' value="Lato">'. __( 'Lato', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Oswald', $wpcloudy_fonts, false ) . ' value="Oswald">'. __( 'Oswald', 'wpcloudy' ) .'</option>
                <option ' . selected( '\'Exo 2\'', $wpcloudy_fonts, false ) . ' value="\'Exo 2\'">'. __( 'Exo 2', 'wpcloudy' ) .'</option>
                <option ' . selected( 'Roboto', $wpcloudy_fonts, false ) . ' value="Roboto">'. __( 'Roboto', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Source Sans Pro', $wpcloudy_fonts, false ) . ' value="Source Sans Pro">'. __( 'Source Sans Pro', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Droid Serif', $wpcloudy_fonts, false ) . ' value="Droid Serif">'. __( 'Droid Serif', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Arvo', $wpcloudy_fonts, false ) . ' value="Arvo">'. __( 'Arvo', 'wpcloudy' ) .'</option>
                <option ' . selected( 'Bitter', $wpcloudy_fonts, false ) . ' value="Bitter">'. __( 'Bitter', 'wpcloudy' ) .'</option>
                <option ' . selected( 'Francois One', $wpcloudy_fonts, false ) . ' value="Francois One">'. __( 'Francois One', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Nunito', $wpcloudy_fonts, false ) . ' value="Nunito">'. __( 'Nunito', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Josefin Sans', $wpcloudy_fonts, false ) . ' value="Josefin Sans">'. __( 'Josefin Sans', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Signika', $wpcloudy_fonts, false ) . ' value="Signika">'. __( 'Signika', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Merriweather Sans', $wpcloudy_fonts, false ) . ' value="Merriweather Sans">'. __( 'Merriweather Sans', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Tangerine', $wpcloudy_fonts, false ) . ' value="Tangerine">'. __( 'Tangerine', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Pacifico', $wpcloudy_fonts, false ) . ' value="Pacifico">'. __( 'Pacifico', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Inconsolata', $wpcloudy_fonts, false ) . ' value="Inconsolata">'. __( 'Inconsolata', 'wpcloudy' ) .'</option>
				<option ' . selected( 'Ribeye', $wpcloudy_fonts, false ) . ' value="Ribeye">'. __( 'Ribeye', 'wpcloudy' ) .'</option>
            </select>
        </p>';

    echo '<span class="wpc-label">'.__( 'Select an icon pack: ', 'wpcloudy' ).'</span>';
	echo 
        '<p>
            <select name="wpcloudy_icons">
				<option ' . selected( 'default', $wpcloudy_icons, false ) . ' value="default">'. __( 'Climacons (default)', 'wpcloudy' ) .'</option>
                <option ' . selected( 'forecast_font', $wpcloudy_icons, false ) . ' value="forecast_font">'. __( 'Forecast Font', 'wpcloudy' ) .'</option>
			</select>
        </p>';

    echo '<span class="wpc-label">'.__( 'Automatic Background Image?', 'wpcloudy' ).'</span>';
	echo 
        '<p>
            <label for="wpcloudy_image_bg_meta">
				<input type="checkbox" name="wpcloudy_image_bg" id="wpcloudy_image_bg_meta" value="yes" '. checked( $wpcloudy_image_bg, 'yes', false ) .' />
					'. __( 'Enable automatic background image based on weather?', 'wpcloudy' ) .'
					<br/ ><span class="dashicons dashicons-editor-help"></span>'.__('For best rendering, use No Skin, and few weather data.','wpcloudy').'
			</label>
        </p>';

    echo '<span class="wpc-label">'.__( 'Background cover?', 'wpcloudy' ).'</span>';
	echo 
        '<p>
            <label for="wpcloudy_image_bg_cover_meta">
				<input type="checkbox" name="wpcloudy_image_bg_cover" id="wpcloudy_image_bg_cover_meta" value="yes" '. checked( $wpcloudy_image_bg_cover, 'yes', false ) .' />
					'. __( 'Enable CSS background cover?', 'wpcloudy' ) .'
			</label>
        </p>';

    echo '<span class="wpc-label">'.__( 'Background position?', 'wpcloudy' ).'</span>';    
	echo 
        '<p>
        	'.__( "Horizontal:", "wpcloudy" ).'
            <select name="wpcloudy_image_bg_position_horizontal">
				<option ' . selected( 'default', $wpcloudy_image_bg_position_horizontal, false ) . ' value="default">'. __( 'Default', 'wpcloudy' ) .'</option>
				<option ' . selected( '0', $wpcloudy_image_bg_position_horizontal, false ) . ' value="0">'. __( '0%', 'wpcloudy' ) .'</option>
                <option ' . selected( '10', $wpcloudy_image_bg_position_horizontal, false ) . ' value="10">'. __( '10%', 'wpcloudy' ) .'</option>
                <option ' . selected( '20', $wpcloudy_image_bg_position_horizontal, false ) . ' value="20">'. __( '20%', 'wpcloudy' ) .'</option>
				<option ' . selected( '30', $wpcloudy_image_bg_position_horizontal, false ) . ' value="30">'. __( '30%', 'wpcloudy' ) .'</option>
				<option ' . selected( '40', $wpcloudy_image_bg_position_horizontal, false ) . ' value="40">'. __( '40%', 'wpcloudy' ) .'</option>
				<option ' . selected( '50', $wpcloudy_image_bg_position_horizontal, false ) . ' value="50">'. __( '50%', 'wpcloudy' ) .'</option>
				<option ' . selected( '60', $wpcloudy_image_bg_position_horizontal, false ) . ' value="60">'. __( '60%', 'wpcloudy' ) .'</option>
				<option ' . selected( '70', $wpcloudy_image_bg_position_horizontal, false ) . ' value="70">'. __( '70%', 'wpcloudy' ) .'</option>
				<option ' . selected( '80', $wpcloudy_image_bg_position_horizontal, false ) . ' value="80">'. __( '80%', 'wpcloudy' ) .'</option>
				<option ' . selected( '90', $wpcloudy_image_bg_position_horizontal, false ) . ' value="90">'. __( '90%', 'wpcloudy' ) .'</option>
				<option ' . selected( '100', $wpcloudy_image_bg_position_horizontal, false ) . ' value="100">'. __( '100%', 'wpcloudy' ) .'</option>
			</select>
        </p>';

	echo 
        '<p>
        	'.__( "Vertical:", "wpcloudy" ).'
            <select name="wpcloudy_image_bg_position_vertical">
				<option ' . selected( 'default', $wpcloudy_image_bg_position_vertical, false ) . ' value="default">'. __( 'Default', 'wpcloudy' ) .'</option>
				<option ' . selected( '0', $wpcloudy_image_bg_position_vertical, false ) . ' value="0">'. __( '0%', 'wpcloudy' ) .'</option>
                <option ' . selected( '10', $wpcloudy_image_bg_position_vertical, false ) . ' value="10">'. __( '10%', 'wpcloudy' ) .'</option>
                <option ' . selected( '20', $wpcloudy_image_bg_position_vertical, false ) . ' value="20">'. __( '20%', 'wpcloudy' ) .'</option>
				<option ' . selected( '30', $wpcloudy_image_bg_position_vertical, false ) . ' value="30">'. __( '30%', 'wpcloudy' ) .'</option>
				<option ' . selected( '40', $wpcloudy_image_bg_position_vertical, false ) . ' value="40">'. __( '40%', 'wpcloudy' ) .'</option>
				<option ' . selected( '50', $wpcloudy_image_bg_position_vertical, false ) . ' value="50">'. __( '50%', 'wpcloudy' ) .'</option>
				<option ' . selected( '60', $wpcloudy_image_bg_position_vertical, false ) . ' value="60">'. __( '60%', 'wpcloudy' ) .'</option>
				<option ' . selected( '70', $wpcloudy_image_bg_position_vertical, false ) . ' value="70">'. __( '70%', 'wpcloudy' ) .'</option>
				<option ' . selected( '80', $wpcloudy_image_bg_position_vertical, false ) . ' value="80">'. __( '80%', 'wpcloudy' ) .'</option>
				<option ' . selected( '90', $wpcloudy_image_bg_position_vertical, false ) . ' value="90">'. __( '90%', 'wpcloudy' ) .'</option>
				<option ' . selected( '100', $wpcloudy_image_bg_position_vertical, false ) . ' value="100">'. __( '100%', 'wpcloudy' ) .'</option>
			</select>
        </p>';
}

add_action('save_post','save_metabox_wpc_skin');
function save_metabox_wpc_skin($post_id){
    if(isset($_POST['wpcloudy_skin'])) {
	  update_post_meta($post_id, '_wpcloudy_skin', $_POST['wpcloudy_skin']);
	}
	if(isset($_POST['wpcloudy_fonts'])) {
	  update_post_meta($post_id, '_wpcloudy_fonts', $_POST['wpcloudy_fonts']);
	}
	if(isset($_POST['wpcloudy_icons'])) {
	  update_post_meta($post_id, '_wpcloudy_icons', $_POST['wpcloudy_icons']);
	}
	if( isset( $_POST[ 'wpcloudy_image_bg' ] ) ) {
		update_post_meta( $post_id, '_wpcloudy_image_bg', 'yes' );
	} else {
		delete_post_meta( $post_id, '_wpcloudy_image_bg', '' );
	}
	if( isset( $_POST[ 'wpcloudy_image_bg_cover' ] ) ) {
		update_post_meta( $post_id, '_wpcloudy_image_bg_cover', 'yes' );
	} else {
		delete_post_meta( $post_id, '_wpcloudy_image_bg_cover', '' );
	}
	if(isset($_POST['wpcloudy_image_bg_position_horizontal'])) {
	  update_post_meta($post_id, '_wpcloudy_image_bg_position_horizontal', $_POST['wpcloudy_image_bg_position_horizontal']);
	}
	if(isset($_POST['wpcloudy_image_bg_position_vertical'])) {
	  update_post_meta($post_id, '_wpcloudy_image_bg_position_vertical', $_POST['wpcloudy_image_bg_position_vertical']);
	}	
}

?>