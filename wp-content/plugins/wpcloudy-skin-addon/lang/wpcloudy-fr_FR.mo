��    >        S   �      H     I     L     P     U     Y     ]     a     e     i     m     q     u     z          �     �     �     �     �     �     �  3        B  6   H          �     �     �     �     �     �     �     �     �  	   �     �                 *     R   D     �     �     �     �     �     �     �     �     �     �     �     �     �  	                  &  	   -     7     @  �  F     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  #   
     *
     =
     [
     b
     u
     }
  !   �
  H   �
     �
  J   �
     E     S     `     n     z     �     �     �     �     �  	   �     �     �     �     �  7   �  e          �     �     �     �     �     �     �     �     �     �     �     �     �  	                  "     )     5     A         )      ;                 9         /              $                  	      8           6   <   
   -   "       (   #            7      4       *                       =                         .   ,             +   !       :   %                   '   >             &      0   2   3      5   1    0% 10% 100% 20% 30% 40% 50% 60% 70% 80% 90% Arvo Asap Automatic Background Image? Background cover? Background position? Bitter Climacons (default) Default Droid Serif Enable CSS background cover? Enable automatic background image based on weather? Exo 2 For best rendering, use No Skin, and few weather data. Forecast Font Francois One Horizontal: Inconsolata Josefin Sans Lato Merriweather Sans No Skin No Webfonts Nunito Open Sans Oswald Pacifico Ribeye Roboto Select a Google Webfont for your weather:  Select a skin for your weather (all settings related to design will be bypassed):  Select an icon pack:  Signika Skin 1 Skin 10 Skin 2 Skin 3 Skin 4 Skin 5 Skin 6 Skin 7 Skin 8 Skin 9 Source Sans Pro Tangerine Theme 1 Theme 2 Ubuntu Vertical: forecast today Project-Id-Version: WP Cloudy
POT-Creation-Date: 2015-10-22 20:52+0200
PO-Revision-Date: 2015-10-22 20:52+0200
Last-Translator: 
Language-Team: Benjamin DENIS <contact@wpcloudy.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.2
X-Poedit-KeywordsList: __;_e;_x
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: ..
 0% 10% 100% 20% 30% 40% 50% 60% 70% 80% 90% Arvo Asap Image d'arrière plan automatique ? Background cover ? Position de l'arrière plan ? Bitter Climacons (defaut) Défaut Droid Serif Activez background cover en CSS ? Activez les images d'arrière plan automatiques basées sur la météo ? Exo 2 Pour un meilleur rendu, utilisez Aucune skin, et peu d'éléments météo. Forecast Font Francois One Horizontale : Inconsolata Josefin Sans Lato Merriweather Sans Aucune skin Aucune webfont Nunito Open Sans Oswald Pacifico Ribeye Roboto Sélectionner une police web Google pour votre météo: Sélectionner une skin pour votre météo (tous les paramètres de personnalisation sont surpassés): Sélectionner un pack d'icônes: Signika Skin 1 Skin 10 Skin 2 Skin 3 Skin 4 Skin 5 Skin 6 Skin 7 Skin 8 Skin 9 Source Sans Pro Tangerine Thème 1 Thème 2 Ubuntu Verticale : prévisions aujourd'hui 