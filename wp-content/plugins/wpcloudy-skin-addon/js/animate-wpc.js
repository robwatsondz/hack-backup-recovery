// Returns true if the specified element has been scrolled into the viewport.
function isElementInViewport(elem) {
	var $elem = jQuery(elem);

	// Get the scroll position of the page.
	var scrollElem = ((navigator.userAgent.toLowerCase().indexOf('webkit') != -1) ? 'body' : 'html');
	var viewportTop = jQuery(scrollElem).scrollTop();
	var viewportBottom = viewportTop + jQuery(window).height();

	// Get the position of the element on the page.
	var elemTop = Math.round( $elem.offset().top );
	var elemBottom = elemTop + $elem.height();

	return ((elemTop < viewportBottom) && (elemBottom > viewportTop));
}

// Check if it's time to start the animation.
function checkAnimation() {
	var $elem = jQuery('#wpc-weather.skin1 .infos .wind,#wpc-weather.skin1 .infos .humidity, #wpc-weather.skin1 .infos .pressure, #wpc-weather.skin1 .infos .cloudiness,#wpc-weather.skin1 .hours .first, #wpc-weather.skin1 .forecast .first,#wpc-weather.skin1 .hours .second, #wpc-weather.skin1 .forecast .second,#wpc-weather.skin1 .hours .third, #wpc-weather.skin1 .forecast .third,#wpc-weather.skin1 .hours .fourth, #wpc-weather.skin1 .forecast .fourth,#wpc-weather.skin1 .hours .fifth, #wpc-weather.skin1 .forecast .fifth,#wpc-weather.skin1 .hours .sixth, #wpc-weather.skin1 .forecast .sixth,#wpc-weather.skin1 .forecast .seventh, #wpc-weather.skin1 .forecast .eighth, #wpc-weather.skin1 .forecast .ninth, #wpc-weather.skin1 .forecast .tenth, #wpc-weather.skin1 .forecast .eleventh, #wpc-weather.skin1 .forecast .twelfth, #wpc-weather.skin1 .forecast .thirteenth, #wpc-weather.skin1 .forecast .fourteenth ');

	// If the animation has already been started
	if ($elem.hasClass('start')) return;

	if (isElementInViewport($elem)) {
		// Start the animation
		$elem.addClass('start');
	}
}

// Capture scroll events
jQuery(window).scroll(function(){
	checkAnimation();
});

