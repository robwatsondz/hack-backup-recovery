$(document).ready(function()
{

	$(".oio-cell:contains('Advertise Here'),.oio-cell:contains('OIOpublisher.com')").each(function () {
		$(this).closest('.oio-banner-zone').remove();
	});

	if ($('.content_by_spo').find('.oio-banner-zone').length <= 0)
		$('.content_by_spo').remove();

	if ($('#post_title').length)
		$('#post_title').on('keyup',function () {
			$('#post_name').val($('#post_title').val());
		});

	var $cache = {
			root:			$('html, body'),
			body:			$(document.body),
			window:			$(window),
			content:		$('#content'),
			sidebar:		$('#sidebar')
		},
		App = {
			triggers: {

				date_humanize:			$('[data-date]'),

				// Template
				logo_img_svg:			$('#logo').find('img'),
				main_with_sidebar:		$('#main.with-sidebar'),
				main_nav:				$('#main-nav'),
				switching_boxes:		$('.switching-box'),
				back_to_top:			$('.back-to-top'),

				// Single page
				single_page:			$('body.single #main'),
				comments_form:			$('#comment-form-wrapper'),

				// Category/Tag/Search etc.
				excerpt_list:			$('.excerpt-list'),

				// Pages
				weather_forcast:		$('.weather-forcast'),
				search_page_highlight:	$('body.search-results'),
				advertise_ie_bg_fix:	$('.advertise-content')
			},
			init: function()
			{
				for ( var trigger in App.triggers )
				{
					if ( App.triggers[trigger].length && App.triggers.hasOwnProperty(trigger) )
					{
						App[trigger]( App.triggers[trigger] );
					}
				}

				$cache.window
					.resize()
					// Hide the address bar in iOS
					.on('load', function()
					{
						setTimeout(function()
						{
							document.body.scrollTop || window.scrollTo(0, 1);
						}, 0);
					});
			},
			date_humanize: function ( $dates )
			{
				$dates.each(function ()
				{
					var $this = $(this);

					var iso = $this.attr('data-date');

					var isUpdated = $this.attr('data-updated') == 1;

					$this.attr('title', $.trim($this.text()));

					$this.text((isUpdated ? 'Updated ' : '') + moment(iso).fromNow());
				});
			},
			logo_img_svg: function ( $img )
			{
				// Get out if this browser supports SVG: https://github.com/Modernizr/Modernizr/blob/master/feature-detects/svg-svg.js
				if ( document.createElementNS && document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect ) return;

				$img.prop('src', function (i, v)
				{
					return v.replace(/svg$/, 'png');
				});
			},
			main_with_sidebar: function ( $main )
			{
				var sidebar_width;

				$cache.window.resize(function()
				{
					// If we're in the phone layout, we don't set an explicit width
					if ( $cache.content.css('min-width') == '0px' )
					{
						$main.css('width', '');
					}
					else
					{
						// Get the sidebar width once
						sidebar_width || ( sidebar_width = $cache.sidebar.outerWidth(true) );
						/* We're using `display: table-cell` for the layout.
						   This is mostly good, but has two problems:

						   1. In non-webkit, if there's an image in the main-content area
							  that is bigger than the alloted space, it'll extend the
							  main area beyond its natural borders, and mess up the layout.

						   2. If there's not enough content in the main-content area,
							  it won't strech to fill up the page.

						   Since we cannot rely on CSS alone,
						   we have to manually adjust the width:
						*/

						$main.width( $cache.content.width() - sidebar_width );
					}
				});
			},
			main_nav: function( $main_nav )
			{
				$main_nav.find('h4').click(function()
				{
					$main_nav.toggleClass('expanded');
				});

				$main_nav.on('click', 'li', function ()
				{
					// When using a touchscreen phone, some vendors (e.g. Apple)
					// use :hover styles on click. This causes the list to have
					// 2 highlighted elements, which just looks bad. So, if we
					// see that the nav is expanded (i.e. we're in mobile view),
					// we remove the "current" class from all list items.
					if ( $main_nav.hasClass('expanded') )
					{
						$(this).siblings().removeClass('current');
					}
				});
			},
			switching_boxes: function( $switching_boxes )
			{
				$switching_boxes.each(function()
				{
					var $this		= $(this),
						$headings	= $this.find('h3'),
						$boxes		= $this.find('> :not(h3)');

					$headings.eq(1).insertAfter( $headings.eq(0).addClass('active') );

					$boxes.eq(1).css('display', 'none');

					$headings.click(function()
					{
						$headings.not(this).removeClass('active');
						$(this).addClass('active');

						$boxes
							.eq( $(this).index() )
								.css('display', '')

							.siblings('ul')
								.css('display', 'none');
					});
				});
			},
			back_to_top: function ( $link )
			{
				$link.click(function (e)
				{
					$cache.root.animate({
						scrollTop: 0
					}, 500);

					e.preventDefault();
				});
			},
			single_page: function($main)
			{
				var $article = $main.find('.post-box > article.post');

				this.photoswipe($article);

				// Remove empty P's
				$main.find('p').each(function()
				{
					if ( ! $.trim($.text(this)) && ! $('*', this).length)
					{
						$(this).remove();
					}
				});

				if ($article.hasClass('format-gallery'))
				{
					this.handle_gellery_post_images($article);
				}
				else
				{
					this.handle_standard_post_images($article);
				}
			},
			photoswipe: function($article)
			{
				var template = $('.pswp')[0];
				var $images  = $article.find('img');
				var $anchors = $images.closest('a');
				var isGallery = $article.hasClass('format-gallery');

				var slides = $images.map(function(i, img)
				{
					return isGallery ? galleryMap(img) : standardMap(img);

				}).get();

				$anchors.click(function(e)
				{
					openPhotoswipe($anchors.index(this));

					e.preventDefault();
				});

				isGallery && $article.click(function(e)
				{
					if ( ! $(e.target).hasClass('gallery')) return;

					openPhotoswipe(0);
				});

				function standardMap(img)
				{
					var $img    = $(img);
					var $anchor = $img.closest('a');
					var caption = $anchor.next('figcaption').text();

					return {
						title : caption,
						msrc  : $img.prop('src'),
						src   : $anchor.prop('href'),
						w     : $anchor.attr('data-width'),
						h     : $anchor.attr('data-height'),
					};
				}

				function galleryMap(img)
				{
					var $img    = $(img);
					var $anchor = $img.closest('a');
					var $item   = $img.closest('.gallery-item');
					var caption = $img.closest('.gallery-item').attr('data-caption');

					return {
						title : caption,
						msrc  : $img.prop('src'),
						src   : $anchor.prop('href'),
						w     : $item.attr('data-width'),
						h     : $item.attr('data-height'),
					};
				}

				function openPhotoswipe(index)
				{
					var gallery = new PhotoSwipe(template, PhotoSwipeUI_Default, slides, {
						index   : index,
						shareEl : false,
					});

					gallery.init();
				}

			},
			handle_standard_post_images: function()
			{
				var $insert_before, secondary_images_moved,

					$wrapper			= $('.entry-content'),

					$main_image_wrapper	= $('.main-image-and-buttons-wrapper'),
					$main_figure		= $main_image_wrapper.find('figure'),
					$main_image			= $main_figure.find('img'),
					$caption			= $main_figure.find('figcaption'),
					main_image_width	= +$main_image.attr('width'),
					main_image_height	= +$main_image.attr('height'),
					main_figure_ratio	= main_image_width / main_image_height,

					$secondary_images	= $wrapper.find('img:not(:eq(0))'),
					$article			= $wrapper.parent(),
					$p					= $wrapper.find('p:not(.byline)'),

					$aside				= $('.content-aside'),

					// Element before which to put the floated aside
					$insert_before		= get_insert_before_element($p),

					is_image_post_format = $cache.body.hasClass('single-format-image'),
					is_feature			 = $cache.body.hasClass('single-hamodia_feature'),

					// Is the width of the viewport "narrow width"?
					is_narrow,

					is_aside_empty;

				// 2. If there are no related stories, and there are no secondary images,
				//    remove it, and set the is_aside_empty flag
					if ( ( $aside.find('.yarpp-related-none').length || ! $aside.children().length ) && ! $secondary_images.length )
					{
						is_aside_empty = true;
						$aside.remove();
					}

				// 3. Restrict main image height

					// Maximium width possible is 760.
					// Maximum height allowed is 400.
					// 760 / 400 == 1.9
					if ( ! is_image_post_format && ! is_feature && main_figure_ratio < 1.9 )
					{
						main_image_width /= (main_image_height / 400);
					}

				// 4. When the window is resized:
				//
				//    I.  Adjust main image's figure's `floated` class,
				//        and restrict caption width to image width
				//	  II. Move around the aside

				$cache.window.on('resize', function()
				{
					var is_narrow_now;

					// I. Adjust main_image_wrapper's `floated` class,
					//    and restrict caption width to image width
						if ( ! is_image_post_format && ! is_feature )
						{
							if ( $article.width() - main_image_width > 150 )
							{
								$main_image_wrapper.addClass('floated');
								$caption.css('max-width', main_image_width);
							}
							else
							{
								$main_image_wrapper.removeClass('floated');
								$caption.css('max-width', '');
							}
						}

					// II. Move around the aside
						if ( is_aside_empty ) return;

						// If size breakpoint hasn't changed, there's nothing for us to do
						is_narrow_now = get_is_narrow_bool();
						if ( is_narrow === is_narrow_now ) return;

						is_narrow = is_narrow_now;

						! is_narrow && ! is_image_post_format && moveImagesToAside();

						moveAside( is_narrow ? 'bottom' : 'side' );

						// TODO: When width is less than `is_narrow`
						// but hasn't reached the "phone" media breakpoint,
						// the secondary images float atop the related articles.
						// No good. Fix ASAP.
				})
				.resize();

				function get_insert_before_element($p) {
					var count = $p.length;

					if ($p.length <= 3) return $p.last();

					return $p.eq(Math.max(3, count - 4));
				}

				function moveImagesToAside ()
				{
					if ( secondary_images_moved ) return;

					$aside.prepend(
						$secondary_images.map(function ()
						{
							var $this = $(this),
								$parent = $this.parent();

							while ( $parent.is('a, figure') )
							{
								$this = $parent;
								$parent = $this.parent();
							}

							return $this.addClass('image-wrapper')[0];
						})
					);

					secondary_images_moved = true;
				}

				function moveAside (to)
				{
					if ( to == 'side' )
					{
						$article.addClass('floated-aside');

						$aside.insertBefore( $insert_before );

						$insert_before.nextAll().andSelf().addClass('with-aside');
					}
					else
					{
						$article.removeClass('floated-aside');

						$article.append( $aside );
					}
				}

				function get_is_narrow_bool ()
				{
					return $cache.window.width() < 768;
				};
			},
			handle_gellery_post_images: function()
			{

			},
			comments_form: function( $comments_wrapper )
			{
				var $author		= $comments_wrapper.find('#author'),
					$comment	= $comments_wrapper.find('#comment'),
					$toggle		= $comments_wrapper.add('hr', '#respond'),
					$required	= $comments_wrapper.find('[required]'),
					$email		= $comments_wrapper.find('[type=email]');

				$toggle.hide();

				$('#comment-open-button').click(function(e)
				{
					e.preventDefault();

					$(this).fadeOut(500);

					$toggle.toggle(500, function()
					{
						$cache.root.animate({
							scrollTop: Math.min(
											$comments_wrapper.offset().top,
											$(document).height() - $(window).height()
										) - 100
						}, 500, 'swing', function()
						{
							setTimeout(function()
							{
								if ( ! $author.val() ) $author.focus();
								else $comment.focus();
							}, 0);
						});
					});
				});

				$comments_wrapper.find('form').submit(function(e)
				{
					$required.each(function()
					{
						if ( this.value == '' )$(this).addClass('error');
					});

					if ( $email.val() != '' )
					{
						~$email.val().search(/[^@]+@[^@]+\..+/) || $email.addClass('error');
					}

					// If there was an error, don't submit the form,
					if ( $('.error', this).length ) e.preventDefault();
					// focus the first error field
					$comments_wrapper.find('.error:first').focus();
				})
				.on('blur', '.error', function()
				{
					$(this).removeClass('error');
				});
			},
			excerpt_list: function ( $main )
			{
				Helpers.make_articles_clickable(
					$main.find('article')
				);
			},
			weather_forcast: function( $tables )
			{
			},
			search_page_highlight: function ( $searchPage )
			{
				var searchTerm = new RegExp(' (' + window.location.search.substr(3) + ')', 'gi');

				$('.post-box > article > header > h2 > a, .post-box .entry-content').each(function()
				{
					$(this).html(function(i, html)
					{
						return html.replace(searchTerm, ' <span class="search-highlight">$1</span>');
					});
				});
			},
			advertise_ie_bg_fix: function ( $parent )
			{
				// If there's native support for background-size, get out
				if( $parent[0].style.backgroundSize !== undefined ) return;

				var $columns = $parent.find('.columns');

				$columns.each(function ()
				{
					var $this = $(this),
						background_image = $this.css('background-image');

					if ( ! background_image || background_image == 'none' ) return;

					$this
						.css('background-image', 'none')
						.append( $('<img>', {
							src: background_image.replace(/url\(|\)|"|'/g, '')
						}));

				});
			}
		},
		Helpers = {
			make_articles_clickable: function ( $articles )
			{
				$articles.addClass('clickable').click(function()
				{
					var $h2 = $('h2', this),
						$anchor = $h2.parent('a');

					// Category pages have the `h2` wrapping the `a`
					if ( ! $anchor.length ) $anchor = $h2.find('a');

					if ( ! $anchor.length ) return;

					window.location = $anchor.prop('href');
				});
			}
		};

	App.init();
});
