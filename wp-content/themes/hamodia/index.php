<?php get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">
	
		<div id="main" class="with-sidebar excerpt-list" role="main">
			<div class="post-box">
				<?php get_template_part('loop', 'index'); ?>
			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- End Content row -->
	
<?php get_footer(); ?>