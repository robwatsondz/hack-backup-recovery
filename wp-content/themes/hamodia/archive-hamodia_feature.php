<?php get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="excerpt-list category-features" role="main">
			<div class="post-box">
				<a href="<?php echo get_post_type_archive_link('hamodia_feature'); ?>"><h1 class="typographic-heading">Features</h1></a>

				<ul class="features-categories">
					<?php foreach(get_terms('feature_category', ['hide_empty' => false]) as $term): ?>

						<li class="<?php echo strtolower( str_replace(' ', '-', $term->name) ); ?>">
							<a href="<?php echo get_term_link($term, 'feature_category'); ?>" title="<?php echo $term->name; ?>">
								<?php echo $term->name; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>

				<div class="features-posts">
					<?php get_template_part('loop', 'features-archive'); ?>
				</div>

			</div>
		</div><!-- /#main -->
	</div><!-- End main row -->

<?php get_footer(); ?>
