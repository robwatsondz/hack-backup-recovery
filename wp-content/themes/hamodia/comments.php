<?php if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) die ('Please do not load this page directly. Thanks!');

$on_comments_page = isset($_GET['letters']);

if ( $on_comments_page && ( ! have_comments() || post_password_required() ) )
{
	// It's too late to do a header redirect, so let's try some JavaScript
	$non_letter_redirect = '/';
	echo "<script>window.location = $non_letter_redirect;</script>";
	return;
}


if ( $on_comments_page ) :

	$category_info	= Hamodia_helpers::get_category_info();

	$category_name	= $category_info['name'];
	$category_link	= $category_info['link']; ?>

	<section id="comments">

		<h1 class="comment-article-heading">
			<?php comments_number('No letters on', '1 letter on', '% letters on'); ?>
			<a class="comment-article-title" href="<?php the_permalink(); ?>">
				<?php the_title(); ?>
			</a>
			in
			<a class="comment-article-category-name" href="<?php echo $category_link; ?>">
				<?php echo $category_name; ?>
			</a>
		</h1>

		<?php echo hamodia_get_entry_date(); ?>

		<ol class="commentlist">
			<?php wp_list_comments([
				'type'     => 'comment',
				'callback' => 'hamodia_render_comments',
			]); ?>
		</ol>

		<footer>
			<nav id="comments-nav">

				<div class="comments-previous">
					<?php previous_comments_link('&larr; Older comments'); ?>
				</div>

				<div class="comments-next">
					<?php next_comments_link('Newer comments &rarr;'); ?>
				</div>

			</nav>
		</footer>

	</section>

<?php endif; // Only displaying comments on letters page ?>



<?php if ( is_user_logged_in() && comments_open() ) : ?>

	<section id="respond">

		<?php if ( ! $on_comments_page ):

			if ( have_comments() ): ?>
				<a href="<?php echo get_current_url() . '?letters'; ?>" class="comments-count">
					<?php comments_number('No letters yet.', 'View <span>one letter</span>.', 'View <span>% letters</span>.'); ?>
				</a>
			<?php else: ?>
				<span class="comments-count">No letters yet.</span>
			<?php endif; ?>

		<?php endif; ?>

		<a id="comment-open-button" href="#" class="comment-button">Write to the editor <span>»</span></a>

		<?php if ( ! $on_comments_page ): ?>
			<hr />
		<?php endif ?>

		<div id="comment-form-wrapper">
			<p class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></p>

			<?php if ( get_option('comment_registration') && ! is_user_logged_in() ) : ?>

				<p><?php printf( __('You must be <a href="%s">logged in</a> to post a comment.', 'reverie'), wp_login_url( get_permalink() ) ); ?></p>

			<?php else: ?>

				<p id="comment-disclaimer">Your letter will be sent to the editor. Hamodia reserves the right to edit any letter before publication.</p>

				<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform" class="nice">

					<?php if ( false && is_user_logged_in() ) : ?>
						<p><?php printf(__('Logged in as <a href="%s/wp-admin/profile.php">%s</a>.', 'reverie'), get_option('siteurl'), $user_identity); ?> <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php __('Log out of this account', 'reverie'); ?>"><?php _e('Log out &raquo;', 'reverie'); ?></a></p>
					<?php else : ?>
						<p>
							<label for="author">Name<?php if ($req) echo ' (required)'; ?>:</label>
							<input type="text" class="input-text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo 'required aria-required="true"'; ?>>
						</p>
						<p>
							<label for="email">Email <?php if ($req) echo ' (required)'; ?> <small>will not be published</small></label>
							<input type="email" class="input-text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo 'required aria-required="true"'; ?>>
						</p>
					<?php endif; ?>

					<p>
						<label for="comment">Your message:</label>
						<textarea name="comment" id="comment" tabindex="4" required aria-required="true"></textarea>
					</p>

					<p><input name="submit_comment" class="comment-button" type="submit" id="submit_comment" tabindex="5" value="Send to editor"></p>

					<?php comment_id_fields(); ?>

					<?php if ( function_exists('get_current_url') ): ?>
						<input type="hidden" name="redirect_to" value="<?php echo esc_attr( get_current_url() ); ?>?letters" />
					<?php endif; ?>

					<?php do_action('comment_form', $post->ID); ?>
				</form>

			<?php endif; // If registration required and not logged in ?>
		</div>
	</section>

<?php endif; // if comments are open




function hamodia_render_comments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment; ?>

	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>">

			<?php if ($comment->comment_approved == '0') : ?>
				<div class="notice">Your letter is awaiting moderation.</div>
			<?php endif; ?>

			<section class="comment">
				<?php comment_text(); ?>
			</section>

			<footer class="comment-author vcard">

				<cite><?php echo comment_author(); ?></cite>

				<?php echo hamodia_format_date( get_comment_time('U', true) ); ?>

				<span><?php edit_comment_link('Edit', '(', ')') ?></span>
			</footer>

			<?php comment_reply_link(array_merge($args, ['depth' => $depth, 'max_depth' => $args['max_depth']])) ?>

		</article>
	</li>
<?php
}
