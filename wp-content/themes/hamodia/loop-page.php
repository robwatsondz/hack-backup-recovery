<?php while (have_posts()) : the_post(); ?>

	<h1 class="typographic-heading"><?php the_title(); ?></h1>

	<?php the_content(); ?>

	<?php wp_link_pages([
		'before' => '<nav id="page-nav"><p>Pages:',
		'after'  => '</p></nav>',
	]); ?>

<?php endwhile; ?>
