<?php

if ( ! is_front_page()) {
    Hamodia_sidebar::bananagrans();
}

if (is_front_page()):

	Hamodia_sidebar::daily_paper();
	Hamodia_sidebar::newsletter();
	Hamodia_sidebar::switching_boxes();
	Hamodia_sidebar::weather();
	Hamodia_sidebar::stocks(true);
	Hamodia_sidebar::currency();
	Hamodia_sidebar::page_links();

elseif (is_single()): ?>

	<div class="more-articles-wrapper">
		<?php Hamodia_sidebar::more_in_current_category(); ?>
	</div>

	<?php
	Hamodia_sidebar::switching_boxes();
	Hamodia_sidebar::weather();
	Hamodia_sidebar::stocks();
	Hamodia_sidebar::currency();

elseif ( is_category() ): ?>

	<div class="more-articles-wrapper">
		<?php Hamodia_sidebar::more_in_categories(); ?>
	</div>

	<?php
	Hamodia_sidebar::switching_boxes();
	Hamodia_sidebar::weather();
	Hamodia_sidebar::stocks();
	Hamodia_sidebar::currency();

else:

	is_page('Stocks') && Hamodia_sidebar::stocks(true);

	if ( get_post_type() != 'letter' ): ?>

		<div class="more-articles-wrapper">
			<?php Hamodia_sidebar::more_in_categories(); ?>
		</div>

	<?php endif;

	Hamodia_sidebar::switching_boxes();

	! is_page('Weather') && Hamodia_sidebar::weather();
	! is_page('Stocks') && Hamodia_sidebar::stocks();
	! is_page('Currency') && Hamodia_sidebar::currency();

	Hamodia_sidebar::page_links();

endif;

Hamodia_sidebar::news_tip();
