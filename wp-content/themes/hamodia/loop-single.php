<?php while (have_posts()) : the_post(); ?>
	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

		<?php if ( ! isset($_GET['letters']) ): // if we are not on the comments page ?>

			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header>

			<div class="entry-content">
				<?php the_content(); ?>
				<?php $custom_data = get_post_custom(get_the_ID());?>
				<?php
					//check for the name and address details.
					if(isset($custom_data['post_holdname'][0]))
					{
						$withhold_name = $custom_data['post_holdname'][0];

						if (empty($withhold_name))
						{

							$first_name = $custom_data['first_name'][0];
							$last_name = $custom_data['last_name'][0];
							$post_address = $custom_data['post_address'][0];
							$post_address = '';

							$line_out = '';
							if ($first_name != '')
								$line_out .= $first_name;

							if ($last_name != '')
							{
								if ($line_out != "")
									$line_out .= " ";

								$line_out .= $last_name;
							}

							if ($post_address != '')
							{
								if ($line_out != "")
									$line_out .= ", ";
	
								$line_out .= $post_address;
							}
	
							echo '<p style="text-align:right;">'.$line_out.'</p>';
	
						}
					}

				?>

				<?php if (HamodiaStoryUpdates::has($post->ID)): ?>
					<hr>
					<div class="story-updates">
						<?php foreach (HamodiaStoryUpdates::get($post->ID) as $update): ?>
							<p class="story-update">
								<span class="story-update-time">Updated <?= $update['date']->format('l, F j, Y \a\t g:i a') ?></span>
								<span class="story-update-body"><?= $update['body'] ?></span>
							</p>
						<?php endforeach ?>
					</div>
				<?php endif; ?>
			</div>

			<footer>
				<?php wp_link_pages([
						'before' => '<nav id="page-nav"><p>' . 'Pages:',
						'after'  => '</p></nav>',
				]); ?>

				<?php function_exists('hamodia_post_meta') && hamodia_post_meta(); ?>
				<?php function_exists('hamodia_opinion_disclaimer') && hamodia_opinion_disclaimer(); ?>

				<p class="tags"><?php the_tags('<strong>Filed under:</strong> '); ?></p>
				<?php echo Hamodia_the_content::get_share_buttons($post); ?>
			</footer>

			<aside class="content-aside">
				<?php if (should_display_related_posts()) {
					related_posts();
				} ?>
			</aside>

		<?php endif; ?>

		<?php // comments_template(); ?>

	</article>
<?php endwhile; ?>
