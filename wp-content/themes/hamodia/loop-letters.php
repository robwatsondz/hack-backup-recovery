<?php

class Hamodia_letters_page
{
	static private $limit = 4;
	static private $offset;
	static private $page;

	static public function display_comments()
	{
		self::set_offset();

		wp_list_comments(['callback' => function ($comment, $args, $depth) {
			Hamodia_letters_page::render_comments($comment, $args, $depth);
		}]), self::get_comments());
	}

	static public function comments_pagination()
	{
		$comments_count = get_comments([
			'status' => 'approve',
			'count'  => true,
		]);

		// If all comments are currently being displayed, then no pagination is needed
		if ($comments_count <= self::$limit) return;

		$links       = [];
		$total_pages = $comments_count / self::$limit;


		if ($total_pages > self::$page) {
			$links['prev'] = get_current_url() . '?page_num=' . (self::$page + 1);
		}

		if (self::$page != 1) {
			$links['next'] = get_current_url() . '?page_num=' . (self::$page - 1);
		}

		self::render_pagination($links);
	}

	static private function set_offset()
	{
		// Get page num
		self::$page = empty($_GET['page_num']) ? 1 : (int) $_GET['page_num'];
		// ascertain that it's a positive integer
		if (self::$page <= 0) self::$page = 1;
		// and set the offset accordingly
		self::$offset = (self::$page - 1) * self::$limit;
	}

	static private function get_comments ()
	{
		return get_comments([
			'status' => 'approve',
			'number' => self::$limit,
			'offset' => self::$offset,
		]);
	}


	static public function render_comments($comment, $args, $depth)
	{
		$GLOBALS['comment'] = $comment;

		$post_id        = $comment->comment_post_ID;

		$title          = get_the_title($post_id);
		$permalink      = get_permalink($post_id);
		$letters_link   = $permalink . '?letters';

		$category_info  = Hamodia_helpers::get_category_info($post_id);

		$category_name  = $category_info['name'];
		$category_link  = $category_info['link'];

		?>
		<li <?php comment_class(); ?>>
			<article id="comment-<?php comment_ID(); ?>">

				<h2 class="comment-article-heading">On
					<a class="comment-article-title" href="<?php echo $permalink; ?>">
						<?php echo $title; ?>
					</a>
					in
					<a class="comment-article-category-name" href="<?php echo $category_link; ?>">
						<?php echo $category_name; ?>
					</a>
				</h2>

				<?php echo hamodia_get_entry_date($post_id); ?>

				<section class="comment">
					<?php comment_text(); ?>

				</section>

				<footer class="comment-author vcard">

					<cite><?php echo comment_author(); ?></cite>

					<?php echo hamodia_format_date( get_comment_time('U', true) ); ?>

					<span><?php edit_comment_link('Edit', '(', ')') ?></span>
				</footer>

				<a class="more-letters-link" href="<?php echo $letters_link; ?>">All letters on <span class="title"><?php echo $title; ?></span></a>

			</article>
		</li>
		<?php
	}

	static private function render_pagination ( $links )
	{
		if (isset($links['prev'])): ?>
			<div class="nav-previous">
				<a href="<?php echo $links['prev']; ?>">« Older letters</a>
			</div>
		<?php endif;

		if (isset($links['next'])): ?>
			<div class="nav-next">
				<a href="<?php echo $links['next']; ?>">Newer letters »</a>
			</div>
		<?php endif;
	}
}

?>


<ol class="commentlist">
	<?php Hamodia_letters_page::display_comments(); ?>
</ol>

<footer>
	<nav id="comments-nav">
		<?php Hamodia_letters_page::comments_pagination(); ?>
	</nav>
</footer>
