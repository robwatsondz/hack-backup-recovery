	</div><!-- Container End -->

	<footer id="footer" role="contentinfo">
		<div class="row">

			<div class="three columns">
				<h6>Sections</h6>
				<ul>
					<?php
					$news_categories = [
						'Business',
						'Markets',
						'Technology',
						'World',
						'Regional',
						'Israel',
						'Community',
						'National',
						'Op-Ed'
					];

					foreach ($news_categories as $cat): ?>
						<li><a href="<?php echo get_category_link( get_cat_ID($cat) ); ?>"><?php echo $cat ?></a></li>
					<?php endforeach; ?>

					<li><a href="<?php echo get_post_type_archive_link('hamodia_feature'); ?>">Features</a></li>
				</ul>
			</div>

			<div class="three columns">
				<h6>Resources</h6>
				<ul>
					<li><a href="<?php echo get_page_link(203); ?>">Weather</a></li>
					<li><a href="<?php echo get_page_link(206); ?>">Currency</a></li>
					<li><a href="<?php echo get_page_link(207); ?>">Stocks</a></li>
					<li><a href="<?php echo get_page_link(232); ?>">Zmanim</a></li>
					<li><a href="<?php echo get_page_link(234); ?>">Shiurim</a></li>
					<li><a href="<?php echo get_page_link(1554); ?>">Simchas</a></li>
				</ul>
			</div>

			<div class="three columns">
				<h6>Readers</h6>
				<ul>
					<li><a href="<?php echo get_page_link(94); ?>">Subscribe</a></li>
					<li><a href="<?php echo get_post_type_archive_link('letter'); ?>">Letters</a></li>
					<li><a href="<?php echo get_page_link(175); ?>">Archives</a></li>
					<li><a href="http://hamodia.com/classifieds-4-2/">Classifieds</a></li>
				</ul>
			</div>

			<div class="three columns">
				<h6>Company</h6>
				<ul>
					<li><a href="<?php echo get_page_link(248); ?>">Advertise</a></li>
					<li><a href="<?php echo get_page_link(4331); ?>">Privacy Policy</a></li>
					<li><a href="<?php echo get_page_link(4250); ?>">Terms of Use</a></li>
					<li><a href="<?php echo get_page_link(3723); ?>">About Us</a></li>
					<li><a href="<?php echo get_page_link(3722); ?>">Contact Us</a></li>
				</ul>
				<a href="#" class="back-to-top" title="Back to top">➧</a>
			</div>

		</div>
	</footer>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/libs/jquery-1.8.3.min.js"><\/script>')</script>

	<script src="<?php echo get_template_directory_uri(); ?>/js/libs/moment-2.10.3.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/foundation.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/libs/photoswipe/photoswipe.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/libs/photoswipe/photoswipe-ui-default.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/app.js?v=2015-12-06"></script>

	<!--[if lt IE 8]>
		<script defer src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
		<script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->

	<?php wp_footer(); ?>

	<?php get_template_part('photoswipe'); ?>
</body>
</html>