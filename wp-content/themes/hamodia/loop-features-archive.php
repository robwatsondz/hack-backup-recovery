<?php /* If there are no posts to display, such as an empty archive page */
if ( ! have_posts() ) : ?>

	<div class="notice">
		<p class="bottom"><?php echo 'Nothing to see here. Try a search:'; ?></p>
	</div>

	<?php get_search_form(); ?>

<?php endif;



/* Start loop */
while ( have_posts() ) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php
		$terms = get_the_terms($post->ID, 'feature_category');
		$term = reset($terms);
		$link = get_term_link($term, 'feature_category');

		echo sprintf('<a class="feature-category-link" href="%s">%s</a>', $link, $term->name);

		echo get_the_thumbnail();
		?>

		<header>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php hamodia_entry_meta(); ?>
		</header>

		<div class="entry-content">
			<?php the_excerpt(); ?>
		</div>

	</article>

<?php endwhile; // End the loop ?>




<?php
/* Display navigation to next/previous pages when applicable */
if ( $wp_query->max_num_pages > 1 ) : ?>

	<nav id="post-nav">
		<div class="nav-previous"><?php next_posts_link('&laquo; Older'); ?></div>
		<div class="nav-next"><?php previous_posts_link('Newer &raquo;'); ?></div>
	</nav>

<?php endif;
