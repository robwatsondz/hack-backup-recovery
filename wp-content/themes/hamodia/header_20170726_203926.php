<!doctype html>
<!--[if IE 8]>    <html class="no-js ie ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">

	<title><?php hamodia_page_title(); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app3.css?v=2017-03-02">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/photoswipe/photoswipe.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/photoswipe/default-skin/default-skin.css">

	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
		<script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>
	<![endif]-->

	<!-- Favicon and Feed -->
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">

	<!--  iPhone Web App Home Screen Icon -->
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-icon-retina.png" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-icon.png" />

	<!-- Enable Startup Image for iOS Home Screen Web App -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/mobile-load.png" />
	<meta name="apple-mobile-web-app-title" content="Hamodia" />

	<!-- Startup Image iPad Landscape (748x1024) -->
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-load-ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />
	<!-- Startup Image iPad Portrait (768x1004) -->
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-load-ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />
	<!-- Startup Image iPhone (320x460) -->
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-load.png" media="screen and (max-device-width: 320px)" />

	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.foundation.js"></script>

	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<article id="newsletter-top-banner">
		<div class="row">
			<h3>Want up-to-the-<br>minute news?</h3>
			<div class="text">
				<p>Sign up for <b>HAMODIA'S</b> <br>Breaking News Emails*</p>
				<footer>*You can select which emails you'd like to receive.</footer>
			</div>
			<form id="newsletter-top-banner-form" action="<?= get_site_url(); ?>/standalone/newsletter.php" target="_blank" method="get">
				<input type="email" placeholder="Your Email Here" name="email" required>
				<button type="submit">Go</button>
			</form>
			<button type="button" id="newsletter-top-banner-dismiss">&times;</button>
		</div>
	</article>

	<script>
	(function() {
		if (localStorage.getItem('newsletter-top-banner-dismissed')) {
			document.getElementById('newsletter-top-banner').className = 'hidden';
		} else {
			document.getElementById('newsletter-top-banner-dismiss').addEventListener('click', dismiss);
			document.getElementById('newsletter-top-banner-form').addEventListener('submit', dismiss);
		}

		function dismiss() {
			try {
				localStorage.setItem('newsletter-top-banner-dismissed', new Date().getTime());
				document.getElementById('newsletter-top-banner').className = 'hidden';
			} catch(e) {
				// In Safari, trying to store stuff in localstorage while in private
				// browsing throws an error. We will catch it here and tell users
				// to quit private browsing to be able to dismiss this banner.
				if (e.message == 'QuotaExceededError: DOM Exception 22') {
					alert('Quit private browsing to dismiss this.');
				}
			}
		}
	}());
	</script>

	<header id="header">

		<div class="top-links">
			<div class="row">
				<div class="left">
					<a href="<?php echo site_url(); ?>">Home</a> |
					<a href="<?php echo get_page_link(175); ?>">Archive</a> |
					<a href="<?php echo get_page_link(248); ?>" class="advertise">Advertise</a> |
					<a href="<?php echo get_page_link(94); ?>">Subscribe</a> |
					<a href="<?php echo get_page_link(3723); ?>">About Us</a> |
					<a href="<?php echo get_page_link(3722); ?>">Contact Us</a>
				</div>
				<div class="right">
					<span class="hebrew-date">

						<?php echo Hamodia_APIs::get_hebrew_date();

						if ( $sefirah = Sefirah::get() )
						{
							echo '&nbsp;&nbsp;-&nbsp;&nbsp;' . $sefirah;
						} ?>
					</span>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<?php echo date_i18n('l, F j, Y'); ?>
				</div>
			</div>
		</div>

		<div class="top-center">
			<div class="row">

				<a id="logo" href="<?php echo site_url(); ?>">
					<img src="//images.hamodia.com/wp-content/themes/hamodia/images/logo.svg" alt="Hamodia Site Logo" width="252" height="44" />
				</a>

				<p id="tagline">Hamodia - The daily newspaper of torah jewry</p>

				<a href="<?php echo get_page_link(94); ?>" class="subscribe-banner">Subscribe to the Hamodia print edition</a>

				<form method="get" id="topsearchform" action="<?php echo site_url('/'); ?>">
					<input type="text" name="s" placeholder="Search hamodia.com">
					<input type="submit" value="Search">
				</form>

			</div>
		</div>

		<nav id="main-nav" class="row" role="navigation">
			<h4>Sections</h4>
			<?php hamodia_main_nav(); ?>
		</nav>

	</header>

	<!-- Start the main container -->
	<div id="container" class="container" role="document">
