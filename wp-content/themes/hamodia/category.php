<?php get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="excerpt-list with-sidebar" role="main">
			<div class="post-box">

				<a href="<?php echo get_category_link( get_query_var('cat') ); ?>">
					<h1 class="category-page-heading">
						<?php single_cat_title(); ?>
					</h1>
				</a>

				<?php HamodiaCategory::make(get_query_var('cat'))->hero(); ?>

				<?php get_template_part('loop', 'category'); ?>
			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- End main row -->

<?php get_footer(); ?>
