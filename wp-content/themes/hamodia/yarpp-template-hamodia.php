<?php /*
Hamodia YARPP Template
Author: Joseph Silber
*/

if ( have_posts() ):?>

<article class="related-posts more-articles">

	<h3>Related</h3>

	<ul>
		<?php while ( have_posts() ) : the_post(); ?>

			<li>
				<a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
			</li>

		<?php endwhile; ?>
	</ul>

</article>

<?php endif;