<?php




$custom_post_types = array( "prime" => "<Optional: Your Archive Slug Here>");
$max_archive_pages = 0;	// 0 == all archive page #s

function post_status( $new_status, $old_status, $post )
{
	global $custom_post_types, $max_archive_pages;

	if ( array_key_exists( $post->post_type, $custom_post_types ) && ( $new_status === "publish" || $old_status === "publish" ) )
	{
		if ( defined( 'W3TC' ) )
		{
			$archive = empty( trim( $custom_post_types[$post->post_type] ) ) ? $post->post_type:trim( $custom_post_types[$post->post_type] );

			$post->post_status = "";
			$link = preg_replace( '~__trashed/$~', '/', get_permalink( $post ) );
			w3tc_flush_url( $link );

			$posts_per_page = get_option( 'posts_per_page' );
			$posts_number = \W3TC\Util_PageUrls::get_archive_posts_count( 0, 0, 0, $post->post_type );
			$posts_pages_number = @ceil( $posts_number / $posts_per_page );

			if ( $max_archive_pages > 0 && $posts_pages_number > $max_archive_pages ) {
				$posts_pages_number = $max_archive_pages;
			}

			if ( $posts_pages_number == 0 ) {
				w3tc_flush_url( \W3TC\Util_Environment::home_domain_root_url() . "/" . $archive );
			} else {
				for ( $pagenum = 1; $pagenum <= $posts_pages_number; $pagenum++ ) {
					$link = \W3TC\Util_PageUrls::get_pagenum_link( $archive, $pagenum );
					w3tc_flush_url( $link );
				}
			}
		}
	}
}
add_action(  'transition_post_status',  'post_status', 10, 3 );


$custom_post_types = array( "frominyan" => "<Optional: Your Archive Slug Here>");
$max_archive_pages = 0;	// 0 == all archive page #s

function post_status( $new_status, $old_status, $post )
{
	global $custom_post_types, $max_archive_pages;

	if ( array_key_exists( $post->post_type, $custom_post_types ) && ( $new_status === "publish" || $old_status === "publish" ) )
	{
		if ( defined( 'W3TC' ) )
		{
			$archive = empty( trim( $custom_post_types[$post->post_type] ) ) ? $post->post_type:trim( $custom_post_types[$post->post_type] );

			$post->post_status = "";
			$link = preg_replace( '~__trashed/$~', '/', get_permalink( $post ) );
			w3tc_flush_url( $link );

			$posts_per_page = get_option( 'posts_per_page' );
			$posts_number = \W3TC\Util_PageUrls::get_archive_posts_count( 0, 0, 0, $post->post_type );
			$posts_pages_number = @ceil( $posts_number / $posts_per_page );

			if ( $max_archive_pages > 0 && $posts_pages_number > $max_archive_pages ) {
				$posts_pages_number = $max_archive_pages;
			}

			if ( $posts_pages_number == 0 ) {
				w3tc_flush_url( \W3TC\Util_Environment::home_domain_root_url() . "/" . $archive );
			} else {
				for ( $pagenum = 1; $pagenum <= $posts_pages_number; $pagenum++ ) {
					$link = \W3TC\Util_PageUrls::get_pagenum_link( $archive, $pagenum );
					w3tc_flush_url( $link );
				}
			}
		}
	}
}
add_action(  'transition_post_status',  'post_status', 10, 3 );







// Initial setup
add_action('after_setup_theme', function ()
{
	// Add language supports. No language files yet.
	load_theme_textdomain('hamodia', get_template_directory() . '/lang');

	add_theme_support('post-formats', ['image', 'gallery']);

	add_filter('post_gallery', function ($output, $attributes) {
		if (is_feed()) return;

		return HamodiaGallery::make($attributes)->render();
	}, 10, 4);
});

/*
 * Customized the output of caption, you can remove the filter to restore back to the WP default output.
 * Courtesy of DevPress. http://devpress.com/blog/captions-in-wordpress/
 */
add_filter('img_caption_shortcode', function ($output, $attr, $content)
{
	// We're not worried about captions in feeds, so just return the output as is.
	if (is_feed()) return $output;

	/* Set up the default arguments. */
	$attr = shortcode_atts([
		'id'      => '',
		'align'   => 'alignnone',
		'width'   => '',
		'caption' => '',
	], $attr);

	/* If the width is less than 1 or there is no caption,
	// return the content wrapped between the [caption]< tags. */
	if (1 > $attr['width'] || empty($attr['caption'])) {
		return $content;
	}

	/* Set up the attributes for the caption <figure>. */
	$attributes = 'class="figure ' . esc_attr($attr['align']) . '"';

	$attr['width'] && $attributes .= ' style="max-width:' . esc_attr($attr['width']) . 'px"';

	return	'<figure ' . $attributes . '>' .
				do_shortcode( $content ) .
				'<figcaption>' . $attr['caption'] . '</figcaption>' .
			'</figure>';
}, 10, 3);

// Clean the output of attributes of images in editor. Courtesy of SitePoint.
// http://www.sitepoint.com/wordpress-change-img-tag-html/
add_filter('get_image_tag_class', function ($class, $id, $align, $size) {
	$align = 'align' . esc_attr($align);
	return $align;
}, 0, 4);



function admin_bar(){

  if(is_user_logged_in()){
    add_filter( 'show_admin_bar', '__return_true' , 1000 );
  }
}

if (isset($_GET['test']) && $_GET['test'] == '1')
{
	add_action('init', 'admin_bar' );
}

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );