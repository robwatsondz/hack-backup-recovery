<?php
/*
Template Name: Tags (private) Template
*/
?>

<style>
	body {
		font-family: sans-serif;
	}

	div {
		width: 33%;
		float: left;
	}

	div:empty {
		display: none;
	}

	div:nth-child(3n + 1) {
		clear: left;
	}

	h1 {
		border: 1px solid #666;
		padding: 6px;
		margin: 4px;
		page-break-inside: avoid;
	}

	p {
		margin: 4px;
	}
</style>

<?php

$tags = array_map(function ( $tag )
{
	static $header = null;

	preg_match('~[a-z\d]~i', $tag->name[0], $matches);

	$letter = strtolower( isset($matches[0]) ? $matches[0] : '-' );

	$out = '<p>' . $tag->name . '</p>';

	if ( $letter !== $header )
	{
		$out = "</div><div><h1>$letter</h1>$out";

		$header = $letter;
	}

	return $out;
},
get_tags());


echo implode('', $tags);