<?php
/*
Template Name: Advertise Template
*/
get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="advertise" role="main">

			<div class="advertise-row copy">
				<h1>When results matter, <br>advertise in Hamodia.</h1>
				<img class="advertise-img" src="<?php echo get_template_directory_uri(); ?>/images/advertise.png?v=2">
				<p>Hamodia is the largest and fastest growing Jewish religious publication in the U.S. and across the globe. Our growth is in tandem with that of the religious communities, where population growth is explosive. With an increase in population, comes an increase in needs, wants and demands.</p>
				<p>The people and families that comprise our audience are deeply committed to the Jewish yearly cycle that generates copious consumption of high-quality goods.</p>
				<p>Hamodia is the best platform to reach our highly insular community, as our readers are engaged and trusting of our integrity. An ad in the pages of Hamodia means you’ll be getting the exposure and recognition you’re looking for.</p>
			</div>

			<div class="advertise-row">
				<div class="col col-1">
					<ul class="statistics">
						<li><strong>61%</strong><p>of advertisers have been advertising in our publications for more than 5 years.</p></li>
						<li><strong>80%</strong><p>of advertisers saw positive results from their promotions.</p></li>
						<li><strong>88%</strong><p>of advertisers prefer placing their ads with Hamodia.</p></li>
					</ul>
				</div>

				<div class="col col-2 call-to-action">
					<h1>Place Your Ad <br>With Hamodia Today!</h1>
					<p>For more information
						<br>on advertising with Hamodia
						<br>Call 718.Hamodia or
						<br>Email <a href="mailto:sales@hamodia.com">sales@hamodia.com</a> or</p>

					<a href="<?php echo get_template_directory_uri(); ?>/images/hamodia-media-kit.pdf" class="download" target="_blank">Download Media Kit</a>
				</div>
			</div>

		</div><!-- /#main -->

	</div><!-- End main row -->

<?php get_footer(); ?>
