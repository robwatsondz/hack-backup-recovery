<?php
/*
Template Name: Weather Template
*/
get_header();

$weather_page_base_url = home_url( $GLOBALS['wp']->request );

$weather_cities = [
	'11219',
	'10952',
	'08701',
	'London, UK',
	'Antwerp, Belguim',
	'Jerusalem, Israel',
];



$main_location = empty($_GET['city']) ? array_shift($weather_cities) : $_GET['city']; ?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="eight columns with-sidebar" role="main">
			<div class="post-box">

				<h1 class="typographic-heading">Weather</h1>

				<article class="main-location">
					<?php $weather_data = Hamodia_APIs::get_weather_info( $main_location );
						if (isset($weather_data['error'])) {
							$weather_data = Hamodia_APIs::get_weather_info(array_shift($weather_cities));
						}
						$icon_url = get_bloginfo('template_directory') . '/images/weather/big/' . $weather_data['current']['icon'];
					?>

					<h2><?= $weather_data['location']['name'] ?></h2>
					<div class="now">
						<img src="<?= $icon_url ?>" />
						<span class="condition">
							<?= $weather_data['current']['temp_f'] ?>&deg;F
							<span class="c">(<?= $weather_data['current']['temp_c'] ?>&deg;C)</span><br />
							<?= $weather_data['current']['condition'] ?>
						</span>
					</div>

					<ul class="forcast">
						<?php foreach ($weather_data['forcast'] as $forcast):
						$icon_url = get_bloginfo('template_directory') . '/images/weather/big/' . $forcast['icon']; ?>
						<li>
							<span class="day"><?= $forcast['day_of_week'] ?></span>
							<img src="<?= $icon_url ?>" />
							<span class="temp"><?= $forcast['tempMaxF'] ?>&deg;F/<?= $forcast['tempMinF'] ?>&deg;F</span>
							<span class="condition"><?= $forcast['condition'] ?></span>
						</li>
						<?php endforeach; ?>
					</ul>

				</article>

				<ul class="locations">
				<?php foreach ($weather_cities as $city):
					$weather_data = Hamodia_APIs::get_weather_info($city); ?>

					<li>
						<a href="<?php echo $weather_page_base_url; ?>?city=<?php echo $city; ?>">
							<span class="location"><?= $weather_data['location']['name'] ?></span>
							<span class="condition"><?= $weather_data['current']['condition'] ?> <?= $weather_data['current']['temp_f'] ?>&deg;F</span>
						</a>
					</li>

				<?php endforeach; ?>
				</ul>

				<form action="" method="get" class="nice feature-box">
					<h4>Find the weather forecast for many other locations.</h4>
					<input type="text" name="city" placeholder="Enter City, State or ZIP" class="input-text" />
					<input type="submit" value="Search" />
				</form>

			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- End main row -->

<?php get_footer();
