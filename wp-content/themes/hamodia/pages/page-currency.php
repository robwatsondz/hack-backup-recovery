<?php
/*
Template Name: Currency Template
*/
get_header();

$currency = Hamodia_APIs::get_currency_info(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="with-sidebar" role="main">
			<div class="post-box">

				<h1 class="typographic-heading">Currency</h1>

				<div class="row">
					<?php foreach (['GBP', 'ILS', 'EUR'] as $key): ?>
					<div class="four columns">
						<div class="feature-box <?php echo $key; ?>">
							<h3 class="key"><?php echo $key; ?></h3>
							<h4 class="name"><?php echo $currency['currencies'][$key]; ?></h4>
							<p class="rate"><?php echo $currency['rates'][$key]; ?></p>
						</div>
					</div>
					<?php endforeach ?>
				</div>

				<table>
					<tbody>
						<?php foreach ($currency['currencies'] as $key => $name): ?>
						<tr>
							<td><?php echo "<b>$key</b> ($name)"; ?></td>
							<td><?php echo $currency['rates'][$key]; ?></td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- End main row -->

<?php get_footer();
