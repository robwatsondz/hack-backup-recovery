<?php
/*
Template Name: Subscriptions template
*/
get_header();
?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" role="main">
			<div class="post-box">

				<div class="subscriptions-header">

					<!--<form class="zip-code-search">-->

						<div style="text-align:center;margi-bottom:2px"><a href="https://brooklynny.circulationpro.com/scripts/WebObjects.exe/BrooklynNYSubscriberSS" style="color:#ffffff" target="_blank"><div style="width:90%;background:#003366;border:none; color:white;padding:12px 16px;text-align:center;text-decoration: none;display:inline-block;font-size:24px;margin:4px 2px;cursor:pointer;border-radius:24px;color:#ffffff">Renew Subscription</div></a></div>
						<div style="text-align:center"><a href="https://brooklynny.circulationpro.com/scripts/WebObjects.exe/BrooklynNYSubscriberSS" style="color:#ffffff" target="_blank"><div style="width:90%;background:#003366;border:none; color:white;padding:12px 16px;text-align:center;text-decoration: none;display:inline-block;font-size:24px;margin:4px 2px;cursor:pointer;border-radius:24px;color:#ffffff">Become a Subscriber!</div></a></div>

						

					<!--</form>-->

				</div>

                <!--<article class="subscriptions-install-mailbox">
                    <p>Already subscribe? Click here.</p>
                </article>-->

                <form class="subscriptions-install-mailbox-form hidden">
                    <p class="mailbox-heading">
                        <strong>Already subscribe?</strong><br>
                        Tell us who you are. We'll install your mailbox & send you the Bananagrams game.
                    </p>
                    <fieldset class="shipping-information">
                        <legend>Address</legend>
                        <input class="input-text" type="text" name="install_mailbox_first_name" placeholder="First Name" required>
                        <input class="input-text" type="text" name="install_mailbox_last_name" placeholder="Last Name" required>
                        <input class="input-text" type="text" name="install_mailbox_address" placeholder="Street Address" required>
                        <input class="input-text" type="text" name="install_mailbox_city" placeholder="City" required>
                        <input class="input-text" type="text" name="install_mailbox_state" placeholder="State" required>
                        <input class="input-text" type="text" name="install_mailbox_zip" placeholder="ZIP Code" required>
                        <select name="install_mailbox_country" required>
                            <option value="USA" selected>USA</option>
                            <option value="Canada">Canada</option>
                        </select>
                        <input class="input-text" type="tel" name="install_mailbox_phone" placeholder="Phone number" required>
                        <input class="input-text" type="email" name="install_mailbox_email" placeholder="Email Address" required>
                        <button type="submit">Submit</button>
                    </fieldset>
                </form>

				<div class="weekly-row subscriptions-row">

					<div class="col col-1">
						<h2>Hamodia Weekly</h2>
						<p>Our weekly edition is available on most newsstands and via home delivery in most locations Wednesday morning in preparation for Shabbos, providing our readers with a trove of reading material that covers every aspect of Jewish life. Special sections dedicated to Israeli and national news roundup and features, a full-color glossy magazine with a dedicated Shabbos section as well as a youth news-zine provides stimulation for the whole family in this not-to-be-missed weekend companion.</p>
					</div>

					<div class="col col-2">
						<h2>Subscription to the WEEKLY&nbsp;EDITION includes:</h2>

						<ul>
							<li class="news odd"><strong>News</strong> – Keeps you informed! Wednesday’s edition brings you breaking national and international news, timely features, youth section and largest classified section.</li>
							<li class="features"><strong>PRIME News Magazine</strong> – NEW: A smorgasbord of hard-hitting opinions and news analyses, political and business features, satire, cartoons and quotes, and interviews in glossy magazine format.</li>
							<li class="community odd"><strong>Community News</strong> – Keeps you connected!
Photos and news tidbits from around the country and around the Jewish globe, special sections, tributes, biographies and more.
</li>
							<li class="inyan"><strong>Inyan</strong> – Redesigned and expanded; Torah, halachah and hashkafah, historic and current feature articles, humor as well as youth pages.</li>
							<!--<li class="binyan odd"><strong>MONTHLY SPARKIT MAGAZINE for youth</strong> – NEW: A dazzling array of articles for the young and young at heart.</li>-->
						</ul>

					</div>

				</div>

				<div class="daily-row subscriptions-row">

					<div class="col col-1">
						<h2>Hamodia Daily</h2>
						<p>Keep your finger on the pulse with a special emphasis on the global and local Jewish communities. The daily edition brings you the latest in politics, business, Israel, local and community news, opinions and analyses that directly affect our lives. The daily is available in print and digital editions.</p>
					</div>

					<div class="col col-2">
						<h2>Subscription to the DAILY&nbsp;EDITION includes:</h2>
						<ul>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/images/subscribe/monday.png?v=4" />
								<span class="day">Monday</span>
							</li>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/images/subscribe/tuesday.png?v=4" />
								<span class="day">Tuesday</span>
							</li>
							<li class="wednesday">
								<img src="<?php echo get_template_directory_uri(); ?>/images/subscribe/wednesday.png?v=4" />
								<span class="day">Wednesday</span>
							</li>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/images/subscribe/thursday.png?v=4" />
								<span class="day">Thursday</span>
							</li>
							<li>
								<img src="<?php echo get_template_directory_uri(); ?>/images/subscribe/friday.png?v=4" />
								<span class="day">Friday</span>
							</li>
						</ul>
					</div>

				</div>

				<div id="first-list"></div>
				<div id="second-list"></div>
				<div id="subscribe-billing"></div>

				<?php include dirname(__FILE__) . '/subscribe.handlebars'; ?>

			</div>
		</div><!-- /#main -->

	</div><!-- End main row -->

<?php get_footer(); ?>
