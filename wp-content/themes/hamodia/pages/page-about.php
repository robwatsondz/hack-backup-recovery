<?php
/*
Template Name: About template
*/
get_header();
?>

	<!-- Row for main content area -->
	<div id="content" class="row">
	
		<div id="main" class="eight columns with-sidebar" role="main">
			<div class="post-box">
				<h1 class="typographic-heading"><?php the_title(); ?></h1>

				<div class="about-header">
					<img class="printing-image" src="<?php echo get_template_directory_uri(); ?>/images/about-printing.jpg">
					<div class="info">
						<h2>Hamodia</h2>
						<p>The Daily Newspaper for Torah Jewry
							[USPS&nbsp;016835] [ISSN&nbsp;#1553-9490]
							is published daily except for Saturday,
							Sunday, and the week of Pesach.</p>
						<p>207 Foster Avenue, Brooklyn, NY 11230.</p>

					</div>
				</div>

				<div class="about-main-text">
					<p><strong>Hamodia</strong> was founded nearly 100 years ago in Eastern Europe with the active support and guidance of the foremost Torah leaders, including Hagaon Harav Chaim Ozer Grodzenski, the Gerrer Rebbe, Harav Avrohom Mordechai Alter, and Hagaon Harav Chaim Brisker, and edited by Rabbi Eliyahu Akiva Rabinowitz, to assist in the critical battle against the strong winds of assimilation that threatened the very foundations of our nation. In this struggle for survival, these <em>Gedolim</em> and others recognized the power of a Torah-true newspaper as the appropriate response to the lure of the spiritual war being waged against the Torah camp by popular youth movements.</p>

					<p>In the aftermath of the destruction of European communities at the hands of the Nazis, <em>Hamodia</em> resurfaced as the authentic voice of the bloodied yet unbowed Torah-true community struggling to rebuild itself on different continents. <em>Hamodia</em>'s growth paralleled the miraculous resurgence of a vibrant Orthodox community in Israel, America, and major Jewish communities around the world. The daily Hebrew-language edition, which was launched in 1950, emerged as the leading publication of the religious community in Israel.  The founding editor, Rabbi Y.L. Levin, is the father of <em>Hamodia</em>'s current publisher, Mrs. Ruth Lichtenstein.</p>

					<p>In 1998, the English-language weekend <em>Hamodia</em> was launched to serve the burgeoning Torah community in America and Europe, as well as the English-speaking community in Israel. <em>Hamodia</em> quickly rose to prominence worldwide. On December 15, 2003, <em>Hamodia</em> revolutionized the American community with its introduction of a daily edition, the only one of its kind in the world.</p>

					<p><em>Hamodia</em>, the name synonymous with the highest level of quality journalism presented through the Torah perspective, reaches and influences hundreds of thousands of Jewish households worldwide through its daily and weekend editions. <em>Hamodia</em>'s digital edition, which began in 2010, makes the daily edition accessible to readers throughout the country and across the world who are beyond the delivery area of a daily newspaper.</p>
				</div>

				<p class="about-footer">&copy; All rights reserved.<br>
				Reproduction by any means without written permission from the publisher is prohibited.</p>

			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- End main row -->
	
<?php get_footer(); ?>