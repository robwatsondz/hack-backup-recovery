<?php
/*
Template Name: Homepage Template
*/
get_header(); ?>

	<div id="content" class="row">

<div style="margin-bottom:10px;margin-top:0px;">
<a href="http://images.hamodia.com/hamod-uploads/2017/10/03110632/Discovery-Quiz-Email.pdf" target="_blank"><img style="width:1000px" src="http://images.hamodia.com/hamod-uploads/2017/10/03110626/DC_970x100-banner.gif" /></a>
</div>

	 	<div style="margin-bottom:10px;margin-top:0px;">
<?php
				if (isset($_GET['test']) && $_GET['test'] == '1')
				{
					if(function_exists('oiopub_banner_zone')) oiopub_banner_zone(1, 'center');
				}
				else
				{
					echo '<a href="/stop-talking-shul/"><img style="width:1000px" src="http://images.hamodia.com/hamod-uploads/2017/02/14192534/banner-shul-talkiing-3.jpg" /></a>';
				}

 		?>
		</div>
		<div id="main" class="with-sidebar" role="main">
			<div class="post-box">

				<?php Hamodia_homepage::promotional(); ?>

				<div class="row">
					<h2 class="home-top-stories-heading">Top Stories</h2>
					<?php Hamodia_homepage::main_articles(); ?>
				</div>

				<div class="row">
					<div class="six columns">
						<?php Hamodia_homepage::category('israel'); ?>
					</div>
					<div class="six columns">
						<?php Hamodia_homepage::category('national'); ?>
                    </div>
                </div>

                <div class="row content_by_spo">
                	<div class="four columns">
                		<!--<strong>Sponsored Content</strong>
                		<?php //if(function_exists('oiopub_banner_zone')) oiopub_banner_zone(2, 'center'); ?>
                	</div>
			<div class="four columns">
				<strong>&nbsp;</strong>
                		<?php //if(function_exists('oiopub_banner_zone')) oiopub_banner_zone(10, 'center'); ?>
                	</div>
                	<div class="four columns">
                		<strong>&nbsp;</strong>
                		<?php //if(function_exists('oiopub_banner_zone')) oiopub_banner_zone(9, 'center'); ?>-->
                	</div>
                </div>

                <div class="row">
                    <div class="six columns">
                        <?php Hamodia_homepage::category('business'); ?>
                    </div>
                    <div class="six columns">
                        <?php Hamodia_homepage::category('community'); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="six columns">
                        <?php Hamodia_homepage::category('world'); ?>
                    </div>
                    <div class="six columns">
                        <?php Hamodia_homepage::category('regional'); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="six columns">
                        <?php Hamodia_homepage::category('politics'); ?>
					</div>
					<div class="six columns">
						<?php Hamodia_homepage::category('technology'); ?>
					</div>
				</div>



				<div class="row">
					<div class="twelve columns">

<a href="http://hamodia.com/2017/07/19/hamodia-awarded-excellence/"><img src="http://images.hamodia.com/hamod-uploads/2017/07/25184146/NYPA_Awards-upd-2017-FOR-WEB.jpg" style="border:0px;margin-top:10px;margin-bottom:10px" /></a>
						<?php Hamodia_homepage::newsletter(); ?>
					</div>
				</div>

			</div>
		</div>

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside>

	</div>

<?php get_footer();