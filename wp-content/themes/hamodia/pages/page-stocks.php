<?php
/*
Template Name: Stocks Template
*/
get_header();

$header_stocks = Hamodia_APIs::get_stocks_info(['^DJI', '^IXIC', '^GSPC']);
$table_symbols = ['^DJI', '^IXIC', '^GSPC', 'AA', 'AAPL', 'ABT', 'ABX', 'ACI', 'AIG', 'AKS', 'ALU', 'AMD', 'ANR', 'BAC', 'BBT', 'BMY', 'BP', 'BSX', 'BTU', 'C', 'CAT', 'CHK', 'CLF', 'CNX', 'COH', 'COP', 'CSX', 'CVS', 'CX', 'DAL', 'DF', 'DHR', 'DNR', 'DOW', 'ECA', 'EMC', 'F', 'FCX', 'GE', 'GEN', 'GGB', 'GLW', 'GM', 'GNW', 'HAL', 'HD', 'HK', 'HL', 'HON', 'HOV', 'HPQ', 'ITUB', 'JNJ', 'JPM', 'KEY', 'KGC', 'KO', 'KOG', 'KR', 'LLY', 'LOW', 'LSI', 'LUV', 'MCD', 'MGM', 'MHR', 'MO', 'MRK', 'MRO', 'MS', 'MT', 'NLSN', 'NLY', 'NOK', 'NXY', 'OSG', 'PBR', 'PCS', 'PFE', 'PG', 'PHM', 'RF', 'S', 'SCHW', 'SD', 'SID', 'SNV', 'STI', 'SVU', 'SWY', 'T', 'TLM', 'TSM', 'UPS', 'USB', 'VALE', 'VZ', 'WAG', 'WFC', 'WFT', 'WLT', 'X', 'XOM', 'XRX'];
$stocks = Hamodia_APIs::get_stocks_info( $table_symbols );

if (isset($_GET['symbol']))
{
	$searched_stock = Hamodia_APIs::get_stocks_info([$_GET['symbol']]);

	array_unshift($stocks, $searched_stock[0]);
}

?>

<!-- Row for main content area -->
<div id="content" class="row">

	<div id="main" class="with-sidebar" role="main">
		<div class="post-box">

			<h1 class="typographic-heading">Stocks</h1>

			<div class="row">
				<?php foreach ($header_stocks as $stock): ?>
				<div class="four columns">
					<div class="feature-box <?php echo substr($stock['change_points'], 0, 1) == '+' ? 'up' : 'down'; ?>">
						<h2><?php echo $stock['symbol']; ?></h2>
						<span class="price"><?php echo $stock['price']; ?></span>
						<span class="change"><?php echo $stock['change_points']; ?></span>
						<span class="change-percent">(<?php echo $stock['change_percent']; ?>)</span>
					</div>
				</div>
				<?php endforeach ?>
			</div>

			<div style="padding-bottom:10px">Note: quotes may be delayed by up to twenty minutes.</div>

			<div class="stocks-wrapper">
				<table class="stocks <?php if (isset($_GET['symbol'])) echo 'search' ?>">
					<thead>
						<tr>
							<th>Company</th>
							<th>Price</th>
							<th>Change</th>
							<th>% Change</th>
							<th>P/E</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($stocks as $stock): ?>
							<tr class="<?php echo substr($stock['change_points'], 0, 1) == '+' ? 'up' : 'down'; ?>">
								<td>
									<span class="symbol"><?php echo $stock['symbol']; ?></span>
									<span class="name"><?php echo $stock['name']; ?></span>
								</td>
								<td class="price"><?php echo $stock['price']; ?></td>
								<td class="change"><?php echo $stock['change_points']; ?></td>
								<td class="change-percent"><?php echo $stock['change_percent']; ?></td>
								<td class="pe"><?php echo $stock['pe']; ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>

		</div>
	</div><!-- /#main -->

	<aside id="sidebar" role="complementary">
		<div class="sidebar-box">
			<?php get_sidebar(); ?>
		</div>
	</aside><!-- /#sidebar -->

</div><!-- End main row -->

<?php get_footer();
