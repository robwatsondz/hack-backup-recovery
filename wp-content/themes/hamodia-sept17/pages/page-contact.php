<?php
/*
Template Name: Contact template
*/
get_header();
?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="eight columns with-sidebar" role="main">
			<div class="post-box">
				<h1 class="typographic-heading"><?php the_title(); ?></h1>

				<div class="contact-header">
					<div class="info">
						<h2>Hamodia</h2>
						<p>
							207 Foster Ave.<br>
							Brooklyn, NY 11230<br>
							Tel: (718) HAMODIA<br>
							<strong>T: (855) 4 HAMODIA</strong><br>
							T: (718) 853-9094<br>
							E: info@hamodia.com
						</p>
					</div>
					<div class="image-wrapper">
						<img class="map-image" src="<?php echo get_template_directory_uri(); ?>/images/contact-map.jpg">
					</div>
				</div>

				<div class="row contact-departments">
					<div class="six columns">
						<h3>Advertising</h3>

						<h4>ADVERTISING/SALES:</h4>
						<p>T: (718) 853-9094 x505<br>
						F: (718) 689-1372<br>
						E: sales@hamodia.com<br>
						ads@hamodia.com</p>

						<h4>ADVERTISING MANAGER:</h4>
						<p>T: (718) 853-9094 x202<br>
						F: (718) 689-1372<br>
						E: manager@hamodia.com</p>

						<h4>CLASSIFIEDS:</h4>
						<p>T: (718) 853-9094 x502<br>
						F: (718) 437-2281<br>
						E: classifieds@hamodia.com</p>

						<h4>LEGAL NOTICES:</h4>
						<p>T: (718) 853-9094 x502<br>
						E: classifieds@hamodia.com</p>
					</div>
					<div class="six columns">
						<h3>Departments</h3>

						<h4>HUMAN RESOURCES:</h4>
						<p>T: (718) 853-9094 x200<br>
						E: humanresources@hamodia.com</p>

						<h4>MAGAZINE (INYAN):</h4>
						<p>T: (718) 853-9094 x234<br>
						E: magazine@hamodia.com</p>

						<h4>SHABBOS MAGAZINE (KINYAN):</h4>
						<p>T: (718) 853-9094 x250<br>
						E: kinyan@hamodia.com</p>

						<h4>YOUTH MAGAZINE (BINYAN):</h4>
						<p>T: (718) 853-9094 x215<br>
						E: binyan@hamodia.com</p>

						<h4>SUPPLEMENTS:</h4>
						<p>T: (718) 853-9094 x505<br>
						E: info@hamodia.com</p>
					</div>
				</div>

				<div class="row contact-departments">
					<div class="six columns">
						<h3>Services</h3>

						<h4>COMMUNITY CALENDAR:</h4>
						<p>T: (718) 853-9094 x248<br>
						E: calendar@hamodia.com</p>

						<h4>DISTRIBUTION:</h4>
						<p>T: (718) 853-9094 x227<br>
						E: distribution@hamodia.com</p>

						<h4>HOME DELIVERY:</h4>
						<p>T: (718) 853-9094 x501<br>
						E: homedelivery@hamodia.com</p>

						<h4>MISASKIM:</h4>
						<p>T: (718) 853-9094 x505<br>
						E: ads@hamodia.com</p>

						<h4>REPRINTS:</h4>
						<p>T: (718) 853-9094 x292<br>
						E: reprints@hamodia.com</p>

						<h4>SIMCHAS:</h4>
						<p>T: (718) 853-9094 x242<br>
						E: Simchas@hamodia.com</p>

						<h4>SPECIAL ORDERS:</h4>
						<p>T: (718) 853-9094 x215<br>
						E: info@hamodia.com</p>

						<h4>SUBSCRIPTIONS:</h4>
						<p>T: (718) 853-9094 x5101<br>
						F: (718) 689-1371<br>
						E: subscriptions@hamodia.com</p>

						<h4>SUGGESTION BOX:</h4>
						<p>T: (718) 853-9094 x211<br>
						E: suggestionbox@hamodia.com</p>
					</div>
					<div class="six columns">
						<h3>Submissions</h3>

						<h4>ARTICLES:</h4>
						<p>T: (718) 853-9094 x277<br>
						E: submissions@hamodia.com</p>

						<h4>COMMENTS:</h4>
						<p>T: (718) 853-9094 x211<br>
						E: comments@hamodia.com</p>

						<h4>COMMUNITY NEWS:</h4>
						<p>T: (718) 853-9094 x292<br>
						E: c2c@hamodia.com</p>

						<h4>LETTERS:</h4>
						<p>T: (718) 853-9094 x211<br>
						E: letters@hamodia.com</p>

						<h4>NEWS TIPS:</h4>
						<p>T: (718) 853-9094<br>
						E: tips@hamodia.com</p>

						<h4>OPINIONS:</h4>
						<p>T: (718) 853-9094 x211<br>
						E: opinions@hamodia.com</p>

						<h4>PHOTOS:</h4>
						<p>T: (718) 853-9094 x219<br>
						E: photos@hamodia.com</p>

						<h4>READERS’ FORUM:</h4>
						<p>T: (718) 853-9094 x211<br>
						E: letters@hamodia.com</p>

						<h4>TRIBUTES:</h4>
						<p>T: (718) 853-9094 x292<br>
						E: weekly@hamodia.com</p>
					</div>
				</div>

				<div class="row contact-locations">
					<div class="six columns">
						<h3>USA</h3>
						<p>HAMODIA<br>
						207 Foster Avenue<br>
						Brooklyn, NY 11230<br>
						<br>
						T: (718) 853-9094<br>
						F: (718) 853-9103<br>
						E: info@hamodia.com</p>
					</div>
					<div class="six columns">
						<h3>ISRAEL</h3>
						<p>HAMODIA<br>
						16A Petach Tikva street<br>
						Jerusalem, Israel<br>
						T: (972) 2 595 2888<br>
						F: (972) 2 569 6098<br>
						Info:  englishweekly@hamodia.com<br>
						Subscription: subisrael@hamodia.com</p>
					</div>
				</div>

				<div class="row contact-locations">
					<div class="six columns">
						<h3>LONDON</h3>
						<p>HAMODIA<br>
						113 Fairview Rd.<br>
						London, N15 6TS<br>
						T: (44) 20 8442 7777<br>
						F: (44) 20 8442 7778<br>
						E: info@hamodia.co.uk</p>
					</div>
					<div class="six columns">
						<h3>BELGIUM</h3>
						<p>HAMODIA<br>
						Lge Leemstr. 206<br>
						2018 Antwerp,<br>
						T: (32) 3 230 27 76<br>
						F: (32) 3 285 98 66<br>
						E: chaim.rand@goldata.be</p>
					</div>
				</div>

			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- End main row -->

<?php get_footer(); ?>
