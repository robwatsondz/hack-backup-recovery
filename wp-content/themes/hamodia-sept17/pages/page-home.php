<?php
/*
Template Name: Homepage Template
*/
get_header(); ?>

	<div id="content" class="row">
		<?php
		if (!X2_IS_APP)
		{
			echo '<div class="header_and_content_split_block" style="margin-bottom:30px;margin-top:10px;">';


			echo "
			
				<div class=\"xdesktop\">
					<!-- /21739840349/large-leaderboard -->
					<div id='div-gpt-ad-1535391032420-0' class=\"header_ad_top\">
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1535391032420-0'); });
					</script>
					</div>
					
					<!-- /21739840349/large-leaderboard-2 -->
					<div id='div-gpt-ad-1543257079329-0' style=\"margin-top:10px\"  class=\"header_ad_top\">
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1543257079329-0'); });
					</script>
					</div>
					
					<!-- /21739840349/large-leaderboard-3 -->
					<div id='div-gpt-ad-1573581814153-0' style=\"margin-top:10px\"  class=\"header_ad_top\">
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1543257079329-0'); });
					</script>
					</div>
					
				</div>
				<div class=\"xmobile\">
				
					<!-- /21739840349/mob_leader1_320 -->
					<div id='div-gpt-ad-1574545166375-0' style=\"margin-top:10px\" class=\"header_ad_top\">
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1574545166375-0'); });
					</script>
					</div>
					
					<!-- /21739840349/mob_leader2_320 -->
					<div id='div-gpt-ad-1574544686419-0' style=\"margin-top:10px\"  class=\"header_ad_top\">
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1574544686419-0'); });
					</script>
					</div>

				</div>
			
			";


			echo '</div>';
		}
 		?>


<!-- THIS IS THE CORRECT HOME TEMPLATE SLOT 2 
<div style="margin-top:10px;margin-bottom:10px;text-align:center"><a href="http://images.hamodia.com/hamod-uploads/2018/03/21152207/project-witness-lecture.pdf" target="_blank"><img src="http://images.hamodia.com/hamod-uploads/2018/04/15225254/ThisWeek970x90.gif" alt="Project Witness" border="0" width="970" /></a></div>
-->

		<div id="main" class="with-sidebar" role="main">
			<div class="post-box">

				<?php Hamodia_homepage::promotional(); ?>

					<div class="top_block">

						<div class="home_layout_container">
						
							<div class="top_block_left">

								<div class="top_stories_header" style="margin-left:5px;margin-bottom:5px;font-size:115%;font-weight:bold">TOP STORIES</div>
								<?php Hamodia_homepage::main_articles(5,1); ?>
							</div>
							
							<div class="top_block_right">
								<?php Hamodia_homepage::main_articles(1); ?>
								<div class="from_inyan_block"></div>
								<div class="main_listing_layout">
									
									<?php Hamodia_homepage::category('israel',3,false); ?>
									<?php Hamodia_homepage::category('community',3,false); ?>
									
								</div>
							</div>
						</div>

						<div class="main_listing_layout">
							<div class="oiomainlisting oiomainlisting1" style="padding-top:30px;padding-bottom:30px; text-align:center;">
								<!-- /21739840349/midleft-720x90 -->
								<div id='div-gpt-ad-1538577834221-0'>
								<script>
								googletag.cmd.push(function() { googletag.display('div-gpt-ad-1538577834221-0'); });
								</script>
								</div>

							</div>
							<?php Hamodia_homepage::category('national',2,false); ?>
							<?php Hamodia_homepage::category('world',2,false); ?>

							
							<div class="oiomainlisting oiomainlisting2" style="padding-top:30px;padding-bottom:30px; text-align:center;">
								<!-- /21739840349/midleft2-720x90 -->

								<div id='div-gpt-ad-1548727342975-0'>
								<script>
								googletag.cmd.push(function() { googletag.display('div-gpt-ad-1548727342975-0'); });
								</script>
								</div>

							</div>
							<div class="hidden_on_desktop">
								<a href="#" class="show_all_sections" data-rel="hidden_sections">---show all sections---</a>
							</div>
							<div class="hidden_on_mobile" id="hidden_sections">
							
							<?php Hamodia_homepage::category('regional',2,false); ?>
							<?php Hamodia_homepage::category('politics',2,false); ?>
							<?php Hamodia_homepage::category('business',2,false); ?>
							<?php Hamodia_homepage::category('technology',2,false); ?>
						</div>
					</div>

				</div>

			</div>
		</div>

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside>

	</div>

<?php get_footer();
