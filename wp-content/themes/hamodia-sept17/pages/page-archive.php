<?php
/*
Template Name: Archives Template
*/
get_header();
?>


    <!-- Row for main content area -->
    <div id="content" class="row">

        <div id="main" class="with-sidebar" role="main">

            <div class="post-box">

                <h1 class="typographic-heading">Archive</h1>

                <?php

                $is_first_heading = true;
                $current_date_archive = '';
                $archives = new WP_Query([
                    'post_type'         => 'post',
                    'post_status'       => 'publish',
                    'order'             => 'DESC',
                    'posts_per_page'    => 200,
                    'tax_query'         => [
                        [
                            'taxonomy'  => 'post_format',
                            'field'     => 'slug',
                            'terms'     => ['post-format-aside'],
                            'operator'  => 'NOT IN'
                        ],
                    ],
                ]);

                while ($archives->have_posts()) : $archives->the_post();

                    if (($date = get_post_time( 'm-d-Y', true )) !== $current_date_archive) {
                        if ($is_first_heading) {
                            $is_first_heading = false;
                        } else {
                            echo '</ul>';
                        }

                        $current_date_archive = $date;

                        $timestamp    = get_post_time('U', true);
                        $hebrew_date  = Hamodia_APIs::get_hebrew_date($timestamp);
                        $secular_date = date('l, F j, Y', $timestamp);

                        echo sprintf('<h2>%s | <span class="hebrew-date">%s</span></h2><ul>', $secular_date, $hebrew_date);
                    }

                    ?>
                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

                <?php endwhile;  ?>
                </ul>
            </div>
        </div><!-- /#main -->

        <aside id="sidebar" role="complementary">
            <div class="sidebar-box">
                <?php get_sidebar(); ?>
            </div>
        </aside><!-- /#sidebar -->

    </div><!-- End main row -->

<?php get_footer(); ?>
