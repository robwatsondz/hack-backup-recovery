<?php
/*
Template Name: Briefs Template
*/
get_header();
?>


    <!-- Row for main content area -->
    <div id="content" class="row">

        <div id="main" class="with-sidebar" role="main">
            <div class="post-box">
                <?php foreach (['Business', 'New York', 'Israel'] as $category): ?>

                <div class="four columns">
                    <h1><?= $category ?> Briefs</h1>
                    <?php $posts = get_posts([
                        'category_name'  => $category,
                        'posts_per_page' => 10,
                        'tax_query' => [[
                            'taxonomy'  => 'post_format',
                            'field'     => 'slug',
                            'terms'     => 'post-format-aside',
                        ]],
                    ]);
                    foreach ($posts as $post): setup_postdata($post); ?>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <header>
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            </header>

                            <div class="entry-content">
                                <?php the_content(); ?>
                            </div>
                        </article>
                    <?php endforeach; ?>
                </div>

                <?php endforeach; ?>
            </div>
        </div><!-- /#main -->

        <aside id="sidebar" role="complementary">
            <div class="sidebar-box">
                <?php get_sidebar(); ?>
            </div>
        </aside><!-- /#sidebar -->

    </div><!-- End main row -->

<?php get_footer(); ?>
