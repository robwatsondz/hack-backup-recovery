<?php
/*
Template Name: Letters Template
*/
get_header();
?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="eight columns with-sidebar" role="main">
			<div class="post-box">

				<a href="<?php echo get_site_url(); ?>/letters">
					<h1 class="typographic-heading">Letters</h1>
				</a>

				<div class="post-box">
					<?php get_template_part('loop', 'letters'); ?>
				</div>

			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- End main row -->

<?php get_footer(); ?>