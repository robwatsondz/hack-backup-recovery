<?php
/*
Template Name: Stocks Template
*/
get_header();

$header_stocks = Hamodia_APIs::get_stocks_info(['^DJI', '^IXIC', '^GSPC']);
$table_symbols = ['^DJI', '^IXIC', '^GSPC', 'AA', 'AAPL', 'ABT', 'ABX', 'ACI', 'AIG', 'AKS', 'ALU', 'AMD', 'ANR', 'BAC', 'BBT', 'BMY', 'BP', 'BSX', 'BTU', 'C', 'CAT', 'CHK', 'CLF', 'CNX', 'COH', 'COP', 'CSX', 'CVS', 'CX', 'DAL', 'DF', 'DHR', 'DNR', 'DOW', 'ECA', 'EMC', 'F', 'FCX', 'GE', 'GEN', 'GGB', 'GLW', 'GM', 'GNW', 'HAL', 'HD', 'HK', 'HL', 'HON', 'HOV', 'HPQ', 'ITUB', 'JNJ', 'JPM', 'KEY', 'KGC', 'KO', 'KOG', 'KR', 'LLY', 'LOW', 'LSI', 'LUV', 'MCD', 'MGM', 'MHR', 'MO', 'MRK', 'MRO', 'MS', 'MT', 'NLSN', 'NLY', 'NOK', 'NXY', 'OSG', 'PBR', 'PCS', 'PFE', 'PG', 'PHM', 'RF', 'S', 'SCHW', 'SD', 'SID', 'SNV', 'STI', 'SVU', 'SWY', 'T', 'TLM', 'TSM', 'UPS', 'USB', 'VALE', 'VZ', 'WAG', 'WFC', 'WFT', 'WLT', 'X', 'XOM', 'XRX'];
$stocks = Hamodia_APIs::get_stocks_info( $table_symbols );

if (isset($_GET['symbol']))
{
//	$searched_stock = Hamodia_APIs::get_stocks_info([$_GET['symbol']]);
//	array_unshift($stocks, $searched_stock[0]);

	$symbol = strip_tags(trim($_GET['symbol']));

}



?>

<!-- Row for main content area -->
<div id="content" class="row">

	<div id="main" class="with-sidebar" role="main">
		<div class="post-box">
			
			<style>
			#stocks-module
			{
				display:none !important;
			}
			</style>
			<?php
				if ($symbol != '')
				{
			?>		
				<h1 class="typographic-heading">Stock Detail: <?php echo $symbol; ?></h1>
				<iframe frameBorder='0' scrolling='no' style='margin-bottom:20px;' width='100%' height='480' src='https://api.stockdio.com/visualization/financial/charts/v1/HistoricalPrices?app-key=090E5F415E9A45369C55F1DE5301FDDD&symbol=<?php echo $symbol;?>&days=365&width=100%&height=480'></iframe>
			<?php
				}
								
			?>
			
			<h1 class="typographic-heading">Stocks</h1>
			
			<iframe id='st_5dab6671a5af443785d70ad6f248fa44' frameBorder='0' scrolling='no' width='100%' height='100%' src='https://api.stockdio.com/visualization/financial/charts/v1/MarketOverview?app-key=090E5F415E9A45369C55F1DE5301FDDD&showHeader=true&palette=Financial-Light&title=Market%20Overview&onload=st_5dab6671a5af443785d70ad6f248fa44&addVolume=false&showUserMenu=false&culture=English-US&motif=Financial&logoMaxHeight=20&logoMaxWidth=90&includeEquities=true&includeIndices=true&includeCommodities=false&includeCurrencies=false&includeLogo=false&includeEquitiesSymbol=true&includeEquitiesName=false&includeIndiceSymbol=false&includeIncidesName=false&includeCommoditiesSymbol=false&includeCommoditiesName=true&includeCurrenciesSymbol=true&includeCurrenciesName=false&allowSort=true&includePrice=true&includeChange=true&includePercentChange=true&includeTrend=true&includeVolume=false&showHeader=false&showBorderAndTitle=false&linkUrl=http://hamodia.com/stocks/?symbol={symbol}&linkTarget=self'></iframe>
		
		<script>
			jQuery('#st_5dab6671a5af443785d70ad6f248fa44').on('load',function () {
				
			   if (typeof(stockdio_events) == "undefined") {
				  stockdio_events = true;
				  var stockdio_eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
				  var stockdio_eventer = window[stockdio_eventMethod];
				  var stockdio_messageEvent = stockdio_eventMethod == "attachEvent" ? "onmessage" : "message";
				  stockdio_eventer(stockdio_messageEvent, function (e) {					 
					 if (typeof(e.data) != "undefined" && typeof(e.data.method) != "undefined") {
						eval(e.data.method);
					 }
				  },false);
			   }
			});
			</script>
		</div>
	</div><!-- /#main -->

	<aside id="sidebar" role="complementary">
		<div class="sidebar-box">
			<?php get_sidebar(); ?>
		</div>
	</aside><!-- /#sidebar -->

</div><!-- End main row -->

<?php get_footer();
