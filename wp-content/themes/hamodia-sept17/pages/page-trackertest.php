<?php
/*
Template Name: Tracker Test
*/
get_header(); ?>

<?php

/*
Copyright (C) 2008  Simon Emery

This file is part of OIOpublisher Direct.
*/


//define vars
define('OIOPUB_LOAD_LITE', 1);



if (isset($_GET['test']) && $_GET['test'] == '1')
{
	DEFINE('ISTEST',1);
}
else
{
	//image type
	header("Content-Type: image/gif");
	header("Content-Length: " . strlen($image_pixel));
}

//init
include_once("../../index.php");

//log visit?
if(isset($oiopub_plugin['tracker'])) {
	$ids = oiopub_var('pids', 'get');
	$oiopub_plugin['tracker']->log_visit($ids);
}
?>


<?php get_footer();
