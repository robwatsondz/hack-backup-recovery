<?php


// Initial setup
add_action('after_setup_theme', function ()
{
	// Add language supports. No language files yet.
	load_theme_textdomain('hamodia', get_template_directory() . '/lang');

	add_theme_support('post-formats', ['image', 'gallery']);

	add_filter('post_gallery', function ($output, $attributes) {
		if (is_feed()) return;

		return HamodiaGallery::make($attributes)->render();
	}, 10, 4);
});

/*
 * Customized the output of caption, you can remove the filter to restore back to the WP default output.
 * Courtesy of DevPress. http://devpress.com/blog/captions-in-wordpress/
 */
add_filter('img_caption_shortcode', function ($output, $attr, $content)
{
	// We're not worried about captions in feeds, so just return the output as is.
	if (is_feed()) return $output;

	/* Set up the default arguments. */
	$attr = shortcode_atts([
		'id'      => '',
		'align'   => 'alignnone',
		'width'   => '',
		'caption' => '',
	], $attr);

	/* If the width is less than 1 or there is no caption,
	// return the content wrapped between the [caption]< tags. */
	if (1 > $attr['width'] || empty($attr['caption'])) {
		return $content;
	}

	/* Set up the attributes for the caption <figure>. */
	$attributes = 'class="figure ' . esc_attr($attr['align']) . '"';

	$attr['width'] && $attributes .= ' style="max-width:' . esc_attr($attr['width']) . 'px"';

	return	'<figure ' . $attributes . '>' .
				do_shortcode( $content ) .
				'<figcaption>' . $attr['caption'] . '</figcaption>' .
			'</figure>';
}, 10, 3);

// Clean the output of attributes of images in editor. Courtesy of SitePoint.
// http://www.sitepoint.com/wordpress-change-img-tag-html/
add_filter('get_image_tag_class', function ($class, $id, $align, $size) {
	$align = 'align' . esc_attr($align);
	return $align;
}, 0, 4);


function wpdocs_theme_name_scripts() {

	if (X2_IS_APP)
	{
		wp_enqueue_style( 'hamodiaapp-css', '/wp-content/themes/hamodia-sept17/css/hamodiaapp.css?d=190122-02' );
	}

	if (isset($_GET['destroy_session']))
	{
		unset($_SESSION['X2_IS_APP']);
	}

	$menu_name = 'Mobile App Menu';
	$menu_exists = wp_get_nav_menu_object( $menu_name );

	// If it doesn't exist, let's create it.
	if( !$menu_exists){
	    $menu_id = wp_create_nav_menu($menu_name);

		// Set up default menu items
	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('Home'),
	        'menu-item-classes' => 'home',
	        'menu-item-url' => home_url( '/' ),
	        'menu-item-status' => 'publish'));

	    wp_update_nav_menu_item($menu_id, 0, array(
	        'menu-item-title' =>  __('Custom Page'),
	        'menu-item-url' => home_url( '/custom/' ),
	        'menu-item-status' => 'publish'));

	}

}



add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );


add_theme_support('menus');


function admin_bar(){

  if(is_user_logged_in()){
    add_filter( 'show_admin_bar', '__return_true' , 1000 );
  }
}

if (isset($_GET['test']) && $_GET['test'] == '1')
{
	add_action('init', 'admin_bar' );
}

add_filter('pnfw_notification_payload', 'my_notification_payload', 10, 2);

function my_notification_payload($payload, $post_id) {

	$post = get_post($post_id);
	if (isset($post->post_title))
	{
		$url = get_permalink($post_id);
		$url = str_replace('http://','https://',$url);
		$payload['message_alert'] = 'Would you like to view this article?';
		$payload['mail_queue_id'] = $post_id;
		$payload['controller'] = 'cms';
		$payload['FROM_GCM'] = 1;
		$payload['data_url'] = $url;
		$payload['data_title'] = 'Hamodia: ' . $post->post_title;
		
		$payload['icon'] = 'ic_stat_name';
		$payload['body'] = 'Tap to read more';
	}
	
	return $payload;
}



function xpnfw_before_sending_notification_to_user( $current = true , $user_id, $post ) {
    // Maybe modify $example in some way.
    
	$custom_taxonomy = '';
	// Dont forget we switched features and columns.
	//echo $post_type;
	switch ($post->post_type) {
		case 'hamodia_frominyan':
							$custom_taxonomy = 'frominyan_category';
							break;
		case 'hamodia_feature':
							$custom_taxonomy = 'column_category';
							break;
		case 'hamodia_column':
							$custom_taxonomy = 'feature_category';
							break;
	}
	
  
    if ($custom_taxonomy != '')
	{
			if ($user_id > 0)
			{

				$exclude = xdoes_user_have_post_type_excluded($user_id, $custom_taxonomy);
	
				return !$exclude;

			}


	}

    return $current;
}

add_filter( 'pnfw_before_sending_notification_to_user', 'xpnfw_before_sending_notification_to_user', 10, 3 );

function xdoes_user_have_post_type_excluded($user_id, $post_type)
{
	GLOBAL $wpdb;

	$args = array(
		'hide_empty' => false
	);

	$terms = get_terms($post_type,$args); // Get all terms of a taxonomy
	// If we have a record.
	if (isset($terms[0]))
	{
		$term = $terms[0];

		$results = $wpdb->get_results('SELECT * FROM hamod_push_excluded_categories WHERE user_id = '.$user_id.' and category_id= '.$term->term_id);

		// If the record exists
		if (isset($results[0]))
			return true;
		
	}

	return false;

}

//Set Default Meta Value
function set_default_meta_new_post($post_ID){

	// turn off posts for default.
	update_post_meta($post_ID, 'pnfw_do_not_send_push_notifications_for_this_post', true);
	
	return $post_ID;

}

//add_action('wp_insert_post','set_default_meta_new_post');

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

