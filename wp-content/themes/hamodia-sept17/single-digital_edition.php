<!doctype html>
<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
		<title><?php wp_title(''); ?></title>

		<meta name="description" content="This page displays the Hamodia Daily Newspaper">
		<meta name="author" content="http://hamodia.com">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<style>
			html, body, iframe {
				margin: 0;
				padding: 0;
				border: 0;
				outline: 0;
				width: 100%;
				height: 100%;
			}
		</style>

		<script type="text/javascript">

		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-40015125-1']);
		  _gaq.push(['_trackPageview']);

		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();

		</script>

	</head>



	<body>

		<?php the_post(); $ePaperFlipID = get_post_meta($post->ID, '_hamodia_epaperflipid', true); ?>
		
		<?php 
		if (strtolower(substr($ePaperFlipID,0,8)) == 'https://')
			echo '<iframe src="' . $ePaperFlipID .'" frameborder="0">';
		else
			echo '<iframe src="https://online.flowpaper.com/'. $ePaperFlipID .'" frameborder="0">';
		?>		
		</iframe>

	</body>

</html>