<!doctype html>
<!--[if IE 8]>    <html class="no-js ie ie8 oldie" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js<?php echo $extra_class;?>" lang="en"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">

	<title><?php hamodia_page_title(); ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css?v=2021-02-21-v2">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/photoswipe/photoswipe.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/photoswipe/default-skin/default-skin.css">

	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
		<script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>
	<![endif]-->

	<!-- Favicon and Feed -->
	<link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">

	<!--  iPhone Web App Home Screen Icon -->
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-icon-retina.png" />
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-icon.png" />

	<!-- Enable Startup Image for iOS Home Screen Web App -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/mobile-load.png" />
	<meta name="apple-mobile-web-app-title" content="Hamodia" />

	<!-- Startup Image iPad Landscape (748x1024) -->
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-load-ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)" />
	<!-- Startup Image iPad Portrait (768x1004) -->
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-load-ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)" />
	<!-- Startup Image iPhone (320x460) -->
	<link rel="apple-touch-startup-image" href="<?php echo get_template_directory_uri(); ?>/images/devices/reverie-load.png" media="screen and (max-device-width: 320px)" />


	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,900|Tinos:400,400i,700,700i" rel="stylesheet">
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.foundation.js"></script>
	<script src="https://use.fontawesome.com/b17ad96723.js"></script>
	
	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>

	<style type="text/css">
	.pswp__button.pswp__button--fs
	{
		display:none !important;
	}

	#mainBody .pswp__button.pswp__button--fs
	{
		display:inline-block !important
	}
	
	.post-box .share-buttons .whatsapp a:before
	{
		display:none;
	}

	.post-box .share-buttons .whatsapp a,
	.post-box .share-buttons .whatsapp a:hover
	{
		padding-left :10px;
		background:#25D366;
		color:#fff;
		border-color:#25D366;
	}

	.post-box .share-buttons .whatsapp a .fa
	{
		font-size:22px;  
		color:#fff;
		display:inline-block;
		vertical-align:middle;
		margin-right:5px;
		margin-top:-2px;
	}
	
	.subCats
{
  background:#104381;
  color:#fff;
  margin:0 !important;
  padding:0 !important;
  list-style: none;
  width:200px;
  margin-right:20px !important;
  margin-top:10px !important;
  margin-bottom:20px !important;
  float:left;
}

.subCats li
{
  margin:0 !important;
  padding:0 !important;
  list-style: none;
  border-bottom:1px solid #fff;
}

.subCats li a
{
  color:#fff;
  display:block;
  padding:5px;
}

@media screen and (min-width: 769px)
{
#main-nav > ul li {
    
    padding: 0px 12px 0px 0px !important;
    
}

#main-nav > ul li ul li {
    
    padding: 0px 16px 0px 0px !important;
    
}
}


@media screen and (max-width:768px)
{
  
	
  .subCats
{
  width:100%;
  float:none;
}


}

	@media screen and (max-width:500px) and (min-width:381px)
{
		.post-box .share-buttons > li > a::before
	{
		width:20px;
		height:18px;
		background-size:cover !important;
	}


	.post-box .share-buttons .gmail > a::before {
			background-position: -40px 0;
	}

	.post-box .share-buttons .email > a::before {
			background-position: -20px 0;
	}

	.post-box .share-buttons .whatsapp a .fa
	{
		font-size:18px;
	}

	.post-box .share-buttons > li > a {    
			font-size: 12px;
			line-height: 25px;
			padding: 0 10px 0 30px;
		overflow:hidden;
	}
}

@media screen and (max-width:380px)
{

	.post-box .share-buttons li
	{
		width:calc(50% - 5px);
	}
	
	.post-box .share-buttons li:nth-of-type(even)
	{
		float:right;
	}
	
	.post-box .share-buttons li a
	{
		margin-left:0;
	}
	
}

	</style>

	<style type="text/css">
	@font-face {
		font-family: 'Climacons-Font';
		src:url('/wp-content/plugins/wp-cloudy/css/climacons-webfont.eot');
		src:url('/wp-content/plugins/wp-cloudy/css/climacons-webfont.eot?#iefix') format('embedded-opentype'),
		url('/wp-content/plugins/wp-cloudy/css/climacons-webfont.svg#Climacons-Font') format('svg'),
		url('/wp-content/plugins/wp-cloudy/css/climacons-webfont.woff') format('woff'),
		url('/wp-content/plugins/wp-cloudy/css/climacons-webfont.ttf') format('truetype');
		font-weight: normal;
		font-style: normal;
	}

	.wpc-weather-id,
	.wpc-weather-id .now,
	.wpc-weather-id .infos,
	.wpc-weather-id .short_condition,
	.wpc-weather-id .today .day
	{
		max-width:100% !important;
		width:100% !important;
	}
#div-gpt-ad-1535391032420-0,
#div-gpt-ad-1535392260808-0
{
    width:100% !important;
}
	</style>


<?php
if (X2_IS_APP)
	echo '<script type="text/javascript">var is_app = true;</script>';
else
	echo '<script type="text/javascript">var is_app = false;</script>';
?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-40015125-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-40015125-1');
</script>

<!--<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-3280339220656440",
    enable_page_level_ads: true
  });
</script>-->


<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];
	
	googletag.cmd.push(function() {
		var pubads = googletag.pubads().addEventListener('slotRenderEnded', function(event) { 

		
			// determine the element so we can adapt.
			switch (event.slot.getSlotElementId())
			{
				case 'div-gpt-ad-1535391032420-0' :
					var adobj = jQuery('#div-gpt-ad-1535391032420-0');
					break;
				case 'div-gpt-ad-1543257079329-0' :
					var adobj = jQuery('#div-gpt-ad-1543257079329-0');
					break;
				case 'div-gpt-ad-1574545166375-0' :
					var adobj = jQuery('#div-gpt-ad-1574545166375-0');
					break;
				case 'div-gpt-ad-1574544686419-0' :
					var adobj = jQuery('#div-gpt-ad-1574544686419-0');
					break;
				case 'div-gpt-ad-1535391583522-0' :					
					//jQuery('#div-gpt-ad-1535391583522-0').closest('.adbox').hide();
					break;
				case 'div-gpt-ad-1535391769376-0' :
					//jQuery('#div-gpt-ad-1535391769376-0').closest('.adbox').hide();
					break;
				case 'div-gpt-ad-1574366942128-0' :					
					var adobj = jQuery('.gpt3');
					break;
				case 'div-gpt-ad-1535392012584-0' :
					var adobj = jQuery('.gpt4');
					break;
				case 'div-gpt-ad-1535392059431-0' :
					var adobj = jQuery('.gpt5');
					break;
				case 'div-gpt-ad-1535392189194-0' :
					var adobj = jQuery('.gpt6');
					break;
				case 'div-gpt-ad-1571615207403-0' :					
					var adobj = jQuery('.gpt3');
					break;
				case 'div-gpt-ad-1571615359407-0' :
					var adobj = jQuery('.gpt4');
					break;
				case 'div-gpt-ad-1571615575865-0' :
					var adobj = jQuery('.gpt5');
					break;
				case 'div-gpt-ad-1571615782942-0' :
					var adobj = jQuery('.gpt6');
					break;
				case 'div-gpt-ad-1538577834221-0' :
					var adobj = jQuery('.oiomainlisting1');
					break;
				case 'div-gpt-ad-1574704436697-0' :
					var adobj = jQuery('#div-gpt-ad-1574704436697-0').closest('.spon_con_row_block');					
					break;
				case 'div-gpt-ad-1574293234127-0' :				
					var adobj = jQuery('#div-gpt-ad-1574293234127-0').closest('.spon_con_row_block');					
					break;
				case 'div-gpt-ad-1574293106466-0' :				
					var adobj = jQuery('#div-gpt-ad-1574293106466-0').closest('.spon_con_row_block');
					break;
				case 'div-gpt-ad-1538576085936-0' :				
					var adobj = jQuery('#div-gpt-ad-1538576085936-0').closest('.adbox');
					break;
				case 'div-gpt-ad-1538575257827-0' :				
					var adobj = jQuery('#div-gpt-ad-1538575257827-0').closest('.adbox');
					break;
				case 'div-gpt-ad-1548727342975-0' :				
					var adobj = jQuery('.oiomainlisting2');
					break;
			}
	
			if (typeof adobj !== 'undefined')
			{
				// if we have no advert defined
				if (event.isEmpty)
				{
					adobj.hide(); // hide block
					adobj.addClass('hidden');
				}
				else
				{
					adobj.show(); // show block.
					adobj.css('height',adobj.find('iframe').height() + 'px');
					adobj.removeClass('hidden');
				}
			}

			hide_all_sponsored_check();
			hide_all_mobile_check();
			hide_all_leaderboard_check();

		});

		// configure break points and sizes accordingly.
		var mappingLeaderboard = googletag.sizeMapping().
			addSize([1024, 0 ], [970, 90]).
			addSize([768, 0 ], [740, 69]).
			addSize([500, 0 ], [468, 43]).
			addSize([0, 0 ], [300, 28]).
			build();
			
		var mappingMidboard = googletag.sizeMapping().
			addSize([1048, 0 ], [720, 90]).
			addSize([500, 0 ], [460, 58]).
			addSize([0, 0 ], [300, 38]).
			build();
			
		var mappingMidboard2 = googletag.sizeMapping().
			addSize([1048, 0 ], [720, 90]).
			addSize([500, 0 ], [460, 58]).
			addSize([0, 0 ], [300, 38]).
			build();
			
		// configure break points and sizes accordingly.
		var mappingLeaderboard2 = googletag.sizeMapping().
			addSize([1024, 0 ], [970, 90]).
			addSize([768, 0 ], [740, 69]).
			addSize([500, 0 ], [468, 43]).
			addSize([0, 0 ], [300, 28]).
			build();

		googletag.defineSlot('/21739840349/large-leaderboard', [[970, 90],[740, 69],[468, 43],[300, 28]],  'div-gpt-ad-1535391032420-0').defineSizeMapping(mappingLeaderboard).addService(pubads).setCollapseEmptyDiv(true);
		
		googletag.defineSlot('/21739840349/large-leaderboard-2', [[970, 90],[740, 69],[468, 43],[300, 28]],  'div-gpt-ad-1543257079329-0').defineSizeMapping(mappingLeaderboard2).addService(pubads).setCollapseEmptyDiv(true);

		googletag.defineSlot('/21739840349/large-leaderboard-3', [[970, 90],[740, 69],[468, 43],[300, 28]],  'div-gpt-ad-1573581814153-0').defineSizeMapping(mappingLeaderboard2).addService(pubads).setCollapseEmptyDiv(true);
		
		googletag.defineSlot('/21739840349/midleft-720x90', [[300, 38], [720, 90], [460, 58]], 'div-gpt-ad-1538577834221-0').defineSizeMapping(mappingMidboard).addService(pubads).setCollapseEmptyDiv(true);
		
		googletag.defineSlot('/21739840349/midleft2-720x90', [[300, 38], [720, 90], [460, 58]], 'div-gpt-ad-1548727342975-0').defineSizeMapping(mappingMidboard2).addService(pubads).setCollapseEmptyDiv(true);
		
		googletag.defineSlot('/21739840349/lefttop-200x100', [200, 100], 'div-gpt-ad-1535391583522-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/righttop-200x100', [200, 100], 'div-gpt-ad-1535391769376-0').addService(pubads).setCollapseEmptyDiv(true);	
		googletag.defineSlot('/21739840349/rightcolumn1-200x200', [300, 250], 'div-gpt-ad-1574366942128-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/rightcol2-200x200', [200, 200], 'div-gpt-ad-1535392012584-0').addService(pubads).setCollapseEmptyDiv(true);	
		googletag.defineSlot('/21739840349/rightcol3-200x200', [200, 200], 'div-gpt-ad-1535392059431-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/rightcol4-200x200', [200, 200], 'div-gpt-ad-1535392189194-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/sponsleft:300x250', [300, 250], 'div-gpt-ad-1574292887948-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/sponsmid-300x250', [300, 250], 'div-gpt-ad-1574293048774-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/sponsright-300x250', [300, 250], 'div-gpt-ad-1574293106466-0').addService(pubads).setCollapseEmptyDiv(true);
		
		// Missing Ad Slots.. 
		googletag.defineSlot('/21739840349/new_right_col_1_200_200', [200, 200], 'div-gpt-ad-1571615207403-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/new_right_column_2_200_200', [200, 200], 'div-gpt-ad-1571615359407-0').addService(pubads).setCollapseEmptyDiv(true);	
		googletag.defineSlot('/21739840349/new_right_column_3_200_200', [200, 200], 'div-gpt-ad-1571615575865-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/new_right_column_4_200_200', [200, 200], 'div-gpt-ad-1571615782942-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/mob_leader1_320', [320, 50], 'div-gpt-ad-1574545166375-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/mob_leader2_320', [320, 50], 'div-gpt-ad-1574544686419-0').addService(pubads).setCollapseEmptyDiv(true);
		
		// mobile ad slots
		googletag.defineSlot('/21739840349/mobile_left_200_100', [150, 75], 'div-gpt-ad-1538576085936-0').addService(pubads).setCollapseEmptyDiv(true);
		googletag.defineSlot('/21739840349/mobile_right_200_100', [150, 75], 'div-gpt-ad-1538575257827-0').addService(pubads).setCollapseEmptyDiv(true);

		googletag.pubads().enableSingleRequest();
		//googletag.pubads().enableSyncRendering();
		googletag.enableServices();
		
		var resizeTimer;

		jQuery(window).on('resize', function(e) {

			clearTimeout(resizeTimer);
			resizeTimer = setTimeout(function() {

				googletag.pubads().refresh();
					
			}, 250);

		});
		
		reload_ads_automatically();
		
		
	});
  
	function reload_ads_automatically()
	{
		googletag.pubads().refresh();
		
		// auto refresh ads after x seconds.
		var REFRESH_INTERVAL = 300;
		
		setTimeout(function() {

				reload_ads_automatically();
					
			}, REFRESH_INTERVAL * 1000)
	}
  
  function hide_all_sponsored_check()
  {
	  if (!jQuery('.spon_con_row_block:visible').length)
		  jQuery('.spon_con').hide();
	 
  }
  
  function hide_all_mobile_check()
  {
	  
	  var ad_count = jQuery('.mobile_adb .adbox').length;
	  var hidden_count = 0;
	  
	  jQuery('.mobile_adb').find('.adbox').each(function () {
		  
		  if (jQuery(this).hasClass('hidden'))
			  hidden_count = hidden_count + 1;
		  
	  });
	  
	if (hidden_count == ad_count) 
		jQuery('.mobile_adb').hide();
	  
  }
  
  function hide_all_leaderboard_check()
  {
	  if (!jQuery('.header_ad_top:visible').length)
		  jQuery('.header_and_content_split_block').hide();
  }
</script>


</head>

<body <?php body_class(); ?> <?php if (!X2_IS_APP){echo 'id="mainBody"';}else{echo 'id="mainBodyApp"';}?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5WSTXQJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

	<?php
	if (!X2_IS_APP)
	{
	?>
	<article id="newsletter-top-banner">
		<div class="row">
			<h3>Want up-to-the-<br>minute news?</h3>
			<div class="text">
				<p>Sign up for <b>HAMODIA'S</b> <br>Breaking News Emails*</p>
				<footer>*You can select which emails you'd like to receive.</footer>
			</div>
			<form id="newsletter-top-banner-form" action="<?= get_site_url(); ?>/standalone/newsletter.php" target="_blank" method="get">
				<input type="email" placeholder="Your Email Here" name="email" required>
				<button type="submit">Go</button>
			</form>
			<button type="button" id="newsletter-top-banner-dismiss">&times;</button>
		</div>
	</article>



	<script>
	(function() {
		if (localStorage.getItem('newsletter-top-banner-dismissed')) {
			document.getElementById('newsletter-top-banner').className = 'hidden';
		} else {
			document.getElementById('newsletter-top-banner-dismiss').addEventListener('click', dismiss);
			document.getElementById('newsletter-top-banner-form').addEventListener('submit', dismiss);
		}

		function dismiss() {
			try {
				localStorage.setItem('newsletter-top-banner-dismissed', new Date().getTime());
				document.getElementById('newsletter-top-banner').className = 'hidden';
			} catch(e) {
				// In Safari, trying to store stuff in localstorage while in private
				// browsing throws an error. We will catch it here and tell users
				// to quit private browsing to be able to dismiss this banner.
				if (e.message == 'QuotaExceededError: DOM Exception 22') {
					alert('Quit private browsing to dismiss this.');
				}
			}
		}
	}());
	</script>


	<header id="header">

		<div class="top-links">
			<div class="row">
				<div class="left">
					<a href="<?php echo site_url(); ?>">Home</a> |
					<a href="<?php echo get_page_link(175); ?>">Archive</a> |
					<a href="<?php echo get_page_link(3723); ?>">About Us</a> |
					<a href="<?php echo get_page_link(3722); ?>">Contact Us</a>
				</div>
				<div class="center">
					<form method="get" id="topsearchform" action="<?php echo site_url('/'); ?>">
						<input type="text" name="s" placeholder="Search hamodia.com">
						<button type="submit"><span class="fa fa-search"></span></button>
					</form>
				</div>
				<div class="right">
					<!--<span class="hebrew-date">

						<?php echo Hamodia_APIs::get_hebrew_date();

						if ( $sefirah = Sefirah::get() )
						{
							echo '&nbsp;&nbsp;-&nbsp;&nbsp;' . $sefirah;
						} ?>
					</span>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<?php echo date_i18n('l, F j, Y'); ?> -->

					<div class="top-links-right-links">
						<a href="<?php echo get_page_link(248); ?>" class="advertise">Advertise</a><span> |
						</span><a href="<?php echo get_page_link(94); ?>">Subscribe</a>
					</div>

				</div>
			</div>
		</div>

		<div class="top-center">
			<div class="row">

				<div class="top_center_row_left">
					<div class="adbox">
						<!-- /21739840349/lefttop-200x100 -->
						<div id='div-gpt-ad-1535391583522-0' style='height:100px; width:200px;margin: 0 auto;'>
						<script>
						googletag.cmd.push(function() { googletag.display('div-gpt-ad-1535391583522-0'); });
						</script>
						</div>
					</div>
					<div class="date_box">
						<span>
						<?php echo date_i18n('F j, Y'); ?>
						</span>
					</div>
				</div>
				<div class="top_center_row_center">
				<a id="logo" href="<?php echo site_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/images/hamodia-logo.png" alt="Hamodia Site Logo" />
				</a>

					<div class="mobile_date" style="display:none">
						<div class="date_box">
							<span>
								<?php echo date_i18n('F j, Y'); ?>
							</span>
							<span class="hebrew-date">

							<?php echo Hamodia_APIs::get_hebrew_date();

								if ( $sefirah = Sefirah::get() )
								{
									echo '&nbsp;&nbsp;-&nbsp;&nbsp;' . $sefirah;
								} ?>
							</span>

						</div>
					</div>
				</div>
				<div class="top_center_row_right">
					<div class="adbox">
						<!-- /21739840349/righttop-200x100 -->
						<div id='div-gpt-ad-1535391769376-0' style='height:100px; width:200px;margin: 0 auto;'>
						<script>
						googletag.cmd.push(function() { googletag.display('div-gpt-ad-1535391769376-0'); });
						</script>
						</div>
					</div>
					<div class="date_box">
						<span class="hebrew-date">

						<?php echo Hamodia_APIs::get_hebrew_date();

							if ( $sefirah = Sefirah::get() )
							{
								echo '&nbsp;&nbsp;-&nbsp;&nbsp;' . $sefirah;
							} ?>
						</span>

					</div>
				</div>

			</div>
		</div>

		<nav id="main-nav" class="row" role="navigation">
			<h4>Sections</h4>
			<form method="get" id="top_mobile_search_form" action="<?php echo site_url('/'); ?>">
				<input type="text" name="s" placeholder="Search hamodia.com">
				<button type="submit"><span class="fa fa-search"></span></button>
			</form>
			<?php hamodia_main_nav(); ?>
		</nav>



	</header>
	<?php
		} //end X2_IS_APP CHECK
		else
		{
	?>

	<div class="date_box" style="text-align:center;margin-top:10px;margin-bottom:20px;">
			<span>
				<?php echo date_i18n('F j, Y'); ?>
			</span>
			<span class="hebrew-date">

			<?php echo Hamodia_APIs::get_hebrew_date();

				if ( $sefirah = Sefirah::get() )
				{
					echo '&nbsp;&nbsp;-&nbsp;&nbsp;' . $sefirah;
				} ?>
			</span>

	</div>
	<?php
		}
	?>

	<!-- Start the main container -->
	<div id="container" class="container" role="document">



	<?php

	if (X2_IS_APP)
	{
		echo '<a id="logo" href="'. site_url(). '" style="display:none !important"><img src="'.get_template_directory_uri().'/images/hamodia-logo.png" alt="Hamodia Site Logo" /></a>';
	}
	?>

	<?php
		if (!is_front_page())
		{
			echo '<div id="content" class="row"><div class="header_and_content_split_block inner_top_advert" style="margin-bottom:30px;margin-top:10px !important;margin-left:0 !important;margin-right:0 !important;width:100%;">';


			echo "<!-- /21739840349/large-leaderboard -->
				<div id='div-gpt-ad-1535391032420-0' class=\"header_ad_top\">
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1535391032420-0'); });
				</script>
				</div>
				
				<!-- /21739840349/large-leaderboard-2 -->
				<div id='div-gpt-ad-1543257079329-0' style=\"margin-top:10px\"  class=\"header_ad_top\">
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1543257079329-0'); });
				</script>
				</div>

				<!-- /21739840349/large-leaderboard-3 -->
				<div id='div-gpt-ad-1573581814153-0' style=\"margin-top:10px\"  class=\"header_ad_top\">
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1573581814153-0'); });
				</script>
				</div>

				";


			echo '</div></div>';
		}
		else
		{
			if (X2_IS_APP)
			{
				echo '<div class="header_and_content_split_block" style="margin-bottom:30px;margin-top:10px;">';


				echo "<!-- /21739840349/large-leaderboard -->
					<div id='div-gpt-ad-1535391032420-0' class=\"header_ad_top\">
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1535391032420-0'); });
					</script>
					</div>
					
					<!-- /21739840349/large-leaderboard-2 -->
					<div id='div-gpt-ad-1543257079329-0' style=\"margin-top:10px\" class=\"header_ad_top\">
					<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1543257079329-0'); });
					</script>
					</div>

				<!-- /21739840349/large-leaderboard-3 -->
				<div id='div-gpt-ad-1573581814153-0' style=\"margin-top:10px\"  class=\"header_ad_top\">
				<script>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1543257079329-0'); });
				</script>
				</div>	

					";

				echo '</div>';
			}
		}
	?>

	<div class="mobile_adb" style="margin-top:20px;">
		<div class="adbox">
			<!-- /21739840349/mobile left-200x100 -->
			<div id='div-gpt-ad-1538576085936-0'>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1538576085936-0'); });
			</script>
			</div>
		</div>
		<div class="adbox">
			<!-- /21739840349/mobile right-200x100 -->
			<div id='div-gpt-ad-1538575257827-0'>
			<script>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1538575257827-0'); });
			</script>
			</div>
		</div>
	</div>

