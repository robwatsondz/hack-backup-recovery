<?php get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="with-sidebar" role="main">
			<div class="post-box">
				<?php get_breadcrumbs(); ?>
				<?php get_template_part('loop', 'single'); ?></div>



<!-- /21739840349/midleft-720x90 -->
								<div style="text-align:center;margin-top:40px" id='div-gpt-ad-1538577834221-0'>
								<script>
								googletag.cmd.push(function() { googletag.display('div-gpt-ad-1538577834221-0'); });
								</script>
								</div>




</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->
</div><!-- End main row -->


<?php get_footer(); ?>