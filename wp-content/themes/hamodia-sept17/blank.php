<?php
/**
 * Template Name: Blank
 *
 */


 //mail('mikeypengelly@gmail.com','Hamodia Debug',print_r($_REQUEST,true));
 //mail('mikeypengelly@gmail.com','Hamodia Debug',print_r($_SERVER,true));
 
$action = $_REQUEST['action'];
$page = $_REQUEST['page'];
$version = 1;

if (isset($_REQUEST['version']))
	$version = $_REQUEST['version'];

$MOBILE_DATA_VERSION = $version;

if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}


switch($action)
{
	case 'get_config':
			get_config();
		break;

}

function load_menu_by_categories()
{
	 $menuArray = array();
	 //Load the categories available for the site.
		 $categories = get_categories( array(
			'orderby' => 'name',
			'order'   => 'ASC'
		) );

		$menuArray[] = array(	'command' => 'cms',
												'cms_url' => '/',
												'cms_title' => 'Home',
												'icon' => 'book',
												'label' => 'Home',
												'label_id' => 'home',
												'roles' => '',
												'config_checks' => array()
								);

		$menuArray[] = array(	'command' => 'cms',
														'cms_url' => '/user-preferences/?token=TOKEN_PLACEHOLDER&os=OS_PLACEHOLDER',
														'cms_title' => 'Preferences',
														'icon' => 'book',
														'label' => 'Preferences',
														'label_id' => 'preferences',
														'roles' => '',
														'config_checks' => array()
								);

		foreach( $categories as $category ) {

			$menuArray[] = array(	'command' => 'cms',
									'cms_url' => str_replace('https://hamodia.com','',esc_url( get_category_link( $category->term_id ) )),
									'cms_title' => esc_html( $category->name ),
									'icon' => 'book',
									'label' => esc_html( $category->name ),
									'label_id' => esc_html( $category->name ),
									'roles' => '',
									'config_checks' => array()
								);
			}

	return  $menuArray;
}

function load_menu_by_menu( ) {

	$menuArray = array();
	$menu_items = wp_get_nav_menu_items('Mobile App Menu');

	$count = 0;

	foreach( $menu_items as $menu_item ) {

		$link = str_replace('https://hamodia.com','',$menu_item->url);
		$title = $menu_item->title;

		$className = '';
		if (isset($menu_item->classes[0]))
			$className = $menu_item->classes[0];

		if ($className == 'section_header')
		{
			$menuArray[] = array(	'command' => 'row_group',
									'cms_url' => '',
									'cms_title' => '',
									'icon' => '',
									'label' => esc_html( $title ),
									'label_id' => 'a_label_'.$count,
									'roles' => '',
									'config_checks' => array()
							);
		}
		else
		{
			$menuArray[] = array(	'command' => 'cms',
									'cms_url' => $link,
									'cms_title' => esc_html( $title ),
									'icon' => $className,
									'label' => esc_html( $title ),
									'label_id' => 'a_label_'.$count,
									'roles' => '',
									'config_checks' => array()
								);
		}


		$count++;
	}

	$menuArray[] = array(	'command' => 'row_group',
										'cms_url' => '',
										'cms_title' => 'Preferences',
										'icon' => '',
										'label' => 'Preferences',
										'label_id' => 'Preferences',
										'roles' => '',
										'config_checks' => array()
							);

	$menuArray[] = array(	'command' => 'cms',
							'cms_url' => '/user-preferences/?token=TOKEN_PLACEHOLDER&os=OS_PLACEHOLDER',
							'cms_title' => 'Notifications',
							'icon' => 'cogs',
							'label' => 'Notifications',
							'label_id' => 'notifications',
							'roles' => '',
							'config_checks' => array()
						);

	$menuArray[] = array(	'command' => 'row_group',
							'cms_url' => '',
							'cms_title' => ' ',
							'icon' => '',
							'label' => ' ',
							'label_id' => 'spacer',
							'roles' => '',
							'config_checks' => array()
						);

	return $menuArray;
}

function get_config()
{

	 //$menuArray = load_menu_by_categories();
	 $menuArray = load_menu_by_menu();


	// setup urls 
	
	if ($_SERVER['HTTP_USER_AGENT'] == 'Android Titanium')
		$app_safe_urls = 'facebook,https://api.stockdio.com,https://www.youtube.com/embed/,https://player.vimeo.com/video/,http://hamodia.com,https://hamodia.com,https://hamodia.epageview.com,http://hamodia.epageview.com,googlesyndication.com,doubleclick.net,google,s0.2mdn.net,https://facebook.com,';
	else		
		$app_safe_urls = ',facebook,https://api.stockdio.com,https://www.youtube.com/embed/,https://player.vimeo.com/video/,http://hamodia.com,https://hamodia.com,https://hamodia.epageview.com,http://hamodia.epageview.com,googlesyndication.com,doubleclick.net,google,s0.2mdn.net,https://facebook.com,';

	 $contentArray = array(	'AllowSignup' => true,
							 'FacebookConnect' => false,
							 'CellRequired' => false,
							 'SiteName' => 'Hamodia',
							 'TeamName' => 'Hamodia',
							 'PlotProjectsToken' => '',
							 'TagLine' => '',
							 'FBAppID' => '',
							 'FBScope' => '',
							 'GCMAPIKey' => 'AIzaSyD2izz5aEm7HXndl45HxJh3i3C2ytdn64Y',
							 'GCMSenderID' => '983954291964',
							 'SiteURL' => 'https://hamodia.com',
							 'SiteTheme' => null,
							 'AppSafeUrls' => $app_safe_urls,
							 'MenuColor' => '#FFFFFF00',
							 'SelfieLabel' => null,
							 'Menu' => $menuArray
							);



	 $resultingArray = array(	'action' => 'new',
								'hash' => 'e322023f61cfc03e46641a863f966df3',
								'count' => 1,
								'content' => $contentArray,
								'expires' => 149460029,
								'result' => 'success',
								'version' => 1
							);

	echo json_encode($resultingArray);

 }
