<?php if ( ! have_posts()): ?>
	<div class="notice">
		<p class="bottom">Sorry, no results were found.</p>
	</div>

	<?php get_search_form(); ?>
<?php endif; ?>


<?php while (have_posts()) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php
		$is_post_format_image = has_post_format('image');
		
		$img = get_the_thumbnail('category-listing-image');
		
		// do we have the image.. if not lets display the large one to save outputing full res...
		if (stripos($img,'760x760') === false)
			$img = get_the_thumbnail('large');
		
		if ($is_post_format_image) extract($img);
		?>

		<?= $img ?>

		<header>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php hamodia_entry_meta(true,true); ?>
		</header>

		<div class="entry-content">
			<?php echo $is_post_format_image ? $caption : the_excerpt(); ?>
		</div>

	</article>

<?php endwhile ?>


<?php if ($wp_query->max_num_pages > 1): ?>
	<nav id="post-nav">
		<div class="nav-previous"><?php next_posts_link('&laquo; Older'); ?></div>
		<div class="nav-next"><?php previous_posts_link('Newer &raquo;'); ?></div>
	</nav>
<?php endif;
