<?php get_header(); ?>

	<!-- Row for main content area -->
	<?php
	
		$category = get_the_category();
	
		$addClass='';
		//if ($category[0]->category_parent == 13027)		
		//	$addClass='category-coronavirus';						
	
	?>
	
	<div id="content" class="row <?php echo $addClass; ?>">

		<div id="main" class="excerpt-list with-sidebar" role="main">
			<div class="post-box">

				<a href="<?php echo get_category_link( get_query_var('cat') ); ?>">
					<h1 class="category-page-heading">
						<?php single_cat_title(); ?>
					</h1>
				</a>

				<?php HamodiaCategory::make(get_query_var('cat'))->hero(); ?>

				<?php
				/*if (get_query_var('cat') == 13027)
				{
					$args = array('child_of' => 13027);
					$categories = get_categories( $args );
					echo '<ul class="subCats">';
					foreach($categories as $category) { 
						echo '<li><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </li> ';					
					}
					echo "</ul>";
				}*/
				
				?>

				<?php get_template_part('loop', 'category'); ?>
			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- End main row -->

<?php get_footer(); ?>
