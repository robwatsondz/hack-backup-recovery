<?php get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">
	
		<div id="main" class="with-sidebar" role="main">
			<div class="post-box">
				<h1 class="typographic-heading">The Daily Paper</h1>

				<?php while (have_posts()) : the_post(); ?>
					<a class="paper-wrapper" href="<?php the_permalink(); ?>" target="_blank">

						<?php $date = new DateTime( get_post_meta($post->ID, '_hamodia_post_meta_date', true) ); ?>

						<span class="day"><?php echo $date->format('l'); ?></span>
						<span class="date"><?php echo $date->format('M jS, Y'); ?></span>
						<span class="thumb"><?php echo the_post_thumbnail('paper-front-page'); ?></span>

					</a>
				<?php endwhile;  ?>

				<?php if ($wp_query->max_num_pages > 1) : ?>
					<nav id="post-nav">
						<div class="post-previous"><?php next_posts_link('&larr; Older posts'); ?></div>
						<div class="post-next"><?php previous_posts_link('Newer posts &rarr;'); ?></div>
					</nav>
				<?php endif; ?>
			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- End main row -->
	
<?php get_footer(); ?>