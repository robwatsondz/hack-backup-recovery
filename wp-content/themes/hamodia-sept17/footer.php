	<div class="spon_con">
		<div class="row">
			<!--<h2 class="sidebar_title">Sponsored Content</h2>-->





			<div class="spon_con_row_block"><!-- /21739840349/sponsleft:300x250 -->
<div class="gpt-ad"  id='div-gpt-ad-1574704436697-0'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1574704436697-0'); });
</script>
</div>
</div>
			<div class="spon_con_row_block"><!-- /21739840349/sponsmid-300x250 -->
<div class="gpt-ad" id='div-gpt-ad-1574293234127-0'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1574293234127-0'); });
</script>
</div>
</div>
			<div class="spon_con_row_block"><!-- /21739840349/sponsright-300x250 -->
<div class="gpt-ad"  id='div-gpt-ad-1574358144479-0'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1574358144479-0'); });
</script>
</div>
</div>
		</div>
	</div>

	</div><!-- Container End -->
<?php
	if (!X2_IS_APP)
	{
	?>
	<footer id="footer" role="contentinfo">
		<div class="row">

			<div class="three columns">
				<h6>Sections</h6>
				<ul>
					<?php
					$news_categories = [
						'Business',
						'Markets',
						'Technology',
						'World',
						'Regional',
						'Israel',
						'Community',
						'National',
						'Op-Ed'
					];

					foreach ($news_categories as $cat): ?>
						<li><a href="<?php echo get_category_link( get_cat_ID($cat) ); ?>"><?php echo $cat ?></a></li>
					<?php endforeach; ?>

					<li><a href="<?php echo get_post_type_archive_link('hamodia_feature'); ?>">Features</a></li>
				</ul>
			</div>

			<div class="three columns">
				<h6>Resources</h6>
				<ul>
					<li><a href="<?php echo get_page_link(203); ?>">Weather</a></li>
					<li><a href="<?php echo get_page_link(206); ?>">Currency</a></li>
					<li><a href="<?php echo get_page_link(207); ?>">Stocks</a></li>
					<li><a href="<?php echo get_page_link(232); ?>">Zmanim</a></li>
					<li><a href="<?php echo get_page_link(234); ?>">Shiurim</a></li>
					<li><a href="<?php echo get_page_link(1554); ?>">Simchas</a></li>
				</ul>
			</div>

			<div class="three columns">
				<h6>Readers</h6>
				<ul>
					<li><a href="<?php echo get_page_link(94); ?>">Subscribe</a></li>
					<li><a href="<?php echo get_post_type_archive_link('letter'); ?>">Letters</a></li>
					<li><a href="<?php echo get_page_link(175); ?>">Archives</a></li>
					<li><a href="<?php echo get_page_link(103419); ?>">Classifieds</a></li>
				</ul>
			</div>

			<div class="three columns">
				<h6>Company</h6>
				<ul>
					<li><a href="<?php echo get_page_link(248); ?>">Advertise</a></li>
					<li><a href="<?php echo get_page_link(4331); ?>">Privacy Policy</a></li>
					<li><a href="<?php echo get_page_link(4250); ?>">Terms of Use</a></li>
					<li><a href="<?php echo get_page_link(3723); ?>">About Us</a></li>
					<li><a href="<?php echo get_page_link(3722); ?>">Contact Us</a></li>
				</ul>
				<a href="#" class="back-to-top" title="Back to top">➧</a>
			</div>

		</div>
	</footer>

	<a href="#" class="btp"><span class="fa fa-chevron-up"></span></a>

	<?php
			} //end X2_IS_APP CHECK
	?>

	<?php
	// removed this from page as there is already jquery included.
	/*
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/libs/jquery-1.8.3.min.js"><\/script>')</script>
	*/
	?>
	

	<script src="<?php echo get_template_directory_uri(); ?>/js/libs/moment-2.10.3.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/foundation.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/libs/photoswipe/photoswipe.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/libs/photoswipe/photoswipe-ui-default.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/libs/imagesloaded/imagesloaded.pkgd.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/app.js?v=2018-08-28v2"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/app_2017.js?v=2018-10-11v1"></script>


	<!--[if lt IE 8]>
		<script defer src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
		<script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->

	<?php wp_footer(); ?>

	<?php

		if (X2_IS_APP)
		{
			echo '<script type="text/javascript" src="/wp-content/themes/hamodia-sept17/TouchSwipe-Jquery-Plugin-master/jquery.touchSwipe.min.js"></script>';
			echo '<script type="text/javascript" src="/wp-content/themes/hamodia-sept17/TouchSwipe-Jquery-Plugin-master/active.js?v=1.2"></script>';
		}

	?>
	
	<?php get_template_part('photoswipe'); ?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5WSTXQJ');</script>
<!-- End Google Tag Manager -—>
	
</body>
</html>
