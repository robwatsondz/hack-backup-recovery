<?php get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="default-page-template" role="main">
			<div class="post-box">
				<?php get_template_part('loop', 'page'); ?>
			</div>
		</div>

	</div><!-- End content row -->

<?php get_footer(); ?>