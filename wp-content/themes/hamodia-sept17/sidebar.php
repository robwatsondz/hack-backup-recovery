<?php

if (is_front_page()):

	echo '<div class="sidebar_block">';
	Hamodia_sidebar::daily_paper();
	echo '</div><div class="sidebar_block image_category_link">';
	echo '<a href="'.get_post_type_archive_link('hamodia_column').'"><img src="/wp-content/themes/hamodia-sept17/images/prime-sidebar.png"></a>';
	echo '</div><div class="sidebar_block image_category_link inyan_link">';
	echo '<a href="/frominyan/"><img src="/wp-content/themes/hamodia-sept17/images/inyan-sidebar.png"></a>';
	
echo '</div><div class="sidebar_block gpt3 bypass_ad_checks" style="width:200px;text-align:center">';
	Hamodia_sidebar::sidebar_ad_single(3);

echo '</div><div class="sidebar_block">';
	Hamodia_sidebar::sidebar_articles(true);
	
	echo '</div><div class="sidebar_block gpt4 bypass_ad_checks" style="width:200px;text-align:center">';
	Hamodia_sidebar::sidebar_ad_single(4);
	// this is the column but we need to load the features taxonomy as it was renamed.
	$column_count = Hamodia_homepage::get_custom_category_post_count('hamodia_feature');
	if ($column_count > 0)
		echo '</div><div class="sidebar_block">';
	else
		echo '</div><div class="sidebar_block" style="display:none;">';
	Hamodia_sidebar::sidebar_articles(false,true,false);
	echo '</div><div class="sidebar_block">';
	Hamodia_sidebar::newsletter();
	echo '</div><div class="sidebar_block gpt5 bypass_ad_checks" style="text-align:center">';
	Hamodia_sidebar::sidebar_ad_single(5);
	echo '</div><div class="sidebar_block">';
	Hamodia_sidebar::page_links();
	?>
	</div>
	<div class="sidebar_block">

<div style="margin-bottom:25px;text-align:center"><?php Hamodia_sidebar::sidebar_ad_single(6); ?></div>


		<h3 class="sidebar_title">Today</h3>
		<div class="side_group">
		<?php
		// add this back in
		//Hamodia_sidebar::weather();
		Hamodia_sidebar::stocks(false);
		Hamodia_sidebar::currency();
		
		?>

		</div>
	</div>
	<?php


elseif (is_single()): ?>

<?php
echo '<div class="sidebar_block">';
		
		Hamodia_sidebar::daily_paper();
		echo '</div><div class="sidebar_block gpt3 bypass_ad_checks" style="width:200px">';
		Hamodia_sidebar::sidebar_ad_single(3);
		echo '</div><div class="sidebar_block gpt4 bypass_ad_checks">';
		Hamodia_sidebar::sidebar_ad_single(4);
		echo '</div><div class="sidebar_block">';
		Hamodia_sidebar::newsletter();
		echo '</div><div class="sidebar_block">';
		Hamodia_sidebar::page_links();
		echo '</div><div class="sidebar_block gpt5 bypass_ad_checks">';
		Hamodia_sidebar::sidebar_ad_single(5);
		echo '</div><div class="sidebar_block gpt6 bypass_ad_checks">';
		Hamodia_sidebar::sidebar_ad_single(6);
		?>
		</div>
		<div class="sidebar_block">
			<h3 class="sidebar_title">Today</h3>
			<div class="side_group">

			<?php
			//add this back in
			//Hamodia_sidebar::weather();
			Hamodia_sidebar::stocks(false);
			Hamodia_sidebar::currency();
			
			?>
			</div>
		</div>


    <?php //Hamodia_sidebar::callathon(); ?>

<?php

elseif ( is_category() ):

    echo '<div class="sidebar_block">';
			Hamodia_sidebar::daily_paper();
			echo '</div><div class="sidebar_block gpt3 bypass_ad_checks" style="width:200px">';
			Hamodia_sidebar::sidebar_ad_single(3);
			echo '</div><div class="sidebar_block gpt4 bypass_ad_checks">';
			Hamodia_sidebar::sidebar_ad_single(4);
			echo '</div><div class="sidebar_block">';
			Hamodia_sidebar::newsletter();
			echo '</div><div class="sidebar_block">';
			Hamodia_sidebar::page_links();
			echo '</div><div class="sidebar_block gpt5 bypass_ad_checks">';
			Hamodia_sidebar::sidebar_ad_single(5);
			echo '</div><div class="sidebar_block gpt6 bypass_ad_checks">';
			Hamodia_sidebar::sidebar_ad_single(6);
			?>
			</div>
			<div class="sidebar_block">
				<h3 class="sidebar_title">Today</h3>
				<div class="side_group">
				<?php
				//add this back in
				//Hamodia_sidebar::weather();
				Hamodia_sidebar::stocks(false);
				Hamodia_sidebar::currency();
				Hamodia_sidebar::daily_paper();
				?>
			</div>
		</div>
<?php
else:

    //Hamodia_sidebar::callathon();

	is_page('Stocks') && Hamodia_sidebar::stocks(true);

	if ( get_post_type() != 'letter' ): ?>

		<div class="more-articles-wrapper">
			<?php Hamodia_sidebar::more_in_categories(); ?>
		</div>

	<?php endif;
	echo '<div class="sidebar_block gpt3 bypass_ad_checks" style="width:200px">';
				Hamodia_sidebar::sidebar_ad_single(3);
			echo '</div>';
	echo '<div class="sidebar_block gpt4 bypass_ad_checks">';
				Hamodia_sidebar::sidebar_ad_single(4);
			echo '</div>';
	echo '<div class="sidebar_block">';
	Hamodia_sidebar::sidebar_articles(false,true);
	echo '</div><div class="sidebar_block gpt5 bypass_ad_checks">';
			Hamodia_sidebar::sidebar_ad_single(5);
	echo '</div><div class="sidebar_block gpt6 bypass_ad_checks">';
			Hamodia_sidebar::sidebar_ad_single(6);
	?>
</div>
 <script type="text/javascript">

        var googletag = googletag || {};
        
        googletag.cmd = googletag.cmd || [];
        
        (function() {
        
        var gads = document.createElement("script"); gads.async = true; gads.type = "text/javascript"; var useSSL = "https:" == document.location.protocol; gads.src = (useSSL ? "https:" : "http:") + "//securepubads.g.doubleclick.net/tag/js/gpt.js";
        
        var node = document.getElementsByTagName("script")[0];
        
        node.parentNode.insertBefore(gads, node); })(); </script> <div id="div-gpt-ad-MPU_1"> <script type='text/javascript'>
        
        googletag.cmd.push(function() {
        
        var slot = googletag.defineSlot('/264857099/Hamodia/SET_1/MPU_1', [300, 250], 'div-gpt-ad-MPU_1'); slot.addService(googletag.pubads());
        
        googletag.enableServices();
        
        googletag.display('div-gpt-ad-MPU_1');
        
        if (googletag.pubads().isInitialLoadDisabled()) googletag.pubads().refresh([slot]);
        
        });
        
        </script>
	<div class="sidebar_block">
		<h3 class="sidebar_title">Today</h3>
		<div class="side_group">
			<?php
			//! is_page('Weather') && Hamodia_sidebar::weather();
			! is_page('Stocks') && Hamodia_sidebar::stocks();
			! is_page('Currency') && Hamodia_sidebar::currency();
			?>
		</div>
	</div>
<?php
	echo '<div class="sidebar_block">';
	Hamodia_sidebar::page_links();
	echo '</div>';
endif;


echo '<div class="sidebar_block">';
Hamodia_sidebar::news_tip();
echo '</div>';
