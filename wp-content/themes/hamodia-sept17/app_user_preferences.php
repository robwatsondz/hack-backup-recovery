<?php
/**
 * Template Name: App User Preferences
 *
 */
 
 if (!isset($_GET['os']) || !isset($_GET['token']))
 	exit();

$x_token = $_GET['token'];
$x_os = $_GET['os'];

function set_custom_post_type_notification_status($post_type,$mode, $user_id, $wpdb)
{
	$ids = '';
	$args = array(
		'hide_empty' => false
	);
	$terms = get_terms($post_type,$args); // Get all terms of a taxonomy
	
	if ( $terms && !is_wp_error( $terms ) )
	{
		
		if ($mode == 'include')
		{

			// put each term into a csv list.
			foreach ( $terms as $term )
			{

				if ($ids != '')
					$ids .= ',';

				$ids .= $term->term_id;
			}

			// To include categories we remove from the table.
			//echo 'DELETE FROM hamod_push_excluded_categories WHERE user_id = '.$user_id.' and category_id in ('.$ids.')';
			$wpdb->query('DELETE FROM hamod_push_excluded_categories WHERE user_id = '.$user_id.' and category_id in ('.$ids.')');
		}
		else
		{
			// We need to insert a record for each category
			$sql = '';
			foreach ( $terms as $term )
			{
				if ($sql != '')
					$sql .= ',';

				$sql .= '('.$user_id.','.$term->term_id.')';
			}
			
			$wpdb->query('INSERT INTO hamod_push_excluded_categories (user_id,category_id) VALUES ' . $sql);
			//echo 'INSERT INTO hamod_push_excluded_categories (user_id,category_id) VALUES ' . $sql;
		}
	}
	
}

 //Pull out the template data...
function doCall($url,$data = array(),$use_get = false,$debug = false)
{
	$ch = curl_init();

		$formFields = $data;

		curl_setopt($ch, CURLOPT_URL, $url );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER , 1 );
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

		if ($debug)
		{
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			$verbose = fopen('php://temp', 'w+');
			curl_setopt($ch, CURLOPT_STDERR, $verbose);
		}

		if (!$use_get)
		{
			$json = json_encode($formFields);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($json))
			);
		}

		$output = curl_exec($ch);

	curl_close ($ch);

return array('output'=>$output,'status'=>$http_code);
}

function does_user_have_post_type_excluded($user_id, $post_type, $wpdb)
{
	$args = array(
		'hide_empty' => false
	);
	$terms = get_terms($post_type,$args); // Get all terms of a taxonomy

	// If we have a record.
	if (isset($terms[0]))
	{
		$term = $terms[0];
		$results = $wpdb->get_results('SELECT * FROM hamod_push_excluded_categories WHERE user_id = '.$user_id.' and category_id= '.$term->term_id);

		// If the record exists
		if (isset($results[0]))
			return true;

		return false;
	}

	return -1;

}

$user_id = -1;

// using prepare safeguards against sql injection
$results = $wpdb->get_results($wpdb->prepare( 
	"
		SELECT user_id 
		FROM hamod_push_tokens 
		WHERE token = %s 
		and os=%s		
	", 
        array(
		$x_token, 
		$x_os
	) 
));


if (isset($results[0]->user_id))
	$user_id = $results[0]->user_id;

if (isset($_GET['reset']))
{
	$categories = json_decode(file_get_contents('https://hamodia.com/pnfw/categories/?token='.$x_token.'&os='.$x_os));
	$wpdb->query("DELETE FROM hamod_push_excluded_categories WHERE user_id = ".$user_id);	
	foreach ($categories->categories as $index=>$category)
	{
		
			//if ( strtolower($category2['name']) != 'breaking news' &&
			//	strtolower($category2['name']) != 'politics' &&
			//	strtolower($category2['name']) != 'israel')
			//	{
					$wpdb->query("INSERT INTO hamod_push_excluded_categories (user_id,category_id) VALUES (".$user_id.",".$category->id.")");
			//	}
	}

	// now we need to set the custom post types to exclude.
	set_custom_post_type_notification_status('frominyan_category','exclude',$user_id,$wpdb);
	set_custom_post_type_notification_status('column_category','exclude',$user_id,$wpdb);
	set_custom_post_type_notification_status('feature_category','exclude',$user_id,$wpdb);

	if (!isset($_GET['from_url']))
		exit();

}

$categories = json_decode(file_get_contents('https://hamodia.com/pnfw/categories/?token='.$x_token.'&os='.$x_os));

// get the main menu and see if we can tally up..
$nav = wp_get_nav_menu_items('Mobile App Menu');

$parent_item_id = 0;
$order_array = array();

//foreach category look through nav and assign menu ordering.

foreach ($categories->categories as $index=>$category)
{

	$new_item = array('menu_order'=>999,'id'=>$category->id,'name'=>$category->name,'parent'=>$category->parent,'exclude'=>$category->exclude);

	foreach ($nav as $nav_index => $nav_item)
	{

		$object_id = (int)$nav_item->object_id;
		$category_id = (int)$category->id;

		if ($object_id == $category_id )
		{
			$new_item['menu_order'] = $nav_item->menu_order;
		}

	}

	$order_array[] = $new_item;
}

// sort on menu order
usort($order_array, function($a, $b) {
    return $a['menu_order'] - $b['menu_order'];
});

$categories = $order_array;


// If there is a custom category clicked we need to hadle it
if (isset($_GET['custom_category_change']))
{

	$custom_category = $_GET['custom_category_change'];
	$custom_taxonomy = '';

	// Dont forget we switched features and columns.
	switch ($custom_category) {
		case 'frominyan':
							$custom_taxonomy = 'frominyan_category';
							break;
		case 'features':
							$custom_taxonomy = 'column_category';
							break;
		case 'columns':
							$custom_taxonomy = 'feature_category';
							break;
	}

	// if we have a custom category set
	if ($custom_taxonomy != '')
		set_custom_post_type_notification_status($custom_taxonomy,$_GET['mode'],$user_id,$wpdb);
	
	exit();
}


?>

<html>
<head>
	<title>Notification Preferences</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
       <meta charset="utf-8">


	   <meta http-equiv="cache-control" content="max-age=0" />
	   <meta http-equiv="cache-control" content="no-cache" />
	   <meta http-equiv="expires" content="0" />
	   <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	   <meta http-equiv="pragma" content="no-cache" />



				<style type="text/css">
				html,body
				{
				  font-size:18px;
				  line-height:18px;
				  margin:0px;
				  padding:0px;
				}

				ul{
				  margin:0px;
				  padding:0px;
				 	list-style:none;
				  width:100%;
				}

				ul li
				{
				  width:calc(100% - 20px);
				  float:left;
				  border-top:1px solid #ccc;
				  padding:10px 10px 6px 10px;
				}

				ul li:nth-of-type(1)
				{
				  border-top:0px;
				}

				ul li label
				{
				  	float:left;
				width:calc(100% - 20px);
				position:absolute;
				cursor:hand;
				cursor:pointer;
				}

				ul li span
				{
				  float:right;
				}


				li
				{
					background:#eee
				}

				li.active
				{
					background:#104381;
					color:#fff;
				}

				li.saving,
				li.active.saving
				{
					background: linear-gradient(200deg, #104381, #ffd400);
					background-size: 400% 400%;

					-webkit-animation: AnimationName 2s ease infinite;
					-moz-animation: AnimationName 2s ease infinite;
					animation: AnimationName 2s ease infinite;
				}

				@-webkit-keyframes AnimationName {
					0%{background-position:0% 52%}
					50%{background-position:100% 49%}
					100%{background-position:0% 52%}
				}
				@-moz-keyframes AnimationName {
					0%{background-position:0% 52%}
					50%{background-position:100% 49%}
					100%{background-position:0% 52%}
				}
				@keyframes AnimationName {
					0%{background-position:0% 52%}
					50%{background-position:100% 49%}
					100%{background-position:0% 52%}
				}



	</style>

	<script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(window).on('load',function () {
			assignCheckBoxActions();
		});

		function assignCheckBoxActions()
		{
			$('ul li input').each(function () {
				$(this).on('change',function () {
					saveSetting($(this));
				});
			});
		}

		function saveSetting(obj)
		{
			$(obj).closest('li').addClass('saving');
			var exclude_cat = true;
			if ($(obj).is(':checked'))
				exclude_cat = false;

			// Custom postt ype or standard?
			if ($(obj).data('custompost') == 'yes')
			{

				if (exclude_cat)
					var mode = 'exclude';
				else
					var mode = 'include';


				$.get( "/user-preferences/?token=<?php echo $_GET['token'];?>&os=<?php echo $_GET['os'];?>&mode="+mode+"&custom_category_change=" + $(obj).data('checkid'))
											  .done(function( data ) {
												$(obj).closest('li').removeClass('saving');

												if ($(obj).is(':checked'))
													$(obj).closest('li').addClass('active');
												else
													$(obj).closest('li').removeClass('active');


				  });

			}
			else
			{

				$.post( "/pnfw/categories/", { exclude: exclude_cat, id: $(obj).data('checkid'), token: "<?php echo $_GET['token'];?>", os: "<?php echo $_GET['os'];?>" })
							  .done(function( data ) {
								console.log(data);
								$(obj).closest('li').removeClass('saving');

								if ($(obj).is(':checked'))
									$(obj).closest('li').addClass('active');
								else
									$(obj).closest('li').removeClass('active');


				  });
			}
		}
	</script>
</head>

<body>

<p style="margin-bottom:0px;padding:10px;text-align:center;">Please select the categories you would like to receive notifications for.</p>

<ul style="margin-bottom:20px;">
	<?php
	foreach ($categories as $category2)
	{
		$checkText = '';
		$activetext = '';
		if (!$category2['exclude'])
		{
			$checkText = 'checked="checked"';
			$activetext = 'class="active"';
		}

		// exclude breaking news as an option.
		if (strtolower($category2['name']) != 'breaking news')
			echo '<li '.$activetext.'><label for="chk'.$category2['id'].'">'.$category2['name'].'</label><span><input '. $checkText .' id="chk'.$category2['id'].'" name="chk'.$category2['id'].'" data-checkid="'.$category2['id'].'" type="checkbox" value="1" /></span></li>';
	}

	// Now we need to include the new post types

	// Get a list of the categories under each post type and check if they are excluded. Dont forget the switch of features and columns.

	$frominyanExcluded = does_user_have_post_type_excluded($user_id, 'frominyan_category',$wpdb);
	$featuresExcluded = does_user_have_post_type_excluded($user_id, 'column_category',$wpdb);
	$columnsExcluded = does_user_have_post_type_excluded($user_id, 'feature_category',$wpdb);

	/*if ($frominyanExcluded === true || $frominyanExcluded == -1)
		echo '<li><label for="chkFI">From Inyan</label><span><input id="chkFI" name="chkFI" data-custompost="yes" data-checkid="frominyan" type="checkbox" value="1" /></span></li>';
	else
		echo '<li class="active"><label for="chkFI">From Inyan</label><span><input id="chkFI" checked="checked" name="chkFI" data-custompost="yes" data-checkid="frominyan" type="checkbox" value="1" /></span></li>';

	if ($featuresExcluded === true || $featuresExcluded == -1)
		echo '<li><label for="chkFE">Features</label><span><input id="chkFE" name="chkFE" data-checkid="features" data-custompost="yes" type="checkbox" value="1" /></span></li>';
	else
		echo '<li class="active"><label for="chkFE">Features</label><span><input id="chkFE" name="chkFE" checked="checked" data-checkid="features" data-custompost="yes" type="checkbox" value="1" /></span></li>';
*/
	if ($columnsExcluded === true || $columnsExcluded == -1)
		echo '<li><label for="chkCO">Columns</label><span><input id="chkCO" name="chkCO" data-custompost="yes" data-checkid="columns" type="checkbox" value="1" /></span></li>';
	else
		echo '<li class="active"><label for="chkCO">Columns</label><span><input id="chkCO" name="chkCO" checked="checked" data-custompost="yes" data-checkid="columns" type="checkbox" value="1" /></span></li>';


?>
</ul>

    <a style="display:inline-block !important;color:#333;margin-bottom:20px !important;width:100%;text-align:center;	padding-top:20px !important;" href="/user-preferences/?token=<?php echo $x_token;?>&os=<?php echo $x_os;?>&reset=true&from_url=true&time=<?php echo time();?>">Unsubscribe from all categories</a>


</body>
</html>