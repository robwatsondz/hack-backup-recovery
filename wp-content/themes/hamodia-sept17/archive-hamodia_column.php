<?php get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="excerpt-list category-features with-sidebar" role="main">
			<div class="post-box">
				<?php // This was renamed and swapped with features ?>
				<a href="<?php echo get_post_type_archive_link('hamodia_column'); ?>"><h1 class="typographic-heading">Prime</h1></a>

				<?php
					$have_categories = false;
					$terms = get_terms('column_category', ['hide_empty' => false]);
					if (count($terms) != 0)
					{
				?>
				<ul class="features-categories">
					<?php foreach($terms as $term): ?>

						<li class="<?php echo strtolower( str_replace(' ', '-', $term->name) ); ?>">
							<a href="<?php echo get_term_link($term, 'column_category'); ?>" title="<?php echo $term->name; ?>">
								<?php echo $term->name; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
				<?php } ?>



				<div class="features-posts <?php if (count($terms) == 0) {echo 'no_categories" style="margin-left:0px;" ';}?>" >
					<?php get_template_part('loop', 'category'); ?>
				</div>

			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->
	</div><!-- End main row -->

<?php get_footer(); ?>
