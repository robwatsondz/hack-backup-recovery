<?php while (have_posts()) : the_post(); ?>

	<?php

		// we go full height on features etc..
		$post_type = get_post_type();
		if ($post_type == 'hamodia_frominyan' || $post_type == 'hamodia_feature' || $post_type == 'hamodia_column')
		{
			$image = get_the_thumbnail('large');
		}
		else
		{
			$image = get_the_thumbnail('hero-image');
		}

$image = get_the_thumbnail('large');
		// If the call returns an array instead of the thumb code
		if (is_array($image))
			$image = $image['img'];

	$post_tags = get_the_tags();
	?>

	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

		<?php if ( ! isset($_GET['letters']) ): // if we are not on the comments page ?>

			<?php if ($image != '' )
			{
				$image = str_replace('<img','<img class="maintrigger"',$image);
			}

			$terms = get_the_category(get_the_ID());

				if ( count($terms) > 0 ) {
					echo '<span class="post_tags">';
					foreach( $terms as $tag ) {
						
						$tagname = $tag->name;
						
						if ($tag->category_parent > 0)
						{
							$tag_parent = get_term($tag->category_parent);							
							echo '<span class="post_tag" data-url="/tag/'.$tag_parent->slug.'">'. $tag_parent->name . '</span> <span style="margin-left: 10px;color: #909193;" class="fa fa-chevron-right"></span>';
						}
						
						if (trim(strtolower($tag->name)) == 'technology')
							$tagname = 'Health, Science & Technology';

						if (trim(strtolower($tag->name)) != 'general')
							echo '<span class="post_tag" data-url="/tag/'.$tag->slug.'">'. $tagname . '</span>';

					}
					echo '</span>';
				}
				else
				{
					$post_type = get_post_type();
					$post_type_link = get_object_taxonomies( $post_type);

					if (!isset($post_type_link[0]))
						$post_type_link = array('');

					$terms = get_the_terms(get_the_ID(), $post_type_link[0], '', ', ', '');

					if ( $terms ) {
						echo '<span class="post_tags">';
						foreach( $terms as $tag ) {
							
							$tagname = $tag->name;

							if ($tag->category_parent > 0)
							{
								$tag_parent = get_term($tag->category_parent);							
								echo '<span class="post_tag" data-url="/tag/'.$tag_parent->slug.'">'. $tag_parent->name . '</span> <span style="margin-left: 10px;color: #909193;" class="fa fa-chevron-right"></span>';
							}

							if (trim(strtolower($tag->name)) == 'technology')
								$tagname = 'Health, Science & Technology';

							if (trim(strtolower($tag->name)) != 'general')
								echo '<span class="post_tag" data-url="/tag/'.$tag->slug.'">'. $tagname . '</span>';

							
						}
						echo '</span>';
					}

				}
			?>
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php echo hamodia_get_author() . hamodia_get_entry_date() ?>

			</header>

			<div class="image_container">
				<?php echo $image; ?>
			</div>

			<div class="entry-content" style="margin-bottom: 25px">
				<?php
					 $state  = get_post_meta(get_the_ID(), '_hamodia_post_meta_state',  true);
					$source = get_post_meta(get_the_ID(), '_hamodia_post_meta_source', true);

					if ($state != '' || $source != '')
					{
						echo '<span class="meta_credit">';
						if ($state != '')
						{
							echo '<strong>'.$state.'</strong>';
						}
						if ($source != '')
						{
							echo ' ('.$source.') ';
						}

						echo ' - </span>' ;
					}


				if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')
				{
					// filter to make sure any content we have is https controlled.

					// start the buffer as the content outputs directly and other function are not configured through the theme functions.
					ob_start();
					
				
					// output the content
					the_content();
		

					// get the contents to a variable
					$page = ob_get_contents();

					// stop the buffer
					ob_end_clean();

					$page = str_replace('src="http:','src="https:',$page);
					// replaced with simple str replace due to multiple errors when using dom. 

/*
					// Load the buffer content into dom doc
					$doc = new DOMDocument();
					$doc->loadHTML($page);

					// find all images and replace with https if required.
					$tags = $doc->getElementsByTagName('img');
					foreach ($tags  as $tag)
					{
						$tag->setAttribute('src', str_replace('http:','https:',$tag->getAttribute('src')));
					}

					$page = $doc->saveHTML();*/
					echo $page;
				}
				else
					the_content();
				
					// Article email & print
				    $subject  = 'Hamodia.com - ' . get_the_title($post->ID);
				    $body_content = strip_tags(strip_shortcodes(get_the_content($post->ID)));

				   $body_word_count = 250;

					$words = explode(" ",$body_content);

					$len = min($body_word_count,count($words));
					$first_words = array_slice($words,0,$len);
					$text = implode(' ',$first_words);

					// Truncate text,
        			$text = substr($text, 0, $body_word_count);
					// Remove broken words,
					$text = substr($text, 0, strrpos($text, ' '));
					// Add elipsis
					$text .= '...';
					// Add link
					$text .= ' - ' . get_permalink();
					$body = $text;


				    $html  = '<ul class="share-buttons">';
	
						$html .=  '<li class="whatsapp">';
						$html .= '<a target="_blank" href="https://api.whatsapp.com/send?text=' . get_the_title($post->ID) . ' - ' . urlencode(get_the_permalink($post->ID)) . '" data-action="share/whatsapp/share"><span class="fa fa-whatsapp"></span> WhatsApp</a>';
						$html .= '</li>';	
						
				      // Print
				      $html .= '<li class="print">';
				      $html .=    '<a href="#" title="Print this" onclick="window.print();return false;">Print</a>';
				      $html .= '</li>';
							        // Email
				      $html .= '<li class="email">';
				      $html .=    '<a href="mailto:?subject=';
				      $html .=        esc_attr($subject);
				      $html .=        '&amp;body=';
				      $html .=        esc_attr($body);
				      $html .=        '" title="Send via email">';
				      $html .=        'Email';
				      $html .=    '</a>';
				      $html .= '</li>';
							        // Gmail
				      $html .= '<li class="gmail">';
				      $html .=    '<a href="http://mail.google.com/mail/?view=cm&amp;fs=1&amp;su=';
				      $html .=        urlencode($subject);
				      $html .=        '&amp;body=';
				      $html .=        urlencode($body);
				      $html .=        '" title="Send via gmail" target="_blank">';
				      $html .=        'Gmail';
				      $html .=        '</a>';
				      $html .= '</li>';
				      $html .= '</ul>';
				      echo $html;
				?>
				<?php $custom_data = get_post_custom(get_the_ID());?>
				<?php
					//check for the name and address details.
					if(isset($custom_data['post_holdname'][0]))
					{
						$withhold_name = $custom_data['post_holdname'][0];

						if (empty($withhold_name))
						{

							$first_name = $custom_data['first_name'][0];
							$last_name = $custom_data['last_name'][0];
							$post_address = $custom_data['post_address'][0];
							$post_address = '';

							$line_out = '';
							if ($first_name != '')
								$line_out .= $first_name;

							if ($last_name != '')
							{
								if ($line_out != "")
									$line_out .= " ";

								$line_out .= $last_name;
							}

							if ($post_address != '')
							{
								if ($line_out != "")
									$line_out .= ", ";
	
								$line_out .= $post_address;
							}
	
							echo '<p style="text-align:right;">'.$line_out.'</p>';
	
						}
					}

				?>

				<?php if (HamodiaStoryUpdates::has($post->ID)): ?>
					<hr style="display:none">
					<div class="story-updates">
						<?php foreach (HamodiaStoryUpdates::get($post->ID) as $update): ?>
							<p class="story-update">
								<span class="story-update-time">Updated <?= $update['date']->format('l, F j, Y \a\t g:i a') ?></span>
								<span class="story-update-body"><?= $update['body'] ?></span>
							</p>
						<?php endforeach ?>
					</div>
				<?php endif; ?>
			</div>

			<footer>
				<?php wp_link_pages([
						'before' => '<nav id="page-nav"><p>' . 'Pages:',
						'after'  => '</p></nav>',
				]); ?>
				
				<?php function_exists('hamodia_post_meta') && hamodia_post_meta(); ?>
				<?php function_exists('hamodia_opinion_disclaimer') && hamodia_opinion_disclaimer(); ?>

				<!--<p class="tags"><?php the_tags('<strong>Filed under:</strong> '); ?></p>-->
				<?php //echo Hamodia_the_content::get_share_buttons($post); ?>
			</footer>

			<aside class="content-aside">
				
			</aside>
			
			<div class="next_prev">			
				<?php 
				if ($post_type == 'hamodia_frominyan' || $post_type == 'hamodia_feature' || $post_type == 'hamodia_column')
				{
					switch ($post_type)
					{
						case 'hamodia_frominyan' : 
							$cat = 'frominyan_category';
							break;
						case 'hamodia_feature' : 
							$cat = 'feature_category';
							break;
						case 'hamodia_column' : 
							$cat = 'column_category';
							break;
					}					
					
				?>
				<?php previous_post_link( '%link', 'Previous', false,'', $cat ); ?>
				<?php next_post_link( '%link', 'Next', false,'', $cat ); ?>				
				<?php
				}
				else
				{
				?>
				<?php previous_post_link( '%link', 'Previous', TRUE ); ?>
				<?php next_post_link( '%link', 'Next', TRUE ); ?>				
				<?php
				}
				?>
			</div>
			
			<?php if (should_display_related_posts()) {
				echo '<div style="display:block;float:left;width:100%;margin-top:-10px;" class="related_posts">';
					related_posts();
				echo '</div>';
				} ?>
		

		<?php endif; ?>

		<?php // comments_template(); ?>

	</article>
<?php endwhile; ?>
