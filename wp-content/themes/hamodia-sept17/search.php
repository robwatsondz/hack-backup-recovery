<?php get_header(); ?>
	<div id="content" class="row">	

		<div id="main" class="with-sidebar excerpt-list" role="main">
			<div class="post-box">
				<h1 class="typographic-heading">Search Results for "<?php echo get_search_query(); ?>"</h1>
				<?php get_template_part('loop', 'search'); ?>
			</div>
		</div><!-- /#main -->	

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- /#content -->
<?php get_footer(); ?>