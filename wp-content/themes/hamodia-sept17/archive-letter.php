<?php get_header(); ?>

	<!-- Row for main content area -->
	<div id="content" class="row">

		<div id="main" class="excerpt-list with-sidebar" role="main">
			<div class="post-box">
				<a href="<?php echo get_post_type_archive_link('letter'); ?>">
					<h1 class="typographic-heading">Letters</h1>
				</a>

				<div>
					<?php get_template_part('loop', 'category'); ?>
				</div>

			</div>
		</div><!-- /#main -->

		<aside id="sidebar" role="complementary">
			<div class="sidebar-box">
				<?php get_sidebar(); ?>
			</div>
		</aside><!-- /#sidebar -->

	</div><!-- End main row -->

<?php get_footer(); ?>