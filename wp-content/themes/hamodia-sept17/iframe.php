<?php
/**
 * Template Name: Iframe Page
 *
 */

 ?>

 <?php get_header(); ?>

	 <?php while (have_posts()) : the_post(); ?>
	<?php
		the_content();
	?>

<?php endwhile; ?>

<style>
html,body
{
  padding:0;
  margin:0;
}

.spon_con,
.date_box,
.container #content,
.mobile_adb
{
  display:none !important;
}

</style>

<?php get_footer(); ?>